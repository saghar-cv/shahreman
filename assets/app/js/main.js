$(function () {
    'use-strict';

    $(".side-nav-left").sideNav({
        edge: 'right',
        closeOnClick: true
    });

    $("#cart-menu").animatedModal();

    $("#nav-menu").animatedModal({
        modalTarget: 'animatedModal2'
    });

    $('.collapsible').collapsible({
        accordion: false
    });

    $('select').material_select();

});