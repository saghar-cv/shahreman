<?php

use yii\db\Migration;

/**
 * Class m180121_125818_remove_planId_form_users
 */
class m180121_125818_remove_planId_form_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey("user_fk_plan_by_planId", '{{%users}}');
        $this->dropColumn('{{%users}}', 'planId');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('{{%users}}','planId' ,$this->integer());
        $this->addForeignKey("user_fk_plan_by_planId", '{{%users}}', 'planId', "{{%plans}}", 'id', 'SET NULL', 'CASCADE');
    }
}
