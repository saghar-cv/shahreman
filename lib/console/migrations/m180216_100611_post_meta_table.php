<?php

use yii\db\Migration;

/**
 * Class m180216_100611_post_meta_table
 */
class m180216_100611_post_meta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post_meta}}', [
            'id' => $this->primaryKey(),
            'postId' => $this->integer(),
            'key' => $this->string(191)->notNull(),
            'value' => $this->string(191),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('postMeta_fk_post_postId',
            '{{%post_meta}}',
            'postId',
            '{{%posts}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('postMeta_fk_post_postId', '{{%post_meta}}');
        $this->dropTable("{{%post_meta}}");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180216_100611_post_meta_table cannot be reverted.\n";

        return false;
    }
    */
}
