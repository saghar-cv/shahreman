<?php

use yii\db\Migration;

/**
 * Class m180529_191911_add_some_new_permisions
 */
class m180529_191911_add_some_new_permisions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createPermissions("site/mobile-menu-update-pos", "ویرایش موقعیت منو موبایل");
        $this->createPermissions("site/mobile-menu-update", "ویرایش آیتم منوی موبایل");
        $this->createPermissions("site/mobile-slider-update", "ویرایش آیتم اسلایدر اپ");
        $this->createPermissions("site/mobile-slider-update-pos", "ویرایش موقعیت اسلایدر موبایل");
        $this->createPermissions("page/delete", "حذف صفحه ثابت");
        $this->createPermissions("page/ajax-add-sub-page", "مدیریت زیر صفحات");
        $this->createPermissions("news/delete", "حذف خبر");
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($auth->getPermission($permission)) {
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("site/mobile-menu-update-pos");
        $this->removePermission("site/mobile-menu-update");
        $this->removePermission("site/mobile-slider-update");
        $this->removePermission("site/mobile-slider-update-pos");
        $this->removePermission("page/ajax-add-sub-page");
        $this->removePermission("news/delete");
    }

    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }
}
