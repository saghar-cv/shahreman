<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180620_150326_chang_slider_structure
 */
class m180620_150326_chang_slider_structure extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach ((new Query)->from('{{%setting}}')->where(['section' => \backend\models\MobileSliderFrom::SETTING_SECTION])->each() as $slider) {
            $key = 'mainPage' . '-' . $slider['key'];
            $this->update('{{%setting}}', ['key' => $key], ['id' => $slider['id']]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        foreach ((new Query)->from('{{%setting}}')->where(['section' => \backend\models\MobileSliderFrom::SETTING_SECTION])->each() as $slider) {
            $key = $slider['key'];
            $cityId = explode('-', $key);
            $cityId = $cityId[1];
            $this->update('{{%setting}}', ['key' => $cityId], ['id' => $slider['id']]);
        }

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180620_150326_chang_slider_structure cannot be reverted.\n";

        return false;
    }
    */
}
