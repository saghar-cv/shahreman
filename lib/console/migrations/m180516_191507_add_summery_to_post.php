<?php

use yii\db\Migration;

/**
 * Class m180516_191507_add_summery_to_post
 */
class m180516_191507_add_summery_to_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'summary', $this->string(191)->after('content'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'summary');
    }

}
