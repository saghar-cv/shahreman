<?php

use yii\db\Migration;

/**
 * Class m180706_183831_add_important_post_permissions
 */
class m180706_183831_add_important_post_permissions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createPermissions("news/set-important", "اخبار مهم");
        $this->createPermissions("page/set-important", "صفحات ویژه");

    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("news/set-important");
        $this->removePermission("page/set-important");
    }

    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }
}
