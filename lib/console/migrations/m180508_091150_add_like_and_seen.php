<?php

use yii\db\Migration;

/**
 * Class m180508_091150_add_like_and_seen
 */
class m180508_091150_add_like_and_seen extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'likeCount', $this->integer()->notNull()->defaultValue(0)
            ->after('status'));
        $this->addColumn('{{%posts}}', 'seenCount', $this->integer()->notNull()->defaultValue(0)
            ->after('likeCount'));

        $this->addColumn('{{%businesses}}', 'likeCount', $this->integer()->notNull()->defaultValue(0)
            ->after('status'));
        $this->addColumn('{{%businesses}}', 'seenCount', $this->integer()->notNull()->defaultValue(0)
            ->after('likeCount'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'likeCount');
        $this->dropColumn('{{%posts}}', 'seenCount');

        $this->dropColumn('{{%businesses}}', 'likeCount');
        $this->dropColumn('{{%businesses}}', 'seenCount');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180508_091150_add_like_and_seen cannot be reverted.\n";

        return false;
    }
    */
}
