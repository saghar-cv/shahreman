<?php

use yii\db\Migration;

/**
 * Class m180221_181228_add_slider_to_post
 */
class m180221_181228_add_slider_to_post extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'slider', $this->string()->defaultValue(null)->after('coverId'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'slider');
    }

}
