<?php

use yii\db\Migration;
use yii\helpers\Console;

/**
 * Class m180704_160500_change_value_of_post_meta_related_page_view_type
 */
class m180704_160500_change_value_of_post_meta_related_page_view_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $viewTypes = \common\models\PostMeta::find()->where(['key' => \common\models\Post::META_RELATED_PAGES_VIEW_TYPE])->all();
        foreach ($viewTypes as $viewType) {
            $viewTypeValue = $viewType->value;
            if (empty($viewTypeValue) or is_null($viewTypeValue) or !isset($viewTypeValue)) {
                \common\models\PostMeta::add($viewType->postId, \common\models\Post::META_RELATED_PAGES_VIEW_TYPE, \common\models\Post::STYLE_DEFAULT);
                $this->stdout("\nPost {$viewType->postId} converted to new view style.\t", Console::FG_GREEN);
            } else {
                $this->stdout("\nPost {$viewTypeValue} view data is not available\t", Console::FG_YELLOW);
            }

        }
        $postsNum = count($viewTypes);
        $this->stdout("\n\n\n{$postsNum} post view data converted.\n", Console::FG_GREY);
    }

    /**
     * Prints a string to STDOUT.
     *
     * You may optionally format the string with ANSI codes by
     * passing additional parameters using the constants defined in [[\yii\helpers\Console]].
     *
     * Example:
     *
     * ```
     * $this->stdout('This will be red and underlined.', Console::FG_RED, Console::UNDERLINE);
     * ```
     *
     * @param string $string the string to print
     *
     * @return int|bool Number of bytes printed or false on error
     */
    public function stdout($string)
    {
        if (true) {
            $args = func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stdout($string);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $viewTypes = \common\models\PostMeta::find()->where(['key' => \common\models\Post::META_RELATED_PAGES_VIEW_TYPE])->all();
        foreach ($viewTypes as $viewType) {
            $viewTypeValue = $viewType->value;
            if ($viewTypeValue == 0) {
                \common\models\PostMeta::add($viewType->postId, \common\models\Post::META_RELATED_PAGES_VIEW_TYPE, '');
                $this->stdout("\nPost {$viewType->postId} converted to old view style.\t", Console::FG_GREEN);
            } else {
                $this->stdout("\nPost {$viewType->postId} view data is not available\t", Console::FG_YELLOW);
            }

        }
        $postsNum = count($viewTypes);

        $this->stdout("\n\n\n{$postsNum} post view data converted.\n");
    }

}
