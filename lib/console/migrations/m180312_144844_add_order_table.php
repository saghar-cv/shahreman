<?php

use yii\db\Migration;

/**
 * Class m180312_144844_add_order_table
 */
class m180312_144844_add_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'businessId' => $this->integer()->notNull(),
            'planId' => $this->integer()->notNull(),
            'amount' => $this->double()->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'createAt' => $this->dateTime(),
            'updateAt' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'orders_businesses_businessId_fk',
            '{{%orders}}',
            'businessId',
            '{{%businesses}}',
            'id'
        );

        $this->addForeignKey(
            'orders_plans_plansId_fk',
            '{{%orders}}',
            'planId',
            '{{%plans}}',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('business_fk', '{{%orders}}');
        $this->dropForeignKey('plan_fk', '{{%orders}}');
        $this->dropTable('{{%orders}}');
    }

}
