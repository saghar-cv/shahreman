<?php

use yii\db\Migration;

/**
 * Class m180121_130545_add_expire_date_to_business_table
 */
class m180121_130545_add_expire_date_to_business_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%businesses}}', 'expireDate', $this->dateTime());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%businesses}}', 'expireDate');
    }
}
