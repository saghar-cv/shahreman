<?php

use yii\db\Migration;

/**
 * Class m180404_142626_add_city_to_user
 *
 * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
 */
class m180404_142626_add_city_id_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'cityId', $this->integer()->after('mobile'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users}}', 'cityId');

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180404_142626_add_city_to_user cannot be reverted.\n";

        return false;
    }
    */
}
