<?php

use yii\db\Migration;

/**
 * Class m180105_071258_category_relation_table
 */
class m180105_071258_category_relation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%category_relation}}', [
            'id' => $this->primaryKey(),
            'catId' => $this->integer(),
            'postId' => $this->integer(),
            'businessId' => $this->integer(),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'category_fk',
            '{{%category_relation}}',
            'catId',
            '{{%categories}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'post_fk',
            '{{%category_relation}}',
            'postId',
            '{{%posts}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'business_fk',
            '{{%category_relation}}',
            'businessId',
            '{{%businesses}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'category_fk',
            '{{%category_relation}}'
        );

        $this->dropForeignKey(
            'post_fk',
            '{{%category_relation}}'
        );

        $this->dropForeignKey(
            'business_fk',
            '{{%category_relation}}'
        );
    }
}
