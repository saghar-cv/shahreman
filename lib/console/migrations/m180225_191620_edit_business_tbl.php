<?php

use yii\db\Migration;

/**
 * Class m180225_191620_edit_business_tbl
 */
class m180225_191620_edit_business_tbl extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn("{{%businesses}}", "ownerName", $this->string(191)->null()->after('ownerId'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn("{{%businesses}}", "ownerName");
    }
}
