<?php

use yii\db\Migration;

/**
 * Class m180108_085011_add_plan_id_to_business
 */
class m180108_085011_add_plan_id_to_business extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%businesses}}',
            'planId',
            $this->integer());

        $this->addForeignKey(
            'business_fk_plan_by_planId',
            '{{%businesses}}',
            'planId',
            '{{%plans}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(
            '{{%businesses}}',
            'planId'
        );

        $this->dropForeignKey(
            'business_fk_plan_by_planId',
            '{{%businesses}}'
        );
    }

}
