<?php

use yii\db\Migration;

/**
 * Class m180227_070027_make_news_important
 */
class m180227_070027_make_news_important extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'important', $this->integer()->defaultValue(0)->after('status'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'important');
    }

}
