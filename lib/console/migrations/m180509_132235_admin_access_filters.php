<?php

use yii\db\Migration;

/**
 * Class m180509_132235_admin_access_filters
 */
class m180509_132235_admin_access_filters extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $accessPermit = $auth->createPermission('permit/access');
        $accessPermit->description = 'ایجاد دسترسی جدید';
        $auth->add($accessPermit);

        $userPermit = $auth->createPermission('permit/user');
        $userPermit->description = 'انتساب دسترسی به کاربر';
        $auth->add($userPermit);


        $role = $auth->createRole('admin');
        $role->description = 'مدیر کل سیستم';
        Yii::$app->authManager->add($role);

        $auth->addChild($role, $accessPermit);
        $auth->addChild($role, $userPermit);

        $this->createPermissions('#dashboard-business', 'آمار داشبودر - کسب و کار', $role);
        $this->createPermissions('#dashboard-finance', 'آمار داشبودر - مالی', $role);
        $this->createPermissions('page/index', 'لیست صفحات ثابت', $role);
        $this->createPermissions('page/update', 'ویرایش صفحات ثابت', $role);
        $this->createPermissions('page/create', 'ایجاد صفحه ثابت', $role);
        $this->createPermissions('news/index', 'لیست اخبار', $role);
        $this->createPermissions('news/create', 'ایجاد خبر', $role);
        $this->createPermissions('news/update', 'ویرایش خبر', $role);
        $this->createPermissions('post-categories', 'مدیریت دسته پست ها', $role);
        $this->createPermissions('business-categories', 'مدیریت دسته اصناف', $role);
        $this->createPermissions('business', 'مدیریت صنف', $role);
        $this->createPermissions('site/mobile-menu-generator', 'مدیریت منو اپ موبایل', $role);
        $this->createPermissions('site/mobile-slider-generator', 'مدیریت اسلایدر اپ', $role);
        $this->createPermissions('plan', 'پلن های فروش', $role);
        $this->createPermissions('payment-data', 'گزارش پرداخت ها', $role);
        $this->createPermissions('user', 'مدیریت کاربران', $role);
        $this->createPermissions('setting', 'تنظیمات', $role);
        $this->createPermissions('payment', 'ریز گزارش تراکنش های بانکی', $role);


        $auth->assign($role, 1);

        $this->addColumn("{{%admins}}", 'accessCities', $this->string()->after('authCode'));
    }

    protected function createPermissions($permission, $des = '', $role)
    {
        $auth = Yii::$app->authManager;
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $this->dropColumn("{{%admins}}", 'accessCities');
    }
}
