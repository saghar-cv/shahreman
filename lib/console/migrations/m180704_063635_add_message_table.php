<?php

use yii\db\Migration;

/**
 * Class m180704_063635_add_message_table
 */
class m180704_063635_add_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%messages}}', [
            'id' => $this->primaryKey(),
            'recipientName' => $this->string(191)->notNull(),
            'mobile' => $this->string(191)->null(),
            'email' => $this->string(191)->null(),
            'content' => $this->text()->notNull(),
            'type' => $this->smallInteger(1)->defaultValue(1),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'createAt' => $this->dateTime(),

        ]);

        $this->createPermissions("message/message-list", "نمایش لیست پیامک های ارسالی");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("message/message-list");
        $this->dropTable('{{%messages}}');
    }


    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }
}
