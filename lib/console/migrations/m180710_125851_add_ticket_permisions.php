<?php

use yii\db\Migration;

/**
 * Class m180710_125851_add_ticket_permisions
 */
class m180710_125851_add_ticket_permisions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createPermissions("ticket/customer-care/index", "لیست تیکت ها");
        $this->createPermissions("ticket/customer-care/department", "مدیریت دپارتمان های تیکت");
        $this->createPermissions("ticket/customer-care/user-department", "انتساب کاربران به دپارتمان تیکت ها");
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("ticket/customer-care/user-department");
        $this->removePermission("ticket/customer-care/department");
        $this->removePermission("ticket/customer-care/index");
    }

    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_125851_add_ticket_permisions cannot be reverted.\n";

        return false;
    }
    */
}
