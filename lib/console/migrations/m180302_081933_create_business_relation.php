<?php

use yii\db\Migration;

/**
 * Class m180302_081933_create_business_relation
 */
class m180302_081933_create_business_relation extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%business_relation}}', [
            'businessId' => $this->integer(),
            'cityId' => $this->integer(),
        ]);

        $this->addPrimaryKey('business_relation_pk1', "{{%business_relation}}", ['businessId', 'cityId']);

        $this->addForeignKey(
            'business_business_fk',
            '{{%business_relation}}',
            'businessId',
            '{{%businesses}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'business_city_fk',
            '{{%business_relation}}',
            'cityId',
            '{{%city}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'business_city_fk',
            '{{%business_relation}}'
        );

        $this->dropForeignKey(
            'business_business_fk',
            '{{%business_relation}}'
        );

        $this->dropPrimaryKey('business_relation_pk1', "{{%business_relation}}");

        $this->dropTable("{{%business_relation}}");
    }
}
