<?php

use yii\db\Migration;
use yii\helpers\Console;

/**
 * Class m180602_180906_change_structure_of_post_meta_map
 */
class m180602_180906_change_structure_of_post_meta_map extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $maps = \common\models\PostMeta::find()->where(['key' => 'map'])->all();
        foreach ($maps as $map) {
            $location = $map->value;
            if ($location) {
                $location = explode(',', $location);
                \common\models\PostMeta::add($map->postId, \common\models\Post::META_MAP_LATITUDE, $location[0]);
                \common\models\PostMeta::add($map->postId, \common\models\Post::META_MAP_LONGITUDE, $location[1]);
                $this->stdout("\nPost {$map->postId} converted to new map style.\t", Console::FG_GREEN);
            } else {
                $this->stdout("\nPost {$map->postId} map data is not available\t", Console::FG_YELLOW);
            }
            $map->delete();
            $this->stdout("Post {$map->postId} map data deleted\n", Console::FG_RED);
        }
        $postsNum = count($maps);
        $this->stdout("\n\n\n{$postsNum} post map data converted.\n", Console::FG_GREY);
    }

    /**
     * Prints a string to STDOUT.
     *
     * You may optionally format the string with ANSI codes by
     * passing additional parameters using the constants defined in [[\yii\helpers\Console]].
     *
     * Example:
     *
     * ```
     * $this->stdout('This will be red and underlined.', Console::FG_RED, Console::UNDERLINE);
     * ```
     *
     * @param string $string the string to print
     *
     * @return int|bool Number of bytes printed or false on error
     */
    public function stdout($string)
    {
        if (true) {
            $args = func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stdout($string);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $lats = \common\models\PostMeta::find()->where(['key' => \common\models\Post::META_MAP_LATITUDE])->all();
        foreach ($lats as $lat) {
            $long = \common\models\PostMeta::findOne(['postId' => $lat->postId, 'key' => \common\models\Post::META_MAP_LONGITUDE]);
            if ($lat->value and $long->value) {
                $location = $lat->value . ',' . $long->value;
                \common\models\PostMeta::add($lat->postId, 'map', $location);
                $this->stdout("\nPost {$long->postId} converted to old map style.\t", Console::FG_GREEN);
            } else {
                $this->stdout("\nPost {$long->postId} map data is not available\t", Console::FG_YELLOW);
            }
            $lat->delete();
            $long->delete();
            $this->stdout("Post {$long->postId} map data deleted\n", Console::FG_RED);
        }
        $postsNum = count($lats);
        $this->stdout("\n\n\n{$postsNum} post map data converted.\n");
    }
}
