<?php

use yii\db\Migration;

/**
 * Class m180604_221022_add_new_permisions
 */
class m180604_221022_add_new_permisions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createPermissions("message/sms-formats", "فرمت های آماده پیام های سیستم");
        $this->createPermissions("message/send-message-to", "ارسال گروهی یا تکی پیام");
        $this->createPermissions("page/ajax-update-sub-page-order", "تغییر ترتیب زیرصفحات پست ها");
        $this->createPermissions("page/ajax-remove-sub-page", "حذف زیر صفحات پست ها");

        $this->createPermissions("#post-just-write-draft", "فقط نوشتن پست بدون دسترسی به ویرایش و انتشار");
        $this->createPermissions("#post-write-own-edit-draft", "نوشتن مطلب و ویرایش مطالب خود بدون دسترسی به ویرایش مطالب دیگران و انتشار");
        $this->createPermissions("#post-write-edit-draft", "نوشتن مطلب و ویرایش مطالب خود و دیگران بدون دسترسی به انتشار");
        $this->createPermissions("#post-write-edit-publish", "نوشتن مطلب و ویرایش مطالب خود و دیگران و انتشار");
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("message/sms-formats");
        $this->removePermission("message/send-message-to");
        $this->removePermission("page/ajax-update-sub-page-order");
        $this->removePermission("page/ajax-remove-sub-page");

        $this->removePermission("#post-just-write-draft");
        $this->removePermission("#post-write-own-edit-draft");
        $this->removePermission("#post-write-edit-draft");
        $this->removePermission("#post-write-edit-publish");
    }

    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }
}
