<?php

use yii\db\Migration;

/**
 * Class m180625_230151_add_missed_permission
 */
class m180625_230151_add_missed_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createPermissions("site/mobile-slider-delete", "حذف اسلایدر");
        $this->createPermissions("site/mobile-menu-delete", "حذف منو اپ");
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removePermission("site/mobile-slider-delete");
        $this->removePermission("site/mobile-menu-delete");

    }

    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }
}
