<?php

use yii\db\Migration;

/**
 * Class m180411_191424_add_edited_field_to_business
 */
class m180411_191424_add_edited_field_to_business extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%businesses}}',
            'edited',
            $this->smallInteger(1)->defaultValue(1)->after('status')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%businesses}}', 'edited');
    }


}
