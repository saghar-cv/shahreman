<?php

use yii\db\Migration;
use yii\helpers\Console;
/**
 * Class m180726_103023_rename_talesh_city
 */
class m180726_103023_rename_talesh_city extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $taleshCity = \saghar\address\models\City::findOne(['latinName' => 'Talesh']);
        $taleshCity->name = 'تالش';
        if ($taleshCity->save()) {
            $this->stdout("\nPost {$taleshCity->id} renamed", Console::FG_GREEN);

        } else {
            $this->stdout("\nPost {$taleshCity->id} did not renamed", Console::FG_GREEN);
        }
    }

    /**
     * Prints a string to STDOUT.
     *
     * You may optionally format the string with ANSI codes by
     * passing additional parameters using the constants defined in [[\yii\helpers\Console]].
     *
     * Example:
     *
     * ```
     * $this->stdout('This will be red and underlined.', Console::FG_RED, Console::UNDERLINE);
     * ```
     *
     * @param string $string the string to print
     *
     * @return int|bool Number of bytes printed or false on error
     */
    public function stdout($string)
    {
        if (true) {
            $args = func_get_args();
            array_shift($args);
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stdout($string);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $taleshCity = \saghar\address\models\City::findOne(['latinName' => 'Talesh']);
        $taleshCity->name = 'طالش';
        if ($taleshCity->save()) {
            $this->stdout("\nPost {$taleshCity->id} renamed", Console::FG_GREEN);

        } else {
            $this->stdout("\nPost {$taleshCity->id} did not renamed", Console::FG_GREEN);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180722_114948_rename_tavalesh_to_talesh cannot be reverted.\n";

        return false;
    }
    */
}
