<?php

use yii\db\Migration;

/**
 * Class m171229_083651_init
 */
class m171229_083651_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%admins}}", [
            'id' => $this->primaryKey(),
            'name' => $this->string(191)->notNull(),
            'family' => $this->string(191),
            'email' => $this->string(191)->null()->unique(),
            'mobile' => $this->string(15)->notNull()->unique(),
            'passwordHash' => $this->string(191)->null(),
            'authKey' => $this->string(191)->notNull()->unique(),
            'authCode' => $this->string(191)->null(),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'ip' => $this->string(15)->null(),
            'lastIp' => $this->string(15)->null(),
            'lastSeen' => $this->dateTime(),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable("{{%users}}", [
            'id' => $this->primaryKey(),
            'name' => $this->string(191)->notNull(),
            'family' => $this->string(191),
            'email' => $this->string(191)->null()->unique(),
            'mobile' => $this->string(15)->notNull()->unique(),
            'passwordHash' => $this->string(191)->null(),
            'authKey' => $this->string(191)->notNull()->unique(),
            'authCode' => $this->string(191)->null(),
            'planId' => $this->integer(),
            'expireAt' => $this->dateTime(),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'ip' => $this->string(15)->null(),
            'lastIp' => $this->string(15)->null(),
            'lastSeen' => $this->dateTime(),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable("{{%posts}}", [
            'id' => $this->primaryKey(),
            'title' => $this->string(191)->notNull(),
            'coverId' => $this->integer(),
            'content' => $this->text(),
            'tags' => $this->string(191),
            'authorId' => $this->integer(),
            'cityId' => $this->integer(),
            'parentId' => $this->integer(),
            'postType' => $this->string(191)->notNull()->defaultValue('post'),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);


        $this->createTable("{{%plans}}", [
            'id' => $this->primaryKey(),
            'name' => $this->string(191)->notNull(),
            'description' => $this->text(),
            'price' => $this->double()->notNull()->defaultValue(0),
            'duration' => $this->integer(15),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable("{{%plan_accesses}}", [
            'planId' => $this->integer(),
            'name' => $this->string(191)->notNull(),
            'value' => $this->string(191),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->addPrimaryKey('plan_access_pk', '{{%plan_accesses}}', ['planId', 'name']);

        $this->createTable("{{%businesses}}", [
            'id' => $this->primaryKey(),
            'name' => $this->string(191)->notNull(),
            'description' => $this->text(),
            'ownerId' => $this->integer(),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%meta}}', [
            'id' => $this->primaryKey(),
            'section' => $this->string(191)->notNull(),
            'key' => $this->string(191)->notNull(),
            'value' => $this->string(191),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('meta_index_by_section', '{{%meta}}', 'section');
        $this->createIndex('meta_index_by_key', '{{%meta}}', 'key');


        // Create forging keys.

        $this->addForeignKey("user_fk_plan_by_planId", '{{%users}}', 'planId', "{{%plans}}", 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey("planAccess_fk_plan_by_planId", '{{%plan_accesses}}', 'planId', "{{%plans}}", 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey("business_fk_user_by_ownerId", '{{%businesses}}', 'ownerId', "{{%users}}", 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey("post_fk_admin_by_authorId", '{{%posts}}', 'authorId', "{{%admins}}", 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey("post_fk_admin_by_authorId", '{{%posts}}');
        $this->dropForeignKey("business_fk_user_by_ownerId", '{{%businesses}}');
        $this->dropForeignKey("planAccess_fk_plan_by_planId", '{{%plan_accesses}}');
        $this->dropForeignKey("user_fk_plan_by_planId", '{{%users}}');

        $this->dropTable("{{%meta}}");
        $this->dropTable("{{%businesses}}");
        $this->dropTable("{{%plan_accesses}}");
        $this->dropTable("{{%plans}}");
        $this->dropTable("{{%posts}}");
        $this->dropTable("{{%users}}");
        $this->dropTable("{{%admins}}");
    }
}
