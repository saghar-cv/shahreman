<?php

use yii\db\Migration;

/**
 * Class m180109_085907_business_metaz_table
 */
class m180109_085907_business_meta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropIndex('meta_index_by_section', '{{%meta}}');
        $this->renameTable('{{%meta}}', '{{%business_meta}}');
        $this->dropColumn('{{%business_meta}}', 'section');
        $this->addColumn('{{%business_meta}}', 'businessId', $this->integer()->after('id'));
        $this->addForeignKey(
            'businessMeta_fk_business_businessId',
            '{{%business_meta}}',
            'businessId',
            '{{%businesses}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo 'This migration can not be reverted';
        return false;
    }
}
