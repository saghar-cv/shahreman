<?php

use yii\db\Migration;

/**
 * Class m180702_142624_add_operator_note_to_business
 */
class m180702_142624_add_operator_note_to_business extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%businesses}}', 'operatorNote', $this->text()->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%businesses}}', 'operatorNote');
    }

}
