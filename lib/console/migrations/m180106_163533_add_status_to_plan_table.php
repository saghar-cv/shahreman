<?php

use yii\db\Migration;

/**
 * Class m180106_163533_add_status_to_plan_table
 */
class m180106_163533_add_status_to_plan_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%plans}}',
            'status',
            $this->smallInteger(1)->notNull()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%plans}}', 'status');
    }
}
