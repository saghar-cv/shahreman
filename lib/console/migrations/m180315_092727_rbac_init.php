<?php

use yii\db\Migration;

/**
 * Class m180315_092727_rbac_init
 */
class m180315_092727_rbac_init extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        // add create page permission
        $createPage = $auth->createPermission('createPage');
        $createPage->description = 'Create a page';
        $auth->add($createPage);

        // add update page permission
        $updatePage = $auth->createPermission('updatePage');
        $updatePage->description = 'Update page';
        $auth->add($updatePage);

        // add pages list permission
        $pagesList = $auth->createPermission('pagesList');
        $pagesList->description = 'Pages list';
        $auth->add($pagesList);

        // add create news permission
        $createNews = $auth->createPermission('createNews');
        $createNews->description = 'Create a news';
        $auth->add($createNews);

        // add update news permission
        $updateNews = $auth->createPermission('updateNews');
        $updateNews->description = 'Update news';
        $auth->add($updateNews);

        // add news list permission
        $newsList = $auth->createPermission('newsList');
        $newsList->description = 'News list';
        $auth->add($newsList);

        // add update business permission
        $updateBusiness = $auth->createPermission('updateBusiness');
        $updateBusiness->description = 'Update business';
        $auth->add($updateBusiness);

        // add businesses list permission
        $businessesList = $auth->createPermission('businessList');
        $businessesList->description = 'Businesses list';
        $auth->add($businessesList);

        // add front menu permission
        $frontMenu = $auth->createPermission('frontMenu');
        $frontMenu->description = 'Front menu';
        $auth->add($frontMenu);

        // add front slider permission
        $frontSlider = $auth->createPermission('frontSlider');
        $frontSlider->description = 'Front slider';
        $auth->add($frontSlider);

        // add create plan permission
        $createPlan = $auth->createPermission('createPlan');
        $createPlan->description = 'Create plan';
        $auth->add($createPlan);

        // add update plan permission
        $updatePlan = $auth->createPermission('updatePlan');
        $updatePlan->description = 'Update plan';
        $auth->add($updatePlan);

        // add plans list permission
        $plansList = $auth->createPermission('plansList');
        $plansList->description = 'Plans list';
        $auth->add($plansList);

        // add payment permission
        $payment = $auth->createPermission('payment');
        $payment->description = 'Payment';
        $auth->add($payment);

        // add business reports permission
        $businessReports = $auth->createPermission('businessReports');
        $businessReports->description = 'Business reports';
        $auth->add($businessReports);

        // add users permission
        $users = $auth->createPermission('users');
        $users->description = 'Users';
        $auth->add($users);

        // add admin permissions
        $adminPermissions = $auth->createPermission('adminPermissions');
        $adminPermissions->description = 'Admin permissions';
        $auth->add($adminPermissions);

        // add "editor" role and give this role the its permissions
        $editor = $auth->createRole('editor');
        $auth->add($editor);
        $auth->addChild($editor, $updatePage);
        $auth->addChild($editor, $pagesList);
        $auth->addChild($editor, $updateNews);
        $auth->addChild($editor, $newsList);

        // add "author" role and give this role the its permissions
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createPage);
        $auth->addChild($author, $createNews);
        $auth->addChild($author, $editor);

        // add "content manager" role and give this role the its permissions
        $contentManager = $auth->createRole('contentManager');
        $auth->add($contentManager);
        $auth->addChild($contentManager, $frontSlider);
        $auth->addChild($contentManager, $frontMenu);
        $auth->addChild($contentManager, $author);

        // add "finance" role and give this role the its permissions
        $finance = $auth->createRole('finance');
        $auth->add($finance);
        $auth->addChild($finance, $payment);
        $auth->addChild($finance, $businessReports);

        // add "business manager" role and give this role the its permissions
        $businessManager = $auth->createRole('businessManager');
        $auth->add($businessManager);
        $auth->addChild($businessManager, $updateBusiness);
        $auth->addChild($businessManager, $businessesList);

        // add "admin" role and give this role the its permissions
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $adminPermissions);
        $auth->addChild($admin, $createPlan);
        $auth->addChild($admin, $updatePlan);
        $auth->addChild($admin, $plansList);
        $auth->addChild($admin, $users);
        $auth->addChild($admin, $contentManager);
        $auth->addChild($admin, $finance);
        $auth->addChild($admin, $businessManager);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
