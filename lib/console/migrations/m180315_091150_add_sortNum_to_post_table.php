<?php

use yii\db\Migration;

/**
 * Class m180315_091150_add_sortnum_to_post_table
 */
class m180315_091150_add_sortnum_to_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%posts}}', 'sortNum', $this->smallInteger(3)->notNull()->defaultValue(0)->after('postType'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%posts}}', 'sortnum');
    }


}
