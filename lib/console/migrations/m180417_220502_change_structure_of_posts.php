<?php

use yii\db\Migration;

/**
 * Class m180417_220502_change_structure_of_posts
 */
class m180417_220502_change_structure_of_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%post_sub_pages}}", [
            'postId' => $this->integer()->notNull(),
            'suggestionId' => $this->integer()->notNull(),
            'suggestionType' => $this->string(64)->notNull(),
            'sortNum' => $this->smallInteger(3)->defaultValue(0)
        ], $tableOptions);

        $this->addPrimaryKey("post_sub_pages_pk", "{{%post_sub_pages}}", [
            'postId', 'suggestionId', 'suggestionType'
        ]);

        $this->addForeignKey(
            "post_sub_pages_fk_posts_by_postId",
            "{{%post_sub_pages}}",
            'postId',
            "{{%posts}}",
            "id",
            "CASCADE",
            "CASCADE"
        );

        $this->dropColumn("{{%posts}}", 'parentId');
        $this->dropColumn("{{%posts}}", 'sortNum');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("post_sub_pages_pk", "{{%post_sub_pages}}");
        $this->dropPrimaryKey("post_sub_pages_fk_posts_by_postId", "{{%post_sub_pages}}");
        $this->dropTable("{{%post_sub_pages}}");

        $this->addColumn("{{%posts}}", "parentId", $this->integer());
        $this->addColumn("{{%posts}}", "sortNum", $this->smallInteger(3)->notNull()->defaultValue(0));
    }
}
