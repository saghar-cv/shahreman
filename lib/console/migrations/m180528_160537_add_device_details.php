<?php

use yii\db\Migration;

/**
 * Class m180528_160537_add_device_details
 */
class m180528_160537_add_device_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%devices}}', [
            'id' => $this->primaryKey(),
            'deviceId' => $this->string(191)->notNull()->unique(),
            'osVersion' => $this->string(191)->null(),
            'appVersion' => $this->string(191),
            'deviceModel' => $this->string(191),
            'operator' => $this->string(191),
            'osType' => $this->string(191),
            'pusheId' => $this->string(191)->null(),
            'updateAt' => $this->dateTime(),
            'createAt' => $this->dateTime(),
        ]);

        $this->createIndex('deviceIdIndex', '{{%devices}}', 'deviceId');


        $this->createTable('{{%user_devices}}', [
            'deviceId' => $this->integer()->notNull(),
            'userId' => $this->integer()->notNull(),
            'createAt' => $this->dateTime(),
        ]);

        $this->addPrimaryKey('user_devices_pk', '{{%user_devices}}', ['deviceId', 'userId']);

        $this->addForeignKey(
            'user_devices_fk_devices_by_deviceId',
            '{{%user_devices}}',
            'deviceId',
            '{{%devices}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createPermissions('#dashboard-device-data', 'آمار دیوایس ها');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('user_devices_fk_devices_by_deviceId', '{{%user_devices}}');

        $this->dropPrimaryKey('user_devices_pk', '{{%user_devices}}');

        $this->dropIndex('deviceIdIndex', '{{%devices}}');

        $this->removePermission('#dashboard-device-data');

        $this->dropTable('{{%devices}}');
        $this->dropTable('{{%user_devices}}');
    }

    protected function createPermissions($permission, $des = '')
    {
        $auth = Yii::$app->authManager;
        if ($per = $auth->getPermission($permission)) {
            $per->description = $des;
            $auth->update($permission, $per);
            return;
        }
        $role = $auth->getRole('admin');
        $permit = $auth->createPermission($permission);
        $permit->description = $des;
        $auth->add($permit);
        $auth->addChild($role, $permit);
    }


    protected function removePermission($per)
    {
        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $access = $auth->getPermission($per);
        if (!$access) {
            return;
        }
        $auth->removeChild($role, $access);
        $auth->remove($access);
    }

}
