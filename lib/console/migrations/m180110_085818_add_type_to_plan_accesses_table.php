<?php

use yii\db\Migration;

/**
 * Class m180110_085818_add_type_to_plan_accesses_table
 */
class m180110_085818_add_type_to_plan_accesses_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%plan_accesses}}', 'type', 'string');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%plan_accesses', 'type');

        return false;
    }

}
