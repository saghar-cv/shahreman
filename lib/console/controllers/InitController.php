<?php

namespace console\controllers;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Class InitController
 * Initialize application on server and set default configurations.
 *
 * @package console\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class InitController extends Controller
{
    public $dbname;
    public $dbuser = 'root';
    public $dbpass = '';
    public $dbhost = '127.0.0.1';
    public $tblprefix = 'tbl_';
    public $composer = 'install';

    public $appConfig = [];

    public function options($actionID)
    {
        return ['dbname', 'dbuser', 'dbpass', 'dbhost', 'tblprefix', 'composer'];
    }


    /**
     * Install and configure project
     *
     * @internal string $token  Bot token.
     *
     * @return int
     *
     * @author   Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionIndex()
    {
        return $this->install();
    }


    /**
     * Initialize database.
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionInjectInitData()
    {
        $admins = \backend\models\Admin::find()->count();
        if ($admins == 0) {
            $admin = new \backend\models\Admin();
            $admin->mobile = '09120813856';
            $admin->name = 'پشتیبان فنی';
            $admin->setPassword("*devmaster-87413");
            $admin->generateAuthKey();
            $admin->save(false);

            $auth = \Yii::$app->authManager;
            $role = $auth->getRole('admin');
            $auth->assign($role, $admin->id);

            echo "User created.";
        } else {
            echo "Install loaded before.";
        }
    }


    /**
     * Shave git and update project.
     *
     * @return int
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionUpdate(){
        $this->update();
        return ExitCode::OK;
    }

    /**
     * Run install commands
     *
     * @return int
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function install(){
        if ($this->dbname) {
            $e = $this->configDatabase($this->dbname, $this->dbuser, $this->dbpass, $this->dbhost, $this->tblprefix);
            if ($e != ExitCode::OK) {
                return $e;
            }
        }

        echo shell_exec("php yii migrate --interactive=0");
        return ExitCode::OK;
    }

    /**
     * Run update commands.
     *
     * @return integer
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function update(){
        $gitPath = dirname(dirname(dirname(__DIR__)));

        echo shell_exec("git  -C $gitPath  fetch --all");
        echo shell_exec("git  -C $gitPath  reset --hard origin/master");
        echo shell_exec("composer ".$this->composer);
        echo shell_exec("php yii migrate --interactive=0");

        return ExitCode::OK;
    }


    /**
     * Load application configurations.
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function loadConfigurations(){
        $config = \yii\helpers\ArrayHelper::merge(
            require(dirname(dirname(__DIR__)) . '/common/config/main.php'),
            require(dirname(dirname(__DIR__)) . '/common/config/main-local.php'),
            require(dirname(dirname(__DIR__)) . '/console/config/main.php'),
            require(dirname(dirname(__DIR__)) . '/console/config/main-local.php')
        );
        $this->appConfig = $config;
    }


    /**
     * Save database connection data into database file.
     *
     * @param string $dbName
     * @param string $username
     * @param string $password
     * @param string $host
     * @param string $tablePrefix
     *
     * @return int
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function configDatabase($dbName, $username = 'root', $password = '', $host = 'localhost', $tablePrefix = 'tbl_')
    {
        $dbConfigPath = dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . "common" . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "db.php";
        $configTxt = <<<TEXT
<?php
return
    [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=$host;dbname=$dbName',
        'username' => '$username',
        'password' => '$password',
        'charset' => 'utf8mb4',
        'enableSchemaCache' => true,
        // Duration of schema cache.
        'schemaCacheDuration' => 3600,
        // Name of the cache component used to store schema information
        'schemaCache' => 'cache',
        'tablePrefix' => '$tablePrefix',
    ];
TEXT;


        if ($dbConfigFile = fopen($dbConfigPath, "w")) {
            fwrite($dbConfigFile, $configTxt);
            fclose($dbConfigFile);
            $this->stdout("Database configs saved in $dbConfigPath\n");

//            $this->loadConfigurations();
//            new \yii\console\Application($this->appConfig);
//            \Yii::$app->runAction('migrate/up', ['interactive' => false]);

            return ExitCode::OK;
        } else {
            $this->stdout("Can not write on " . $dbConfigPath . "\n", Console::FG_RED);
            $this->stdout("Please check file permissions.\n", Console::FG_RED);
            return ExitCode::NOPERM;
        }

    }
}