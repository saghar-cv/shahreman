<?php
if (!YII_ENV_TEST and YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
    if (YII_DEBUG_PANEL) {
        $config['modules']['debug']['allowedIPs'] = ['*'];
        $config['components']['request']['cookieValidationKey'] = 'Ko4GeWFgM-uT9jt99Z2VHPJwJPMyoVuj';
    }

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
