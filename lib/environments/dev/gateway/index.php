<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

if (YII_DEBUG) {
    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
}

require(__DIR__ . '/../lib/vendor/autoload.php');
require(__DIR__ . '/../lib/vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../lib/common/config/bootstrap.php');
require(__DIR__ . '/../lib/frontend/config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../lib/common/config/main.php'),
    require(__DIR__ . '/../lib/common/config/main-local.php'),
    require(__DIR__ . '/../lib/frontend/config/main.php'),
    require(__DIR__ . '/../lib/frontend/config/main-local.php')
);

$application = new yii\web\Application($config);
$application->run();
