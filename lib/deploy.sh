#!/usr/bin/env bash
git fetch --all
if [ -z "$1" ]; then
        git reset --hard origin/master
else
        git reset --hard origin/$1
fi
php -d "disable_functions=" /usr/local/bin/composer install
php yii migrate --interactive=0
rm -rf api/runtime/cache
rm -rf backend/runtime/cache
rm -rf console/runtime/cache
rm -rf frontend/runtime/cache