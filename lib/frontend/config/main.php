<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'mycity-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'gate/error',
        ],
        'apiUrlManager' => [
            'class' => '\yii\web\UrlManager',
            'baseUrl' => '@web/api',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '.json',
            'rules' => require(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'url-rules.php')
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                // use temporary redirection instead of permanent for debugging
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
            ],
            'rules' => [
//                [
//                    'class' => 'frontend\components\AppUrl',
//                ],
                '<business:\d+>' => '/gate/pay',
                'verify' => '/gate/verify',
            ],
        ],
    ],
    'modules' => [
        'payment' => [
            'class' => 'aminkt\payment\Payment',
            'controllerNamespace' => 'aminkt\payment\controllers\frontend',
            // Add this part to add your own gates.
            'paymentComponentConfiguration' => [
                'class' => 'aminkt\payment\components\Payment',
                'callback' => ['/gate/verify'],
                'sendPage' => false,
                'gates' => [
                    \aminkt\payment\lib\ZarinPal::$gateId => [
                        'class' => \aminkt\payment\lib\ZarinPal::className(),
                        'identityData' => [
                            'merchantCode' => '6d7cd972-220d-11e9-bcbc-005056a205be',
                            'redirectUrl' => 'https://zarinpal.com/pg/StartPay/%u',
                            'payRequestUrl' => 'https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json',
                            'verifyRequestUrl' => 'https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json',
                            'isZarin' => false,
                            'enableSandbox' => false
                        ]
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];