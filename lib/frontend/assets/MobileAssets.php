<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MobileAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'assets/app/css/materialize.css',
        '../assets/app/font-awesome/css/font-awesome.min.css',
//        'assets/app/css/normalize.css',
//        'assets/app/css/owl.carousel.css',
//        'assets/app/css/owl.theme.css',
//        'assets/app/css/owl.transitions.css',
//        'assets/app/css/fakeLoader.css',
//        'assets/app/css/animate.css',
//        'assets/app/css/magnific-popup.css',
//        'assets/app/css/fonts.css',
//        'assets/app/css/style.css',
//        'assets/app/css/rtl.css',
//        'assets/app/css/orange-theme.css',
        '../assets/app/css/all.min.css',
        '../assets/app/css/custom.css',
    ];
    public $js = [
//        'assets/app/js/jquery.min.js',
        '../assets/app/js/materialize.min.js',
        '../assets/app/js/owl.carousel.min.js',
//        'assets/app/js/fakeLoader.min.js',
        '../assets/app/js/animatedModal.min.js',
        '../assets/app/js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
