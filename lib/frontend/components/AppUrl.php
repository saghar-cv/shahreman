<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 1/26/2018
 * Time: 01:14 PM
 */

namespace frontend\components;


use saghar\address\models\City;
use yii\base\BaseObject;
use yii\web\UrlRuleInterface;

class AppUrl extends BaseObject implements UrlRuleInterface
{
    private $rulableControllers = [
        'news',
        'page',
        'business',
        'search',
        'site'
    ];

    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function createUrl($manager, $route, $params)
    {
        $res = false;
        if(preg_match('%^(\w+)/(index)$%', $route, $matches)  and $this->checkController($matches[1])){
            $res = "{$matches[1]}";
        }elseif(preg_match('%^(\w+)/(view)$%', $route, $matches)  and $this->checkController($matches[1])){
            $id = $params['id'];
            unset($params['id']);
            $res = "{$matches[1]}/$id";
        } elseif (preg_match('%^site/([a-z]+-?[a-z]+)$%', $route, $matches)) {
            if ($matches[1] != 'home') {
                $res = "{$matches[1]}";
            }
        }elseif(preg_match('%^(\w+)/([a-z]+-?[a-z]+)$%', $route, $matches) and $this->checkController($matches[1])){
            $res = "{$matches[1]}/{$matches[2]}";
        }else{
            return false;
        }

        if(isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']){
            $cityName = strtolower($city->latinName);
            $cityName = str_replace(' ', '_', $cityName);
            $res = "$cityName/$res";
        }

        if (!empty($params) && ($query = http_build_query($params)) !== '') {
            $res .= '?' . $query;
        }
        return $res;
    }

    /**
     * Check if controller founded in request is valid or not.
     *
     * @param string $controller Controller name.
     *
     * @return boolean
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function checkController($controller){
        return in_array($controller, $this->rulableControllers);
    }

    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();
        $params = $request->getQueryParams();

        if (preg_match('%^([a-zA-Z-_\']+)\/(\w+)\/([a-z]+-?[a-z]+)$%', $pathInfo, $matches)) {
            // Check format for city/controller/action like rasht/business/update-info
            $city = $this->getCity($matches[1]);
            if($city and $this->checkController($matches[2])){
                return ["/{$matches[2]}/{$matches[3]}", $params];
            }
        } elseif (preg_match('%^([a-zA-Z-_\']+)\/(\w+)\/(\d+)$%', $pathInfo, $matches)) {
            // Check format for city/controller/id like rasht/news/6516 to run view action
            $city = $this->getCity($matches[1]);
            if($city and $this->checkController($matches[2])) {
                $params['id'] = $matches[3];
                return ["/{$matches[2]}/view", $params];
            }
        } elseif (preg_match('%^([a-zA-Z-_\']+)\/([a-z]+-?[a-z]+)$%', $pathInfo, $matches)) {
            // Check format for city/controller like rasht/news
            $city = $this->getCity($matches[1]);
            if ($city) {
                if ($this->checkController($matches[2])) {
                    return ["/{$matches[2]}/index", $params];
                } else {
                    return ["/site/{$matches[2]}", $params];
                }
            }
        } elseif (preg_match('%^([a-zA-Z-_\']+)$%', $pathInfo, $matches)) {
            // Check format for city/controller like rasht/news
            $city = $this->getCity($matches[1]);
            if($city) {
                return ["/site/index", $params];
            }
        }
        return false;
    }

    /**
     * Get city and save it in params.
     *
     * @param string $latinName
     *
     * @return City
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function getCity($latinName){
        $latinName = str_replace('_', ' ', $latinName);
        $city = City::findOne([
            'latinName'=>$latinName
        ]);
        \Yii::$app->params['city'] = $city;
        return $city;
    }
}