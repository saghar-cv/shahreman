<?php

namespace frontend\controllers;

use aminkt\components\appcache\AppCacheFilter;
use common\models\Post;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class NewsController
 * Handle newses.
 *
 * @package frontend\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class NewsController extends \yii\web\Controller
{
    public $layout = 'app';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'appcache' => [
                'class' => AppCacheFilter::className(),
                'actions' => ['index', 'view']
            ],
        ];
    }

    /**
     * Show list of newses.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'postType' => Post::TYPE_NEWS_POST,
            'status' => Post::STATUS_PUBLISH,
        ]);

        if (isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }


    /**
     * View a news by id.
     *
     * @param integer $id Post id.
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        if (isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']) {
            $post = Post::findOne([
                'id' => $id,
                'cityId' => $city->id
            ]);
        } else {
            $post = Post::findOne($id);
        }

        if (!$post) {
            throw new NotFoundHttpException("خبر مورد نظر یافت نشد.");
        }

        return $this->render('view', [
            'model' => $post
        ]);
    }

    /**
     * Overwrite render to handle ajax requests.
     *
     * @param string $view
     * @param array  $params
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function render($view, $params = [])
    {
        if (\Yii::$app->getRequest()->isAjax) {
            return $this->renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}
