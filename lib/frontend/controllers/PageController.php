<?php

namespace frontend\controllers;

use aminkt\components\appcache\AppCacheFilter;
use common\models\Post;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * Created to show pages in application and website.
 *
 * @package frontend\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class PageController extends \yii\web\Controller
{
    public $layout = 'app';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'appcache' => [
                'class' => AppCacheFilter::className(),
                'actions' => ['index', 'view']
            ],
        ];
    }

    /**
     * Show list of static posts.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'status' => Post::STATUS_PUBLISH,
            'parentId' => null,
            'postType' => Post::TYPE_STATIC_POST
        ]);

        if (isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }


    /**
     * View a post by id.
     *
     * @param integer $id Post id.
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        if (isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']) {
            $model = Post::findOne([
                'id' => $id,
                'postType' => Post::TYPE_STATIC_POST,
                'status' => Post::STATUS_PUBLISH,
                'cityId' => $city->id
            ]);
        } else {
            $model = Post::findOne([
                'id' => $id,
                'postType' => Post::TYPE_STATIC_POST,
                'status' => Post::STATUS_PUBLISH
            ]);
        }

        if (!$model) {
            throw new NotFoundHttpException("صفحه پیدا نشد.");
        }

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Overwrite render to handle ajax requests.
     *
     * @param string $view
     * @param array  $params
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function render($view, $params = [])
    {
        if (\Yii::$app->getRequest()->isAjax) {
            return $this->renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}
