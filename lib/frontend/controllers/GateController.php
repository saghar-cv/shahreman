<?php

namespace frontend\controllers;


use aminkt\payment\Payment;
use backend\models\Order;
use common\models\Business;
use Imagine\Exception\RuntimeException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class GateController
 * This controller will control all payment scenarios.
 *
 * @package frontend\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class GateController extends Controller
{
    public $layout = 'app';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Send a new pay request to bank gateway.
     *
     * @param integer $business Business id.
     *
     * @return string|\yii\web\Response
     *
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \Exception
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionPay($business)
    {
        $business = Business::findOne($business);
        if (!$business) {
            throw new NotFoundHttpException("صنف مورد نظر یافت نشد.");
        } elseif ($business->status != $business::STATUS_NOT_PAYED) {
            throw new BadRequestHttpException("مبلغ پلن این صنف قبلا پرداخت شده است.");
        }

        $payment = Payment::getInstance()->payment;
        if ($city = \Yii::$app->getRequest()->get('city')) {
            $payment->callback = ['/gate/verify', 'city' => $city];
        }

        $plan = $business->plan;
        if (!$plan) {
            throw new BadRequestHttpException("پلن برای بیزینس ثبت نشده است.");
        }

        if (!$business->cities) {
            throw new BadRequestHttpException("هیج شهری برای انتشار کسب و کار انتخاب نشده است.");
        }

        $amount = count($business->cities) * $plan->price;

        if ($order = Order::add($business->id, $plan->id, $amount)) {
            $data = $payment->payRequest($amount, $order->id);
        } else {
            throw new RuntimeException('خطا در ذخیره اطلاعات');
        }

        // $data is an array that hold a form information that you should send to bank gateway
        if (is_array($data) and array_key_exists('redirect', $data) and isset($data['redirect'])) {
            return $this->redirect($data['redirect']);
        } else {
            return $this->render('send', [
                'data' => $data
            ]);
        }

    }

    /**
     * Verify payment request.
     *
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionVerify()
    {
        /** @var \aminkt\payment\components\Payment $payment */
        $payment = Payment::getInstance()->payment;
        $verify = $payment->verify();
        if ($verify) {
            $session = $verify->getTransactionModel();
            $order = Order::findOne($session->orderId);
            if ($order) {
                $business = Business::findOne($order->businessId);
                if (!$business) {
                    throw new NotFoundHttpException("شناسه صنف معتبر نیست.");
                } elseif ($business->plan->id != $order->planId) {
                    throw new ForbiddenHttpException("شما قبل از پرداخت موفق و بعد از ورود به بانک پلن صنف را عوض کرده اید. دوباره تلاش کنید.");
                }
                $business->upgradeBusinessPlan();
                $order->setStatus($order::STATUS_PAID);
            } else {
                throw new NotFoundHttpException('اطلاعات پرداخت یافت نشد');
            }
        }
        return $this->render('verify', [
            'verify' => $verify,
        ]);
    }
}
