<?php

namespace frontend\controllers;

use aminkt\components\appcache\AppCacheFilter;
use aminkt\normalizer\Normalize;
use aminkt\widgets\alert\Alert;
use frontend\models\ContactForm;
use frontend\models\LoginAuthedForm;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'app';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'login'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'appcache' => [
                'class' => AppCacheFilter::className(),
                'actions' => ['index', 'contact']
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionHome()
    {
        return $this->renderFile(Yii::getAlias("@webroot") . '/landing-template/index.html');
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * in this function, user enters its phone number
     * if user exists in databse just creates a new auth code and sends by sms to phone
     * and redirect the user to authe-login route
     * else if user doesn't exists, redirects the user to the signup route
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user) {
                $authCode = mt_rand(10000, 99999);
                $user->authCode = $authCode;

                /** @var \aminkt\sms\yii\SmsComponent $smsComponent */
                $smsComponent = Yii::$app->sms;
                $smsComponent->send([
                    'message' => 'رمز عبور شما برای ورود به نرم افزار شهر من : ' . $authCode,
                    'numbers' => [$user->mobile]
                ]);
                $user->save();
                return $this->redirect(['/site/login-authed', 'mobile' => $user->mobile]);
            } else {
                return $this->redirect(['/site/signup', 'mobile' => $model->mobile]);
            }
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * when user exists in database, an sms is sent to the user phone
     * so when user is in this route
     *
     * @param $mobile
     *
     * @return string|\yii\web\Response
     */
    public function actionLoginAuthed($mobile)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = \frontend\models\User::findByUsername($mobile);
        if ($user) {
            $model = new LoginAuthedForm();
            $model->mobile = Normalize::normalizeMobile($mobile);
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->login()) {
                    return $this->goBack();
                } else {
                    Alert::error("ورود موفقیت آمیز نبود.", "مقادیر ورودی را مجددا کنترل نمایید");
                    $this->redirect(['/site/login']);
                }
            } else {
                return $this->render('login-authed', [
                    'model' => $model
                ]);
            }
        }

        return $this->redirect(['/site/login']);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(['index']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->subject = "فرم ارتباط با ما - ".$model->subject;
            $mailStatus = $model->sendEmail(Yii::$app->params['adminEmail']);
            Yii::debug($mailStatus);
            if ($mailStatus) {
                Alert::success('پیام ارسال شد', 'با تشکر از شما. پیام شما برای ما ارسال شد و به آن سریعا پاسخ خواهیم داد.');
            } else {
                Alert::error('خطا در ارسال پیام', 'متاسفانه مشکلی در ارسال پیام شما به وجود آمده است.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Signs user up.
     *
     * @param string $mobile Mobile number.
     *
     * @return mixed
     */
    public function actionSignup($mobile)
    {
        $model = new SignupForm();
        $model->mobile = Normalize::normalizeMobile($mobile);
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return $this->redirect(['/site/login-authed', 'mobile' => $model->mobile]);
            }
        }
        return $this->render('signup', [
            'model' => $model
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Alert::success('لینک تغییر گذرواژه به ایمیل شما ارسال شد.', ' ');
                return $this->redirect(['site/login']);
            } else {
                Alert::error('خطا در عملیات', 'متاسفانه امکان ارسال اطلاعات برای ایمیل شما وجود ندارد.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Alert::success('گذرواژه جدید ثبت شد.', ' ');

            return $this->redirect(['site/login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Show user profile
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionProfile()
    {
        $model = User::findOne(Yii::$app->getUser()->id);
        if (!$model) {
            Alert::error('صفحه مورد نظر در دسترس نمی باشد', '');
            return $this->redirect(['index']);
        }

        if (Yii::$app->getRequest()->isPost and $model->load(Yii::$app->request->post())) {
            $model->mobile = Normalize::normalizeMobile($model->mobile);
            if (!$model->save()) {
                Alert::error('خطا در انجام عملیات', 'تغییرات ذخیره نشد.');
                return $this->redirect(['index']);
            }
            Alert::success('عملیات با موفقیت انجام شد', '');
            return $this->redirect(['profile']);
        }

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * Overwrite render to handle ajax requests.
     *
     * @param string $view
     * @param array $params
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function render($view, $params = [])
    {
        Yii::warning(Yii::$app->getRequest()->isAjax);
        if (\Yii::$app->getRequest()->isAjax) {
            return $this->renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}
