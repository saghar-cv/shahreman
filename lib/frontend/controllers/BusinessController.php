<?php

namespace frontend\controllers;

use aminkt\widgets\alert\Alert;
use backend\models\CategoryRelation;
use backend\models\Plan;
use common\models\Business;
use frontend\models\BusinessDataForm;
use frontend\models\LoginAuthedForm;
use frontend\models\search\Search;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Response;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class BusinessController
 *
 * @package frontend\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 * @author  Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessController extends \yii\web\Controller
{
    public $layout = 'app';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'plans', 'view', 'list', 'search'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        'actions' => ['register', 'update-info', 'update', 'my'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update-info' => ['post'],
                ],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if ($token = \Yii::$app->getRequest()->get('t')) {
            LoginAuthedForm::loginByToken($token);
        }
        return parent::beforeAction($action);
    }

    /**
     * Category traverse to find business
     *
     * @param int $depth
     * @param int $parentId
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex($depth = 0, $parentId = null)
    {
        $searchModel = new Search();
        $searchModel->depth = $depth;
        $searchModel->parentId = $parentId;
        $dataProvider = $searchModel->searchCategories(\Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * List all businesses in specific category.
     *
     * @param $catId
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionList($catId = null)
    {
        $searchModel = new Search();
        $searchModel->catId = $catId;
        $dataProvider = $searchModel->searchBusiness(\Yii::$app->request->queryParams);

        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * List of plans
     *
     * @return string
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionPlans()
    {
        $plans = Plan::find()->where(['!=', 'status', Plan::STATUS_REMOVED])->all();
        if (!$plans) {
            throw new NotFoundHttpException('پلانی یافت نشد');
        }
        return $this->render('plan_list', [
            'plans' => $plans
        ]);
    }

    /**
     * Register business
     *
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionRegister($id)
    {
        $plan = Plan::findOne($id);
        if (!$plan) {
            throw new NotFoundHttpException('پلان انتخاب شده معتبر نیست');
        }

        $model = new Business();
        $categoryModel = new CategoryRelation();

        if ($model->load(\Yii::$app->getRequest()->post()) and
            $categoryModel->load(\Yii::$app->getRequest()->post())) {
            $model->planId = $id;

            if ($model->save()) {
                $categoryModel->businessId = $model->id;

                if ($categoryModel->save()) {
                    Alert::success('صنف با موفقیت ذخیره شد', '');
                    return $this->redirect(['update', 'businessId' => $model->id]);
                } else
                    Alert::error('در ثبت دسته خطایی رخ داد', 'صنف ثبت نشد');
            } else {
                Alert::error('در ثبت اطلاعات خطایی رخ داد', 'صنف ثبت نشد');
            }
        }

        return $this->render('register', [
            'model' => $model,
            'categoryModel' => $categoryModel
        ]);
    }

    /**
     * Edit or add business information.
     *
     * @param integer $businessId
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     *
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($businessId)
    {
        $business = Business::findOne($businessId);
        if (!$business) {
            throw new NotFoundHttpException("صنف مورد نظر یافت نشد.");
        }

        $businessData = new BusinessDataForm();
        $businessData->setBusiness($business);

        /**
         * Get filed data
         */
        $url = \Yii::$app->apiUrlManager->createAbsoluteUrl(['/v1/business/edit-data', 'businessId' => $businessId]);
        $client = new Client([
            'baseUrl' => $url,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        /** @var Response $response */
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('GET')
            ->addHeaders(['Authorization' => "Bearer " . \Yii::$app->getUser()->getIdentity()->getAuthKey()])
            ->setData([
                'businessId' => $businessId,
                'name' => \Yii::$app->getRequest()->post('name'),
                'value' => \Yii::$app->getRequest()->post('value'),
            ])->send();

        if ($response->getStatusCode() == 403) {
            throw new ForbiddenHttpException("شما مالک این صنف نیستید.");
        } elseif ($response->getStatusCode() != 200) {
            throw new ServerErrorHttpException("خطای سروری رخ داده است.");
        }
        $response = Json::decode($response->getContent());

        /**
         * Get available city lists.
         */
        $url = \Yii::$app->getUrlManager()->getHostInfo() . \Yii::$app->getHomeUrl() . 'api/v1/cities.json';
        $client = new Client([
            'baseUrl' => $url,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        /** @var Response $c ' */
        $cities = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('GET')
            ->send();

        $cities = Json::decode($cities->getContent());
        $cityList = [];
        foreach ($cities['data'] as $city) {
            $cityList[$city['id']] = $city['name'];
        }
        return $this->render('update', [
            'businessDataModel' => $businessData,
            'contactData' => $response['data'],
            'cities' => $cityList
        ]);
    }


    /**
     * Create an http request and send data to api. this is a wrapper method..
     * @param $businessId
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdateInfo($businessId)
    {

        $url = \Yii::$app->getUrlManager()->getHostInfo() . \Yii::$app->getHomeUrl() . 'api/v1/business/update-info.json';
        $client = new Client([
            'baseUrl' => $url,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        /** @var Response $response */
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('POST')
            ->addHeaders(['Authorization' => "Bearer " . \Yii::$app->getUser()->getIdentity()->getAuthKey()])
            ->setData([
                'businessId' => $businessId,
                'name' => \Yii::$app->getRequest()->post('name'),
                'value' => \Yii::$app->getRequest()->post('value'),
            ])->send();

        $res = Json::decode($response->getContent());

        if (!$res['success']) {
            \Yii::$app->getResponse()->setStatusCode($res['data']['status']);
            return $res['data']['message'];
        }

        return $response->content;
    }


    /**
     * View a business page.
     *
     * @param string $id
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @throws ForbiddenHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionView($id)
    {
        $business = Business::findOne($id);
        if (!$business) {
            throw new NotFoundHttpException("صنف مورد نظر یافت نشد.");
        } elseif ($business->ownerId != \Yii::$app->getUser()->getId() and
            $business->status != $business::STATUS_CONFIRMED) {
            throw new ForbiddenHttpException('شما مجاز به دیدن این صفحه نیستید');
        }

        $editable = $business->ownerId == \Yii::$app->getUser()->getId();

        return $this->render('view', [
            'editable' => $editable,
            'business' => $business,
        ]);
    }

    /**
     * Search businesses
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionSearch()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->searchBusiness(\Yii::$app->request->queryParams);

        return $this->render('search', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Return user's businesses list.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionMy()
    {
        $userId = \Yii::$app->getUser()->getId();
        $businesses = Business::find()->where(['ownerId' => $userId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $businesses
        ]);
        return $this->render('my', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Overwrite render to handle ajax requests.
     *
     * @param string $view
     * @param array  $params
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function render($view, $params = [])
    {
        if (\Yii::$app->getRequest()->isAjax) {
            return $this->renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}
