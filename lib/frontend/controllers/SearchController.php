<?php

namespace frontend\controllers;

use frontend\models\search\Search;

/**
 * Class SearchController
 * Control search actions.
 *
 * @package frontend\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class SearchController extends \yii\web\Controller
{
    public $layout = 'app';

    /**
     * Main search page.
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $searchModel = new Search();
        $dataProviders = $searchModel->generalSearch(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProviders' => $dataProviders
        ]);
    }

    /**
     * Show all searched businesses
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionBusinesses()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->generalBusinessSearch(\Yii::$app->request->queryParams);

        return $this->render('businesses', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Show all searched news
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionNews()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->newsSearch(\Yii::$app->request->queryParams);
        return $this->render('news', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Show all searched posts
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionPosts()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->postSearch(\Yii::$app->request->queryParams);
        return $this->render('posts', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Overwrite render to handle ajax requests.
     *
     * @param string $view
     * @param array  $params
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function render($view, $params = [])
    {
        if (\Yii::$app->getRequest()->isAjax) {
            return $this->renderAjax($view, $params);
        }
        return parent::render($view, $params);
    }
}
