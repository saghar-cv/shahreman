<?php

namespace frontend\controllers;

use yii\helpers\Url;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;

class ApiController extends Controller
{
    public $token = "eNz-h4pm_Nlv4IHnJxD1cjQcbzOdz_7a";

    public function beforeAction($action)
    {
        $token = \Yii::$app->getRequest()->getHeaders()->get('Authorization');
        if (!$token) {
            throw new ForbiddenHttpException("شما دسترسی مجاز به این صفحه را ندارید.");
        }
        $token = trim($token);
        $token = explode(' ', $token);
        if ((strtolower($token[0]) != 'basic') or ($token[1] != $this->token)) {
            throw new ForbiddenHttpException("شما دسترسی مجاز به این صفحه را ندارید.");
        }

        return parent::beforeAction($action);
    }

    /**
     * Return page urls that should cache in mobile application.
     *
     * @return array
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionGetOfflinePages()
    {
        $pages = [
            Url::to(['/site/index'], true),
            Url::to(['/news/view', 'id' => 2], true),
            Url::to(['/page/view', 'id' => 2], true),
        ];

        return $pages;
    }
}
