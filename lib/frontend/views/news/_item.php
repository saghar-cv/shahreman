<?php
/**
 * @var \common\models\Post $model
 */
?>
<?php if ($index == 0) : ?>
    <div class="row">
        <div class="col s12">
            <a href="<?= \yii\helpers\Url::to(['/news/view', 'id' => $model->id]) ?>">
                <div class="blog-content news-bold">
                    <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId, 'thumb') ?>"
                         alt="<?= \yii\helpers\Html::encode($model->title) ?>">
                    <div class="blog-detailt">
                        <h3><?= \yii\helpers\Html::encode($model->title) ?></h3>
                        <div class="date">
                            <span><i class="fa fa-calendar"></i> &nbsp; <?= $model->getJalaliCreateTime() ?></span>
                        </div>
                        <p>
                            <?= (\yii\helpers\StringHelper::truncateWords($model->content, 200)) ?>
                        </p>
                    </div>
                </div>
            </a>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col s12">
            <a href="<?= \yii\helpers\Url::to(['/news/view', 'id' => $model->id]) ?>">
                <div class="blog-content news-summary">
                    <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId, 'thumb') ?>"
                         alt="<?= \yii\helpers\Html::encode($model->title) ?>">
                    <div class="blog-detailt">
                        <h4><?= \yii\helpers\Html::encode($model->title) ?></h4>
                        <div class="date">
                            <span><i class="fa fa-calendar"></i>  &nbsp; <?= $model->getJalaliCreateTime() ?></span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
<?php endif; ?>