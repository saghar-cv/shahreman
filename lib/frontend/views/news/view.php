<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Post */

$this->title = "خبر - {$model->title}"
?>
<!-- single post -->
<div class="pages section">
    <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId) ?>"
         style="width: 100%;margin-top: -35px;margin-bottom: 20px;"
         alt="<?= \yii\helpers\Html::encode($model->title) ?>">
    <div class="container">
        <div class="blog-single">
            <div class="blog-single-content">
                <h3><?= \yii\helpers\Html::encode($model->title) ?> </h3>
                <div class="date">
                    <span><i class="fa fa-calendar"></i> <?= $model->getJalaliCreateTime() ?></span>
                </div>
                <div class="categories">
                    <span><i class="fa fa-list"></i> <?php
                        $i = 0;
                        foreach ($model->categories as $category) {
                            if ($i) {
                                echo ', ';
                            }
                            echo $category->name;
                            $i++;
                        }
                        ?></span>
                </div>
                <div class="blog-content" style="text-align: justify; padding: 10px; color: #000000">
                    <?= $model->content ?>
                </div>
                <?php /*
                <div class="share-post">
                    <ul>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-google"></i></a></li>
                        <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                */ ?>
            </div>
            <?php /*
            <div class="comment">
                <h5>1 Comments</h5>
                <div class="comment-details">
                    <div class="row">
                        <div class="col s3">
                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/assets/app/img/user-comment.jpg" alt="">
                        </div>
                        <div class="col s9">
                            <div class="comment-title">
                                <span><strong>John Doe</strong> | Juni 5, 2016 at 9:24 am | <a href="">Reply</a></span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis accusantium corrupti asperiores et praesentium dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment-form">
                <div class="comment-head">
                    <h5>Post Comment in Below</h5>
                    <p>Lorem ipsum dolor sit amet consectetur*</p>
                </div>
                <div class="row">
                    <form class="col s12 form-details">
                        <div class="input-field">
                            <input type="text" required class="validate" placeholder="NAME">
                        </div>
                        <div class="input-field">
                            <input type="email" class="validate" placeholder="EMAIL" required>
                        </div>
                        <div class="input-field">
                            <input type="text" class="validate" placeholder="SUBJECT" required>
                        </div>
                        <div class="input-field">
                            <textarea name="textarea-message" id="textarea1" cols="30" rows="10" class="materialize-textarea" class="validate" placeholder="YOUR COMMENT"></textarea>
                        </div>
                        <div class="form-button">
                            <button class="button-default">Post Comments</button>
                        </div>
                    </form>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</div>
<!-- end single post -->