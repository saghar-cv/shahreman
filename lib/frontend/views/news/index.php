<?php
/* @var yii\web\View $this */

/* @var \yii\data\ActiveDataProvider $dataProvider */

use yii\widgets\ListView;


$this->title = "اخبار"
?>
<!-- blog -->
<div class="pages section">
    <div class="container">
        <div class="blog news">
            <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'itemView' => '_item',
            ]);
            ?>
            <?php /*
            <div class="row">
                <div class="col s12">
                    <div class="pagination-blog">
                        <ul>
                            <li class="active"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</div>
    <!-- end blog -->