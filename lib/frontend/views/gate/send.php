<?php
/** @var \yii\web\View $this */
/** @var $data array */

$this->title = 'شهر من - ارسال به بانک';
?>

<div class="container" id="purse" style="text-align: center">
    <div class="row panel">
        <div class="col s12">
            <h1>درحال ارسال به بانک</h1>
            <p>
                لطفا منتظر بمانید تا به صفحه بانک منتقل شوید.
            </p>
            <p>
                در صورتی که به صفحه بانک منتقل نشدید لطفا با پشتیبانی تماس بگیرید.
            </p>
        </div>
    </div>
</div>
<form id="send-to-bank" style="visibility: hidden;" action="<?= $data['action'] ?>" method="<?= $data['method'] ?>">
    <?php
    if (is_array($data['inputs'])) {
        foreach ($data['inputs'] as $name => $value) {
            echo '<input type="hidden" name="' . htmlentities($name) . '" value="' . htmlentities($value) . '">';
        }
    }
    ?>
</form>
<script type="text/javascript">
    //    document.getElementById('send-to-bank').submit();
</script>