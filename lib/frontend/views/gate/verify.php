<?php
/** @var \yii\web\View $this */
/** @var $verify boolean|aminkt\payment\lib\AbstractGate */

$this->title = 'شهر من - تائید پرداخت';
?>

<div class="container" id="purse" style="text-align: center">
    <div class="row panel">
        <div class="col s12">
            <?php if ($verify) : ?>
                <h1>خرید با موفقیت انجام شد</h1>
            <?php else: ?>
                <h1>خرید شما موفق نبود</h1>
            <?php endif; ?>
            <br> <br>
            <?php if ($verify) : ?>
                <p>
                    کد پیگیری تراکنش: <?= $verify->getTrackingCode() ?> <br>
                    برای اطلاعات بیشتر با پشتیبانی تماس بگیرید
                </p>
            <?php else: ?>
                <p>
                    <?php foreach (\aminkt\payment\components\Payment::getErrors() as $error) : ?>
                        <?= $error['code'] ?> : <?= $error['message'] ?>
                        <br>
                    <?php endforeach; ?>
                </p>
                <p>
                    برای اطلاعات بیشتر با پشتیبانی تماس بگیرید
                </p>
            <?php endif; ?>
        </div>
    </div>


    <br> <br>
    <p>
        <?= \aminkt\payment\components\Payment::decryptBankName(Yii::$app->request->get('bc')) ?>
    </p>
</div>