<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception \yii\web\HttpException|\Exception */

use yii\helpers\Html;
$this->title = $name;
$this->params['description']=" ";

$code = isset($exception->statusCode)? $exception->statusCode : $exception->getCode();
?>
<!-- error 404 -->
<div class="pages section">
    <div class="container">
        <div class="error404">
            <h4><?= $code ?></h4>
            <h5><?= nl2br(Html::encode($message)) ?></h5>
            <p>
                خطای بالا زمانی رخ داد که سرور های ما درخواست شما را پردازش میکردند
            </p>
            <p>
دوباره تلاش کنید و                 درصورتی که فکر میکنید این خطا یک خطای سیستمی میباشد لطفا آن را با ما در میان بگذارید

            </p>
        </div>
    </div>
</div>
<!-- end error 404 -->
