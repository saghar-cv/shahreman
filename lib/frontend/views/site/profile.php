<?php
/** @var $this \yii\web\View */
/** @var $model \frontend\models\User */
$this->title = 'ویرایش پروفایل';
?>

<div class="profile">
    <div class="row">
        <div class="col s12">
            <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>
            <?= $form->field($model, 'name')->textInput() ?>
            <?= $form->field($model, 'family')->textInput() ?>
            <?= $form->field($model, 'family')->textInput() ?>
            <?= $form->field($model, 'email')->textInput() ?>
            <?= $form->field($model, 'mobile')->textInput() ?>
            <?php
            $cities = \saghar\address\models\City::find()->all();
            $cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
            echo $form->field($model, 'cityId', [
                'options' => [
                    'class' => 'form-group select2-dropdownlist'
                ]
            ])->widget(\kartik\select2\Select2::class, [
                'data' => $cities,
                'options' => ['placeholder' => 'شهر را انتخاب کنید ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            <br>
            <div class="text-center">
                <?= \yii\helpers\Html::submitButton('ویرایش', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>
