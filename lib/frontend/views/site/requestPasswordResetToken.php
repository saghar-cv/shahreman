<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'درخواست تغییر گذرواژه';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <div class="row">
        <div class="col-lg-5 col-md-7" style="margin: 0 auto; float: none;">

            <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

            <p>لطفا ایمیل خود را در کادر زیر وارد کنید. لینک تغییر گذرواژه برای شما ارسال خواهد شد.</p>

            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

                <div class="form-group text-center">
                    <?= Html::submitButton('ارسال', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('وارد شوید!', ['/site/login'], ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
