<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'تماس با ما';
$this->params['description'] = "راه های ارتباطی شما با تل بیت: info[at]telbit[dot]ir";

$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$('#refCaptcha').on('click', function(e){
        e.preventDefault();
        $('#contactform-verifycode-image').click();
});
JS;
$this->registerJs($js);
?>
<div class="pages section">
    <div class="container">
        <div class="pages-head">
            <h3><?= $this->title ?></h3>
        </div>
        <div class="contact-us">
            <div class="row">
                <div class="col s12">
                    <?php $form = ActiveForm::begin([
                        'id' => 'contact-form',
                        'class' => 'contact-form',
                        'method' => 'post',
                    ]); ?>
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'phone') ?>

                    <?= $form->field($model, 'subject') ?>

                    <?= $form->field($model, 'body')->textArea([
                        'rows' => 6,
                    ]) ?>
                    <br>
                    <div class="form-group text-center">
                        <?= Html::submitButton('ارسال', ['class' => 'btn btn-sm', 'name' => 'contact-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>