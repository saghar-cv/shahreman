<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'ثبت نام';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages section">
    <div class="container">
        <div class="site-signup">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>لطفا اطلاعات زیر برای ثبت نام وارد کنید:</p>

            <div class="row">
                <div class="col-lg-12">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'family')->textInput() ?>

                    <?= $form->field($model, 'mobile')->hiddenInput()->label(false) ?>

                    <?= $form->field($model, 'email') ?>
                    <?php
                    $cities = \saghar\address\models\City::find()->all();
                    $cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
                    echo $form->field($model, 'cityId', [
                        'options' => [
                            'class' => 'form-group select2-dropdownlist'
                        ]
                    ])->widget(\kartik\select2\Select2::class, [
                        'data' => $cities,
                        'options' => ['placeholder' => 'شهر را انتخاب کنید ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>


                    <br>

                    <div class="form-group text-center">
                        <?= Html::submitButton('ثبت نام', ['class' => 'btn btn-orange text-center', 'name' => 'signup-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

