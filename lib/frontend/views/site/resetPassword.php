<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'تغییر گذرواژه';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">

    <div class="row">
        <div class="col-lg-5 col-md-7" style="margin: 0 auto; float: none;">

            <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

            <p>کلمه عبور جدید را وارد کنید: </p>

            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label("کلمه عبور جدید") ?>

                <div class="form-group text-center">
                    <?= Html::submitButton('ذخیره', ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('وارد شوید!', ['/site/login'], ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
