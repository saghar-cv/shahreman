<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'ورود به ناحیه کاربری | تل بیت';
$this->params['breadcrumbs'][] = $this->title;
$this->params['description']="با تل بیت فروشگاه تلگرامی خود را راه اندازی کنید و از امکان پرداخت آنلاین تل بیت رایگان بهره مند شوید.";

?>
<div class="pages section">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-7" style="margin: 0 auto; float: none;">

                <h2 class="text-center">ورود به ناحیه کاربری</h2>

                <br>
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'mobile')->input('number', ['class' => 'text-center'])->label("شماره تلفن همراه خود را وارد کنید") ?>

                <p class="note">
                    در صورتی که قبلا ثبت نام نکرده اید مراحل ثبت نام خودکار طی خواهد شد.
                </p>
                <br>

                <div class="form-group text-center">
                    <?= Html::submitButton('ورود', ['class' => 'btn btn-orange', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
