<?php
/** @var \yii\web\View $this */
if (isset(\Yii::$app->params['city']) and $city = Yii::$app->params['city']) {
    $cityName = $city->name;
} else {
    $cityName = "شهر";
}

$js = <<<JS
    // slider
    $(".slider").slider({full_width: true});
JS;
$this->registerJs($js);
?>

<!-- slider -->
<div class="slider">

    <ul class="slides">
        <li>
            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/shahdari-night.jpg" alt="">
            <div class="caption slider-content">
                <h2>درباره شهرداری رشت</h2>
                <h4>با شهرداری رشت و سازمان‌های تابعه آشنا شوید</h4>
                <a href="<?= \yii\helpers\Url::to(['/page/view', 'id' => 1]) ?>'" class="button-default">بیشتر
                    بخوانید</a>
            </div>
        </li>
        <li>
            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/dish.jpg" alt="">
            <div class="caption slider-content">
                <h2>سوغات رشت</h2>
                <h4>با سوغات رشت بیشتر آشنا شوید</h4>
                <a href="<?= \yii\helpers\Url::to(['/page/view', 'id' => 5]) ?>'" class="button-default">بیشتر
                    بخوانید</a>
            </div>
        </li>
        <li>
            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/food.jpg" alt="">
            <div class="caption slider-content">
                <h2>رشت شهر خلاق غذا</h2>
                <h4>درباره ثبت رشت به عنوان شهر خلاق غذا در یونسکو</h4>
                <a href="<?= \yii\helpers\Url::to(['/page/view', 'id' => 6]) ?>'" class="button-default">بیشتر
                    بخوانید</a>
            </div>
        </li>
    </ul>

</div>
<!-- end slider -->
