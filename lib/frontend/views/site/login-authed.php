<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'ورود به ناحیه کاربری | تل بیت';

?>
<div class="pages section">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-7" style="margin: 0 auto; float: none;">

                <h2 class="text-center">ورود به ناحیه کاربری</h2>

                <br>

                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'mobile')->hiddenInput()->label(false) ?>

                <?= $form->field($model, 'authCode')->input('number')->label("کد پیامک شده به تلفن خود را وارد کنید") ?>


                <br>

                <div class="form-group text-center">
                    <?= Html::submitButton('ورود', ['class' => 'btn btn-orange', 'name' => 'login-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>