<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

$this->title = "شهر من";
$this->params['description'] = "پرتال جامع شهری گیلان";

if (isset(\Yii::$app->params['city']) and $city = Yii::$app->params['city']) {
    $cityName = $city->name;
} else {
    $cityName = "شهر";
}
?>
<?php
echo $this->render('_slide-show');
?>

<!-- features -->
<a class="panel row" href="<?= Url::to(['/page/view', 'id' => 1]) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/About_City.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5>درباره <?= $cityName ?></h5>
        <p>با اماکن دیدنی، مناطق گردشگری و سایر اطلاعات شهر آشنا شوید.</p>
    </div>
</a>
<a class="panel row" href="<?= Url::to(['/page/view', 'id' => 8]) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/shahrdari.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5> درباره شهرداری رشت </h5>
        <p>با شهرداری رشت و سازمان‌های تابعه بیشتر آشنا شوید.</p>
    </div>
</a>
<a class="panel row" href="<?= Url::to(['/page/view', 'id' => 17]) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/turism.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5> گردشگری در رشت </h5>
        <p>جاذبه های گردشگر رشت را بیشتر بشناسید.</p>
    </div>
</a>
<a class="panel row" href="<?= Url::to(['/search/index']) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/Search.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5>جستجو در <?= $cityName ?></h5>
        <p>در شهر به دنبال اصناف، مناطق گردشگری، ادارات و سازمان ها و اطلاعات ضروری بگردید</p>
    </div>
</a>
<a class="panel row" href="<?= Url::to(['/business/index']) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/Asnaf.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5>بانک اصناف <?= $cityName ?></h5>
        <p>لیست کامل کسب و کار های موجود در شهر من
            <br>به راحتی واحد های تجاری شهر را پیدا کنید</p>
    </div>
</a>
<a class="panel row" href="<?= Url::to(['/news/index']) ?>">
    <div class="col s3 icon">
        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/images/icons/News.png" class="img icon-img">
    </div>
    <div class="col s9 post">
        <h5>اخبار <?= $cityName ?></h5>
        <p>مشاهده اخبار و رویداد های شهر <?= $cityName ?></p>
    </div>
</a>
<!-- end features -->