<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\MobileAssets;
use yii\helpers\Html;
use yii\helpers\Url;

MobileAssets::register($this);

$appCache = \aminkt\components\appcache\AppCacheFilter::getManifestFileUrl($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html <?= $appCache ? "manifest=\"$appCache\"" : '' ?> lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1  maximum-scale=1 user-scalable=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="HandheldFriendly" content="True">
    <link rel="icon" href="<?= Yii::$app->getHomeUrl() ?>../assets/images/favicon.ico" type="image/x-icon">
    <meta content="<?= isset($this->params['description']) ? $this->params['description'] : 'شهر من، پرتال شهر های استان گیلان' ?>"
          name="description"/>
    <meta content="Amin Keshavarz" name="author"/>
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/favicon/favicon-16x16.png">
    <link rel="shortcut icon" type="image/png"
          href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/favicon/favicon.ico">
    <meta name="theme-color" content="#ff6d00">
    <link rel="canonical" href="<?= Url::canonical() ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- loading image -->
<div id="loading">
    <img src="/assets/app/img/loading.gif">
</div>
<!-- end loading image -->
<div class="page">

    <!-- navbar top -->
    <div class="navbar-top navbar-fixed">
        <?php /*
        <div class="side-nav-panel-right">
            <a href="#" data-activates="slide-out-right" class="side-nav-left"><i class="fa fa-user"></i></a>
        </div>
        */ ?>
        <!-- site brand	 -->
        <div class="site-brand">
            <a href="<?= Url::to(['site/index']) ?>"><img class="img"
                                                          src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/logo.png"></a>
        </div>
        <?php /*
        <!-- end site brand	 -->
        <div class="side-nav-panel-left">
            <a href="<?= Url::to(['search/index']) ?>"><i class="fa fa-search"></i></a>
        </div>
        */ ?>
    </div>
    <!-- end navbar top -->

    <!-- side nav right-->
    <div class="side-nav-panel-right">
        <ul id="slide-out-right" class="side-nav side-nav-panel collapsible">
            <li class="profil">
                <?php if (Yii::$app->getUser()->isGuest) : ?>
                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../upload/gust_user.png" alt="">
                    <h2>کاربر مهمان</h2>
                <?php else : ?>
                    <script src="https://www.avatarapi.com/js.aspx?email=<?= Yii::$app->getUser()->getIdentity()->email ?>&size=80"></script>
                    <h2><?= Yii::$app->getUser()->getIdentity()->fullName ?></h2>
                <?php endif; ?>
            </li>
            <li><a href="<?= Url::to(['/site/index']) ?>"><i class="fa fa-home"></i>صفحه اصلی</a></li>

            <li><a href="<?= Url::to(['/business/plans']) ?>"><i class="fa fa-cart-plus"></i>ثبت صنف</a></li>
            <?php if (!Yii::$app->getUser()->isGuest) : ?>
                <li><a href="<?= Url::to(['/business/my']) ?>"><i class="fa fa-shopping-bag"></i>اصناف من</a></li>

                <li><a href="<?= Url::to(['/site/profile']) ?>"><i class="fa fa-user"></i>ویرایش پروفایل</a></li>
            <?php endif; ?>

            <li><a href="<?= Url::to(['/site/contact']) ?>"><i class="fa fa-envelope-o"></i>تماس با ما</a></li>

            <?php if (Yii::$app->getUser()->isGuest) : ?>
                <li><a href="<?= Url::to(['/site/login']) ?>"><i class="fa fa-sign-in"></i>ورود یا ثبت نام</a></li>
            <?php else : ?>
                <li><a href="<?= Url::to(['/site/logout']) ?>"><i class="fa fa-sign-out"></i>خروج</a></li>
            <?php endif; ?>
        </ul>
    </div>
    <!-- end side nav right-->

    <?php /*
    <!-- navbar bottom -->
    <div class="navbar-bottom">
        <div class="row">
            <div class="col s4">
                <a href="<?= Url::to(['/site/index']) ?>"><i class="fa fa-home"></i></a>
            </div>
            <div class="col s4">
                <a href="#animatedModal2" id="nav-menu"><i class="fa fa-bars"></i></a>
            </div>
            <div class="col s4">
                <a href="<?= Url::to(['site/contact']) ?>"><i class="fa fa-envelope-o"></i></a>
            </div>
        </div>
    </div>
    <!-- end navbar bottom -->

    <!-- menu -->
    <div class="menus" id="animatedModal2">
        <div class="close-animatedModal2 close-icon">
            <i class="fa fa-close"></i>
        </div>
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col s4">
                        <a href="<?= Url::to(['/site/index']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-home"></i>
                                </div>
                                صفحه اصلی
                            </div>
                        </a>
                    </div>
                    <div class="col s4">
                        <a href="<?= Url::to(['/business/index']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-shopping-bag"></i>
                                </div>
                                لیست اصناف
                            </div>
                        </a>
                    </div>
                    <div class="col s4">
                        <a href="<?= Url::to(['/business/plans']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-cart-plus"></i>
                                </div>
                                ثبت صنف شما
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        <a href="<?= Url::to(['/page/view', 'id' => 2]) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-info"></i>
                                </div>
                                درباره شهر
                            </div>
                        </a>
                    </div>
                    <div class="col s4">
                        <a href="<?= Url::to(['/news/index']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-newspaper-o"></i>
                                </div>
                                اخبار شهر
                            </div>
                        </a>
                    </div>
                    <div class="col s4">
                        <a href="<?= Url::to(['/page/index']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                لیست نوشته ها
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col s4">
                        <a href="<?= Url::to(['/site/contact']) ?>" class="button-link">
                            <div class="menu-link">
                                <div class="icon">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                                گزارش خطا
                            </div>
                        </a>
                    </div>
                    <?php if(Yii::$app->getUser()->isGuest) : ?>
                        <div class="col s4">
                            <a href="<?= Url::to(['/site/signin']) ?>" class="button-link">
                                <div class="menu-link">
                                    <div class="icon">
                                        <i class="fa fa-user-plus"></i>
                                    </div>
                                    ثبت نام
                                </div>
                            </a>
                        </div>

                        <div class="col s4">
                            <a href="<?= Url::to(['/site/login']) ?>" class="button-link">
                                <div class="menu-link">
                                    <div class="icon">
                                        <i class="fa fa-sign-in"></i>
                                    </div>
                                    ورود
                                </div>
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="col s4">
                            <a href="<?= Url::to(['/business/my']) ?>" class="button-link">
                                <div class="menu-link">
                                    <div class="icon">
                                        <i class="fa fa-shopping-basket"></i>
                                    </div>
                                   لیست اصناف من
                                </div>
                            </a>
                        </div>

                        <div class="col s4">
                            <a href="<?= Url::to(['/site/logout']) ?>" class="button-link">
                                <div class="menu-link">
                                    <div class="icon">
                                        <i class="fa fa-sign-out"></i>
                                    </div>
                                    خروج
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end menu -->
    */ ?>

    <?= \aminkt\widgets\alert\Alert::widget(); ?>
    <!-- main content -->
    <div id="app-content">
        <?= $content ?>
    </div>
    <!-- end main content -->


    <!-- footer -->
    <div class="footer" style="margin-bottom: 0;">
        <div class="container">
            <div class="about-us-foot">
                <a href="<?= Url::to(['/site/index']) ?>">
                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/../logos/footerlogo.png" style="width: 30%;"
                         class="img">
                </a>
            </div>
            <div class="social-media">
                <a href="https://telegram.me/shahremancity" target="_blank"><i class="fa fa-paper-plane fa-2x"></i></a>
                <a href="https://www.instagram.com/shahremancity/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
            </div>
            <!--
            <div class="copyright">
                <span>© 2017 تمام حقوق محفوظ میباشد</span>
            </div>
            -->
        </div>
    </div>
    <!-- end footer -->

</div>
<script>
    // $('body').on('click', 'a', function (e) {
    //     e.preventDefault(); // stop the browser from following the link
    //     const regex = /^(http|https|\/).+$/g;
    //     let url = $(this).attr('href');
    //     let loading = $("#loading");
    //     if (url && url != '#' && url != '' && (regex.exec(url)) !== null) {
    //         loading.css("display", "flex");
    //         $('body > .page').fadeOut('fast', function () {
    //             $('div#app-content').load(url, function () {
    //                 loading.css("display", "none");
    //                 $('body > .page').fadeIn('fast');
    //             }); // load the html response into a DOM element=
    //         });
    //
    //     }
    // })
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
