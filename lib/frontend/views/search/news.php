<?php
/** @var $dataProvider \yii\data\ActiveDataProvider */
$this->title = "جستجو در شهر";
?>

<div class="container">
    <div class="title">
        <h3>نتایج جستجو در اخبار</h3>
    </div>

    <div class="news">
        <?=
        yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'itemView' => '_news_item'
        ]);
        ?>
    </div>
</div>
