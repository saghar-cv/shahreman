<?php
/** @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = "جستجو در شهر";
?>

<div class="container">
    <div class="title">
        <h3>نتایج جستجو در پست ها</h3>
    </div>

    <div class="posts">
        <?=
        yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'itemView' => '_post_item'
        ]);
        ?>
    </div>
</div>

