<?php
/** @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = "جستجو در شهر";
?>

<div class="container">
    <div class="title">
        <h3>نتایج جستجو در اصناف</h3>
    </div>
    <div class="businesses">
        <?=
        yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'summary' => '',
            'itemView' => '/business/_business_item'
        ]);
        ?>
    </div>
</div>
