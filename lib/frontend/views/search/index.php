<?php
/* @var $this yii\web\View */

/** @var $searchModel \frontend\models\search\Search */

/** @var $dataProviders \yii\data\ActiveDataProvider[] */

$this->title = "جستجو در شهر";
?>


<div class="container">

    <!-- Search box -->
    <?php $form = yii\widgets\ActiveForm::begin([
        'method' => 'get',
    ]);
    ?>
    <?= $form->field($searchModel, 'search')->textInput(['placeholder' => 'جستجو', 'class' => 'search-input'])->label(false) ?>
    <?= \yii\helpers\Html::submitButton("<i class='fa fa-search'></i>", [
        'class' => 'btn btn-search',

    ]) ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
    <!-- end search box -->

    <br>
    <?php if ($dataProviders['business']->count != 0): ?>
        <br>
        <div class="title">
            <h3>نتایج جستجو در اصناف</h3>
        </div>
        <br>
        <div class="businesses">
            <?php for ($i = 0; $i < 5; $i++): ?>
                <?php if (isset($dataProviders['business']->models[$i])): ?>
                    <div class="row">
                        <div class="col s12">
                            <a href="<?= yii\helpers\Url::to(['/business/view', 'id' => $dataProviders['business']->models[$i]->id]) ?>">
                                <div class="blog-content business-summary">
                                    <?php if ($imgUrl = $dataProviders['business']->models[$i]->getImage(false)) : ?>
                                        <img src="<?= $imgUrl ?>"
                                             alt="">
                                    <?php endif; ?>
                                    <div class="details">
                                        <?php if ($dataProviders['business']->models[$i]->name): ?>
                                            <h5> <?= $dataProviders['business']->models[$i]->name ?> </h5>
                                        <?php endif; ?>
                                        <br>
                                        <?php if ($dataProviders['business']->models[$i]->owner): ?>
                                            <div class="owner">
                                                <span><i class="fa fa-user"></i> &nbsp;<?= $dataProviders['business']->models[$i]->owner->fullName ?> </span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($dataProviders['business']->models[$i]->getMainPhone()): ?>
                                            <div class="phone">
                                                <span><i class="fa fa-phone"></i> &nbsp;<?= $dataProviders['business']->models[$i]->getMainPhone() ?> </span>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($dataProviders['business']->models[$i]->getAddress()): ?>
                                            <div class="address">
                                                <span><i class="fa fa-map-signs"></i> &nbsp;<?= $dataProviders['business']->models[$i]->getAddress()->address ?> </span>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
        <?php if ($dataProviders['business']->count > 5): ?>

            <?php if (\Yii::$app->getRequest()->get("Search")): ?>
                <?= yii\helpers\Html::a('بیشتر', ['/search/businesses',
                    "Search[search]" => \Yii::$app->getRequest()->get("Search")['search']],
                    ['class' => 'btn btn-primary']) ?>
            <?php else: ?>
                <?= yii\helpers\Html::a('بیشتر', ['/search/businesses'],
                    ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>


    <?php if ($dataProviders['news']->count != 0): ?>
        <br>
        <div class="title">
            <h3>نتایج جستجو در اخبار</h3>
        </div>
        <br>
        <div class="news">
            <?php for ($i = 0; $i < 5; $i++): ?>
                <?php if (isset($dataProviders['news']->models[$i])): ?>
                    <div class="row">
                        <div class="col s12">
                            <a href="<?= \yii\helpers\Url::to(['/news/view', 'id' => $dataProviders['news']->models[$i]->id]) ?>">
                                <div class="blog-content news-summary">
                                    <?php if ($dataProviders['news']->models[$i]->coverId) : ?>
                                        <img src="<?= Yii::$app->getModule('uploadManager')->image($dataProviders['news']->models[$i]->coverId, 'thumb') ?>"
                                             alt="">
                                    <?php endif; ?>
                                    <div class="blog-detailt">
                                        <h5> <?= $dataProviders['news']->models[$i]->title ?> </h5>
                                        <div class="date">
                                            <span><i class="fa fa-calendar"></i> &nbsp;&nbsp;<?= $dataProviders['news']->models[$i]->getJalaliCreateTime() ?> </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
        <?php if ($dataProviders['news']->count > 5): ?>
            <?php if (\Yii::$app->getRequest()->get("Search")): ?>
                <?= yii\helpers\Html::a('بیشتر', ['/search/news',
                    "Search[search]" => \Yii::$app->getRequest()->get("Search")['search']],
                    ['class' => 'btn btn-primary']) ?>
            <?php else: ?>
                <?= yii\helpers\Html::a('بیشتر', ['/search/news'],
                    ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>


    <?php if ($dataProviders['post']->count != 0): ?>
        <br>
        <div class="title">
            <h3>نتایج جستجو در سایر موراد</h3>
        </div>
        <br>
        <div class="posts">
            <?php for ($i = 0; $i < 5; $i++): ?>
                <?php if (isset($dataProviders['post']->models[$i])): ?>
                    <div class="row">
                        <div class="col s12">
                            <a href="<?= \yii\helpers\Url::to(['/news/view', 'id' => $dataProviders['post']->models[$i]->id]) ?>">
                                <div class="blog-content news-summary">
                                    <?php if ($dataProviders['post']->models[$i]->coverId) : ?>
                                        <div class="img-holder">
                                            <img src="<?= Yii::$app->getModule('uploadManager')->image($dataProviders['post']->models[$i]->coverId, 'normal') ?>"
                                                 alt="">
                                        </div>
                                    <?php endif; ?>
                                    <div class="blog-detailt">
                                        <h5><?= $dataProviders['post']->models[$i]->title ?></h5>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
            <?php if ($dataProviders['post']->count > 5): ?>
                <?php if (\Yii::$app->getRequest()->get("Search")): ?>
                    <?= yii\helpers\Html::a('بیشتر', ['/search/posts',
                        "Search[search]" => \Yii::$app->getRequest()->get("Search")['search']],
                        ['class' => 'btn btn-primary']) ?>
                <?php else: ?>
                    <?= yii\helpers\Html::a('بیشتر', ['/search/posts'],
                        ['class' => 'btn btn-primary']) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>

