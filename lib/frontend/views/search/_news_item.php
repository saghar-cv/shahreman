<?php
/** @var $model \common\models\Post */ ?>

<div class="row">
    <div class="col s12">
        <a href="<?= \yii\helpers\Url::to(['/news/view', 'id' => $model->id]) ?>">
            <div class="blog-content news-summary">
                <?php if ($model->coverId) : ?>
                    <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId, 'thumb') ?>" alt="">
                <?php endif; ?>
                <div class="blog-detailt">
                    <h5> <?= $model->title ?> </h5>
                    <div class="date">
                        <span><i class="fa fa-calendar"></i> <?= $model->getJalaliCreateTime() ?> </span>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
