<?php
/* @var $this yii\web\View */
/* @var $model \common\models\Post */

$this->title = "جاذبه های گردشگری لاهیجان"
?>
<!-- single post -->
<div class="pages section">
    <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId) ?>"
         style="width: 100%;margin-top: -35px;margin-bottom: 20px;" alt="">
    <div class="container">
        <div class="blog-single">
            <div class="blog-single-content">
                <div class="blog-content" style="padding: 10px;">
                    <?= $model->content ?>
                </div>

            </div>
            <?php /*
            <div class="comment">
                <h5>1 Comments</h5>
                <div class="comment-details">
                    <div class="row">
                        <div class="col s3">
                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?>/assets/app/img/user-comment.jpg" alt="">
                        </div>
                        <div class="col s9">
                            <div class="comment-title">
                                <span><strong>John Doe</strong> | Juni 5, 2016 at 9:24 am | <a href="">Reply</a></span>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis accusantium corrupti asperiores et praesentium dolore.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comment-form">
                <div class="comment-head">
                    <h5>Post Comment in Below</h5>
                    <p>Lorem ipsum dolor sit amet consectetur*</p>
                </div>
                <div class="row">
                    <form class="col s12 form-details">
                        <div class="input-field">
                            <input type="text" required class="validate" placeholder="NAME">
                        </div>
                        <div class="input-field">
                            <input type="email" class="validate" placeholder="EMAIL" required>
                        </div>
                        <div class="input-field">
                            <input type="text" class="validate" placeholder="SUBJECT" required>
                        </div>
                        <div class="input-field">
                            <textarea name="textarea-message" id="textarea1" cols="30" rows="10" class="materialize-textarea" class="validate" placeholder="YOUR COMMENT"></textarea>
                        </div>
                        <div class="form-button">
                            <button class="button-default">Post Comments</button>
                        </div>
                    </form>
                </div>
            </div>
            */ ?>
        </div>
        <?php
        $children = $model->children;
        if ($children) :
            ?>
            <br><br>
            <div class="blog news">
                <?php foreach ($children as $child) : ?>
                    <div class="row">
                        <div class="col s12">
                            <a href="<?= \yii\helpers\Url::to(['/page/view', 'id' => $child->id]) ?>">
                                <div class="blog-content news-summary">
                                    <?php if ($child->coverId) : ?>
                                        <img src="<?= Yii::$app->getModule('uploadManager')->image($child->coverId) ?>"
                                             alt="">
                                    <?php endif; ?>
                                    <div class="blog-detailt">
                                        <h5><?= \yii\helpers\Html::encode($child->title) ?></h5>
                                        <p>
                                            <?= \yii\helpers\StringHelper::truncate($child->content, 100); ?>
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<!-- end single post -->