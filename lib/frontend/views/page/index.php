<?php
/* @var $this yii\web\View */

/* @var \yii\data\ActiveDataProvider $dataProvider */

use yii\widgets\ListView;

$this->title = "بلاگ"
?>
<!-- blog -->
<div class="pages section">
    <div class="container">
        <div class="blog">
            <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'summary' => '',
            ]);
            ?>
        </div>
    </div>
</div>
    <!-- end blog -->