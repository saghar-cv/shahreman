<?php
/**
 * @var \common\models\Post $model
 */
?>

<div class="row">
    <div class="col s12">
        <a href="<?= \yii\helpers\Url::to(['page/view', 'id' => $model->id]) ?>">
            <div class="blog-content">
                <img src="<?= Yii::$app->getModule('uploadManager')->image($model->coverId, 'thumb') ?>"
                     alt="<?= \yii\helpers\Html::encode($model->title) ?>">
                <div class="blog-detailt">
                    <h5><?= \yii\helpers\Html::encode($model->title) ?></h5>
                    <?php /*<div class="date">
                        <span><i class="fa fa-calendar"></i><?= $model->getJalaliCreateTime() ?></span>
                    </div> */ ?>
                    <p>
                        <?php \yii\helpers\StringHelper::truncateWords($model->content, 100) ?>
                    </p>
                </div>
            </div>
        </a>
    </div>
</div>