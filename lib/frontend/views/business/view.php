<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $editable boolean */
/* @var $business \common\models\Business */

$this->title = "جزئیات صنف - " . $business->name;
$url = \yii\helpers\Url::toRoute(['/business/update-info', 'businessId' => $business->id]);
$js = <<<JS
$(".media-btn").on('click', function(e) {
  e.preventDefault();
  let enable = $(this).data('media-set-active');
  let active = $(".media-container").find('.active');
  active.removeClass('active');
  $("#"+enable).addClass('active');
  
  $(".business_links").find(".active").removeClass('active');
  $(this).addClass('active');
});

$("#upload-main-image-input").on('change', function() {
    let val = $(this).val();
    $.post("$url",
    {
        name: "MainImage",
        value: val
    },
    function(data, status){
        if(status!="success"){
            alert(data);
        }
    });
});

$("#upload-video-input").on('change', function() {
    let val = $(this).val();
    $.post("$url",
    {
        name: "Video",
        value: val
    },
    function(data, status){
        if(status!="success"){
            alert(data);
        }
    });
});

$("#btn-map").on('click', function(e) {
  e.preventDefault();
  let lat = $("#us2-lat").val();
  let lon = $("#us2-lon").val();
  $.post("$url",
    {
        name: "Map",
        value: lat + '-' + lon
    },
    function(data, status){
        if(status!="success"){
            alert(data);
        }
    });
})

JS;

$this->registerJs($js);


$GuilanState = \saghar\address\models\State::findOne(['name' => 'گیلان']);
$cities = \saghar\address\models\City::findAll(['stateId' => $GuilanState->id]);
$cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name');
$addressRecord = \saghar\address\models\Address::findOne($business->getMeta(\common\models\Business::BUSINESS_META_ADDRESS));
if (!$addressRecord) {
    $city = null;
    $address = null;
} else {
    $city = $addressRecord->cityName;
    $address = $addressRecord->address;
}

?>

<?php if ($business->ownerId == Yii::$app->getUser()->getId() and $business->status == $business::STATUS_NOT_PAYED) : ?>
    <div class="alert upgrade">
        <h6>صنف شما هنوز منتشر نشده است.</h6>
        <p>
            شما هنوز مبلغ پلن انتخابی خود را پرداخت نکرده اید لذا صنف ثبت شده شما در لیست اصناف نشان داده نخواهد شد.<br>
            برای پرداخت هزینه پلن و فعال سازی پلن لطفا دکمه زیر را لمس کنید.
        </p>
        <?php
        $price = number_format($business->plan->price);
        $url = ['/gate/pay', 'business' => $business->id];
        if (isset(Yii::$app->params['city']) and $cityUrl = Yii::$app->params['city']) {
            $url['city'] = strtolower($cityUrl->latinName);
        }
        echo Html::a("پرداخت {$price} تومان بابت انتشار کسب و کار شما ", $url, [
            'class' => 'btn btn-orange',
            'style' => 'display:block; margin:10px auto;',
        ]) ?>
    </div>
<?php endif; ?>
<!-- single post -->
<div class="pages section business">
    <div class="media-container">
        <div id="media-img" class="media active">
            <?php $mainImage = \common\models\BusinessMeta::findOne([
                'businessId' => $business->id,
                'key' => $business::BUSINESS_META_MAIN_IMAGE
            ]);
            if (!$mainImage) {
                $mainImage = new \common\models\BusinessMeta();
            }
            ?>
            <?php
            if ($editable):
                echo \aminkt\uploadManager\components\UploadManager::widget([
                    'id' => 'upload-main-image',
                    'model' => $mainImage,
                    'attribute' => 'value',
                    'titleTxt' => 'تصویر را وارد کنید.',
                    'helpBlockEnable' => false,
                    'showImageContainer' => '#main-image-container',
                    'sizeOfImageInImageContainer' => null,
                    'showImagesTemplate' => "<img src='{url}' style='z-index:-100;' class='img'>",
                    'btnTxt' => '<i class="fa fa-edit"></i>'
                ]);
                ?>
                <div id="main-image-container"></div>
            <?php else: ?>
                <div id="main-image-container"><img src='<?= $business->getImage() ?>'
                                                    style='z-index:-100;' class='img'></div>
            <?php endif; ?>
        </div>

        <div id="media-video" class="media">
            <?php $video = \common\models\BusinessMeta::findOne([
                'businessId' => $business->id,
                'key' => $business::BUSINESS_META_VIDEO
            ]);
            if (!$video) {
                $video = new \common\models\BusinessMeta();
            }
            ?>
            <?php
            if ($editable):
                echo \aminkt\uploadManager\components\UploadManager::widget([
                    'id' => 'upload-video',
                    'model' => $video,
                    'attribute' => 'value',
                    'titleTxt' => 'ویدئو را وارد کنید.',
                    'helpBlockEnable' => false,
                    'showImageContainer' => '#main-video-container',
                    'sizeOfImageInImageContainer' => null,
                    'showImagesTemplate' => "<video width='100%' height='300px' controls><source src='{url}' type='video/{file_extension}'>سیستم شما از ویدیو پشتیبانی نمیکند. </video>",
                    'btnTxt' => '<i class="fa fa-edit"></i>'
                ]);
                ?>
                <div id="main-video-container"></div>
            <?php else:
                $url = $business->getVideo();
                if ($url['url']) :
                    ?>
                    <div id="main-video-container">
                        <video width='100%' height='300px' controls>
                            <source src='<?= $url['url'] ?>' type='video/<?= $url['extension'] ?>'>
                            سیستم شما از ویدیو پشتیبانی نمیکند.
                        </video>
                    </div>
                <?php else : ?>
                    <div id="main-video-container">
                        <img src='<?= $url['cover'] ?>'
                             style='transform: translateY(-30%);z-index:-100;' class='img'>

                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>


        <?php if ($addressRecord): ?>
            <div id="media-map" class="media">
                <?php if (!$editable): ?>
                    <?php if ($addressRecord->latitude and $addressRecord->longitude): ?>
                        <?= \pigolab\locationpicker\LocationPickerWidget::widget([
                            'key' => 'AIzaSyDfE7neBu1PRVxGaU39_7sn7dY0nf1y9U4',
                            'options' => [
                                'style' => 'width: 100%; height: 400px', // map canvas width and height
                            ],
                            'clientOptions' => [
                                'location' => [
                                    'latitude' => $addressRecord->latitude,
                                    'longitude' => $addressRecord->longitude,
                                ],
                                'addressFormat' => 'street_number',
                                'inputBinding' => [
                                    'latitudeInput' => new JsExpression("$('#us2-lat')"),
                                    'longitudeInput' => new JsExpression("$('#us2-lon')"),
                                ],
                                'markerDraggable' => false,
                            ]
                        ]);
                        ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?= Html::hiddenInput('latitude', '', ['id' => 'us2-lat']) ?>
                    <?= Html::hiddenInput('longitude', '', ['id' => 'us2-lon']) ?>
                    <?= \yii\helpers\Html::submitButton("ذخیره", [
                        'class' => 'btn btn-primary', 'id' => 'btn-map'
                    ]) ?>
                    <?= \pigolab\locationpicker\LocationPickerWidget::widget([
                        'key' => 'AIzaSyDfE7neBu1PRVxGaU39_7sn7dY0nf1y9U4',
                        'options' => [
                            'style' => 'width: 100%; height: 400px', // map canvas width and height
                        ],
                        'clientOptions' => [
                            'location' => [
                                'latitude' => isset($addressRecord->latitude) ? $addressRecord->latitude : 37.28083300,
                                'longitude' => isset($addressRecord->longitude) ? $addressRecord->longitude : 49.58305600,
                            ],
                            'addressFormat' => 'street_number',
                            'inputBinding' => [
                                'latitudeInput' => new JsExpression("$('#us2-lat')"),
                                'longitudeInput' => new JsExpression("$('#us2-lon')"),
                            ],
                        ]
                    ]);
                    ?>
                <?php endif; ?>
            </div>

        <?php endif; ?>
    </div>

    <div class="container">
        <div class="business_links row">
            <div class="col s4">
                <a href="#" class="btn btn-business media-btn" data-media-set-active="media-video"> ویدئو </a>
            </div>
            <div class="col s4">
                <a href="#" class="btn btn-business media-btn active" data-media-set-active="media-img"> تصویر </a>
            </div>
            <div class="col s4">
                <a href="#" class="btn btn-business media-btn" data-media-set-active="media-map"> نقشه </a>
            </div>
        </div>
        <div class="blog-single">
            <div class="blog-single-content" style="margin-top: 35px;">
                <div class="row">
                    <div class="col s12">
                        <div class="business-info">
                            <?=
                            $editable ? \dosamigos\editable\Editable::widget([
                                'name' => 'name',
                                'mode' => 'pop',
                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                'type' => 'text',
                                'value' => \yii\helpers\Html::encode($business->name),
                                'options' => [
                                    'class' => 'h5'
                                ],
                                'clientOptions' => [
                                    'emptytext' => 'نام واحد تجاری',
                                    'showbuttons' => 'bottom'
                                ]
                            ]) : \yii\helpers\Html::tag('h5', \yii\helpers\Html::encode($business->name)); ?>
                            <div class="owner">
                                <span><i class="fa fa-user"></i> <?= $business->owner->fullName ?> </span>
                            </div>
                            <div class="address">
                                <span><i class="fa fa-map-signs"></i>
                                    <?=
                                    $addressRecord ? \yii\helpers\Html::encode($city) . ' - ' . \yii\helpers\Html::encode($address) : "آدرس ثبت نشده است." ?> </span>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($business->getMeta($business::BUSINESS_META_PHONE . '-1')): ?>
                    <div class="row">
                        <a style="float: left;font-weight: bolder" href="tel:"<?= $business->getMeta(
                            $business::BUSINESS_META_PHONE . '-1') ?> class="btn btn-orange">
                            <?= $business->getMeta($business::BUSINESS_META_PHONE . '-1') ?>
                            <i class="fa fa-phone"></i> </a>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="blog-content" style="text-align: justify; padding: 10px; color: #000000">
                        <?php
                        echo $editable ? \dosamigos\editable\Editable::widget([
                            'name' => 'description',
                            'mode' => 'pop',
                            'url' => ['/business/update-info', 'businessId' => $business->id],
                            'type' => 'textarea',
                            'value' => \yii\helpers\Html::encode($business->description),
                            'clientOptions' => [
                                'emptytext' => 'توضیحات واحد تجاری',
                                'showbuttons' => 'bottom'
                            ]
                        ]) : nl2br(\yii\helpers\Html::encode($business->description)); ?>
                    </div>

                </div>

                <div class="row">
                    <div class="panel info business-details">
                        <?php if ($addressRecord or $editable): ?>
                            <div class="row">
                                <div class="col s12">
                                    <i class="fa fa-map-signs"></i>
                                    <?=
                                    $editable ? \dosamigos\editable\Editable::widget([
                                            'name' => $business::BUSINESS_META_ADDRESS . '-city',
                                            'mode' => 'pop',
                                            'url' => ['/business/update-info', 'businessId' => $business->id],
                                            'type' => 'select',
                                            'value' => \yii\helpers\Html::encode($city),
                                            'clientOptions' => [
                                                'emptytext' => 'شهر',
                                                'source' => $cities,
                                                'showbuttons' => 'bottom'
                                            ]
                                        ]) . ' - ' .
                                        \dosamigos\editable\Editable::widget([
                                            'name' => $business::BUSINESS_META_ADDRESS . '-address',
                                            'mode' => 'pop',
                                            'url' => ['/business/update-info', 'businessId' => $business->id],
                                            'type' => 'text',
                                            'value' => \yii\helpers\Html::encode($address),
                                            'clientOptions' => [
                                                'emptytext' => 'آدرس',
                                                'showbuttons' => 'bottom'
                                            ]
                                        ]) : \yii\helpers\Html::encode($city) . ' - ' . \yii\helpers\Html::encode($address) ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php $plan = $business->plan; ?>
                        <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_PHONE) ?>
                        <div class="row">
                            <?php for ($i = 1; $i <= $access->value; $i++): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_PHONE . '-' . $i))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href="tel:"<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_PHONE . '-' . $i)) ?> >
                                            <i class="fa fa-phone"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_PHONE . '-' . $i,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_PHONE . '-' . $i)),
                                                'clientOptions' => [
                                                    'emptytext' => 'تلفن',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_PHONE . '-' . $i)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_MOBILE) ?>
                            <?php for ($i = 1; $i <= $access->value; $i++): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_MOBILE . '-' . $i))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a
                                           href="tel:"<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_MOBILE . '-' . $i)) ?> >
                                            <i class="fa fa-mobile"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_MOBILE . '-' . $i,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_MOBILE . '-' . $i)),
                                                'clientOptions' => [
                                                    'emptytext' => 'تلفن همراه',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_MOBILE . '-' . $i)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endfor; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_SOCIAL_TELEGRAM) ?>
                            <?php if ($access->value == 1): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_SOCIAL_TELEGRAM))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href=<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_TELEGRAM)) ?>>
                                            <i class="fa fa-paper-plane"></i>
                                            <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_SOCIAL_TELEGRAM,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_TELEGRAM)),
                                                'clientOptions' => [
                                                    'emptytext' => 'تلگرام',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_TELEGRAM)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_SOCIAL_INSTALGRAM) ?>
                            <?php if ($access->value == 1): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_SOCIAL_INSTALGRAM))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href=<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_INSTALGRAM)) ?>>
                                            <i class="fa fa-instagram"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_SOCIAL_INSTALGRAM,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_INSTALGRAM)),
                                                'clientOptions' => [
                                                    'emptytext' => 'اینستاگرام',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_SOCIAL_INSTALGRAM)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_WEBSITE) ?>
                            <?php if ($access->value == 1): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_WEBSITE))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href=<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_WEBSITE)) ?>>
                                            <i class="fa fa-link"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_WEBSITE,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_WEBSITE)),
                                                'clientOptions' => [
                                                    'emptytext' => 'سایت',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_WEBSITE)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_EMAIL) ?>
                            <?php if ($access->value == 1): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_EMAIL))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href=<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_EMAIL)) ?>>
                                            <i class="fa fa-envelope-o"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_EMAIL,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_EMAIL)),
                                                'clientOptions' => [
                                                    'emptytext' => 'ایمیل',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_EMAIL)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <?php $access = $plan->getAccessByName(\common\models\Business::BUSINESS_META_FAX) ?>
                            <?php for ($i = 1; $i <= $access->value; $i++): ?>
                                <?php if ((!$editable and $business->getMeta($business::BUSINESS_META_FAX . '-' . $i))
                                    or $editable): ?>
                                    <div class="col s6">
                                        <a href="tel:"<?= \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_FAX . '-' . $i)) ?>>
                                            <i class="fa fa-fax"></i> <?=
                                            $editable ? \dosamigos\editable\Editable::widget([
                                                'name' => $business::BUSINESS_META_FAX . '-' . $i,
                                                'mode' => 'pop',
                                                'url' => ['/business/update-info', 'businessId' => $business->id],
                                                'type' => 'text',
                                                'value' => \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_FAX . '-' . $i)),
                                                'clientOptions' => [
                                                    'emptytext' => 'فکس',
                                                    'showbuttons' => 'bottom'
                                                ]
                                            ]) : \yii\helpers\Html::encode($business->getMeta($business::BUSINESS_META_FAX . '-' . $i)); ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end single post -->