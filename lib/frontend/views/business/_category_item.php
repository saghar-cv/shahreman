<?php
/** @var $model \saghar\category\models\Category */
?>


<?php if ($model->depth == 2): ?>
    <li>
        <a href="<?= yii\helpers\Url::to(['/business/list', 'catId' => $model->id]) ?>"><i
                    class="fa fa-bars"></i> <?= $model->name ?>
            <!--                            <span class="badge">12</span>-->
        </a>
    </li>
<?php else: ?>
    <li><a href="<?= yii\helpers\Url::to(['/business/index', 'depth' => $model->depth + 1,
            'parentId' => $model->id]) ?>"><i class="fa fa-bars"></i> <?= $model->name ?>
        </a>
    </li>
<?php endif; ?>
