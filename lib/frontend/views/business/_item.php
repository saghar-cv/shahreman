<?php
/**
 * @var \common\models\Business $model
 */

use yii\helpers\Url;

$address = $model->getAddress();
$address = $address ? "{$address->cityName} - {$address->address}" : "مشخص نشده";

if ($model->status == $model::STATUS_NOT_PAYED) {
    $style = "style='background:#e6e0b4;border:1px solid #ccc;'";
} elseif ($model->status == $model::STATUS_REJECTED or $model->status == $model::STATUS_EXPIRED) {
    $style = "style='background:#e7bcbc;border:1px solid #ccc;'";
} else {
    $style = "";
}

?>

<a class="row panel" href="<?= Url::to(['/business/update', 'businessId' => $model->id]) ?>" <?= $style ?>>
    <div class="col s3 icon">
        <?= \yii\helpers\Html::img($model->getImage(), [
            'class' => 'img',
        ]); ?>
    </div>
    <div class="col s9 post">
        <h5><?= \yii\helpers\Html::encode($model->name) ?></h5>
        <br>
        <div class="status">
            <span><i class="fa fa-tag"></i> &nbsp; <?= \yii\helpers\Html::encode($model->getStatusLabel()) ?> </span>
        </div>
        <div class="owner">
            <span><i class="fa fa-user"></i> &nbsp; <?= \yii\helpers\Html::encode($model->owner->name) ?> </span>
        </div>
        <div class="phone">
            <span><i class="fa fa-phone"></i> &nbsp; <?= \yii\helpers\Html::encode($model->owner->mobile) ?> </span>
        </div>
        <div class="address">
            <span><i class="fa fa-map-signs"></i> &nbsp; <?= \yii\helpers\Html::encode($address) ?></span>
        </div>
    </div>
</a>