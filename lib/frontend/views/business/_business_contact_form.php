<?php
/** @var \yii\web\View $this */
/** @var \frontend\models\BusinessDataForm $model */
/** @var \common\models\Business $business */
/** @var array $formFields */

?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>

<?php foreach ($formFields['EditBusinessExtraData']['filds'] as $field) :
    $fieldKey = explode('-', $field['fildkey']);
    ?>
    <div class="form-group">
        <?php if (!isset($fieldKey[1]) or $fieldKey[1] == 1) : ?>
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
        <?php endif; ?>
        <input type="text" id="<?= $field['fildkey'] ?>" value="<?= $field['fildvalue'] ?>"
               class="form-control inline-editable" name="BusinessContactForm[<?= $field['fildkey'] ?>]">
    </div>

<?php endforeach; ?>

<?php \yii\widgets\ActiveForm::end(); ?>
