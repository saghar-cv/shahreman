<?php
/** @var \common\models\Business $model */
?>
<div class="row">
    <div class="col s12">
        <a href="<?= yii\helpers\Url::to(['/business/view', 'id' => $model->id]) ?>">
            <div class="blog-content business-summary">
                <?php if ($imgUrl = $model->getImage(false)) : ?>
                    <img src="<?= $imgUrl ?>"
                         alt="">
                <?php endif; ?>
                <div class="details">
                    <?php if ($model->name): ?>
                        <h5> <?= $model->name ?> </h5>
                    <?php endif; ?>
                    <br>
                    <?php if ($model->owner): ?>
                        <div class="owner">
                            <span><i class="fa fa-user"></i> &nbsp;<?= $model->owner->fullName ?> </span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->getMainPhone()): ?>
                        <div class="phone">
                            <span><i class="fa fa-phone"></i> &nbsp;<?= $model->getMainPhone() ?> </span>
                        </div>
                    <?php endif; ?>
                    <?php if ($model->getAddress()): ?>
                        <div class="address">
                            <span><i class="fa fa-map-signs"></i> &nbsp;<?= $model->getAddress()->address ?> </span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </a>
    </div>
</div>