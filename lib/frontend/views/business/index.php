<?php
/** @var $this yii\web\View */

/** @var $dataProvider \yii\data\ActiveDataProvider */

/** @var $searchModel \frontend\models\search\Search */

$this->title = "لیست دسته بندی ها";
?>

<div class="container">

    <!-- Search box -->
    <?php $form = yii\widgets\ActiveForm::begin([
        'method' => 'get',
    ]);
    ?>
    <?= $form->field($searchModel, 'search')->textInput(['placeholder' => 'جستجو', 'class' => 'search-input'])->label(false) ?>
    <?= \yii\helpers\Html::submitButton("<i class='fa fa-search'></i>", [
        'class' => 'btn btn-search',

    ]) ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
    <!-- end search box -->

    <div class="categories">
        <ul class="category-list">
            <?=
            yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'itemView' => '_category_item'
            ]);
            ?>
        </ul>
    </div>

</div>


