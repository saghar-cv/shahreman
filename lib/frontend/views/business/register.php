<?php
/** @var \yii\web\View $this */
/** @var \common\models\Business $model */
/** @var \saghar\category\models\Category[] $categories */
/** @var \backend\models\CategoryRelation $categoryModel */

$this->title = "ثبت صنف جدید"
?>
<div class="pages section">
    <div class="container">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'post'
        ]); ?>
        <?= $form->field($model, 'name')->textInput()->label("نام کسب و کار شما") ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 5])->label("توضیحات کسب و کار شما") ?>

        <?php $categories = \saghar\category\models\Category::find()
            ->where(['!=', 'status', \saghar\category\models\Category::STATUS_REMOVED])
            ->andWhere([
                'section' => \backend\models\CategoryRelation::SECTION_BUSINESS,
                'depth' => 0
            ])
            ->all();
        $categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'name');

        echo $form->field($categoryModel, 'catId', [
            'options' => [
                'class' => 'form-group select2-dropdownlist'
            ]
        ])->widget(\kartik\select2\Select2::className(), [
            'data' => $categories,
            'options' => ['placeholder' => 'دسته را انتخاب کنید ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label("انتخاب دسته بندی");
        ?>
        <br>
        <div class="text-center">
            <?= \yii\helpers\Html::submitButton("ذخیره", [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>
    </div>
</div>

