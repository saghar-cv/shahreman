<?php

use kartik\select2\Select2;

/** @var \yii\web\View $this */
/** @var \frontend\models\BusinessDataForm $model */
/** @var \common\models\Business $business */
/** @var array $formFields */
/** @var array $cities */


$url = Yii::$app->getUrlManager()->getHostInfo() . Yii::$app->getHomeUrl() . 'api/v1/cities.json';

$categories = \saghar\category\models\Category::find()
    ->where(['!=', 'status', \saghar\category\models\Category::STATUS_REMOVED])
    ->andWhere(['section' => 'business'])->all();
$categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'name');
?>

<style>
    .select-wrapper {
        padding: 0;
    }
</style>

<?php $form = \yii\widgets\ActiveForm::begin() ?>

<h3>مشخصات صنف</h3>

<?php foreach ($formFields['EditBusinessBasic']['filds'] as $field) : ?>

    <?php if ($field['fildtype'] == 'spinner') : ?>

        <?php if ($field['fildkey'] == 'Address-city') : ?>
            <div class="form-group">
                <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
                <?php
                $options = ['placeholder' => 'محل استقرار کسب و کار خود را مشخص کنید...', 'class' => 'form-control inline-editable', 'id' => $field['fildkey']];
                if ($field['fildrequire']) {
                    $options['data-required'] = true;
                }
                echo Select2::widget([
                    'data' => $cities,
                    'name' => $field['fildkey'],
                    'value' => $field['fildvalue'],
                    'options' => $options,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        <?php elseif ($field['fildkey'] == 'cities') : ?>

            <div class="form-group">
                <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
                <?php
                $options = ['placeholder' => 'محل انتشار کسب و کار خود را مشخص کنید...', 'class' => 'form-control inline-editable', 'id' => $field['fildkey'], 'multiple' => true];
                if ($field['fildrequire']) {
                    $options['data-required'] = true;
                }
                $values = [];
                foreach ($field['fildvalue'] as $item) {
                    $values[] = $item['cityId'];
                }
                echo Select2::widget([
                    'data' => $cities,
                    'name' => $field['fildkey'],
                    'value' => $values,
                    'options' => $options,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'tags' => true,
                    ],
                ]);
                ?>
            </div>
        <?php elseif ($field['fildkey'] == 'category') : ?>

            <div class="form-group">
                <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
                <?php
                $options = ['placeholder' => 'دسته کسب و کار خود را مشخص کنید...', 'class' => 'form-control inline-editable', 'id' => $field['fildkey']];
                if ($field['fildrequire']) {
                    $options['data-required'] = true;
                }
                echo Select2::widget([
                    'data' => $categories,
                    'name' => $field['fildkey'],
                    'value' => $field['fildvalue'],
                    'options' => $options,
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        <?php endif; ?>

    <?php elseif ($field['fildtype'] == 'textarea'): ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
            <textarea <?= $field['fildrequire'] ? 'data-required="true"' : '' ?>
                    id="<?= $field['fildkey'] ?>" rows="7" class="form-control inline-editable"
                    name="BusinessContactForm[<?= $field['fildkey'] ?>]"><?= $field['fildvalue'] ?></textarea>
        </div>
    <?php else: ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
            <input <?= $field['fildrequire'] ? 'data-required="true"' : '' ?> type="text" id="<?= $field['fildkey'] ?>"
                                                                              value="<?= $field['fildvalue'] ?>"
                                                                              class="form-control inline-editable"
                                                                              name="BusinessContactForm[<?= $field['fildkey'] ?>]">
        </div>
    <?php endif; ?>
    <br>

<?php endforeach; ?>

<?php \yii\widgets\ActiveForm::end(); ?>
