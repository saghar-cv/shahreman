<?php

use yii\bootstrap\Tabs;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \frontend\models\BusinessDataForm $businessDataModel */
/** @var array $cities */
/** @var array $contactData */

$this->title = "ویرایش صنف";


$url = \yii\helpers\Url::to(['/business/update-info', 'businessId' => $businessDataModel->getBusiness()->id]);
$js = <<<JS
var maxHeight = Math.max.apply(null, $(".business-update-tab a").map(function ()
{
    return $(this).height();
}).get());
console.log(maxHeight);

$(".business-update-tab a").css('height', maxHeight+30);

$(document).on('change', '.inline-editable', function(e) {
    e.preventDefault();
    let input = $(this);
    console.log(input);
    if(!input.attr('name')){
        return;
    }
    input.attr('disabled', true);
    
    if(input.val() == "" && input.data('required')){
        input.parent().removeClass('has-success');
        input.parent().addClass('has-error');
        input.parent().find(".help-block").remove();
        input.parent().append('<div class="help-block">مقدار این فیلد نمیتواند خالی باشد.</div>');
        input.attr('disabled', false);
        return;
    }else{
        $.ajax({
              type: "POST",
              url: "{$url}",
              data: {
                  name: input.attr('id'),
                  value: input.val()
              },
              success: function (data) {
                input.parent().removeClass('has-error');
                input.parent().addClass('has-success');
                input.parent().find(".help-block").remove();
                input.parent().append('<div class="help-block">اطلاعات ذخیره شد</div>');
                input.attr('disabled', false);
                console.log(data);
              },
              dataType: 'json'
            })
            .fail(function(data) {
                input.parent().removeClass('has-success');
                input.parent().addClass('has-error');
                input.parent().find(".help-block").remove();
                input.parent().append('<div class="help-block">'+data.responseText+'</div>');
                input.attr('disabled', false);
                console.log(data);
        });
    }
});
JS;

$this->registerJs($js);
?>
<style>
    .business-update-tab {
        width: 33%;
    }

    .business-update-tab a {
        margin: 0 auto;
        text-align: center;
        background: #ff6c01;
        border-radius: 0;
        color: #ffffff;
    }

    .business-update-tab a:hover, .business-update-tab.active a {
        background: #b05201 !important;
        color: #ffffff !important;
    }

    .business-update-tab a i {
        font-size: 25px;
        display: block;
        margin-bottom: 5px;
    }
</style>

<?php if ($businessDataModel->getBusiness()->ownerId == Yii::$app->getUser()->getId() and $businessDataModel->getBusiness()->status == \common\models\Business::STATUS_NOT_PAYED) : ?>
    <div class="alert upgrade">
        <h6>صنف شما هنوز منتشر نشده است.</h6>
        <p>
            شما هنوز مبلغ پلن انتخابی خود را پرداخت نکرده اید لذا صنف ثبت شده شما در لیست اصناف نشان داده نخواهد شد.<br>
            برای پرداخت هزینه پلن و فعال سازی پلن لطفا دکمه زیر را لمس کنید.
        </p>
        <?php
        $num = count($businessDataModel->getBusiness()->cities);
        $num = $num ? $num : 0;
        $price = number_format($businessDataModel->getBusiness()->plan->price * $num);
        $url = ['/gate/pay', 'business' => $businessDataModel->getBusiness()->id];
        if (isset(Yii::$app->params['city']) and $cityUrl = Yii::$app->params['city']) {
            $url['city'] = strtolower($cityUrl->latinName);
        }
        echo Html::a("پرداخت {$price} تومان بابت انتشار کسب و کار شما ", $url, [
            'class' => 'btn btn-orange',
            'style' => 'display:block; margin:10px auto;',
        ]) ?>
    </div>
<?php endif; ?>
<div class="pages section">
    <div class="container">
        <?php
        echo Tabs::widget([
            'items' => [
                [
                    'label' => '<i class="fa fa-globe"></i> مشخصات کسب و کار',
                    'content' => $this->render('_business_data_form', ['business' => $businessDataModel->getBusiness(), 'model' => $businessDataModel, 'formFields' => $contactData, 'cities' => $cities]),
                    'headerOptions' => ['class' => 'business-update-tab'],
                ],
                [
                    'label' => '<i class="fa fa-phone"></i> مشخصات تماس',
                    'content' => $this->render('_business_contact_form', ['business' => $businessDataModel->getBusiness(), 'model' => $businessDataModel, 'formFields' => $contactData]),
                    'headerOptions' => ['class' => 'business-update-tab'],
                ],
                [
                    'label' => '<i class="fa fa-video-camera"></i> چند رسانه ای',
                    'content' => $this->render('_business_media_form', ['business' => $businessDataModel->getBusiness(), 'model' => $businessDataModel, 'formFields' => $contactData]),
                    'headerOptions' => ['class' => 'business-update-tab'],
                ],
            ],
            'options' => ['tag' => 'div'],
            'itemOptions' => ['tag' => 'div'],
            'headerOptions' => ['class' => 'my-class'],
            'encodeLabels' => false,
            'clientOptions' => ['collapsible' => false],
        ]);
        ?>
    </div>
</div>

