<?php
/** @var $this yii\web\View */

/** @var $searchModel \frontend\models\search\Search */

/** @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = "لیست اصناف";
?>

<div class="container">

    <!-- Search box -->
    <?php $form = yii\widgets\ActiveForm::begin([
        'method' => 'get',
    ]);
    ?>
    <?= $form->field($searchModel, 'search')->textInput(['placeholder' => 'جستجو', 'class' => 'search-input'])->label(false) ?>
    <?= \yii\helpers\Html::submitButton("<i class='fa fa-search'></i>", [
        'class' => 'btn btn-search',

    ]) ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
    <!-- end search box -->

    <br>
    <div class="businesses">
        <?=
        yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_business_item',
            'summary' => '',
        ]);
        ?>
    </div>

</div>


