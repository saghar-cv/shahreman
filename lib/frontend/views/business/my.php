<?php
/** @var \yii\web\View $this */

/** @var \yii\data\ActiveDataProvider $dataProvider */

use yii\widgets\ListView;

$this->title = "اصناف من"
?>

<div class="pages section">
    <div class="container">
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'itemView' => '_item',
    ]);
    ?>
</div>
