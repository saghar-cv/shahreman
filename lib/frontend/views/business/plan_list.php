<?php
/** @var \yii\web\View $this */
/** @var \backend\models\Plan[] $plans */

$this->title = "لیست پلن ها";
?>

<style>
    table {
        margin: 0 auto;
        margin-bottom: 6px;
    }

    td, th {
        border: #efefef solid 1px;

        text-align: center;
    }

    .panel {
        color: black;
    }
</style>
<?php
echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' />";
?>
<div class="pages section">
    <div class="container">
        <?php foreach ($plans as $plan) : ?>
            <div class="panel text-center">
                <h3><?= $plan->name ?></h3>

                <h5>قیمت : <?= number_format($plan->price) ?> تومان</h5>
                <h5>اعتبار : <?= $plan->duration ?> ماه</h5>
                <br>
                <table class="table table-border">
                    <tr>
                        <th>ویژگی</th>
                        <th>وضعیت</th>
                    </tr>
                    <?php
                    $accessList = $plan->planAccesses;
                    $cmplist = ['تصویر شاخص', 'گالری تصاویر', 'ویدئو', 'نقشه', 'آدرس', 'آدرس وب سایت', 'آدرس ایمیل',
                        'لینک دانلود اپلیکیشن iOS', 'لینک دانلود اپلیکیشن اندروید', 'تعداد شماره موبایل', 'تعداد شماره تلفن', 'تعداد شماره فکس'];
                    $totalList = [];
                    $totalList[0]=['نام مدیر'];
                    for ($i = 0; $i < count($cmplist); $i++) {

                        for ($j = 0; $j < count($accessList); $j++) {
                            if ($cmplist[$i] == $accessList[$j]->getLabel()) {
                                $totalList[$i] = $accessList[$j];
                            }
                        }

                    }

                    ?>
                    <tr>
                        <td><span>نام مدیر</span></td>
                        <td><span class="fa fa-check" style="color: green"></td>
                    </tr>
                    <?php foreach ($totalList as $access): ?>


                        <tr>
                            <td><span> <?= $access->getLabel() ?> </span></td>
                            <?php if ($access->type == 'boolean') : ?>
                                <?php if ($access->value == 1): ?>
                                    <td><span class="fa fa-check" style="color: green"></td>
                                <?php elseif ($access->value == 0): ?>
                                    <td><span class="fa fa-close" style="color: red"></td>
                                <?php endif ?>

                            <?php elseif ($access->type == 'integer'): ?>
                                <td><span> <?= $access->value ? $access->value : 'صفر' ?></span></td>
                            <?php endif ?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <br>

                <div class="text-center">
                    <?= \yii\helpers\Html::a('ثبت اطلاعات صنف', ['/business/register', 'id' => $plan->id], ['class' => 'btn btn-orange']) ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
