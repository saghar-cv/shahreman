<?php

/** @var \yii\web\View $this */
/** @var \frontend\models\BusinessDataForm $model */
/** @var \common\models\Business $business */
/** @var array $formFields */

$siteUrl = Yii::$app->getUrlManager()->getBaseUrl();
?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>

<h3>مشخصات صنف</h3>

<?php foreach ($formFields['EditBusinessMedia']['filds'] as $field) : ?>
    <?php if ($field['fildtype'] == "Map") : ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
            <?php
            if (isset($field['fildvalue']['lat']) and $field['fildvalue']['lat'] and isset($field['fildvalue']['lon']) and $field['fildvalue']['lon']) {
                $val = "{$field['fildvalue']['lat']},{$field['fildvalue']['lon']}";
            } else {
                $val = null;
            }
            echo \aminkt\widgets\google\map\LocationInput::widget([
                'apiKey' => 'AIzaSyDfE7neBu1PRVxGaU39_7sn7dY0nf1y9U4',
                'name' => $field['fildkey'],
                'id' => $field['fildkey'],
                'value' => $val,
                'latLanDivider' => ',',
                'options' => [
                    'class' => 'form-control inline-editable',
                ],
                'mapOptions' => [
                    'center' => [
                        'lat' => 37.2780935995924,
                        'lng' => 49.58731538658958,
                    ],
                    'zoom' => 10
                ],
                'width' => '100%',
                'height' => '400px',
                'markerOptions' => [
                    'draggable' => true,
                ]
            ]); ?>

        </div>
    <?php elseif ($field['fildtype'] == "Gallery") : ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>

            <?php
            $value = null;
            if (count($field['fildvalue']) > 0) {
                $value = '';
                foreach ($field['fildvalue'] as $item) {
                    $value .= $item['attachmentId'] . ',';
                }
                $value = rtrim($value, ",");
            }
            echo \aminkt\uploadManager\components\UploadManager::widget([
                'id' => $field['fildkey'],
                'name' => $field['fildkey'],
                'value' => $value,
                'titleTxt' => 'تصویر را وارد کنید.',
                'multiple' => true,
                'helpBlockEnable' => false,
                'options' => [
                    'class' => 'form-control inline-editable',
                ],
                'showImageContainer' => '#gallery-container',
                'sizeOfImageInImageContainer' => null,
                'showImagesTemplate' => "<img src='{url}' style='z-index:-100;' class='img'>",
                'btnTxt' => 'تصاویر گالری را انتخاب کنید'
            ]);
            ?>
            <br><br>
            <div id="gallery-container">
            </div>

        </div>
    <?php elseif ($field['fildtype'] == "Image") : ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>

            <?php
            echo \aminkt\uploadManager\components\UploadManager::widget([
                'id' => $field['fildkey'],
                'name' => $field['fildkey'],
                'value' => $field['fildvalue']['attachmentId'],
                'titleTxt' => 'تصویر را وارد کنید.',
                'helpBlockEnable' => false,
                'options' => [
                    'class' => 'form-control inline-editable',
                ],
                'showImageContainer' => '#main-image-container',
                'sizeOfImageInImageContainer' => null,
                'showImagesTemplate' => "<img src='{url}' style='z-index:-100;' class='img'>",
                'btnTxt' => 'انتخاب تصویر شاخص'
            ]);
            ?>
            <br><br>
            <div id="main-image-container">
            </div>

        </div>
    <?php elseif ($field['fildtype'] == "Video") : ?>
        <div class="form-group">
            <label class="control-label" for="<?= $field['fildkey'] ?>"><?= $field['fildlabel'] ?></label>
            <?php
            echo \aminkt\uploadManager\components\UploadManager::widget([
                'id' => $field['fildkey'],
                'name' => $field['fildkey'],
                'value' => $field['fildvalue']['attachmentId'],
                'titleTxt' => 'ویدئو را انتخاب کنید.',
                'helpBlockEnable' => false,
                'options' => [
                    'class' => 'form-control inline-editable',
                ],
                'showImageContainer' => '#video-container',
                'sizeOfImageInImageContainer' => null,
                'showImagesTemplate' => "<video width='100%' controls><source src='{url}'>Your browser does not support HTML5 video.</video>",
                'btnTxt' => 'انتخاب ویدئو'
            ]);
            ?>
            <br><br>
            <div id="video-container">
            </div>
        </div>
    <?php else : ?>
        نوع مشخص نشده
    <?php endif; ?>
    <br><br>
<?php endforeach; ?>

<?php \yii\widgets\ActiveForm::end(); ?>
