<?php

namespace frontend\models;

use aminkt\ticket\interfaces\CustomerInterface;
use aminkt\ticket\models\Department;
use aminkt\ticket\models\Ticket;
use backend\models\SettingForm;
use Yii;
use yii\base\Model;
use yii2mod\settings\components\Settings;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['body', 'name', 'subject'], 'required'],
            [['name', 'email', 'phone'], 'string'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
//            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'نام',
            'email' => 'پست الکترونیکی',
            'phone' => 'تلفن',
            'subject' => 'موضوع',
            'body' => 'پیام',
            'verifyCode' => 'کد امنیتی',
        ];
    }

    /**
     * Sends an message as ticket.
     *
     * @return boolean whether the email was sent
     */
    public function send()
    {
        if (!$this->validate()) {
            return false;
        }
        /** @var Settings $settings */
        $settings = Yii::$app->settings;
        $settings->invalidateCache();
        $department = $settings->get(SettingForm::SETTING_SECTION_CONTENT, 'contactDefaultDepartment');
        $department = Department::findOne($department);

        if (!$department) {
            $this->addError('subject', 'دپارتمانی برای دریافت پیام شما مشخص نشده است.');
            return false;
        }

        $customer = new class($this->name, $this->phone, $this->email) implements CustomerInterface
        {
            public $name;
            public $mobile;
            public $email;

            /**
             *  constructor.
             *
             * @param string $name
             * @param string $mobile
             * @param string $email
             */
            public function __construct($name, $mobile, $email)
            {
                $this->name = $name;
                $this->mobile = $mobile;
                $this->email = $email;
            }

            /**
             * Return User Id.
             *
             * @return integer
             */
            function getId()
            {
                return null;
            }

            /**
             * Return user full name.
             *
             * @return string
             */
            function getName()
            {
                return $this->name;
            }

            /**
             * Return user email.
             * @return string|null
             */
            function getEmail()
            {
                return $this->email ? $this->email : "no@email.found";
            }

            /**
             * Return user mobile.
             *
             * @return string|null
             */
            function getMobile()
            {
                return $this->mobile ? $this->mobile : "+98";
            }
        };

        $this->subject = "فرم ارتباط با ما - " . $this->subject;
        $ticket = Ticket::createNewTicket($this->subject, $customer, $department);
        if ($ticket->save()) {
            $message = $ticket->sendNewMessage($this->body, []);
            if ($message->save()) {
                return true;
            }
            Yii::error($message->getErrors());
            return false;
        }

        Yii::error($ticket->getErrors());
        return false;
    }

    /**
     * Load model by api inputs.
     *
     * @param array $params
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     *
     * @return bool
     */
    public function loadByApi($params)
    {
        foreach ($params as $key => $val) {
            if ($this->isAttributeActive($key)) {
                $this->$key = $val;
            }
        }
        return true;
    }
}
