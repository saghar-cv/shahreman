<?php

namespace frontend\models;

use common\models\Business;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class BusinessDataForm extends Model
{
    public $name;
    public $description;
    public $owner;
    public $address;
    public $city;
    public $publishCity;
    public $category;
    public $gallery;
    public $cover;
    public $video;
    public $mapCoordinate;
    public $contactData;

    /** @var Business $business */
    private $business;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'owner', 'description', 'publishCity', 'category'], 'required'],
            [['name', 'description', 'owner', 'address', 'mapCoordinate', 'gallery'], 'string'],
            [['city', 'category', 'cover', 'video'], 'integer'],
            [['publishCity', 'contactData'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'نام',
            'owner' => 'مدیر صنف',
            'description' => 'توضیخات',
            'address' => 'آدرس',
            'city' => 'شهر محل استقرار صنف',
            'publishCity' => 'در کدام شهر منتشر شود؟',
            'category' => 'دسته بندی',
            'gallery' => 'گالری',
            'cover' => 'تصویر شاخص',
            'video' => 'ویدئو',
            'mapCoordinate' => 'نقشه',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->body)
            ->send();
    }

    /**
     * Return Business model.
     *
     * @return Business
     */
    public function getBusiness(): Business
    {
        return $this->business;
    }

    /**
     * Set Business model.
     *
     * @param Business $business
     *
     * @return void
     */
    public function setBusiness(Business $business)
    {
        $this->business = $business;
    }
}
