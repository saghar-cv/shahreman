<?php

namespace frontend\models;

use aminkt\normalizer\Normalize;
use aminkt\ticket\interfaces\CustomerInterface;
use common\models\AbstractUser;
use saghar\address\models\City;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property int $cityId
 * @property City $city
 * @property integer $telId  User telegram id
 * @property string $step   User chat step.
 */
class User extends AbstractUser implements CustomerInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public static function findByUsername($mobile)
    {
        $mobile = Normalize::normalizeMobile($mobile);
        return static::findOne(['mobile' => $mobile]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['authKey' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'family', 'mobile', 'passwordHash', 'ip'], 'required'],
            [['status', 'lastSeen', 'cityId'], 'integer'],
            [['name', 'family'], 'string', 'max' => 70],
            [['mobile', 'ip', 'lastIp'], 'string', 'max' => 15],
            [['mobile', 'email'], 'unique'],
            [['passwordHash', 'authKey', 'authKey'], 'string', 'max' => 255],
            [['cityId'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['cityId' => 'id']],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'نام',
            'email' => 'ایمیل',
            'family' => 'نام خانوادگی',
            'mobile' => 'موبایل',
            'cityId' => 'شهر',
            'passwordHash' => 'Password Hash',
            'passwordResetCode' => 'Password Reset Code',
            'authKey' => 'Auth Key',
            'planId' => 'Plan Id',
            'expireAt' => 'تاریخ انقضا',
            'status' => 'وضعیت',
            'ip' => 'IP',
            'lastIp' => 'آخرین IP',
            'lastSeen' => 'آخرین بازدید',
            'updateAt' => 'تاریخ ویرایش',
            'createAt' => 'تاریخ ایجاد',
        ];
    }

    /**
     * Return author mobile
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->mobile;
    }

    /**
     * Return author name
     *
     * @return string
     */
    public function getName()
    {
        return $this->fullName;
    }

    /**
     * Return author family
     *
     * @return string
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Return author email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Clear saved step.
     */
    public function resetStep()
    {
        $this->step = null;
        $this->save(false);
    }

    /**
     * Set new Step.
     *
     * @param integer $step
     */
    public function addStep($step)
    {
        if ($this->step and !empty($this->steps)) {
            $this->steps .= ',' . $step;
        } else {
            $this->step = $step;
        }
        $this->save(false);
    }

    /**
     * Set step individually
     *
     * @param $step
     */
    public function setStep($step)
    {
        $this->step = $step;
        $this->save(false);
    }

    /**
     * Remove Step.
     *
     * @param integer|null $step if null remove last step.
     *
     * @return bool
     */
    public function removeStep($step = null)
    {
        $steps = $this->getSteps();
        if (!empty($this->step) and $this->step and is_array($step)) {
            if (!$step) {
                array_pop($step);
            } else {
                if (($key = array_search($step, $steps)) !== false) {
                    unset($steps[$key]);
                }
            }
            $this->resetStep();
            foreach ($steps as $step) {
                $this->addStep($step);
            }
        }
        return false;
    }

    /**
     * Get steps as array
     *
     * @return array
     */
    public function getSteps()
    {
        if ($this->step and !empty($this->steps)) {
            $steps = explode(',', $this->steps);
            return $steps;
        }
        return [];
    }

    /**
     * Return step position
     *
     * @param $step
     *
     * @return int
     */
    public function getStepPosition($step)
    {
        $steps = $this->getSteps();
        $key = array_search($step, $steps);
        if ($key)
            return $key + 1;
        return -1;
    }

    /**
     * Return last item in steps.
     *
     * @return mixed
     */
    public function getLastStep()
    {
        $steps = $this->getSteps();
        if (count($steps))
            return array_last($steps);
        return null;
    }

    public function getFirstStep()
    {
        $steps = $this->getSteps();
        if (count($steps))
            return $steps[0];
        return null;
    }

    /**
     * return the plan id
     *
     * @return Integer
     */
    public function getPlanId()
    {
        return $this->planId;
    }

    /**
     * return expire time of plan id
     *
     * @return string
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function load($data, $formName = null)
    {
        if (!parent::load($data, $formName)) {
            foreach ($data as $key => $val) {
                if ($this->isAttributeActive($key)) {
                    $this->$key = $val;
                }
            }
        }
        return true;
    }

    /**
     * Return email hash.
     *
     * @return string
     */
    public function getEmailHash()
    {
        return md5($this->email);
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'family',
            'email',
            'emailHash',
            'mobile',
            'city',
            'ip',
            'updateAt',
            'createAt'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'cityId']);
    }

    /**
     * @inheritdoc
     */
    public function getUserIdsCan($access)
    {
        return [$this->id];
    }
}
