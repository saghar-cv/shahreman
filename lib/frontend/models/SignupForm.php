<?php

namespace frontend\models;

use aminkt\normalizer\Normalize;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $family;
    public $mobile;
    public $email;
    public $cityId;
    public $city;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile', 'name', 'family'], 'required'],
            [['mobile'], 'validateMobile'],
            [['cityId', 'city'], 'integer'],
            [['email'], 'email'],
        ];
    }

    public function validateMobile($attribute, $params, $validator)
    {
        $mobile = Normalize::normalizeMobile($this->$attribute);
        if ($mobile) {
            $this->mobile = $mobile;
        } else {
            $this->addError($attribute, 'شماره تلفن وارد شده معتبر نیست.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'نام',
            'family' => 'نام خانوادگی',
            'mobile' => 'شماره تلفن همراه',
            'email' => 'ایمیل',
            'cityId' => 'شهر'
        ];
    }

    /**
     * Signs user up.
     * get all required information from user and signs user up
     * then send an auth code to user and redirects to login page
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = new User();
        $mobile = \aminkt\normalizer\Normalize::normalizeMobile($this->mobile);
        $user->mobile = $mobile;
        $user->family = $this->family;
        $user->name = $this->name;
        $user->email = $this->email ? $this->email : null;
        if ($this->city and !$this->cityId) {
            $this->cityId = $this->city;
        }
        $user->cityId = $this->cityId;
        $user->password = $this->mobile;
        $user->generateAuthKey();
        $authCode = mt_rand(10000, 99999);
        $user->authCode = $authCode;
        // $sms = new \aminkt\sms\Sms();
        // $sms->setLoginData([
        //   'username'=> 'youUserNmae',
        //   'password'=> 'yourPassword',
        // ]);
        // $sms->sendSms([
        //   'message'=> "رمز عبور شما برای سایت شهر من $authCode است",
        //   'numbers'=> [$user->mobile]
        // ]);
        if ($user->save()) {
            return $user;
        } else {
            \Yii::error($user->getErrors(), self::className());
            $this->addErrors($user->getErrors());
        }
        return null;
    }

    /**
     * Load model by api inputs.
     *
     * @param array $params
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     *
     * @return bool
     */
    public function loadByApi($params)
    {
        foreach ($params as $key => $val) {
            if ($this->isAttributeActive($key)) {
                $this->$key = $val;
            }
        }
        return true;
    }
}
