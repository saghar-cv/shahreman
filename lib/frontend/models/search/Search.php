<?php

namespace frontend\models\search;

use aminkt\normalizer\yii2\PersianStringValidator;
use common\models\Business;
use common\models\Post;
use saghar\address\models\City;
use saghar\category\models\Category;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class SearchBusiness
 *
 * @property string $search
 * @property integer $catId
 * @property integer $parentId
 * @property integer $depth
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class Search extends Model
{

    public $search;
    public $catId;
    public $parentId;
    public $depth;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search', 'catId', 'depth', 'parentId'], 'safe'],
            ['search', PersianStringValidator::class, 'normalize' => true, 'convertToPersianNumber' => false, 'convertToEnglishNumber' => false],
            ['search', 'trim'],
        ];
    }

    /**
     * Search businesses
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function searchBusiness($params)
    {
        $query = Business::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                    'likeCount' => SORT_DESC,
                    'seenCount' => SORT_DESC
                ]
            ],
        ]);

        if ($city = $this->getCity()) {
            $query->leftJoin("{{%business_relation}}", "{{%businesses}}.id = {{%business_relation}}.businessId")
                ->andWhere(["{{%business_relation}}.cityId" => $city->id]);
        }

        if ($this->catId) {
            $query->innerJoin('{{%category_relation}}', '{{%businesses}}.id = {{%category_relation}}.businessId');
            $query->andWhere(['catId' => $this->catId]);
        }

        $query->andWhere(['{{%businesses}}.edited' => Business::BUSINESS_CHANGES_APPROVED]);
        $query->andWhere(['status' => Business::STATUS_CONFIRMED]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->leftJoin("{{%business_meta}}", "{{%businesses}}.id = {{%business_meta}}.businessId")
            ->andWhere([
                "{{%business_meta}}.key" => Business::BUSINESS_META_ADDRESS
            ]);
        $query->leftJoin("{{%address}}", "{{%business_meta}}.value = {{%address}}.id");
        $query->andFilterWhere(['or',
            ['like', 'name', $this->search],
            ['like', 'ownerName', $this->search],
            ['like', '{{%address}}.address', $this->search]
        ]);

        return $dataProvider;
    }

    /**
     * Search categories
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function searchCategories($params)
    {
        $query = Category::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ]
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query
                ->where(['section' => 'business',])
                ->andWhere(['!=', 'status', Category::STATUS_REMOVED])
                ->andFilterWhere(['depth' => $this->depth])
                ->andFilterWhere(['parentId' => $this->parentId]);
            return $dataProvider;
        }

        $query->innerJoin('{{%category_relation}}', '{{%categories}}.id = {{%category_relation}}.catId');
        $query->innerJoin('{{%businesses}}', '{{%businesses}}.id = {{%category_relation}}.businessId');

        if ($city = $this->getCity()) {
            $query->innerJoin('{{%business_relation}}', '{{%business_relation}}.businessId = {{%businesses}}.id')
                ->andWhere(['{{%business_relation}}.cityId' => $city->id]);
        }

        $query->groupBy("{{%categories}}.id");

        $query
            ->andFilterWhere(['or',
                ['like', '{{%categories}}.name', $this->search],
                ['like', '{{%businesses}}.name', $this->search],
                ['like', 'tags', $this->search]
            ]);
        $query
            ->andWhere(['section' => 'business',])
            ->andWhere(['!=', '{{%categories}}.status', Category::STATUS_REMOVED])
            ->andFilterWhere(['depth' => $this->depth])
            ->andFilterWhere(['parentId' => $this->parentId])
            ->andWhere(['{{%businesses}}.edited' => Business::BUSINESS_CHANGES_APPROVED]);

        return $dataProvider;
    }

    /**
     * Returns an array of data providers for general search
     *
     * @param $params
     *
     * @return array
     */
    public function generalSearch($params)
    {
        return [
            'news' => self::newsSearch($params),
            'post' => self::postSearch($params),
            'business' => self::generalBusinessSearch($params),
            'category' => self::searchCategories($params),
        ];
    }

    /**
     * Search news
     *
     * @param $params
     *
     * @return ActiveDataProvider
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function newsSearch($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC,
                    'likeCount' => SORT_DESC,
                    'seenCount' => SORT_DESC
                ]
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->where(['status' => Post::STATUS_PUBLISH, 'postType' => Post::TYPE_NEWS_POST]);
            if ($city = $this->getCity()) {
                $query->andWhere(['cityId' => $city->id]);
            }
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'title', $this->search])
            ->orFilterWhere(['like', 'tags', $this->search]);
        $query->andWhere(['status' => Post::STATUS_PUBLISH, 'postType' => Post::TYPE_NEWS_POST]);

        if ($this->catId) {
            $query->innerJoin('{{%category_relation}}', '{{%posts}}.id = {{%category_relation}}.postId');
            $query->andWhere(['catId' => $this->catId]);
        }

        if ($city = $this->getCity()) {
            $query->andWhere(['cityId' => $city->id]);
        }

        return $dataProvider;
    }

    /**
     * Overwrite load function for api compatibility
     *
     * @param array $data
     * @param null $formName
     *
     * @return bool
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName))
            return true;
        foreach ($data as $key => $val) {
            if ($this->isAttributeActive($key)) {
                $this->$key = $val;
            }
        }
        return true;
    }

    /**
     * Search posts
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function postSearch($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC,
                    'likeCount' => SORT_DESC,
                    'seenCount' => SORT_DESC
                ]
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->where(['status' => Post::STATUS_PUBLISH, 'postType' => Post::TYPE_STATIC_POST]);
            if ($city = $this->getCity()) {
                $query->andWhere(['or',
                    'cityId = ' . $city->id,
                    'cityId IS NULL'
                ]);
            }
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'title', $this->search])
            ->orFilterWhere(['like', 'tags', $this->search]);
        $query->andWhere(['status' => Post::STATUS_PUBLISH, 'postType' => Post::TYPE_STATIC_POST]);

        if ($city = $this->getCity()) {
            $query->andWhere(['or',
                'cityId = ' . $city->id,
                'cityId IS NULL'
            ]);
        }

        return $dataProvider;
    }

    /**
     * Search in businesses for general search
     *
     * @param $params
     *
     * @return ActiveDataProvider
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function generalBusinessSearch($params)
    {
        $query = Business::find();
        $query->leftJoin("{{%business_meta}}", "{{%businesses}}.id = {{%business_meta}}.businessId");

        if ($this->catId) {
            $query->innerJoin('{{%category_relation}}', '{{%businesses}}.id = {{%category_relation}}.businessId');
            $query->andWhere(['catId' => $this->catId]);
        }

        if ($city = $this->getCity()) {
            $query->leftJoin("{{%business_relation}}", "{{%businesses}}.id = {{%business_relation}}.businessId")
                ->andWhere([
                    '{{%business_relation}}.cityId' => $city->id
                ]);
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC,
                    'likeCount' => SORT_DESC,
                    'seenCount' => SORT_DESC
                ]
            ],
        ]);

        $query->andWhere(['{{%businesses}}.edited' => Business::BUSINESS_CHANGES_APPROVED]);
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere(['or',
                ['like', '{{%business_meta}}.value', $this->search],
                ['like', '{{%businesses}}.name', $this->search]
            ]);
//            ->orFilterWhere(['like', '{{%business_meta}}.value', $this->search])
//            ->andFilterWhere(['like', '{{%businesses}}.name', $this->search]);

        return $dataProvider;
    }

    /**
     * Return city model.
     *
     * @return array|mixed|null
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private static function getCity()
    {
        if (isset(\Yii::$app->params['city']) and $city = \Yii::$app->params['city']) {
            return $city;
        } elseif ($city = \Yii::$app->getRequest()->get('city')) {
            $city = City::findOne($city);
            if ($city) {
                return $city;
            }
        }
        return null;
    }

    /**
     * Search in categories and show their businesses
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
//    public function generalCategorySearch($params)
//    {
//        $query = Business::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query
//        ]);
//
//        if (!($this->load($params) && $this->validate())) {
//            return $dataProvider;
//        }
//
//        $query->leftJoin('{{%category_relation}}', '{{%category_relation}}.businessId = {{%businesses}}.id')
//            ->leftJoin('{{%categories}}', '{{%categories}}.id = {{%category_relation}}.catId');
//        $query->orFilterWhere(['like', '{{%categories}}.name',  $this->search]);
//
//        return $dataProvider;
//    }


}