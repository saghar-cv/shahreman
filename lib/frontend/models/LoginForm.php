<?php
namespace frontend\models;

use aminkt\normalizer\Normalize;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $mobile;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['mobile'], 'required'],
            [['mobile'], 'validateMobile'],
        ];
    }

    public function validateMobile($attribute, $params, $validator)
    {
        $mobile = Normalize::normalizeMobile($this->$attribute);
        if ($mobile) {
            $this->mobile = $mobile;
        } else {
            $this->addError($attribute, 'شماره تلفن وارد شده معتبر نیست.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mobile' => 'موبایل',
            'rememberMe' => 'من را به یاد بسپار',
        ];
    }

    /**
     * Finds user by [[mobile]]
     * 
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === null) {
            $mobile = \aminkt\normalizer\Normalize::normalizeMobile($this->mobile);
            $this->_user = User::findByUsername($this->mobile);
        }

        return $this->_user;
    }
}
