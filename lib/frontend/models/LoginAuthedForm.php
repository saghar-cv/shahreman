<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginAuthedForm extends Model
{
    public $mobile;
    public $authCode;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['mobile', 'authCode'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mobile' => 'موبایل',
            'authCode' => 'گذرواژه',
            'rememberMe' => 'من را به یاد بسپار',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'شماره موبایل یا گذرواژه صحیح نیست.');
            }
        }
    }

    /**
     * Logs in a user using the auth code username and password.
     * 
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
      if ($this->validate()) {
        $mobile = \aminkt\normalizer\Normalize::normalizeMobile($this->mobile);
        $user = User::findByUsername($this->mobile);
          if ($user and $user->authCode == $this->authCode) {
              $user->authCode = null;
              $user->save(false);
              return Yii::$app->user->login($user, 3600 * 24 * 30);
        }else
          return false;
      } else {
        Yii::warning($this->getErrors(), self::className());
        return false;
      }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $mobile = \aminkt\normalizer\Normalize::normalizeMobile($this->mobile);
            $this->_user = User::findByUsername($mobile);
        }

        return $this->_user;
    }


    /**
     * Login user by access token.
     *
     * @param $token
     * @return bool
     */
    public static function loginByToken($token)
    {
        $user = User::findIdentityByAccessToken($token);
        if ($user) {
            return Yii::$app->user->login($user, 3600 * 24 * 30);
        }

        return false;
    }
}
