<?php

/* @var $this yii\web\View */
/* @var $user panel\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->passwordResetCode]);
?>
سلام  <?= $user->name ?>,

لطفا از لینک زیر برای بازیابی کلمه عبور خود استفاده کنید.

<?= $resetLink ?>
