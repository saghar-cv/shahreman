<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user panel\models\User|\frontend\models\Member */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->passwordResetCode]);
?>
<div class="password-reset">
    <p>سلام  <?= Html::encode($user->name) ?>,</p>

    <p>لطفا از لینک زیر برای بازیابی کلمه عبور خود استفاده کنید:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
