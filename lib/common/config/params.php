<?php
return [
    'adminEmail' => 'admin@telbit.ir',
    'supportEmail' => 'support@telbit.ir',
    'user.passwordResetTokenExpire' => 3600,
    'botToken' => 'bot_token'
];
