<?php
return [
    'name'=>'bitcoin',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'queue', // The component registers own console commands
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => [
                "@console/migrations",
                "@vendor/yii2mod/yii2-settings/migrations",
                "@vendor/saghar/yii2-address-module/migrations",
                "@vendor/saghar/yii2-category-module/migrations",
                "@vendor/yii2mod/yii2-settings/migrations",
                "@vendor/aminkt/yii2-upload-manager/migrations",
                "@vendor/aminkt/yii2-payment-module/migrations",
                "@yii/rbac/migrations",
                "@vendor/aminkt/yii2-notifications/migrations",
                "@vendor/aminkt/yii2-ticket-module/migrations",
            ],
            'migrationNamespaces' => [
                'yii\queue\db\migrations',
            ],
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'settings' => [
            'class' => 'yii2mod\settings\components\Settings',
        ],
        'i18n' => [
            'translations' => [
                'yii2mod.settings' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/settings/messages',
                ],
                'db_rbac' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en',
                    'basePath' => '@developeruz/db_rbac/messages',
                ]
            ],
        ],
        'sms' => [
            'class' => \aminkt\sms\yii\SmsComponent::className(),
            'loginData' => [
                'username' => 'shahreman',
                'password' => '10203090',
                'fromNumber' => '3000819131',
                'WSID' => '4072'
            ],
            'driverClass' => '\aminkt\sms\drivers\Pardad',
        ],
        'formatter' => [
            'dateFormat' => 'd MMMM YYYY',
            'datetimeFormat' => 'd MMMM YYYY hh:mm:ss',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
            'currencyCode' => 'IRR',
            'locale' => 'fa_IR@calendar=persian',
//            'calendar' => \IntlDateFormatter::TRADITIONAL,
            'timeZone' => 'Asia/Tehran',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ],
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'as log' => \yii\queue\LogBehavior::class,
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => \yii\mutex\MysqlMutex::class, // Mutex used to sync queries
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
    ],
    'modules'=>[
    ],

    'language' => 'fa-IR',
    'timeZone' => 'Asia/Tehran',
];