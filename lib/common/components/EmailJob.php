<?php

namespace common\components;

use common\models\Message;
use Yii;
use yii\base\BaseObject;
use yii\queue\Queue;

/**
 * Class EmailJob for save emails in queue and then send theme
 *
 * @package common\components
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class EmailJob extends BaseObject implements \yii\queue\JobInterface
{
    public $email;
    public $subject;
    public $content;

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        $mailer = Yii::$app->mailer->compose();
        $mailer->setTo($this->email);
        $mailer->setSubject($this->subject);
        $mailer->setTextBody($this->content);
        if ($mailer->send()){
            $sms = Message::findOne(['email' => $this->email, 'content' => $this->message]);
            $sms->status = 2;
            $sms->save();
        }
    }
}