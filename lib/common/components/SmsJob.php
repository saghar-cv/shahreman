<?php

namespace common\components;


use common\models\Message;
use Yii;
use yii\base\BaseObject;
use yii\queue\Queue;

/**
 * Class SmsJob for save sms in queue then execute  and send it to user
 *
 * @package common\components
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class SmsJob extends BaseObject implements \yii\queue\JobInterface
{
    public $mobile;
    public $message;

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {

        $smsFormat = mb_convert_encoding($this->message, "UTF-8", "auto");

        /** @var \aminkt\sms\yii\SmsComponent $smsComponent */
        $smsComponent = Yii::$app->sms;
        if ($smsComponent->send([
            'message' => $smsFormat,
            'numbers' => [$this->mobile]
        ])) {
            $sms = Message::findOne(['mobile' => $this->mobile, 'content' => $this->message]);
            $sms->status=2;
            $sms->save();
        }

    }

    /**
     * Add a new sms job into queue.
     *
     * @param string $mobile Mobile number to send sms.
     * @param string $message Message that should be send.
     *
     * @return  void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public static function addJob($mobile, $message)
    {
        Yii::$app->queue->push(new \common\components\SmsJob([
            'mobile' => $mobile,
            'message' => $message,
        ]));
    }
}