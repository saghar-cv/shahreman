<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%post_meta}}".
 *
 * @property int    $id
 * @property int    $postId
 * @property string $key
 * @property string $value
 * @property string $updateAt
 * @property string $createAt
 *
 * @property Post  $post
 */
class PostMeta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post_meta}}';
    }

    /**
     * Add extra information to posts
     *
     * @param integer $postId
     * @param string  $key
     * @param string  $value
     *
     * @throws \InvalidArgumentException
     *
     * @return self
     */
    public static function add($postId, $key, $value)
    {
        $meta = self::findOne([
            'postId' => $postId,
            'key' => $key
        ]);
        if (!$meta)
            $meta = new self();
        $meta->postId = $postId;
        $meta->key = $key;
        $meta->value = $value;
        if ($meta->save()) {
            Yii::$app->getCache()->delete('posts-meta-' . $postId . '-' . $key);
            Yii::$app->getCache()->set('posts-meta-' . $postId . '-' . $key, $meta->value, 24 * 60 * 60);
            return $meta;
        }
        Yii::error($meta->getErrors(), self::className());
        throw new \InvalidArgumentException("Inputs are not valid");
    }

    /**
     * Return meta value for a post.
     *
     * @param integer $postId
     * @param string  $key
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function get($postId, $key)
    {
        if ($val = Yii::$app->getCache()->get('posts-meta-' . $postId . '-' . $key)) {
            return $val;
        }

        $meta = self::findOne([
            'postId' => $postId,
            'key' => $key
        ]);
        if (!$meta) {
            return null;
        }

        Yii::$app->getCache()->set('posts-meta-' . $postId . '-' . $key, $meta->value, 24 * 60 * 60);
        return $meta->value;
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postId'], 'integer'],
            [['key'], 'required'],
            [['updateAt', 'createAt'], 'safe'],
            [['key'], 'string', 'max' => 191],
            [['value'], 'safe'],
            [['postId'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['postId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'postId' => 'Post ID',
            'key' => 'Key',
            'value' => 'Value',
            'updateAt' => 'Update At',
            'createAt' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'postId']);
    }
}
