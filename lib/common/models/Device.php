<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%devices}}".
 *
 * @property int $id
 * @property string $deviceId
 * @property string $osVersion
 * @property string $appVersion
 * @property string $deviceModel
 * @property string $operator
 * @property string $osType
 * @property string $pusheId
 * @property string $updateAt
 * @property string $createAt
 *
 * @property \yii\db\ActiveQuery $userDevices
 * @property UserDevice[] $userDevice
 */
class Device extends ActiveRecord
{
    const OS_TYPE_ANDROID = 'android';
    const OS_TYPE_IOS = 'iOS';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%devices}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deviceId'], 'required'],
            [['deviceId'], 'unique'],
            [['deviceId', 'osVersion', 'appVersion', 'deviceModel', 'operator', 'osType', 'pusheId'], 'string', 'max' => 191],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'شناسه',
            'deviceId' => 'شناسه دستگاه',
            'osVersion' => 'نسخه سیستم عامل',
            'appVersion' => 'نسخه اپلیکیشن',
            'deviceModel' => 'مدل دستگاه',
            'operator' => 'اپراتور',
            'osType' => 'نوع سیستم عامل',
            'pusheId' => 'شناسه پوشه',
            'updateAt' => 'تاریخ ویرایش',
            'createAt' => 'تاریخ ایجاد',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDevices()
    {
        return $this->hasMany(UserDevice::class, ['deviceId' => 'id']);
    }
}
