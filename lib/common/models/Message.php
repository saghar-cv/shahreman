<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%messages}}".
 *
 * @property int $id
 * @property string $recipientName
 * @property string $mobile
 * @property string $email
 * @property string $content
 * @property int $type
 * @property string $createAt
 * @property int $status
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class Message extends BaseActiveRecord
{
    const STATUS_NOT_SEND = 1;
    const STATUS_SEND = 2;

    const TYPE_SMS = 1;
    const TYPE_EMAIL = 2;
    const TYPE_SMS_EMAIL = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%messages}}';

    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipientName', 'content'], 'required'],
            [['mobile', 'type', 'status'], 'integer'],
            [['mobile', 'email', 'content', 'recipientName'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipientName' => 'نام',
            'content' => 'متن پیام',
            'mobile' => 'شماره موبایل',
            'email' => 'ایمیل',
            'type' => 'نوع پیام',
            'status' => 'وضعیت ارسال',
            'createAt' => 'تاریخ ارسال',
        ];
    }

    /**
     * Return status label.
     *
     * @return string
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function getStatusLabel()
    {
        switch ($this->status) {
            case self::STATUS_SEND:
                return "ارسال شده";
                break;
            case self::STATUS_NOT_SEND:
                return "ارسال نشده";
                break;
            default:
                return "";
        }
    }

    /**
     * Return type label
     *
     * @return string
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function getTypeLabel()
    {
        switch ($this->type) {
            case self::TYPE_SMS:
                return "پیامک";
                break;
            case self::TYPE_EMAIL:
                return "ایمیل";
                break;
            case self::TYPE_SMS_EMAIL:
                return "پیامک و ایمیل";
                break;
            default:
                return "نامشخص";
        }
    }


    /**
     * @return string
     */
    public function getRecipientName(): string
    {
        return $this->recipientName;
    }

    /**
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

}