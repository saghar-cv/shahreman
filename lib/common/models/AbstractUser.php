<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Abstract User model
 *
 * @property integer $id
 * @property string  $name
 * @property string  $family
 * @property string  $mobile
 * @property string  $email
 * @property string  $passwordHash
 * @property string  $authCode
 * @property string  $authKey
 * @property integer $status
 * @property string  $ip
 * @property string  $lastIp
 * @property string  $lastSeen
 * @property string  $updateAt
 * @property string  $createAt
 *
 * @property string  $password read-only password
 * @property string  $fullName read-only full name
 */
abstract class AbstractUser extends BaseActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAITING = 2;
    const STATUS_BLOCK = 3;


    public function beforeValidate()
    {
        $this->ip = Yii::$app->getRequest()->getUserIP();
        return parent::beforeValidate();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email','email'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_WAITING,self::STATUS_BLOCK, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($mobile)
    {
        return static::findOne(['mobile' => $mobile, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'passwordResetCode' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if($this->passwordHash)
            return Yii::$app->security->validatePassword($password, $this->passwordHash);
        return false;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->authCode = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->authCode = null;
    }

    /**
     * Return full name of user.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->family;
    }

        /**
     * Return status
     * @author mr-exception
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Return ip
     * @author mr-exception
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Return last ip
     * @author mr-exception
     * @return string
     */
    public function getLastIp()
    {
        return $this->lastIp;
    }

    /**
     * Return lastSeen
     * @author mr-exception
     * @return string
     */
    public function getLastSeen()
    {
        return $this->lastSeen;
    }

    /**
     * Return update at
     * @author mr-exception
     * @return string
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Return create at
     * @author mr-exception
     * @return string
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }


    /**
     * Return admin username(phone number)
     * @author mr-exception
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Return author name
     * @author mr-exception
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Return author family
     * @author mr-exception
     * @return string
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * Return author email
     * @author mr-exception
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Return user ids that have selected accesses.
     *
     * @param string|array $access Access to search user ids.
     *
     * @return array
     */
    public abstract function getUserIdsCan($access);
}
