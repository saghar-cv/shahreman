<?php

namespace common\models;

use aminkt\uploadManager\UploadManager;
use api\components\UrlMaker;
use backend\models\Admin;
use backend\models\CategoryRelation;
use backend\models\NotificationHandler;
use backend\models\Plan;
use backend\models\SmsForm;
use common\components\SmsJob;
use frontend\models\User;
use saghar\address\models\Address;
use saghar\address\models\City;
use saghar\category\models\Category;
use Yii;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%businesses}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $ownerId
 * @property string $ownerName
 * @property int $status
 * @property string $updateAt
 * @property string $createAt
 * @property string $expireDate
 * @property int $planId
 * @property int $edited
 * @property int $likeCount
 * @property int $seenCount
 * @property string $operatorNote
 *
 * @property BusinessMeta[] $businessMetas
 * @property Plan $plan
 * @property User $owner
 * @property CategoryRelation[] $categoryRelations
 * @property string $cityName
 * @property City[] $cities
 * @property boolean $canEdit
 * @property string $categoryName
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 * @author Amin Keshavarz <amin@keshavarz.pro>
 */
class Business extends ActiveRecord
{
    const STATUS_NOT_PAYED = 1;
    const STATUS_PAYED = 2;
    const STATUS_CONFIRMED = 3;
    const STATUS_REJECTED = 4;
    const STATUS_EXPIRED = 5;
    const STATUS_REMOVED = 6;

    const BUSINESS_CHANGES_APPROVED = 2;
    const BUSINESS_IS_EDITED = 1;

    const BUSINESS_META_MAIN_IMAGE = 'MainImage';
    const BUSINESS_META_VIDEO = 'Video';
    const BUSINESS_META_SOCIAL_TELEGRAM = 'SocialTelegram';
    const BUSINESS_META_SOCIAL_INSTALGRAM = 'SocialInstagram';
    const BUSINESS_META_WEBSITE = 'Website';
    const BUSINESS_META_EMAIL = 'Email';
    const BUSINESS_META_PHONE = 'PhoneNumber';
    const BUSINESS_META_MOBILE = 'MobileNumber';
    const BUSINESS_META_FAX = 'FaxNumber';
    const BUSINESS_META_ADDRESS = 'Address';
    const BUSINESS_META_GALLERY = 'Gallery';
    const BUSINESS_META_IOS = 'IOS';
    const BUSINESS_META_ANDROID = 'Android';

    /** Event const that define an business registered right now. */
    const EVENT_BUSINESS_REGISTERED = 'eventBusinessRegistered';
    /** Event const that define an business edited right now and going to draft until operators publish it again. */
    const EVENT_BUSINESS_EDITED = 'eventBusinessEdited';
    /** Event const that define an business confirmed by operator after user edition and now accessible by other users.  */
    const EVENT_BUSINESS_PUBLISHED = 'eventBusinessPublished';
    /** Event const that define an business confirmed after first step of registration. */
    const EVENT_BUSINESS_CONFIRMED = 'eventBusinessConfirmed';
    /** Event const that define an business rejected by operators and blocked. */
    const EVENT_BUSINESS_REJECTED = 'eventBusinessRejected';
    /** Event const that define an business payment confirmed correctly. */
    const EVENT_BUSINESS_PAYMENT_CONFIRMED = 'eventBusinessPaymentConfirmed';
    /** Event const that define an business expired. */
    const EVENT_BUSINESS_EXPIRED = 'eventBusinessExpired';

    private $category = null;

    /**
     * @inheritdoc
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function init()
    {
        parent::init();

        // Attach event handlres.
        $this->on(self::EVENT_BUSINESS_REGISTERED, [$this, 'onRegister'], $this);
        $this->on(self::EVENT_BUSINESS_EDITED, [$this, 'onEdit'], $this);
        $this->on(self::EVENT_BUSINESS_PUBLISHED, [$this, 'onPublish'], $this);
        $this->on(self::EVENT_BUSINESS_CONFIRMED, [$this, 'onConfirmation'], $this);
        $this->on(self::EVENT_BUSINESS_PAYMENT_CONFIRMED, [$this, 'onPaymentConfirmation'], $this);
        $this->on(self::EVENT_BUSINESS_EXPIRED, [$this, 'onExpiration'], $this);
        $this->on(self::EVENT_BUSINESS_REJECTED, [$this, 'onReject'], $this);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%businesses}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'ownerName', 'operatorNote'], 'string'],
            [['ownerId', 'status', 'planId', 'edited'], 'integer'],
            [['name'], 'string', 'max' => 191],
            [['planId'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::class, 'targetAttribute' => ['planId' => 'id']],
            [['ownerId'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['ownerId' => 'id']],
            [['expireDate'], 'string'],
            [['expireDate'], 'default', 'value' => null],
            [['name', 'description', 'ownerName'], 'arabicToPersian']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'نام کسب و کار شما',
            'description' => 'توضیحات',
            'ownerId' => 'صاحب کسب و کار',
            'status' => 'وضعیت',
            'updateAt' => 'تاریخ ویرایش',
            'createAt' => 'تاریخ ایجاد',
            'planId' => 'پلان',
            'paln' => 'پلن',
            'expireDate' => 'تاریخ انقضاء',
            'ownerName' => 'نام صاحب کسب و کار',
            'cityName' => 'شهر',
            'cities' => 'محل انتشار کسب و کار',
            'edited' => 'وضعیت آخرین تغییرات',
            'operatorNote' => 'پیام اپراتور'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinessMetas()
    {
        return $this->hasMany(BusinessMeta::class, ['businessId' => 'id'])->where(['!=', 'key', 'SocialLinkedin']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['id' => 'cityId'])
            ->viaTable(BusinessRelation::tableName(), ['businessId' => 'id']);
    }

    /**
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPublishedCities()
    {
        return $this->hasMany(City::className(), ['id' => 'cityId'])->viaTable('{{%business_relation}}', ['businessId' => 'id']);
    }

    /**
     *get published cities names
     *
     * @return string
     */
    public function getPublishedCitiesNames()
    {
        $cities = $this->publishedCities;
        $citiesName = '';

        $i = 0;
        foreach ($cities as $city) {
            if ($i++ > 0) {
                $citiesName .= ', ';
            }
            $citiesName .= $city->name;
        }

        return $citiesName;
    }


    /**
     * Return meta value.
     *
     * @param string $metaKey
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getMeta($metaKey)
    {
        return BusinessMeta::get($this->id, $metaKey);
    }

    /**
     * Return meta label
     *
     * @param $metaKey
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @return string
     */
    public function getMetaLabel($metaKey)
    {
        return BusinessMeta::getLabelString($this->id, $metaKey);
    }

    /**
     * Add a meta if not exist or update that.
     *
     * @param $metaKey
     * @param $val
     *
     * @return \common\models\BusinessMeta
     *
     * @throws \InvalidArgumentException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function add($metaKey, $val)
    {
        $meta = BusinessMeta::add($this->id, $metaKey, $val);
        return $meta;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::class, ['id' => 'planId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::class, ['id' => 'ownerId']);
    }

    /**
     * Return owner name.
     * @return null|string
     */
    public function getOwnerName()
    {
        if (!$this->ownerName) {
            if ($this->owner) {
                $this->ownerName = $this->owner->fullName;
            }
        }
        return $this->ownerName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryRelations()
    {
        return $this->hasMany(CategoryRelation::class, ['businessId' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->status = self::STATUS_NOT_PAYED;
            $this->ownerId = Yii::$app->getUser()->getId();
        }

        $oldStatus = $this->getOldAttribute('status');
        if ($oldStatus != $this->status) {
            switch ($this->status) {
                case self::STATUS_PAYED :
                    $this->trigger(self::EVENT_BUSINESS_PAYMENT_CONFIRMED);
                    break;
                case self::STATUS_CONFIRMED :
                    $this->trigger(self::EVENT_BUSINESS_CONFIRMED);
                    break;
                case self::STATUS_REJECTED :
                    $this->trigger(self::EVENT_BUSINESS_REJECTED);
                    break;
                case self::STATUS_EXPIRED :
                    $this->trigger(self::EVENT_BUSINESS_EXPIRED);
                    break;
            }
        }

        $oldEdited = $this->getOldAttribute('edited');
        if ($oldEdited != $this->edited) {
            switch ($this->edited) {
                case self::BUSINESS_IS_EDITED :
                    $this->trigger(self::EVENT_BUSINESS_EDITED);
                    break;
                case self::BUSINESS_CHANGES_APPROVED :
                    $this->trigger(self::EVENT_BUSINESS_PUBLISHED);
                    break;
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->trigger(self::EVENT_BUSINESS_REGISTERED);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Event handler that runs when one business is registered.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onRegister($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::success(
                    Business::EVENT_BUSINESS_REGISTERED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }

        $business = $this;
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_REGISTERED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when one business is published and can accessed in public.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onPublish($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::success(
                    Business::EVENT_BUSINESS_PUBLISHED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }

        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_PUBLISHED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when one business is begin edit.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onEdit($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::warning(
                    Business::EVENT_BUSINESS_EDITED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }
        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_EDITED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when one business is confirmed by operators.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onConfirmation($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::success(
                    Business::EVENT_BUSINESS_CONFIRMED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }

        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_CONFIRMED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when one payment of business is verified and confirmed.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onPaymentConfirmation($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::success(
                    Business::EVENT_BUSINESS_PAYMENT_CONFIRMED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }

        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_PAYMENT_CONFIRMED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when a business expires.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onExpiration($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::error(
                    Business::EVENT_BUSINESS_EXPIRED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }

        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_EXPIRED);
            $content = $this->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }
    }

    /**
     * Event handler that runs when a business reject.
     *
     * @param Event $event Event object that happened.
     *
     * @return void
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function onReject($event)
    {
        $ids = Admin::getAdminIdsCan(['business', 'business/update']);
        foreach ($ids as $id) {
            if ($id != Yii::$app->getUser()->getId()) {
                NotificationHandler::error(
                    Business::EVENT_BUSINESS_REJECTED,
                    $id,
                    NotificationHandler::USER_TYPE_ADMIN,
                    $event->data->id
                );
            }
        }


        $business = self::findOne($event->data->id);
        if ($business->owner) {
            $content = SmsForm::getSmsContent(SmsForm::TYPE_BUSINESS_REJECTED);
            $content = $business->shortCodeFormatter($content);
            $mobile = $business->owner->mobile;
            $message = new Message();
            $message->content = $content;
            $message->recipientName = $business->ownerName;
            $message->mobile = $mobile;
            $message->type = Message::TYPE_SMS;
            $message->save();
            SmsJob::addJob($mobile, $content);
        }

    }

    /**
     * This method will replace current business data by short code that write in message.
     *
     * @param $message
     * @return mixed
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function shortCodeFormatter($message)
    {
        $message = str_replace("{%businessOwnerName%}", $this->getOwnerName(), $message);

        $message = str_replace("{%businessName%}", $this->name, $message);

        $message = str_replace("{%publishCitiesCount%}", count($this->cities), $message);

        $message = str_replace("{%businessRegistrationAmount%}", number_format($this->plan->price * count($this->cities)), $message);

        $message = str_replace("{%businessExpireDate%}", $this->expireDate, $message);

        $message = str_replace("{%businessCategoryName%}", $this->getCategoryName(), $message);

        $message = str_replace("{%businessPublishedCitiesNames%}", $this->getPublishedCitiesNames(), $message);

        $message = str_replace("{%operatorNote%}", $this->operatorNote, $message);

        return $message;
    }


    /**
     * Set name of business
     *
     * @param string $val
     *
     * @return self|array
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function setName($val)
    {
        $this->name = $val;
        if (!$this->save()) {
            return $this->getErrors('name');
        }

        return $this;
    }

    /**
     * Set description of business
     *
     * @param string $val
     *
     * @return self|array
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function setDescription($val)
    {
        $this->description = $val;
        if (!$this->save()) {
            return $this->getErrors('description');
        }

        return $this;
    }

    /**
     * Set owner name of business
     *
     * @param $val
     *
     * @return $this|array
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function setOwnerName($val)
    {
        $this->ownerName = $val;
        if (!$this->save()) {
            return $this->getErrors('ownerName');
        }

        return $this;
    }

    /**
     * Set status for business
     *
     * @param $status
     * @param $force    boolean Force to change status even some warning exist.
     *
     * @return $this|array
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws BadRequestHttpException
     */
    public function setStatus($status, $force = false)
    {
        $warningConditions = [
            self::STATUS_NOT_PAYED,
            self::STATUS_EXPIRED,
            self::STATUS_REJECTED
        ];

        if (in_array($this->status, $warningConditions) and !$force) {
            throw new BadRequestHttpException("Cant change status of business before payment.");
        }
        $this->status = $status;
        if (!$this->save()) {
            return $this->getErrors('status');
        }

        return $this;
    }

    /**
     * Set edited status
     *
     * @param $status
     *
     * @return $this|array
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function setEditedStatus($status)
    {
        $this->edited = $status;
        if (!$this->save()) {
            if ($this->status == self::BUSINESS_IS_EDITED) {
                $this->trigger(self::EVENT_BUSINESS_EDITED);
            } elseif ($this->status == self::BUSINESS_CHANGES_APPROVED) {
                $this->trigger(self::EVENT_BUSINESS_PUBLISHED);
            }
            return $this->getErrors('edited');
        }

        return $this;
    }

    /**
     * Get status label
     *
     * @return string
     */
    public function getStatusLabel()
    {
        if ($this->edited == self::BUSINESS_IS_EDITED) {
            return 'در انتظار تایید اپراتور';
        }
        switch ($this->status) {
            case self::STATUS_NOT_PAYED:
                return 'پرداخت نشده';
            case self::STATUS_PAYED:
                return 'پرداخت شده';
            case self::STATUS_CONFIRMED:
                return 'تایید شده';
            case self::STATUS_REJECTED:
                return 'رد شده';
            case self::STATUS_EXPIRED:
                return 'منقضی شده';
            case self::STATUS_REMOVED:
                return 'حذف شده';
            default:
                return 'نامشخص';
        }
    }

    /**
     * Get all phone numbers
     *
     * @return string
     */
    public function getMainPhone()
    {
        $phone = BusinessMeta::findOne([
            'businessId' => $this->id,
            'key' => self::BUSINESS_META_PHONE . '-1'
        ]);
        if ($phone)
            return $phone->value;
        return null;
    }

    /**
     * Get Address
     *
     *
     * @return Address  If address model not found a new Address object will returned.
     */
    public function getAddress()
    {
        $addressId = $this->getMeta(self::BUSINESS_META_ADDRESS);
        if ($addressId) {
            return Address::findOne($addressId);
        }
        return new Address();
    }

    /**
     * Return city name that business are in there.
     *
     * @return null|string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getCityName()
    {
        if ($cache = Yii::$app->getCache()->get('cityeName-of-business-' . $this->id)) {
            return $cache;
        }

        $address = $this->getAddress();
        if ($address) {
            $cityName = $address->cityName;
            Yii::$app->getCache()->set('cityeName-of-business-' . $this->id, $cityName, 24 * 60 * 60 * 2);
            return $cityName;
        }

        return null;
    }

    /**
     * Get main image url
     *
     * @param bool $noImageIfNotFound Return "No image file" url if no image not found.
     * @param bool $justUrl If set false attachment id will returned with url.
     *
     * @return string|array
     *
     * @throws InvalidConfigException
     */
    public function getImage($noImageIfNotFound = true, $justUrl = true, $size = null)
    {
        $img = BusinessMeta::findOne([
            'businessId' => $this->id,
            'key' => self::BUSINESS_META_MAIN_IMAGE
        ]);
        try {
            if ($img) {
                $url = UploadManager::getInstance()->getFile($img->value)->getUrl($size);

                if ($justUrl) {
                    return $url;
                } else {
                    return [
                        'url' => $url,
                        'attachmentId' => $img->value
                    ];
                }
            }
        } catch (NotFoundHttpException $e) {
            \Yii::error("File not found.");
        }
        if ($noImageIfNotFound) {
            $url = Yii::$app->getUrlManager()->getHostInfo() . '/logos/logo2.jpeg';
            return $url;
        }
        return null;
    }

    /**
     * Return thumbnail image.
     *
     * @return string|array
     *
     * @throws InvalidConfigException
     */
    public function getThumbImage()
    {
        return $this->getImage(true, true, 'small');
    }

    /**
     * Get main video url
     *
     * @return array
     * <code>
     * [
     *  'url' => 'video url,
     *  'extension' => 'video/mp4'
     *  'cover' => 'cover image url',
     * ]
     * </code>
     * @throws NotFoundHttpException
     */
    public function getVideo()
    {
        $video = BusinessMeta::findOne([
            'businessId' => $this->id,
            'key' => self::BUSINESS_META_VIDEO
        ]);
        if ($video) {
            /** @var \aminkt\uploadManager\models\UploadmanagerFiles $file */
            $file = UploadManager::getInstance()->getFile($video->value);
            return [
                'url' => $file->getUrl(),
                'extension' => $file->extension,
                'cover' => null,
                'attachmentId' => $file->id,
            ];
        }
        return [
            'url' => null,
            'extension' => null,
            'cover' => Yii::$app->getModule('uploadManager')->noImage,
            'attachmentId' => null
        ];
    }


    /**
     * Get gallery
     *
     * @param bool $justUrl
     * @return array|null
     * @throws NotFoundHttpException
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getGallery($justUrl = true)
    {
        $gallery = BusinessMeta::findOne([
            'businessId' => $this->id,
            'key' => self::BUSINESS_META_GALLERY
        ]);

        if ($gallery) {
            $items = explode(',', $gallery->value);

            $images = [];

            foreach ($items as $item) {
                if ($justUrl) {
                    $images[] = UploadManager::getInstance()->getFile($item)->getUrl();
                } else {
                    $images[] = [
                        'url' => UploadManager::getInstance()->getFile($item)->getUrl(),
                        'attachmentId' => $item
                    ];
                }

            }

            return $images;
        }

        return null;
    }

    /**
     * @param null $name
     * @param null $description
     *
     * @return $this
     * @throws \RuntimeException if edit model become failed
     */
    public function edit($name = null, $description = null)
    {
        $this->name = $name;
        $this->description = $description;
        if ($this->save())
            return $this;

        \Yii::error($this->getErrors(), self::class);
        throw new \RuntimeException("Business model edit become failed");
    }


    /**
     * Return UpdateTime as Jalali date.
     *
     * @param string $format the format used to convert the value into a date string.
     *                       If null, [[datetimeFormat]] will be used.
     *
     * This can be "short", "medium", "long", or "full", which represents a preset format of different lengths.
     * It can also be a custom format as specified in the [ICU
     * manual](http://userguide.icu-project.org/formatparse/datetime).
     *
     * Alternatively this can be a string prefixed with `php:` representing a format that can be recognized by the
     * PHP [date()](http://php.net/manual/en/function.date.php)-function.
     *
     *
     * @return mixed|null|string
     * @throws InvalidConfigException
     */
    public function getJalaliExpireDate($format = null)
    {
        if (!$this->expireDate) {
            return null;
        }
        return \Yii::$app->getFormatter()->asDatetime($this->updateAt, $format);
    }

    /**
     * Upgrade business paln.
     * This function maybe execute even in first time to buy a new business or to upgrade a plan to another.
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function upgradeBusinessPlan()
    {
        if ($this->status == self::STATUS_NOT_PAYED or $this->status == self::STATUS_EXPIRED) {
            $this->status = self::STATUS_PAYED;
            $expireTimeStamp = $this->plan->duration * (60 * 60 * 24 * 30) + time();
            $this->expireDate = date("Y-m-d H:i:s", $expireTimeStamp);
            if (!$this->save()) {
                throw new \RuntimeException("Cant update business information in buy scenario.");
            }
        }
    }

    /**
     * Get business link
     *
     * @param bool $api
     *
     * @return null|string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getLink($api = true)
    {
        $route = ['/business/view', 'id' => $this->id];

        if ($api) {
            return UrlMaker::to($route);
        } else {
            return Url::to($route);
        }
    }

    /**
     * Can edit business or not by login user.
     *
     * @return bool
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getCanEdit()
    {
        if ($this->ownerId == \Yii::$app->getUser()->getId()) {
            return true;
        }
        return false;
    }

    public function getPaymentLink()
    {
        if ($this->status == self::STATUS_NOT_PAYED or $this->status == self::STATUS_EXPIRED)
            return "http://shahreman.city/gateway/{$this->id}";
        return null;
    }

    /**
     * Get category id
     *
     * @return int|null
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getCatId()
    {
        if ($cache = Yii::$app->getCache()->get('business-' . $this->id . '-category-id')) {
            return $cache;
        }

        $categoryRelation = CategoryRelation::findOne(['businessId' => $this->id]);
        if ($categoryRelation) {
            Yii::$app->getCache()->set('business-' . $this->id . '-category-id', $categoryRelation->catId, 30);
            return $categoryRelation->catId;
        }
        return null;
    }

    /**
     * Return category object of current business.
     *
     * @return Category|null
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function getCategory()
    {
        if ($this->category) {
            return $this->category;
        }

        $category = Category::findOne($this->getCatId());
        if ($category) {
            $this->category = $category;
            return $this->category;
        }

        return null;
    }

    /**
     * Return current business category name.
     *
     * @return string
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function getCategoryName()
    {
        if ($this->getCategory()) {
            return $this->getCategory()->name;
        }

        return '';
    }

    /**
     * Increment like count.
     *
     * @param int $increaseNum How much should increase like count.
     *
     * @return void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function increaseLikeCount($increaseNum = 1)
    {
        $this->likeCount = $this->likeCount ? $this->likeCount + $increaseNum : $increaseNum;
        $this->save(false);
    }

    /**
     * Increment like count.
     *
     * @param int $increaseNum How much should increase seen count.
     *
     * @return void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function increaseSeenCount($increaseNum = 1)
    {
        $this->seenCount = $this->seenCount ? $this->seenCount + $increaseNum : $increaseNum;
        $this->save(false);
    }

    /**
     * Get nearby businesses using procedure
     *
     * @param float $longitude
     * @param float $latitude
     * @param integer $radius In meter. default value is 0 mean all businesses.
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function getNearbyBusinesses($longitude, $latitude, $radius = 0)
    {
        $results = \Yii::$app->db->createCommand("CALL GetNearbyBusinesses(:radius, :longitude, :latitude)", [
            ':radius' => $radius,
            ':longitude' => $longitude,
            ':latitude' => $latitude
        ])->queryAll();
        $businessIds = [];
        foreach ($results as $result) {
            $businessIds[] = $result['bussid'];
        }
        return Business::find()->where(['id' => $businessIds]);
    }

    public function fields()
    {
        $this->getImage();

        $fields = [
            'id',
            'canEdit',
            'name',
            'categoryName',
            'plan',
            'image',
            'thumbImage',
            'gallery',
            'video',
            'address',
            'cityName',
            'description',
            'status',
            'statusLabel',
            'paymentLink',
            'updateAt',
            'createAt',
            'expireDate',
            'ownerName',
            'cityName',
            'link',
            'businessMetas',
            'catId',
            'operatorNote'
        ];

        return $fields;
    }

    /**
     * Changes arabic letters with persian letters
     *
     * @param $attribute
     *
     * @return mixed
     *
     * @author Pooria Anvari <masonalex540@gmail.com>
     */

    public function arabicToPersian($attribute)
    {
        $characters = [
            'ك' => 'ک',
            'دِ' => 'د',
            'بِ' => 'ب',
            'زِ' => 'ز',
            'ذِ' => 'ذ',
            'شِ' => 'ش',
            'سِ' => 'س',
            'ى' => 'ی',
            'ي' => 'ی',
            '١' => '۱',
            '٢' => '۲',
            '٣' => '۳',
            '٤' => '۴',
            '٥' => '۵',
            '٦' => '۶',
            '٧' => '۷',
            '٨' => '۸',
            '٩' => '۹',
            '٠' => '۰',
        ];

        return $this->$attribute = str_replace(array_keys($characters), array_values($characters), $this->$attribute);
    }
}
