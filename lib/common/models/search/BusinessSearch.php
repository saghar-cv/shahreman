<?php

namespace common\models\search;

use backend\components\GlobalComponent;
use common\models\BaseActiveRecord;
use common\models\Business;
use yii\data\ActiveDataProvider;

/**
 * Class PostSearch search model for Business
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessSearch extends Business
{
    public $ownerName;
    public $cities;

    public $filterExpireDate;
    public $filterCreateAt;
    public $filterUpdateAt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'ownerName', 'status', 'ownerId', 'planId', 'expireDate', 'updateAt', 'createAt', 'cities'],
                'safe'],
        ];
    }

    public function afterValidate()
    {
        parent::beforeValidate();

        if ($this->expireDate) {
            $this->filterExpireDate = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->expireDate),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->expireDate . ' 23:59:59'),
            ];
        }
        if ($this->createAt) {
            $this->filterCreateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt . ' 23:59:59'),
            ];
        }
        if ($this->updateAt) {
            $this->filterUpdateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt . ' 23:59:59'),
            ];
        }

    }

    /**
     * Search businesses
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Business::find();
        $query->leftJoin('{{%users}}', '{{%users}}.id = {{%businesses}}.ownerId');
        $query->leftJoin("{{%business_relation}}", '{{%businesses}}.id = {{%business_relation}}.businessId');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC,
                    'updateAt' => SORT_DESC,
                ]
            ]
        ]);

        $accessCities = GlobalComponent::getAccessCities();

        if ($this->cities and (!$accessCities or in_array($this->cities, $accessCities))) {
            $query->andWhere(['{{%business_relation}}.cityId' => $this->cities]);
        } elseif ($accessCities) {
            $query->andWhere(['{{%business_relation}}.cityId' => $accessCities]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%businesses}}.status' => $this->status,
            'planId' => $this->planId,
        ]);

        $query->andFilterWhere(['like', '{{%businesses}}.status', $this->status])
            ->andFilterWhere(['like', '{{%businesses}}.name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'concat({{%users}}.name, " " , {{%users}}.family) ', $this->ownerName]);

        if ($this->cities) {
            $query->andFilterWhere(['{{%business_relation}}.cityId' => $this->cities]);
        }

        if ($this->updateAt) {
            $query->andFilterWhere(['between', '{{%businesses}}.updateAt', $this->filterUpdateAt[0], $this->filterUpdateAt[1]]);
        }

        if ($this->createAt) {
            $query->andFilterWhere(['between', '{{%businesses}}.createAt', $this->filterCreateAt[0], $this->filterCreateAt[1]]);
        }

        if ($this->expireDate) {
            $query->andFilterWhere(['between', '{{%businesses}}.expireDate', $this->filterExpireDate[0], $this->filterExpireDate[1]]);
        }

        return $dataProvider;
    }

}