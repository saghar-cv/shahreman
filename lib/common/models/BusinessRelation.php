<?php

namespace common\models;

use saghar\address\models\City;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "{{%business_relation}}".
 *
 * @property int $businessId
 * @property int $cityId
 *
 * @property City $city
 * @property Business $business
 * @property string $cityName
 *
 *
 * @author Saghar Mojdehi
 *
 * @author Amin Keshavarz <Amin@keshavarz.pro>
 */
class BusinessRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%business_relation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['businessId', 'cityId'], 'required'],
            [['businessId', 'cityId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'businessId' => 'Business ID',
            'cityId' => 'City ID',
        ];
    }

    /**
     * Add a city to business.
     *
     * @param integer $businessId
     * @param integer $cityId
     *
     * @return array|BusinessRelation
     */
    public static function add($businessId, $cityId)
    {
        $relation = new self();
        $relation->businessId = $businessId;
        $relation->cityId = $cityId;

        if (!$relation->save()) {
            return $relation->getErrors();
        }

        return $relation;
    }

    /**
     * Clean business from cities.
     * @param $businessId
     *
     * @throws StaleObjectException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public static function clean($businessId)
    {
        $relations = BusinessRelation::findAll(['businessId' => $businessId]);
        foreach ($relations as $relation) {
            $relation->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'cityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public function getBusiness()
    {
        return $this->hasOne(Business::className(), ['id' => 'cityId']);
    }

    /**
     * Return city name.
     *
     * @return string
     */
    public function getCityName()
    {
        if ($this->city) {
            return $this->city->name;
        }
        return '';
    }
}
