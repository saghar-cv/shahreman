<?php

namespace common\models;

use aminkt\uploadManager\UploadManager;
use api\components\UrlMaker;
use backend\models\Admin;
use backend\models\PostSubPages;
use backend\models\SettingForm;
use saghar\address\models\City;
use saghar\category\models\Category;
use Yii;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii2mod\settings\components\Settings;

/**
 * This is the model class for table "{{%posts}}".
 *
 * @property int $id
 * @property string $title
 * @property int $coverId
 * @property string $slider
 * @property string $content
 * @property string $tags
 * @property int $authorId
 * @property int $cityId
 * @property string $postType
 * @property int $status
 * @property string $updateAt
 * @property string $createAt
 * @property int $likeCount
 * @property int $seenCount
 * @property int $important
 *
 * @property Admin $author
 * @property \backend\models\PostSubPages[] $children
 * @property City $city
 * @property string $authorName
 * @property string $cityName
 * @property string $statusLabel
 * @property string $filteredContent
 * @property string $summary
 * @property array $map
 * @property self|null $parentPosts
 * @property null|array $sliderData
 * @property string $relatedPagesViewType
 * @property null|array $galleryData
 * @property string $gallery
 * @property string|int $newsViewOption
 * @property null $video
 * @property string $css
 * @property Category[] $categories
 *
 *
 * @author Amin Keshavarz <amin@keshavatz.pro>
 * @author Shayan Khalegparast
 * @author Saghar Mojdehi
 * @author Mohammad Parvaneh
 */
class Post extends BaseActiveRecord
{
    const TYPE_POST = 'post';
    const TYPE_STATIC_POST = 'static_post';
    const TYPE_NEWS_POST = 'news_post';

    const META_MAP_STATUS = 'map_status';
    const META_MAP_LATITUDE = 'map_latitude';
    const META_MAP_LONGITUDE = 'map_longitude';
    const META_MAP_ZOOM = 'map_zoom';
    const META_RELATED_PAGES_VIEW_TYPE = 'related_pages_view_type';
    const META_CHILD_PAGES_VIEW_TYPE = 'child_pages_view_type';
    const META_NEWS_VIEW_OPTION = 'news_view_option';
    const META_GALLERY = 'gallery';
    const META_VIDEO = 'video';

    const STATUS_DRAFT = 1;
    const STATUS_PUBLISH = 2;

    const POST_IS_NOT_IMPORTANT = 0;
    const POST_IS_IMPORTANT = 1;

    const STYLE_DEFAULT = 0;
    const STYLE_CLASSIC = 1;
    const STYLE_BIG = 2;
    const STYLE_JUST_PHOTO = 3;
    const STYLE_JUST_TITLE = 4;
    const STYLE_TITLE_AND_DESCRIPTION = 5;

    const DO_NOT_SHOW_TITLE_AND_SUMMERY = 0;
    const SHOW_TITLE_AND_SUMMERY = 1;

    /**
     * Cover picture url.
     *
     * @var null|string
     */
    public $cover = null;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%posts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['coverId', 'authorId', 'cityId', 'status', 'important'], 'integer'],
            [['status'], 'validatePublishAccess'],
            [['content', 'summary'], 'string'],
            [['slider'], 'string'],
            [['cityId'], 'validateCityAccess'],
            [['updateAt', 'createAt', 'cover'], 'safe'],
            [['title', 'tags', 'postType', 'summary'], 'string', 'max' => 191],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::class, 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validateCityAccess($attribute, $params, $validator)
    {
        if ($user = Yii::$app->getUser()->getIdentity() and $user->accessCities) {
            if (!$this->$attribute or empty($this->$attribute)) {
                $this->addError($attribute, 'شما دسترسی ثبت پست برای همه شهر ها را ندارید.');
            } else {
                $cities = explode(',', $user->accessCities);
                if (!in_array($this->$attribute, $cities)) {
                    $this->addError($attribute, 'شهر وارد شده مجاز نیست.');
                }
            }
        }
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validatePublishAccess($attribute, $params, $validator)
    {
        if ($this->$attribute == self::STATUS_PUBLISH and !Yii::$app->getUser()->can("#post-write-edit-publish")) {
            $this->addError($attribute, "شما اجازه انتشار پست را ندارید.");
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'عنوان',
            'coverId' => 'Cover ID',
            'slider' => 'اسلایدر',
            'cover' => 'تصویر شاخص',
            'content' => 'محتوا',
            'summary' => 'خلاصه متن',
            'tags' => 'تگ ها',
            'authorId' => 'نویسنده',
            'cityId' => 'شهر',
            'parentPosts' => 'صفحه والد',
            'postType' => 'نوع پست',
            'status' => 'وضعیت',
            'important' => 'اخبار مهم',
            'updateAt' => 'زمان ویرایش',
            'createAt' => 'زمان ایجاد',
            'cityName' => ' شهر',
            'authorName' => 'نویسنده',
            'statusLabel' => 'وضعیت',
        ];
    }

    public function getTagsList()
    {
        return explode(',', $this->tags);
    }

    /**
     * Return author name.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getAuthorName()
    {
        return $this->author->fullName;
    }

    /**
     * Return city name.
     *
     * @return null|string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getCityName()
    {
        if ($this->city)
            return $this->city->name;
        return null;
    }

    /**
     * return parent name
     *
     * @return string
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function getParentPosts()
    {
        return $this->hasMany(Post::class, ['id' => 'postId'])
            ->viaTable(PostSubPages::tableName(), ['suggestionId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Admin::class, ['id' => 'authorId']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(PostSubPages::class, ['postId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'catId'])
            ->viaTable('{{%category_relation}}', ['postId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'cityId']);
    }

    /**
     * Return status label.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getStatusLabel()
    {
        switch ($this->status) {
            case self::STATUS_PUBLISH:
                return "منتشر شده";
                break;
            case self::STATUS_DRAFT:
                return "پیش نویس";
                break;
            default:
                return "نامشخص";
        }
    }

    /**
     * Return cover photo.
     *
     * @param string|null $size
     *
     * @return null|string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getCover($size = null)
    {
        if ($this->cover)
            return $this->cover;
        try {
            if ($this->coverId) {
                $this->cover = Yii::$app->getModule('uploadManager')->getFile($this->coverId)->getUrl($size);
            }
        } catch (NotFoundHttpException $e) {
            \Yii::error("File not found.");
        }

        return $this->cover;
    }

    /**
     * Return thumbnail image.
     *
     * @return string|array
     */
    public function getThumbCover()
    {
        return $this->getCover('small');
    }

    /**
     * Get page link.
     *
     * @param bool $api
     *
     * @return null|string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getLink($api = true)
    {
        if ($this->postType == self::TYPE_STATIC_POST) {
            $route = ['/page/view', 'id' => $this->id];
        } elseif ($this->postType == self::TYPE_NEWS_POST) {
            $route = ['/news/view', 'id' => $this->id];
        } else {
            return null;
        }

        if ($api) {
            return UrlMaker::to($route);
        } else {
            return Url::to($route);
        }
    }

    /**
     * Return map as meta.
     *
     * @return array
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function getMap()
    {
        $status = PostMeta::get($this->id, self::META_MAP_STATUS);
        $latitude = PostMeta::get($this->id, self::META_MAP_LATITUDE);
        $longitude = PostMeta::get($this->id, self::META_MAP_LONGITUDE);
        $zoom = PostMeta::get($this->id, self::META_MAP_ZOOM);
        if ($status == true and (!$latitude or !$longitude)) {
            $status = false;
        }
        return [
            'status' => $status ? true : false,
            'latitude' => $latitude ? $latitude : null,
            'longitude' => $longitude ? $longitude : null,
            'zoom' => $zoom ? $zoom : 10
        ];

    }

    /**
     * Filter of out put of content filed.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function getFilteredContent()
    {
        $content = $this->content;
        $content =  str_replace(["\n", "\t", "\r"], '', $content);
        return str_replace("&nbsp;", ' ', $content);
    }

    /**
     * get summery of content filed.
     *
     * @return string
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function getFilteredSummary(): string
    {
        if ($summary = $this->summary) {
            return str_replace(["\n", "\t", "\r"], '', $summary);
        }
        $content = $this->content;
        $content = StringHelper::truncate($content, 100);
        $paragraf = preg_split('/\r\n|\r|\n/', $content);
        return $paragraf[0];
    }


    /**
     * Return css object.
     *
     * @return string
     */
    public function getCss()
    {
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        $css = $settings->get(SettingForm::SETTING_SECTION_CONTENT, 'postStyleSheet');
        $style = <<<HTML
<style>{$css}</style>
HTML;
        return $css;
    }


    /**
     * Return slider data.
     */
    public function getSliderData()
    {
        if (!$this->slider) {
            return null;
        }

        $slider = explode(',', $this->slider);

        $slides = [];

        foreach ($slider as $item) {
            $img = UploadManager::getInstance()->getFile($item)->getUrl();

            $slides[] = [
                'image' => $img,
                'title' => $this->title,
                'caption' => StringHelper::truncateWords($this->content, '10'),
                'link' => UrlMaker::to(['/page/view', 'id' => $this->id])
            ];
        }

        return $slides;
    }

    /**
     * Returns gallery data
     *
     * @return array|null
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getGalleryData()
    {
        $meta = PostMeta::get($this->id, self::META_GALLERY);

        if (!$meta) {
            return null;
        }

        $images = explode(',', $meta);

        $gallery = [];

        foreach ($images as $item) {
            $img = UploadManager::getInstance()->getFile($item)->getUrl();

            $gallery[] = $img;
        }

        return $gallery;
    }

    /**
     * Returns gallery attachments id
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getGallery()
    {
        return PostMeta::get($this->id, self::META_GALLERY);
    }

    /**
     * Get video url
     *
     * @return null/array
     * <code>
     * [
     *  'url' => 'video url,
     *  'extension' => 'video/mp4'
     *  'cover' => 'cover image url',
     * ]
     * </code>
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getVideo()
    {
        $video = PostMeta::get($this->id, self::META_VIDEO);
        if (!$video) {
            return null;
        }

        $video = UploadManager::getInstance()->getFile($video);
        return [
            'url' => $video->getUrl(),
            'extension' => $video->extension,
            'cover' => null,
            'attachmentId' => $video->id
        ];
    }

    /**
     * Set post important
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function setImportant()
    {
        if ($this->important == self::POST_IS_NOT_IMPORTANT) {
            $this->important = self::POST_IS_IMPORTANT;
        } else {
            $this->important = self::POST_IS_NOT_IMPORTANT;
        }
        $this->save();
    }

    /**
     * Get related pages view type as meta
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getRelatedPagesViewType()
    {
        $type = PostMeta::get($this->id, self::META_RELATED_PAGES_VIEW_TYPE);

        $parents = $this->parentPosts;
        $parentId = '';
        foreach ($parents as $parent) {
            $parentId = $parent->id;
        }
        $childType = PostMeta::get($parentId, self::META_CHILD_PAGES_VIEW_TYPE);
        /** @var Settings $settings */
        $settings = Yii::$app->settings;
        $defaultType = $settings->get(SettingForm::SETTING_SECTION_CONTENT, 'relatedPagesViewType');


        if ($type != 0) {
            return $type;
        } elseif ($childType != 0) {
            return $childType;
        } elseif ($defaultType != 0) {
            return $defaultType;
        } else {
            return 0;
        }
    }

    public function getChildViewType()
    {

        $childType = PostMeta::get($this->id, self::META_CHILD_PAGES_VIEW_TYPE);
        if ($childType != 0)
            return $childType;
        else {
            return 0;
        }

    }

    /**
     * Get news view on slide show option as meta
     *
     * @return int|string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getNewsViewOption()
    {
        $option = PostMeta::get($this->id, self::META_NEWS_VIEW_OPTION);
        return $option ? $option : 0;
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $this->getCover();

        $fields = [
            'id',
            'title',
            'cover',
            'thumbCover',
            'filteredContent',
            'tagsList',
            'css',
            'sliderData',
            'video',
            'createAt',
            'updateAt',
            'link',
            'filteredSummary',
            'likeCount',
            'seenCount'
        ];

        if ($this->postType == self::TYPE_NEWS_POST) {
            $fields[] = 'categories';
            $fields[] = 'newsViewOption';
        }
        if ($this->postType == self::TYPE_STATIC_POST) {
            $fields[] = 'relatedPagesViewType';
            $fields[] = 'galleryData';
            $fields[] = 'map';
        }

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        // Delete all sub pages data.
        PostSubPages::deleteAll(['postId' => $this->id]);

        // Condition that current post is a sub page
        PostSubPages::deleteAll([
            'suggestionType' => ($this->postType == self::TYPE_STATIC_POST) ? PostSubPages::TYPE_PAGE : PostSubPages::TYPE_NEWS,
            'suggestionId' => $this->id
        ]);

        return parent::beforeDelete();
    }


    /**
     * Increment like count.
     *
     * @param int $increaseNum How much should increase like count.
     *
     * @return void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function increaseLikeCount($increaseNum = 1)
    {
        $this->likeCount = $this->likeCount ? $this->likeCount + $increaseNum : $increaseNum;
        $this->save(false);
    }

    /**
     * Get nearby posts using procedure
     *
     * @param float $longitude
     * @param float $latitude
     * @param integer $radius default value is 0 mean all place. in meter
     *
     * @return \yii\db\ActiveQuery
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function getNearbyPosts($longitude, $latitude, $radius = 0)
    {
        $results = \Yii::$app->db->createCommand("CALL GetNearbyPosts(:latitude, :longitude, :radius)", [
            ':latitude' => $latitude,
            ':longitude' => $longitude,
            ':radius' => $radius,
        ])->queryAll();
        $postIds = [];
        foreach ($results as $result) {
            $postIds[] = $result['id'];
        }
        return Post::find()->where(['id' => $postIds]);
    }

    /**
     * Increment like count.
     *
     * @param int $increaseNum How much should increase seen count.
     *
     * @return void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function increaseSeenCount($increaseNum = 1)
    {
        $this->seenCount = $this->seenCount ? $this->seenCount + $increaseNum : $increaseNum;
        $this->save(false);
    }
}
