<?php

namespace common\models;

use aminkt\normalizer\Normalize;
use aminkt\normalizer\Validation;
use backend\models\PlanAccess;
use Imagine\Exception\InvalidArgumentException;
use saghar\address\models\Address;
use saghar\address\models\City;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\validators\EmailValidator;
use yii\validators\NumberValidator;
use yii\validators\StringValidator;
use yii\validators\UrlValidator;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%business_meta}}".
 *
 * @property int $id
 * @property int $businessId
 * @property string $key
 * @property string $value
 * @property string $updateAt
 * @property string $createAt
 *
 * @property Business $business
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessMeta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%business_meta}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['businessId'], 'integer'],
            [['key'], 'required'],
            [['key',], 'string', 'max' => 191],
            [['value'], 'validateValue'],
            [['businessId'], 'exist', 'skipOnError' => true, 'targetClass' => Business::className(), 'targetAttribute' => ['businessId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'businessId' => 'Business ID',
            'key' => 'Key',
            'value' => 'Value',
            'updateAt' => 'Update At',
            'createAt' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusiness()
    {
        return $this->hasOne(Business::className(), ['id' => 'businessId']);
    }

    /**
     * Add plans information
     *
     * @param integer $businessId
     * @param string $key
     * @param string $value
     *
     * @throws \InvalidArgumentException
     *
     * @return BusinessMeta
     */
    public static function add($businessId, $key, $value)
    {
        $meta = self::findOne([
            'businessId' => $businessId,
            'key' => $key
        ]);
        if (!$meta)
            $meta = new self();
        $meta->businessId = $businessId;
        $meta->key = $key;
        $meta->value = $value;
        if ($meta->save()) {
            Yii::$app->getCache()->delete('business-meta-' . $businessId . '-' . $key);
            Yii::$app->getCache()->set('business-meta-' . $businessId . '-' . $key, $meta->value, 24 * 60 * 60);
            return $meta;
        }
        Yii::error($meta->getErrors(), self::className());
        throw new \InvalidArgumentException("Inputs are not valid");
    }

    /**
     * Return meta value for a business.
     *
     * @param integer $businessId
     * @param string $key
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function get($businessId, $key)
    {
        if ($val = Yii::$app->getCache()->get('business-meta-' . $businessId . '-' . $key)) {
            return $val;
        }

        $meta = BusinessMeta::findOne([
            'businessId' => $businessId,
            'key' => $key
        ]);
        if (!$meta) {
            return null;
        }

        Yii::$app->getCache()->set('business-meta-' . $businessId . '-' . $key, $meta->value, 24 * 60 * 60);
        return $meta->value;
    }

    /**
     * Return meta label for a business.
     *
     * @param integer $businessId
     * @param string $key
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function getLabelString($businessId, $key)
    {
        if ($val = Yii::$app->getCache()->get('business-meta-label-' . $businessId . '-' . $key)) {
            return $val;
        }

        $meta = new self();
        $label = $meta->getLabel($key);
        Yii::$app->getCache()->set('business-meta-label-' . $businessId . '-' . $key, $label, 24 * 60 * 60);
        return $label;
    }


    /**
     * Check if meta key is valid or not.
     *
     * @param string $key
     *
     * @return bool
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function validateMetaKey($key)
    {
        $accesses = PlanAccess::getAccessesNames();
        if (key_exists('access' . $key, $accesses)) {
            return true;
        }
        return false;
    }

    /**
     * Add or edit address
     *
     * @param integer $businessId
     * @param string/integer $value
     * @param string $dataType
     *
     * @return array|BusinessMeta|null
     * @throws NotFoundHttpException
     */
    public static function addAddress($businessId, $value, $dataType)
    {
        $meta = self::findOne([
            'businessId' => $businessId,
            'key' => Business::BUSINESS_META_ADDRESS
        ]);
        if ($meta) {
            $address = Address::findOne($meta->value);
            if (!$address) {
                throw new NotFoundHttpException('آدرس مورد نظر قابل ویرایش نمی باشد');
            }
        } else if ($dataType == 'city') {
            $address = new Address();
        } else {
            throw new InvalidArgumentException('city id is null');
        }

        if ($dataType == 'city') {
            $city = City::findOne($value);
            if ($city) {
                $address->cityId = $value;
                Yii::$app->getCache()->set('cityeName-of-business-' . $businessId, $city->name, 24 * 60 * 60 * 2);
            } else {
                throw new InvalidArgumentException('city id is not valid');
            }
        } elseif ($dataType == 'address') {
            $address->address = $value;
        }
        if (!$address->save()) {
            return $address->getErrors();
        }

        if (!$meta) {
            $meta = self::add($businessId, Business::BUSINESS_META_ADDRESS, (string)$address->id);
        }

        return $meta;
    }

    /**
     * Validate Url
     *
     * @param $url
     *
     * @return bool
     */
    public static function validateUrl($url)
    {
        if (preg_match('/(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/si', $url)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate email
     *
     * @param $email
     *
     * @return bool
     */
    public static function validateEmail($email)
    {
        $validator = new EmailValidator();

        if ($validator->validate($email, $error)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate string for social profile id
     *
     * @param $string
     *
     * @return bool
     */
    public function validateString($string)
    {
        $validator = new StringValidator();

        if ($validator->validate($string, $error)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validate phone
     *
     * @param $phone
     *
     * @return bool
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function validatePhone($phone)
    {
        $validator = new NumberValidator();

        if ($validator->validate($phone, $error)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Custom validator for meta values
     *
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function validateValue($attribute, $params, $validator)
    {
        switch (explode('-', $this->key)[0]) {
            case Business::BUSINESS_META_WEBSITE:
                if (!preg_match('/^(http|https):\/\//si', $this->value)) {
                    $this->value =  'http://' . $this->value;
                }
                if (!self::validateUrl($this->value)) {
                    $this->addError('آدرس وبسایت معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_EMAIL:
                if (!self::validateEmail($this->value)) {
                    $this->addError('ایمیل معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_PHONE:
                if (!self::validatePhone($this->value)) {
                    $this->addError('شماره تلفن معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_FAX:
                if (!self::validatePhone($this->value)) {
                    $this->addError('شماره فکس معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_MOBILE:
                if (!Normalize::normalizeMobile($this->value)) {
                    $this->addError('شماره موبایل معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_SOCIAL_INSTALGRAM:
                if (!$this->validateString($this->value)) {
                    $this->addError('شناسه معتبر نیست');
                }
                break;

            case Business::BUSINESS_META_SOCIAL_TELEGRAM:
                if (!$this->validateString($this->value)) {
                    $this->addError('شناسه معتبر نیست');
                }
                break;

        }
    }

    /**
     * Get meta lables
     *
     * @param null $key
     *
     * @return string
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function getLabel($key = null)
    {
        $key = $key ? $key : $this->key;
        switch (explode('-', $key)[0]) {
            case 'MainImage':
                return 'تصویر شاخص';
            case 'Video':
                return 'ویدئو';
            case 'SocialTelegram':
                return 'لینک تلگرام';
            case 'SocialInstagram':
                return 'لینک اینستاگرام';
            case 'Website':
                return 'آدرس وبسایت';
            case 'Email':
                return 'آدرس ایمیل';
            case 'PhoneNumber':
                return 'تلفن';
            case 'FaxNumber':
                return 'فکس';
            case 'MobileNumber':
                return 'موبایل';
            case 'Address':
                return 'آدرس';
            case 'IOS':
                return 'لینک دانلود اپلیکیشن iOS';
            case 'Android':
                return 'لینک دانلود اپلیکیشن اندروید';
            default:
                return '';
        }
    }


    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function fields()
    {
        return [
            'id',
            'key',
            'label',
            'value',
            'key'
        ];
    }
}
