<?php
namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Most of project ActiveRecords should extend from this class.
 * Class BaseActiveRecord
 * @package common\models
 *
 * @property string jalaliUpdateAt
 * @property string jalaliCreateAt
 */
class BaseActiveRecord extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * Return UpdateTime as Jalali date.
     *
     * @param string $format the format used to convert the value into a date string.
     * If null, [[datetimeFormat]] will be used.
     *
     * This can be "short", "medium", "long", or "full", which represents a preset format of different lengths.
     * It can also be a custom format as specified in the [ICU manual](http://userguide.icu-project.org/formatparse/datetime).
     *
     * Alternatively this can be a string prefixed with `php:` representing a format that can be recognized by the
     * PHP [date()](http://php.net/manual/en/function.date.php)-function.
     *
     *
     * @return mixed|null|string
     */
    public function getJalaliUpdateTime($format = null)
    {
        if ($this->hasAttribute('updateAt')) {
            return \Yii::$app->getFormatter()->asDatetime($this->updateAt, $format);
        }
        return null;
    }

    /**
     * Return CreateTime as Jalali date.
     *
     * @param string $format the format used to convert the value into a date string.
     * If null, [[datetimeFormat]] will be used.
     *
     * This can be "short", "medium", "long", or "full", which represents a preset format of different lengths.
     * It can also be a custom format as specified in the [ICU manual](http://userguide.icu-project.org/formatparse/datetime).
     *
     * Alternatively this can be a string prefixed with `php:` representing a format that can be recognized by the
     * PHP [date()](http://php.net/manual/en/function.date.php)-function.
     *
     *
     * @return mixed|null|string
     */
    public function getJalaliCreateTime($format = null)
    {
        if ($this->hasAttribute('createAt')) {
            return \Yii::$app->getFormatter()->asDatetime($this->createAt, $format);
        }
        return null;
    }

    /**
     * @param string $date
     *
     * @author   Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function convertJalaliDateToSqlDateTime($date)
    {
        $dateTime = explode(' ', $date);

        $date = \IntlCalendar::createInstance(
            'Asia/Tehran',
            'fa_IR@calendar=persian'
        );

        $datePart = explode('-', $dateTime[0]);
        if (count($datePart) != 3) {
            throw new \InvalidArgumentException("Date part should be like 1396-1-22");
        }
        if (isset($dateTime[1])) {
            $timePart = explode(':', $dateTime[1]);
            if (count($datePart) != 3) {
                throw new \InvalidArgumentException("Time part should be like 11:22:33");
            }
            $datePart[3] = ($timePart[0]);
            $datePart[4] = ($timePart[1]);
            $datePart[5] = ($timePart[2]);
        } else {
            $datePart[3] = 0;
            $datePart[4] = 0;
            $datePart[5] = 0;
        }

        if (count($datePart) != 6) {
            throw new \InvalidArgumentException("Date format should be like 1396-1-8 11:22:33");
        }

        $date->set(intval($datePart[0]), intval($datePart[1] - 1), intval($datePart[2]),
            intval($datePart[3]), intval($datePart[4]), intval($datePart[5]));

        $formatter = \Yii::$app->getFormatter();
        $formatter->calendar = \IntlDateFormatter::GREGORIAN;
        $formatter->locale = "en_US@calendar=gregorian";
        $date = \Yii::$app->getFormatter()->asDatetime($date->toDateTime(), 'yyyy-MM-dd HH:mm:ss');
        $formatter->calendar = \IntlDateFormatter::TRADITIONAL;
        $formatter->locale = "fa_IR@calendar=persian";
        return $date;
    }
}