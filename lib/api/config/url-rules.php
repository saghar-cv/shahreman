<?php
return [
    'OPTIONS <route:[a-zA-Z0-9\/-]+>' => '/v1/main/options',
    'GET v1/cities' => '/v1/main/cities',
    'GET v1/active-cities' => '/v1/main/active-cities',
    'GET v1/states' => '/v1/main/states',
    'GET v1/home' => '/v1/main/homepage-data',
    'GET v1/sliders' => '/v1/main/sliders',
    'POST v1/login' => '/v1/main/login',
    'POST v1/login-auth' => '/v1/main/login-authed',
    'POST v1/revoke-token' => '/v1/main/revoke-token',
    'POST v1/user/<mobile:\w+>' => '/v1/main/sign-up',
    'POST v1/user/edit/<mobile:\w+>' => '/v1/main/edit-profile',
    'GET v1/user/profile' => '/v1/main/profile',
    'POST v1/user/delete/<mobile:\w+>' => '/v1/main/delete-user',
    'POST v1/contact' => '/v1/main/contact',

    'GET v1/<controller:(page|news)>' => '/v1/<controller>/index',
    'GET v1/<controller:(page|news)>/<id:\d+>' => '/v1/<controller>/view',
    'GET v1/news/categories' => '/v1/news/categories',
    'GET v1/page/related-pages/<id:\d+>' => '/v1/page/related-pages',
    'GET v1/page/nearby' => '/v1/page/nearby',

    'GET v1/search' => '/v1/search/index',
    'GET v1/search/businesses' => '/v1/search/businesses',
    'GET v1/search/news' => '/v1/search/news',
    'GET v1/search/posts' => '/v1/search/posts',

    'GET v1/business' => '/v1/business/index',
    'GET v1/business/list' => '/v1/business/list',
    'GET v1/business/my' => '/v1/business/my',
    'GET v1/business/plans' => '/v1/business/plans',
    'POST v1/business/register' => '/v1/business/register',
    'GET v1/business/view' => '/v1/business/view',
    'GET v1/business/categories' => '/v1/business/business-categories',
    'POST v1/business/update-info' => '/v1/business/update-info',
    'GET v1/business/edit-data' => '/v1/business/edit-data',
    'GET v1/business/nearby' => '/v1/business/nearby',

    'POST v1/devices/add' => '/v1/device/add',

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => ['v2' => 'v2/home'],
        'pluralize' => false,
        'extraPatterns' => [
            'GET cities' => 'cities',
            'GET states' => 'states',
            'GET home/<city:\d+>' => 'home',
            'GET sliders/<city:\d+>/<place>' => 'sliders',
            'GET special-offers' => 'special-offers',
            'GET special-offers/<cityId:\d+>' => 'special-offers',
            'GET statistics' => 'statistics'
        ]
    ],

    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'v2/page',
        'extraPatterns' => [
            'GET nearby' => 'nearby',
            'GET <id:\d+>/related-pages' => 'related-pages',
            'GET <id:\d+>/like' => 'like'
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'v2/news',
        'extraPatterns' => [
            'GET categories' => 'categories',
            'GET <id:\d+>/like' => 'like'
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'v2/user',
        'extraPatterns' => [
            'GET profile' => 'profile',
            'PUT profile' => 'edit-profile',
            'POST profile/edit' => 'edit-profile',
            'POST login' => 'login',
            'POST register' => 'sign-up',
            'POST login/auth' => 'login-authed',
            'POST revoke-token' => 'revoke-token',
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'controller' => 'v2/business',
        'extraPatterns' => [
            'GET categories' => 'categories',
            'GET plans' => 'plans',
            'GET my' => 'my-businesses',
            'POST <id:\d+>/edit' => 'update',
            'GET nearby/<radius:\d+>/<longitude>/<latitude>' => 'nearby',
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['v2/upload' => 'uploadManager/apiV1/upload'],
        'extraPatterns' => [
            'GET load/<id:\d+>' => 'load',
        ]
    ],

    // Ticket module api

    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['v2/tickets/departments' => 'ticket/department']
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['v2/tickets' => 'ticket/customer'],
        'extraPatterns' => [
            'GET <trackingCode:[a-zA-Z0-9]+>' => 'view',
            'POST <trackingCode:[a-zA-Z0-9]+>' => 'add-message'
        ]
    ],


    // Debug panel
    'debug/default/<action>' => 'debug/default/<action>'
];