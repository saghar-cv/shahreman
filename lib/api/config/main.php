<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'language' => 'en',
    'bootstrap' => ['log'],
//    'catchAll' => ['v2/home/offline'],
    'modules' => [
        'v1' => [
            'basePath' => '@api/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
        'v2' => [
            'basePath' => '@api/modules/v2',
            'class' => 'api\modules\v2\Module'
        ],
        'uploadManager' => [
            'class' => aminkt\uploadManager\UploadManager::className(),
            'uploadPath' => Yii::getAlias("@frontendWeb") . "/upload",
            'uploadUrl' => "/upload",
            'acceptedFiles' => "*",
            'fileIcon' => "/upload/image_not_found.jpg",
            'userClass' => \frontend\models\User::class,
            'noImage' => "/upload/image_not_found.jpg"
        ],
        'notifications' => [
            'class' => 'machour\yii2\notifications\NotificationsModule',
            // Point this to your own Notification class
            // See the "Declaring your notifications" section below
            'notificationClass' => 'backend\models\NotificationHandler',
            // Allow to have notification with same (user_id, key, key_id)
            // Default to FALSE
            'allowDuplicate' => true,
            'userType' => \machour\yii2\notifications\models\Notification::USER_TYPE_ADMIN,
            // This callable should return your logged in user Id
            'userId' => function () {
                return \Yii::$app->user->id;
            }
        ],
        'ticket' => [
            'class' => \aminkt\ticket\Ticket::class,
            'adminModel' => \backend\models\Admin::class,
            'userModel' => \frontend\models\User::class,
        ]
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'on beforeSend' => function ($event) {
                header("Access-Control-Allow-Origin: *");
                header("Access-Control-Allow-Method: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS");
                header("Access-Control-Allow-Headers: content-type, authorization, access-control-allow-headers, access-control-allow-method, access-control-allow-credentials");
                header("Access-Control-Allow-Credentials: true");

                $debugRoute = preg_match('%debug%', Yii::$app->getRequest()->getPathInfo());
                if (!YII_DEBUG_PANEL or !$debugRoute) {
                    $response = $event->sender;
                    if ($response->data !== null) {
                        if (!$response->isSuccessful) {
                            if (isset($response->data['type'])) {
                                unset($response->data['type']);
                            }
                        }
                        if (isset($response->data['data'])) {
                            $response->data['success'] = $response->isSuccessful;
                        } else {
                            $response->data = [
                                'success' => $response->isSuccessful,
                                'data' => $response->data,
                            ];
                        }
                    }
                }
            },
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'frontend\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
            'loginUrl' => null
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '.json',
            'rules' => require('url-rules.php')
        ]
    ],
    'params' => $params,
];