<?php

namespace api\components;

use yii\base\Component;

/**
 * Class UrlMaker
 *
 * @package api\components
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class UrlMaker extends Component
{
    public static function to($route)
    {
        $r = $route[0];
        $prefix = '/';

        if (substr($r, 0, strlen($prefix)) == $prefix) {
            $r = substr($r, strlen($prefix));
        }
        unset($route[0]);
        $route['type'] = $r;
        return $route;
    }
}