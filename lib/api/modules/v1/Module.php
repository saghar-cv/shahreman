<?php

namespace api\modules\v1;

/**
 * Class Module
 * API version 1
 *
 * @package api\modules\v1
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }
}