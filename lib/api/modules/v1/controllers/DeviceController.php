<?php

namespace api\modules\v1\controllers;

use common\models\Device;
use common\models\UserDevice;
use yii\filters\AccessControl;
use yii\rest\Controller;
use yii\web\ServerErrorHttpException;

/**
 * Class DeviceController
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 *
 * @package api\modules\v1\controllers
 */
class DeviceController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['add'],
            'rules' => [
                [
                    'actions' => ['add'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
            ]
        ];

        return $behaviors;
    }


    /**
     * Add or update device
     *
     * @throws ServerErrorHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionAdd()
    {
        $deviceId = \Yii::$app->getRequest()->post('deviceId');
        $device = Device::findOne(['deviceId' => $deviceId]);
        if (!$device) {
            $device = new Device();
            $device->deviceId = $deviceId;
        }
        $device->osVersion = \Yii::$app->getRequest()->post('osVersion');
        $device->osType = \Yii::$app->getRequest()->post('osType');
        $device->operator = \Yii::$app->getRequest()->post('operator');
        $device->pusheId = \Yii::$app->getRequest()->post('pusheId');
        $device->deviceModel = \Yii::$app->getRequest()->post('deviceModel');
        $device->appVersion = \Yii::$app->getRequest()->post('appVersion');

        if (!$device->save()) {
            \Yii::error($device->getErrors());
            \Yii::error($device->getErrors());
            throw new ServerErrorHttpException('Could not save device');
        }
        $userId = \Yii::$app->getRequest()->post('userId');
        if ($userId) {
            $userDevice = UserDevice::findOne([
                'deviceId' => $device->id,
                'userId' => $userId
            ]);

            if (!$userDevice) {
                $userDevice = new UserDevice();
                $userDevice->deviceId = $device->id;
            }
            $userDevice->userId = $userId;
            if (!$userDevice->save()) {
                \Yii::error($userDevice->getErrors());
                throw new ServerErrorHttpException('Could not save user device');
            }
        }
        return $device;
    }

}