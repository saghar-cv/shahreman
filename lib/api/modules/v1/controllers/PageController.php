<?php

namespace api\modules\v1\controllers;

use api\components\UrlMaker;
use common\models\Post;
use saghar\address\models\City;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * Handle page actions of applications.
 *
 * @package api\modules\v1\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class PageController extends \yii\rest\Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * Return list of static posts.
     *
     * @return string
     *
     * @throws NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'status' => Post::STATUS_PUBLISH,
            'postType' => Post::TYPE_STATIC_POST
        ]);

        if ($city = $this->getCity()) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Return city model.
     *
     * @return City|null
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function getCity()
    {
        $city = Yii::$app->getRequest()->get('city');
        if ($city) {
            $city = City::findOne($city);
            if (!$city) {
                throw new NotFoundHttpException("City id is not correct");
            }
        }

        return $city;
    }

    /**
     * Return a post by id.
     *
     * @param integer $id Post id.
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        $model = Post::findOne([
            'id' => $id,
            'postType' => Post::TYPE_STATIC_POST,
            'status' => Post::STATUS_PUBLISH
        ]);

        if (!$model) {
            throw new NotFoundHttpException("Page not found.");
        }

        $model->increaseSeenCount();

        try {
            $related = $this->actionRelatedPages($model->id);
        } catch (NotFoundHttpException $e) {
            $related = null;
        }

        return [
            'item' => $model,
            'related-pages' => $related
        ];
    }

    /**
     * Return list of related pages.
     *
     * @param integer $id Page id.
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionRelatedPages($id)
    {
        $model = Post::findOne([
            'id' => $id,
            'postType' => Post::TYPE_STATIC_POST,
            'status' => Post::STATUS_PUBLISH,
        ]);

        if (!$model) {
            throw new NotFoundHttpException("Page not found.");
        }

        $children = $model->getChildren()->orderBy(['sortNum' => SORT_ASC])->all();
        $parents = $model->getParentPosts()->all();

        if (!$children && !$parents) {
            throw new NotFoundHttpException("No related page found");
        }

        $city = $this->getCity();

        $items = [];
        foreach ($children as $child) {
            $itemModel = $child->getSubPageModel();
            if (!$city || ($city && $itemModel->cityId == $city->id)|| !$itemModel->cityId) {
                if ($child->isPage()) {
                    $items[] = [
                        'title' => $itemModel->title,
                        'createAt' => $itemModel->createAt,
                        'cover' => $itemModel->getCover(),
                        'filteredSummary' => $itemModel->getFilteredSummary(),
                        'summary' => $itemModel->summary,
                        'link' => UrlMaker::to(['page/view', 'id' => $itemModel->id]),
                        'type' => 'page',
                        'relatedPagesViewType' => $itemModel->relatedPagesViewType,
                    ];
                } elseif ($child->isBusinessCategory()) {
                    $items[] = [
                        'title' => $itemModel->name,
                        'createAt' => $itemModel->createAt,
                        'cover' => null,
                        'summary' => StringHelper::truncate($itemModel->description, 100),
                        'link' => UrlMaker::to(['business/list', 'id' => $itemModel->id]),
                        'type' => 'business',
                        'relatedPagesViewType' => Post::STYLE_CLASSIC,
                    ];
                }
            }

        }

        foreach ($parents as $parent) {
            if (!$city || ($city && $parent->cityId == $city->id)) {
                $items[] = [
                    'title' => $parent->title,
                    'createAt' => $parent->createAt,
                    'cover' => $parent->getCover(),
                    'filteredSummary' => $parent->getFilteredSummary(),
                    'summary' => $parent->summary,
                    'link' => UrlMaker::to(['page/view', 'id' => $parent->id]),
                    'type' => 'page',
                    'relatedPagesViewType' => Post::STYLE_JUST_TITLE,
                ];
            }
        }

        return $items;
    }

    /**
     * Get nearby posts
     *
     * @param integer $radius Radius of search in meter
     * @param float $longitude
     * @param float $latitude
     *
     * @return ActiveDataProvider
     *
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionNearby($radius = 0, $longitude, $latitude)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::getNearbyPosts($longitude, $latitude, $radius)
        ]);

        return $dataProvider;
    }
}