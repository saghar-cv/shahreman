<?php

namespace api\modules\v1\controllers;

use aminkt\uploadManager\components\Upload;
use backend\models\CategoryRelation;
use backend\models\Order;
use backend\models\Plan;
use common\models\Business;
use common\models\BusinessMeta;
use common\models\BusinessRelation;
use frontend\models\search\Search;
use frontend\models\User;
use Imagine\Exception\RuntimeException;
use saghar\address\models\Address;
use saghar\address\models\City;
use saghar\category\models\Category;
use Yii;
use yii\base\InvalidArgumentException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class BusinessController
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 *
 * @property null|\saghar\address\models\City $city
 */
class BusinessController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'optional' => ['view', 'plans', 'list', 'index', 'nearby']
        ];

        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['index', 'list', 'view', 'my', 'plans', 'register', 'edit-data', 'nearby'],
            'rules' => [
                [
                    'actions' => ['my', 'register', 'edit-data'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'actions' => ['view', 'plans', 'index', 'list', 'nearby'],
                    'allow' => true,
                ]
            ],
        ];
        return $behaviors;
    }

    /**
     * Category traverse to find business
     *
     * @return \yii\data\ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $searchModel = new Search();

        $depth = \Yii::$app->getRequest()->get('depth');
        isset($depth) ?: $depth = 0;
        $searchModel->depth = $depth;

        $parentId = \Yii::$app->getRequest()->get('parentId');
        isset($parentId) ?: $parentId = null;
        $searchModel->parentId = $parentId;

        $dataProvider = $searchModel->searchCategories(\Yii::$app->request->queryParams);

        return $dataProvider;
    }

    /**
     * List all businesses in specific category.
     *
     * @return \yii\data\ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionList()
    {
        $searchModel = new Search();

        $catId = \Yii::$app->getRequest()->get('catId');
        isset($catId) ?: $catId = null;
        $searchModel->catId = $catId;

        $dataProvider = $searchModel->searchBusiness(\Yii::$app->request->queryParams);
        return $dataProvider;
    }

    /**
     * List of plans
     *
     * @return string
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionPlans()
    {
        $plans = Plan::find()->where(['!=', 'status', Plan::STATUS_REMOVED])->all();
        if (!$plans) {
            throw new NotFoundHttpException('پلانی یافت نشد');
        }
        return $plans;
    }

    /**
     * @param $businessId
     *
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionEditData($businessId)
    {
        $business = Business::findOne($businessId);
        if (!$business) {
            throw new NotFoundHttpException("Business not found");
        }
        if ($business->ownerId != Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("You are not owner of this business.");
        }

        $address = $business->getAddress();
        $edit = [
            'EditBusinessBasic' => [
                'filds' => [
                    [
                        'fildlabel' => $business->getAttributeLabel('name'),
                        'fildkey' => 'name',
                        'fildvalue' => $business->name,
                        'fildtype' => 'textinput',
                        'fildrequire' => true,
                    ],
                    [
                        'fildlabel' => $business->getAttributeLabel('ownerName'),
                        'fildkey' => "ownerName",
                        'fildvalue' => $business->ownerName,
                        'fildtype' => 'textinput',
                        'fildrequire' => true,
                    ],
                    [
                        'fildlabel' => $business->getAttributeLabel('description'),
                        'fildkey' => 'description',
                        'fildvalue' => $business->description,
                        'fildtype' => 'textarea',
                        'fildrequire' => true,
                    ],
                    [
                        'fildlabel' => $business->getAttributeLabel('cities'),
                        'fildkey' => 'cities',
                        'fildvalue' => $business->cities,
                        'fildtype' => 'multispinner',
                        'fildrequire' => true,
                    ],
                    [
                        'fildlabel' => 'شهر',
                        'fildkey' => $business::BUSINESS_META_ADDRESS . "-city",
                        'fildvalue' => ($address) ? $address->city : null,
                        'fildtype' => 'spinner',
                        'fildrequire' => true,
                    ],
                    [
                        'fildlabel' => 'آدرس',
                        'fildkey' => $business::BUSINESS_META_ADDRESS . "-address",
                        'fildvalue' => ($address) ? $address->address : null,
                        'fildtype' => 'textarea',
                        'fildrequire' => false,
                    ],
                    [
                        'fildlabel' => 'دسته',
                        'fildkey' => 'category',
                        'fildvalue' => $business->getCategory(),
                        'fildtype' => 'spinner',
                        'fildrequire' => true,
                    ]
                ]
            ],
        ];

        $accesses = $business->plan->planAccesses;
        $count = 0;
        foreach ($accesses as $access) {
            $count++;
            $name = str_replace('access', '', $access->name);
            switch ($name) {
                case "Map":
                    $edit['EditBusinessMedia']['filds'][0] = [
                        'fildlabel' => $access->getLabel(),
                        'fildkey' => $name,
                        'fildvalue' => [
                            'lat' => ($address = $business->getAddress()) ? $address->latitude : null,
                            'lon' => ($address = $business->getAddress()) ? $address->longitude : null,
                        ],
                        'fildtype' => 'Map',
                        'fildrequire' => false,
                    ];
                    break;
                case "MainImage":
                    $edit['EditBusinessMedia']['filds'][1] = [
                        'fildlabel' => $access->getLabel(),
                        'fildkey' => $name,
                        'fildvalue' => $business->getImage(false, false),
                        'fildtype' => 'Image',
                        'fildrequire' => false,
                    ];
                    break;
                case "Gallery":
                    $edit['EditBusinessMedia']['filds'][2] = [
                        'fildlabel' => $access->getLabel(),
                        'fildkey' => $name,
                        'fildvalue' => $business->getGallery(false),
                        'fildtype' => 'Gallery',
                        'fildrequire' => false,
                    ];
                    break;
                case "Video":
                    $edit['EditBusinessMedia']['filds'][3] = [
                        'fildlabel' => $access->getLabel(),
                        'fildkey' => $name,
                        'fildvalue' => $business->getVideo(),
                        'fildtype' => 'Video',
                        'fildrequire' => false,
                    ];
                    break;
                default:
                    switch ($name) {
                        case "MobileNumber":
                            $index = 1;
                            break;
                        case "PhoneNumber":
                            $index = 2;
                            break;
                        case "FaxNumber":
                            $index = 3;
                            break;
                        case "Email":
                            $index = 40;
                            break;
                        case "Website":
                            $index = 41;
                            break;
                        case "SocialTelegram":
                            $index = 42;
                            break;
                        default:
                            $index = null;
                    }
                    if ($name != "Address") {
                        if ($access->type == 'boolean') {
                            if ($index) {
                                $edit['EditBusinessExtraData']['filds'][$index] = [
                                    'fildlabel' => $business->getMetaLabel($name),
                                    'fildkey' => $name,
                                    'fildvalue' => $business->getMeta($name),
                                    'fildtype' => 'textinput',
                                    'fildrequire' => false,
                                ];
                            } else {
                                $edit['EditBusinessExtraData']['filds'][42 + $count] = [
                                    'fildlabel' => $business->getMetaLabel($name),
                                    'fildkey' => $name,
                                    'fildvalue' => $business->getMeta($name),
                                    'fildtype' => 'textinput',
                                    'fildrequire' => false,
                                ];
                            }
                        } elseif ($access->type == 'integer') {
                            for ($i = 1; $i <= $access->value; $i++) {
                                if ($index) {
                                    $edit['EditBusinessExtraData']['filds'][($index * 10 - 10) + $i - 1] = [
                                        'fildlabel' => $business->getMetaLabel("{$name}-{$i}"),
                                        'fildkey' => "{$name}-{$i}",
                                        'fildvalue' => $business->getMeta("{$name}-{$i}"),
                                        'fildtype' => 'textinput',
                                        'fildrequire' => false,
                                    ];
                                } else {
                                    $edit['EditBusinessExtraData']['filds'][42 + $count + $i] = [
                                        'fildlabel' => $business->getMetaLabel("{$name}-{$i}"),
                                        'fildkey' => "{$name}-{$i}",
                                        'fildvalue' => $business->getMeta("{$name}-{$i}"),
                                        'fildtype' => 'textinput',
                                        'fildrequire' => false,
                                    ];
                                }

                            }
                        }
                    }
            }
        }
        ksort($edit['EditBusinessMedia']['filds']);
        ksort($edit['EditBusinessExtraData']['filds']);
        Yii::warning($edit);
        return $edit;
    }

    /**
     * Return user's businesses list.
     *
     * @return null|ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionMy()
    {
        $userId = \Yii::$app->getUser()->getId();
        $user = User::findOne($userId);
        if ($user) {
            $businesses = Business::find()->where(['ownerId' => $user->id])->andWhere(['!=', 'status', Business::STATUS_REMOVED])->orderBy(['createAt' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $businesses
            ]);
            return $dataProvider;
        }
        return null;
    }

    /**
     * Register business
     *
     * @throws NotFoundHttpException
     * @throws RuntimeException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionRegister()
    {
        $plan = Plan::findOne(\Yii::$app->getRequest()->post('planId'));
        if (!$plan) {
            throw new NotFoundHttpException('Plan does not exist');
        }

        $business = new Business();
        $categoryRelation = new CategoryRelation();

        $business->name = \Yii::$app->getRequest()->post('name');
        $business->description = \Yii::$app->getRequest()->post('description');
        $business->planId = \Yii::$app->getRequest()->post('planId');
        $business->ownerId = \Yii::$app->getUser()->getId();
        $user = User::findOne($business->ownerId);
        if ($user)
            $business->ownerName = $user->name;

        $catId = \Yii::$app->getRequest()->post('catId');
        if (!$catId) {
            throw new InvalidArgumentException('دسته انتخاب نشده');
        }
        if ($business->save()) {
            $categoryRelation->catId = $catId;
            $categoryRelation->businessId = $business->id;
            if ($categoryRelation->save()) {
                try {
                    if ($city = $this->getCity()) {
                        $meta = BusinessMeta::addAddress($business->id, $city->id, 'city');
                    }
                } catch (NotFoundHttpException $e) {
                    \Yii::warning("City not sended to api");
                }

                return $business;
            }
            throw new RuntimeException('Could not save category');
        }

        throw new RuntimeException('Could not save business');
    }

    /**
     * Return city model.
     *
     * @return City|null
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function getCity()
    {
        $city = Yii::$app->getRequest()->get('city');
        if ($city) {
            $city = City::findOne($city);
            if (!$city) {
                throw new NotFoundHttpException("City id is not correct");
            }
        }

        return $city;
    }

    /**
     * View a business page
     *
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionView()
    {
        $business = Business::findOne(\Yii::$app->getRequest()->get('id'));

        if (!$business) {
            throw new NotFoundHttpException('Business does not exist');
        } else if ($business->ownerId != \Yii::$app->getUser()->getId() and
            $business->status != $business::STATUS_CONFIRMED) {
            throw new ForbiddenHttpException('User is not allowed to see this page');
        }

        $business->increaseSeenCount();

        return $business;
    }

    /**
     * Edit or add business information
     *
     * @return array|Business|null
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionUpdateInfo()
    {
        $key = \Yii::$app->getRequest()->post('name');
        $value = \Yii::$app->getRequest()->post('value');

        if (!$key)
            throw new BadRequestHttpException('information is not valid');

        $business = Business::findOne(\Yii::$app->getRequest()->post('businessId'));
        if (!$business)
            throw new NotFoundHttpException('business is not valid');

        switch ($key) {
            case 'name':
                $res = $business->setName($value);
                if (is_array($res)) {
                    \Yii::$app->getResponse()->setStatusCode(400);
                    return $res;
                } else {
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                }
                break;
            case 'description':
                $res = $business->setDescription($value);
                if (is_array($res)) {
                    \Yii::$app->getResponse()->setStatusCode(400);
                    return $res;
                } else {
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                }
                break;
            case 'ownerName':
                $res = $business->setOwnerName($value);
                if (is_array($res)) {
                    \Yii::$app->getResponse()->setStatusCode(400);
                    return $res;
                } else {
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                }
                break;
            case $business::BUSINESS_META_ADDRESS . '-city':
                $meta = BusinessMeta::addAddress($business->id, $value, 'city');
                $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                return $business;
            case $business::BUSINESS_META_ADDRESS . '-address':
                try {
                    $meta = BusinessMeta::addAddress($business->id, $value, 'address');
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                } catch (\InvalidArgumentException $exception) {
                    \Yii::$app->getResponse()->setStatusCode(400);
                    echo "'ابتدا شهر مورد نظر را انتخاب کنید'";
                }
                break;
            case 'MainImage':
                try {
                    if (is_numeric($value)) {
                        BusinessMeta::add($business->id, $key, (string)$value);
                    } else {
                        $file = Upload::directUpload('value');
                        BusinessMeta::add($business->id, $key, (string)$file->id);
                    }
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                } catch (\InvalidArgumentException $exception) {
                    throw new BadRequestHttpException("Input is not valid");
                } catch (\Exception $exception) {
                    throw new ServerErrorHttpException($exception->getMessage());
                }
                break;
            case 'Video':
                try {
                    if (is_numeric($value)) {
                        BusinessMeta::add($business->id, $key, (string)$value);
                    } else {
                        $file = Upload::directUpload('value');
                        BusinessMeta::add($business->id, $key, (string)$file->id);
                    }
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                } catch (\InvalidArgumentException $exception) {
                    throw new BadRequestHttpException("Input is not valid");
                } catch (\Exception $exception) {
                    throw new ServerErrorHttpException();
                }
                break;
            case 'Gallery':
                try {
                    BusinessMeta::add($business->id, $key, (string)$value);
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    return $business;
                } catch (\InvalidArgumentException $exception) {
                    throw new BadRequestHttpException("Input is not valid");
                } catch (\Exception $exception) {
                    throw new ServerErrorHttpException($exception->getMessage());
                }
                break;
            case 'Map':
                $address = Address::findOne($business->getMeta($business::BUSINESS_META_ADDRESS));
                if (!$address) {
                    throw new NotFoundHttpException('آدرس مورد نظر قابل ویرایش نمی باشد');
                }
                $mapPoints = explode(',', $value);
                $address->latitude = $mapPoints[0];
                $address->longitude = $mapPoints[1];
                if (!$address->save()) {
                    throw new RuntimeException('مقدار وارد شده معتبر نیست');
                }
                $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                return $business;
            case 'cities':
                if (!is_array($value)) {
                    $value = explode(',', $value);
                }
                $order = new Order();
                $order->businessId = $business->id;
                $order->planId = $business->planId;
                foreach ($value as $city) {
                    $relation = BusinessRelation::findOne(['cityId' => $city]);
                    if (!$relation) {
                        $order->amount = $order->amount + $business->plan->price;
                        BusinessRelation::add($business->id, $city);
                    }
                }
                if ($order->amount != 0) {
                    if (!$order->save()) {
                        throw new RuntimeException('درخواست مورد نظر ثبت نشد');
                    }
                }
                $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                return $business;
            case 'category':
                $category = Category::findOne($value);
                if (!$category) {
                    throw new NotFoundHttpException('دسته مورد نظر یافت نشد');
                }
                $categoryRelation = CategoryRelation::findOne(['businessId' => $business->id]);
                if (!$categoryRelation) {
                    $categoryRelation = new CategoryRelation();
                    $categoryRelation->businessId = $business->id;
                }

                $categoryRelation->catId = $category->id;
                if (!$categoryRelation->save()) {
                    throw new RuntimeException('مقدار وارد شده معتبر نیست');
                }
                $business = $business->setEditedStatus(Business::BUSINESS_IS_EDITED);
                return $business;
            case 'operatorNote':
                return $business;
            default:
                try {
                    if (BusinessMeta::validateMetaKey($key)) {
                        BusinessMeta::add($business->id, $key, $value);
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                        return $business;
                    } elseif (BusinessMeta::validateMetaKey(explode('-', $key)[0])) {
                        BusinessMeta::add($business->id, $key, $value);
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                        return $business;
                    } else {
                        throw new NotFoundHttpException("کلید یافت نشد.");
                    }
                } catch (\InvalidArgumentException $exception) {
                    throw new BadRequestHttpException("Input is not valid");
                } catch (NotFoundHttpException $exception) {
                    throw $exception;
                } catch (\Exception $exception) {
                    throw new ServerErrorHttpException();
                }
        }
    }

    /**
     * Return business categories.
     *
     * @return array|\yii\db\ActiveRecord[]
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @throws NotFoundHttpException
     */
    public function actionBusinessCategories()
    {
        $categories = Category::find()
            ->innerJoin("{{%category_relation}}", "{{%categories}}.id={{%category_relation}}.catId")
            ->innerJoin("{{%businesses}}", "{{%category_relation}}.businessId={{%businesses}}.id")
            ->where(['!=', '{{%categories}}.status', Category::STATUS_REMOVED])
            ->andWhere([
                "{{%businesses}}.status" => Business::STATUS_CONFIRMED,
                'section' => CategoryRelation::SECTION_BUSINESS,
                'depth' => 0
            ]);
        if ($city = $this->getCity()) {
            $categories->leftJoin("{{%business_relation}}", "{{%businesses}}.id = {{%business_relation}}.businessId")
                ->andWhere([
                    '{{%business_relation}}.cityId' => $city->id
                ]);
        }
        $categories=$categories->all();
        return $categories;
    }

    /**
     * Get nearby businesses
     *
     * @param integer $radius Radius of search in meter
     * @param float $longitude
     * @param float $latitude
     *
     * @return ActiveDataProvider
     *
     *
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionNearby($radius = 0, $longitude, $latitude)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Business::getNearbyBusinesses($longitude, $latitude, $radius)
        ]);

        return $dataProvider;
    }
}