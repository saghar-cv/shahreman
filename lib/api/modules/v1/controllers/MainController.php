<?php

namespace api\modules\v1\controllers;

use aminkt\normalizer\Normalize;
use aminkt\uploadManager\UploadManager;
use backend\models\MobileMenuForm;
use backend\models\MobileSliderFrom;
use backend\models\SettingForm;
use frontend\models\ContactForm;
use frontend\models\SignupForm;
use frontend\models\User;
use saghar\address\models\City;
use saghar\address\models\State;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii2mod\settings\components\Settings;

/**
 * Class MainController
 * Handle main actions of applications.
 *
 * @package api\modules\v1\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class MainController extends \yii\rest\Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::class,
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
            ],
            'actions' => [
                'login' => [
                    'Access-Control-Allow-Credentials' => true,
                ],
                'login-authed' => [
                    'Access-Control-Allow-Credentials' => true,
                ],
                'revoke-token' => [
                    'Access-Control-Allow-Credentials' => true,
                ]
            ]
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['login-authed', 'login', 'revoke-token', 'sign-up', 'delete-user', 'profile', 'options'],
            'except' => ['options'],
            'rules' => [
                [
                    'actions' => ['login-authed', 'login', 'sign-up'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => ['revoke-token', 'profile', 'options'],
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'login-authed' => ['post'],
                'revoke-token' => ['post'],
                'login' => ['post'],
                'sign-up' => ['post'],
                'options' => ['options'],
            ],
        ];
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'only' => ['revoke-token', 'profile'],
            'except' => ['options']
        ];
        return $behaviors;
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    /**
     * Return available cities list to user.
     *
     * @param string $search Search in cities
     * @param boolean $all Return all cities or active cities.
     * @param integer $state State id to return cities of selected state.
     *
     * @deprecated use v2/cities
     *
     * @return array|\yii\db\ActiveRecord[]
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionCities($search = null, $all = false, $state = null)
    {
        $cities = City::find();
        if (!$all) {
            /** @var Settings $settings */
            $settings = \Yii::$app->settings;
            $activeCities = $settings->get(SettingForm::SETTING_SECTION_CONTENT, 'activeCities', "");
            $activeCities = explode(',', $activeCities);
            $cities->orWhere(['id' => $activeCities]);
        }

        if ($state) {
            $cities->andWhere(['stateId' => $state]);
        }

        if ($search) {
            $cities->andWhere(['like', 'name', $search]);
        }
        $cities = $cities->orderBy('name ASC')->all();

        return $cities;
    }

    /**
     * Return list of cities which have post
     *
     * @return array|\yii\db\ActiveRecord[]
     *
     * @deprecated use v2/active-cities
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionActiveCities()
    {
        $cities = City::find();
        $cities->innerJoin('{{%posts}}', '{{%city}}.id = {{%posts}}.cityId');
        $cities->orderBy('name ASC');
        return $cities->all();
    }

    /**
     * Return states list to user.
     *
     * @param string $search Search in states
     *
     * @deprecated use v2/states
     *
     * @return array|\yii\db\ActiveRecord[]
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionStates($search = null)
    {
        $states = State::find();

        if ($search) {
            $states->andWhere(['like', 'name', $search]);
        }
        $states = $states->all();

        return $states;
    }

    /**
     * Return home page data.
     *
     * @return array
     *
     * @deprecated use v2/home
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @throws NotFoundHttpException
     */
    public function actionHomepageData()
    {
        $city = $this->getCity();
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        $settings->invalidateCache();
        $menu = Json::decode($settings->get(MobileMenuForm::SETTING_SECTION, $city->id, "{}"));

        $items = [];
        foreach ($menu as $item) {
            try {
                $img = UploadManager::getInstance()->image($item['icon']);
            } catch (\yii\web\NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->noImage;
            }

            $item['icon'] = $img;

            $items[] = $item;
        }


        return [
            'slider' => $this->actionSliders(),
            'menu' => $items
        ];
    }


    /**
     * Return city model.
     *
     * @return City|null
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function getCity()
    {
        $city = Yii::$app->getRequest()->get('city');
        if ($city) {
            $city = City::findOne($city);
            if (!$city) {
                throw new NotFoundHttpException("City id is not correct");
            }
        }

        return $city;
    }

    /**
     * Return main slider data to user.
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     *
     * @param null|integer $city City id.
     * @param $place
     *
     * @deprecated use /v2/sliders
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSliders($place = 'mainPage')
    {
        $city = $this->getCity();
        $slides = MobileSliderFrom::loadSlider($city->id,$place);
        $items = [];
        foreach ($slides as $item) {
            try {
                $img = UploadManager::getInstance()->image($item['image']);
            } catch (\yii\web\NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->noImage;
            }

            $item['image'] = $img;

            $items[] = $item;
        }
        return $items;
    }

    /**
     * @param string $mobile
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionLogin($mobile)
    {
        $user = User::findByUsername($mobile);
        if ($user) {
            $authCode = mt_rand(10000, 99999);
            $user->authCode = $authCode;

            /** @var \aminkt\sms\yii\SmsComponent $smsComponent */
            $smsComponent = Yii::$app->sms;
            $smsComponent->send([
                'message' => 'رمز عبور شما برای ورود به نرم افزار شهر من : ' . $authCode,
                'numbers' => [$user->mobile]
            ]);
            $user->save();

            return [
                'message' => 'Verification code send to user.'
            ];
        } else {
            throw new NotFoundHttpException("User not found");
        }
    }

    /**
     * Check user auth code and if correct return an auth key.
     *
     * @param string $mobile
     * @param string $code
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionLoginAuthed($mobile, $code)
    {
        $user = User::findByUsername($mobile);
        if ($user) {
            if ($user->authCode == $code) {
                $user->authCode = null;
                $user->generateAuthKey();
                $user->save();

                return [
                    'authKey' => $user->authKey
                ];
            } else {
                throw new BadRequestHttpException("Auth code is not valid.");
            }
        } else {
            throw new NotFoundHttpException("User not found.");
        }
    }

    /**
     * Revoke access token.
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionRevokeToken()
    {
        /** @var \frontend\models\User $user */
        $user = \Yii::$app->getUser()->getIdentity();
        if ($user) {
            $user->generateAuthKey();
            $user->save();
            return [
                'authKey' => $user->authKey
            ];
        } else {
            throw new NotFoundHttpException("User not found.");
        }
    }

    /**
     * Sign up function.
     *
     * @param string $mobile User mobile number.
     *
     * @return array
     * @throws \yii\web\BadRequestHttpException
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionSignUp($mobile)
    {
        $user = User::findByUsername($mobile);
        if ($user) {
            throw new BadRequestHttpException("User registered before.");
        }
        $model = new SignupForm();
        $model->mobile = $mobile;
        if ($model->loadByApi(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return [
                    'message' => "You are registered by {$user->mobile}"
                ];
            } else {
                Yii::$app->getResponse()->setStatusCode(400);
                return [
                    'message' => 'Validation false.',
                    'errors' => $model->getErrors()
                ];
            }
        } else {
            throw new BadRequestHttpException("Post data not defined in request");
        }
    }

    /**
     * Return user profile.
     *
     * @return null|User
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionProfile()
    {
        try {
            /** @var User $user */
            $user = \Yii::$app->getUser()->getIdentity();
        } catch (\Exception $e) {
            throw new NotFoundHttpException("User not found.");
        } catch (\Throwable $e) {
            throw new NotFoundHttpException("User not found.");
        }
        if (!$user) {
            throw new NotFoundHttpException("User not found.");
        }

        return $user;
    }

    /**
     * Edit user profile.
     *
     * @param $mobile
     *
     * @return array|\yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionEditProfile($mobile)
    {
        $user = User::findByUsername($mobile);
        if (!$user) {
            throw new NotFoundHttpException("User not found.");
        }
        if ($user->load(Yii::$app->request->post())) {
            $user->mobile = Normalize::normalizeMobile($user->mobile);
            if (!$user->save()) {
                Yii::$app->getResponse()->setStatusCode(400);
                return [
                    'message' => 'Validation false.',
                    'errors' => $user->getErrors()
                ];
            }
            return [
                'message' => 'User data changed successfully.'
            ];
        }
        return new BadRequestHttpException("Inputs are not valid.");
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     *
     * @throws BadRequestHttpException
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->loadByApi(Yii::$app->request->post())) {
            if ($model->send()) {
                return [
                    'message' => "You're message sent successfully"
                ];
            } else {
                Yii::$app->getResponse()->setStatusCode(400);
                return [
                    'message' => "Fail to verify inputs.",
                    'errors' => $model->getErrors()
                ];
            }
        }

        throw new BadRequestHttpException("Post params not found.");
    }

}