<?php

namespace api\modules\v1\controllers;

use api\components\UrlMaker;
use backend\models\CategoryRelation;
use common\models\Post;
use saghar\address\models\City;
use saghar\category\models\Category;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * Handle news pages actions of applications.
 *
 * @package api\modules\v1\controllers
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class NewsController extends \yii\rest\Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * Show list of newses.
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'postType' => Post::TYPE_NEWS_POST,
            'status' => Post::STATUS_PUBLISH,
        ]);

        if ($city = $this->getCity()) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);

        $sliders = [];
        $news = Post::find()->where(['important' => Post::POST_IS_IMPORTANT]);
        if ($city = $this->getCity()) {
            $news->andWhere([
                'cityId' => $city->id
            ]);
        }
        $news = $news->limit(5)->all();
        if ($news) {
            foreach ($news as $item) {
                $sliders[] = [
                    'image' => $item->getCover(),
                    'title' => $item->title,
                    'caption' => $item->content,
                    'link' => UrlMaker::to(['/news/view', 'id' => $item->id]),
                    'option' => $item->getNewsViewOption()
                ];
            }
        }

        return [
            'sliders' => $sliders,
            'models' => $dataProvider->getModels()
        ];
    }

    /**
     * Return all categories of news.
     *
     * @return array|Post[]|Category[]|\yii\db\ActiveRecord[]
     * @throws NotFoundHttpException
     */
    public function actionCategories()
    {
        $categories = Category::find()
            ->innerJoin("{{%category_relation}}", "{{%categories}}.id={{%category_relation}}.catId")
            ->innerJoin("{{%posts}}", "{{%category_relation}}.postId={{%posts}}.id")
            ->where([
                "{{%posts}}.status" => Post::STATUS_PUBLISH,
                "{{%categories}}.status" => Category::STATUS_ACTIVE,
                'section' => CategoryRelation::SECTION_NEWS,
                'depth' => 0
            ]);
        if ($city = $this->getCity()) {
            $categories->andWhere([
                '{{%posts}}.cityId' => $city->id
            ]);
        }
        $categories=$categories->all();
        return $categories;
    }

    /**
     * Return city model.
     *
     * @return City|null
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    private function getCity()
    {
        $city = Yii::$app->getRequest()->get('city');
        if ($city) {
            $city = City::findOne($city);
            if (!$city) {
                throw new NotFoundHttpException("City id is not correct");
            }
        }

        return $city;
    }

    /**
     * View a news by id.
     *
     * @param integer $id Post id.
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        if ($city = $this->getCity()) {
            $post = Post::findOne([
                'id' => $id,
                'cityId' => $city->id
            ]);
        } else {
            $post = Post::findOne($id);
        }

        if (!$post) {
            throw new NotFoundHttpException("News not found.");
        }

        $post->increaseSeenCount();

        return $post;
    }
}