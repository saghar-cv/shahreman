<?php

namespace api\modules\v1\controllers;

use frontend\models\search\Search;
use yii\data\ActiveDataProvider;
use yii\rest\Controller;

/**
 * Class SearchController
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class SearchController extends Controller
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    /**
     * Main search page.
     *
     * @return array
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $searchModel = new Search();
        $catId = \Yii::$app->getRequest()->get('catId');
        isset($catId) ?: $catId = null;
        $searchModel->catId = $catId;
        $dataProviders = $searchModel->generalSearch(\Yii::$app->request->queryParams);

        return [
            'business' => $dataProviders['business']->getModels(),
            'news' => $dataProviders['news']->getModels(),
            'post' => $dataProviders['post']->getModels(),
            'category' => $dataProviders['category']->getModels()
        ];

    }

    /**
     * Show all searched businesses
     * filter :category,city,faxNumber,phoneNumber,address
     *
     * @return ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @author Mohammad Parvaneh <Mohammad.pvn1375@gmail.com>
     */
    public function actionBusinesses()
    {
        $searchModel = new Search();
        $catId = \Yii::$app->getRequest()->get('catId');
        isset($catId) ?: $catId = null;
        $searchModel->catId = $catId;
        $dataProvider = $searchModel->generalBusinessSearch(\Yii::$app->request->queryParams);
        return $dataProvider;
    }

    /**
     * Show all searched news
     * filter:category,city
     *
     * @return ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @author Mohammad Parvaneh <Mohammmad.pvn1375@gmail.com>
     */
    public function actionNews()
    {
        $searchModel = new Search();
        $catId = \Yii::$app->getRequest()->get('catId');
        isset($catId) ?: $catId = null;
        $searchModel->catId = $catId;
        $dataProvider = $searchModel->newsSearch(\Yii::$app->request->queryParams);

        return $dataProvider;
    }

    /**
     * Show all searched posts
     *
     * @return ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionPosts()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->postSearch(\Yii::$app->request->queryParams);

        return $dataProvider;
    }

}