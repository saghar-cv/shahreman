<?php

namespace api\modules\v2\controllers;


use api\components\UrlMaker;
use common\models\Post;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * @package api\modules\v2\controllers
 */
class PageController extends ActiveRestController
{
    public $modelClass = 'common\models\Post';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => 'yii\filters\HttpCache',
            'only' => ['view'],
            'etagSeed' => function ($action, $params) {
                $business = Post::findOne(\Yii::$app->getRequest()->get('id'));
                return serialize([$business->updateAt, $business->createAt]);
            },
            'lastModified' => function ($action, $params) {
                $business = Post::findOne(\Yii::$app->getRequest()->get('id'));
                return \Yii::$app->formatter->asTimestamp($business->updateAt);
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the default actions
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * Return list of static posts.
     *
     * @return string
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'status' => Post::STATUS_PUBLISH,
            'postType' => Post::TYPE_STATIC_POST
        ]);

        if ($city = $this->city) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);

        return $dataProvider;
    }

    /**
     * Return a post by id.
     *
     * @param integer $id Post id.
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        $model = Post::findOne([
            'id' => $id,
            'postType' => Post::TYPE_STATIC_POST,
            'status' => Post::STATUS_PUBLISH
        ]);

        if (!$model) {
            throw new NotFoundHttpException("Page not found.");
        }

        $model->increaseSeenCount();

        try {
            $related = $this->actionRelatedPages($model->id);
        } catch (NotFoundHttpException $e) {
            $related = null;
        }

        return [
            'item' => $model,
            'relatedPages' => $related
        ];
    }

    /**
     * increase like Count on pressing like button
     *
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function actionLike($id)
    {
        $model = Post::findOne([
            'id' => $id,
            'postType' => Post::TYPE_STATIC_POST,
            'status' => Post::STATUS_PUBLISH
        ]);
        if (!$model) {
            throw new NotFoundHttpException("Page not found.");
        }
        $model->increaseLikeCount();
        return $model;
    }

    /**
     * Return list of related pages.
     *
     * @param integer $id Page id.
     *
     * @return array
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionRelatedPages($id)
    {
        $model = Post::findOne([
            'id' => $id,
            'postType' => Post::TYPE_STATIC_POST,
            'status' => Post::STATUS_PUBLISH,
        ]);

        if (!$model) {
            throw new NotFoundHttpException("Page not found.");
        }

        /** @var Post $children */
        $children = $model->getChildren()->orderBy(['sortNum' => SORT_ASC])->all();
        /** @var Post $parents */
        $parents = $model->getParentPosts()->all();

        if (!$children && !$parents) {
            throw new NotFoundHttpException("No related page found");
        }

        $city = $this->getCity();

        $items = [];
        foreach ($children as $child) {
            /** @var Post $itemModel */
            $itemModel = $child->getSubPageModel();
            if (!$city || ($city && $itemModel->cityId == $city->id) || !$itemModel->cityId) {
                if ($child->isPage()) {
                    $items[] = [
                        'title' => $itemModel->title,
                        'createAt' => $itemModel->createAt,
                        'cover' => $itemModel->getCover(),
                        'filteredSummary' => $itemModel->getFilteredSummary(),
                        'summary' => $itemModel->summary,
                        'link' => UrlMaker::to(['page/view', 'id' => $itemModel->id]),
                        'type' => 'page',
                        'isParent' => false,
                        'relatedPagesViewType' => $itemModel->relatedPagesViewType,
                    ];
                } elseif ($child->isBusinessCategory()) {
                    $items[] = [
                        'title' => $itemModel->name,
                        'createAt' => $itemModel->createAt,
                        'cover' => null,
                        'summary' => StringHelper::truncate($itemModel->description, 100),
                        'link' => UrlMaker::to(['business/list', 'id' => $itemModel->id]),
                        'type' => 'business',
                        'isParent' => false,
                        'relatedPagesViewType' => Post::STYLE_CLASSIC,
                    ];
                }
            }

        }

        foreach ($parents as $parent) {
            if (!$city || ($city && $parent->cityId == $city->id)) {
                $items[] = [
                    'title' => $parent->title,
                    'createAt' => $parent->createAt,
                    'cover' => $parent->getCover(),
                    'filteredSummary' => $parent->getFilteredSummary(),
                    'summary' => $parent->summary,
                    'link' => UrlMaker::to(['page/view', 'id' => $parent->id]),
                    'type' => 'page',
                    'isParent' => true,
                    'relatedPagesViewType' => Post::STYLE_JUST_TITLE,
                ];
            }
        }

        return $items;
    }

    /**
     * Get nearby posts
     *
     * @param integer $radius Radius of search in meter
     * @param float $longitude
     * @param float $latitude
     *
     * @return ActiveDataProvider
     *
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionNearby($radius, $longitude, $latitude)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Post::getNearbyPosts($radius, $longitude, $latitude)
        ]);

        return $dataProvider;
    }

    /**
     * Delete a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Update a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Create a model in database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }
}