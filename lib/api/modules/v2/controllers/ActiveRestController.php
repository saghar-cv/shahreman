<?php

namespace api\modules\v2\controllers;


use saghar\address\models\City;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ActiveRestController
 *
 * @property null|\saghar\address\models\City $city
 *
 * @package api\modules\v2\controllers
 *
 * @author Amin Keshavarz <amin@keshavarz.pro>
 */
class ActiveRestController extends ActiveController
{
    /**
     * @var bool See details {@link \yii\web\Controller::$enableCsrfValidation}.
     */
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \api\components\CustomCorsFilter::class,
            'cors' => [
                // restrict access to
                'Origin' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Request-Headers' => ['content-type', 'authorization', 'accept'],
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
            'except' => ['options'],
            'optional' => ['*']
        ];

        return $behaviors;
    }

    /**
     * Return city model.
     *
     * @return City|null
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    protected function getCity()
    {
        $city = \Yii::$app->getRequest()->get('city');
        if ($city) {
            $city = City::findOne($city);
            if (!$city) {
                throw new NotFoundHttpException("City id is not correct");
            }
        }

        return $city;
    }

    /**
     * List of allowed domains.
     * Note: Restriction works only for AJAX (using CORS, is not secure).
     *
     * @return array List of domains, that can access to this API
     */
    public static function allowedDomains()
    {
        return [
            '*',                        // star allows all domains
            'http://lahijanapp.com',
            'https://lahijanapp.com',
            'http://shahreman.city',
            'https://shahreman.city',
        ];
    }
}