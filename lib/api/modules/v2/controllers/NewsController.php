<?php

namespace api\modules\v2\controllers;


use api\components\UrlMaker;
use backend\models\CategoryRelation;
use common\models\Post;
use saghar\category\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

/**
 * Class NewsController
 * @package api\modules\v2\controllers
 */
class NewsController extends ActiveRestController
{
    public $modelClass = 'common\models\Post';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => 'yii\filters\HttpCache',
            'only' => ['view'],
            'etagSeed' => function ($action, $params) {
                $business = Post::findOne(\Yii::$app->getRequest()->get('id'));
                return serialize([$business->updateAt, $business->createAt]);
            },
            'lastModified' => function ($action, $params) {
                $business = Post::findOne(\Yii::$app->getRequest()->get('id'));
                return \Yii::$app->formatter->asTimestamp($business->updateAt);
            },
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the default actions
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * Show list of newses.
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $query = Post::find()->where([
            'postType' => Post::TYPE_NEWS_POST,
            'status' => Post::STATUS_PUBLISH,
        ]);

        if ($city = $this->city) {
            $query->andWhere([
                'cityId' => $city->id
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);

        $sliders = [];
        $news = Post::find()->where(['important' => Post::POST_IS_IMPORTANT]);
        if ($city = $this->getCity()) {
            $news->andWhere([
                'cityId' => $city->id
            ]);
        }
        $news = $news->limit(5)->all();
        if ($news) {
            foreach ($news as $item) {
                $sliders[] = [
                    'image' => $item->getCover(),
                    'title' => $item->title,
                    'caption' => $item->content,
                    'link' => UrlMaker::to(['/news/view', 'id' => $item->id]),
                    'option' => $item->getNewsViewOption()
                ];
            }
        }

        return [
            'sliders' => $sliders,
            'models' => $dataProvider->getModels()
        ];
    }

    /**
     * View a news by id.
     *
     * @param integer $id Post id.
     *
     * @return string
     *
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionView($id)
    {
        if ($city = $this->city) {
            $post = Post::findOne([
                'id' => $id,
                'cityId' => $city->id
            ]);
        } else {
            $post = Post::findOne($id);
        }

        if (!$post) {
            throw new NotFoundHttpException("News not found.");
        }

        $post->increaseSeenCount();

        return $post;
    }

    /**
     * increase like Count on pressing like button
     *
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function actionLike($id)
    {
        if ($city = $this->city) {
            $post = Post::findOne([
                'id' => $id,
                'cityId' => $city->id
            ]);
        } else {
            $post = Post::findOne($id);
        }
        if (!$post) {
            throw new NotFoundHttpException("Page not found.");
        }
        $post->increaseLikeCount();
        return $post;
    }

    /**
     * Return all categories of news.
     *
     * @return array|Post[]|Category[]|\yii\db\ActiveRecord[]
     */
    public function actionCategories()
    {
        $categories = Category::find()->where([
            'section' => CategoryRelation::SECTION_NEWS,
            'status' => Category::STATUS_ACTIVE,
            'depth' => 0
        ])->all();

        return $categories;
    }

    /**
     * Delete a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Update a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Create a model in database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }
}