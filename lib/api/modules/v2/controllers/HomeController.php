<?php

namespace api\modules\v2\controllers;

use aminkt\uploadManager\UploadManager;
use backend\models\MobileMenuForm;
use backend\models\MobileSliderFrom;
use backend\models\SettingForm;
use common\models\Business;
use common\models\Device;
use common\models\Post;
use frontend\models\User;
use saghar\address\models\City;
use saghar\address\models\State;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii2mod\settings\components\Settings;

/**
 * Class HomeController
 * @package api\modules\v2\controllers
 */
class HomeController extends ActiveRestController
{
    public $modelClass = null;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];


    /**
     * @inheritdoc
     */
    public function init()
    {
        try {
            parent::init();
        } catch (InvalidConfigException $exception) {
            \Yii::error($exception->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        // disable the default actions
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * Catch offline action.
     *
     * @throws \yii\web\NotAcceptableHttpException
     */
    public function actionOffline()
    {
        throw new \yii\web\NotAcceptableHttpException("سرور درحال به روز رسانی میباشد.");
    }

    /**
     * Return home page data.
     *
     * @return array
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     * @throws NotFoundHttpException
     */
    public function actionHome($city)
    {
        $city = $this->getCity();
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        $settings->invalidateCache();
        $menu = Json::decode($settings->get(MobileMenuForm::SETTING_SECTION, $city->id, "{}"));

        $items = [];
        foreach ($menu as $item) {
            try {
                $img = UploadManager::getInstance()->image($item['icon']);
            } catch (\yii\web\NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->noImage;
            }

            $item['icon'] = $img;

            $items[] = $item;
        }


        return $items;
    }

    /**
     * Return main slider data to user.
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     *
     * @param null|integer $city City id.
     * @param $place
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSliders($city, $place = 'mainPage')
    {
        $city = $this->getCity();
        $slides = MobileSliderFrom::loadSlider($city->id, $place);
        $items = [];
        foreach ($slides as $item) {
            try {
                $img = UploadManager::getInstance()->image($item['image']);
            } catch (\yii\web\NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->noImage;
            }

            $item['image'] = $img;

            $items[] = $item;
        }
        return $items;
    }

    /**
     * Return available cities list to user.
     *
     * @param string $search Search in cities
     * @param boolean $all Return all cities or active cities.
     * @param integer $state State id to return cities of selected state.
     *
     * @return array|\yii\db\ActiveRecord[]
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionCities($search = null, $all = false, $state = null)
    {
        $cities = City::find();
        if (!$all) {
            /** @var Settings $settings */
            $settings = \Yii::$app->settings;
            $activeCities = $settings->get(SettingForm::SETTING_SECTION_CONTENT, 'activeCities', "");
            $activeCities = explode(',', $activeCities);
            $cities->orWhere(['id' => $activeCities]);
        }

        if ($state) {
            $cities->andWhere(['stateId' => $state]);
        }

        if ($search) {
            $cities->andWhere(['like', 'name', $search]);
        }
        $cities = $cities->orderBy('name ASC')->all();

        return $cities;
    }

    /**
     * Return states list to user.
     *
     * @param string $search Search in states
     *
     * @return array|\yii\db\ActiveRecord[]
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionStates($search = null)
    {
        $states = State::find();

        if ($search) {
            $states->andWhere(['like', 'name', $search]);
        }
        $states = $states->all();

        return $states;
    }

    /**
     * list of all models from users.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * View a single user.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Delete a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Update a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Create a model in database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Show last five special offers
     *
     * @param null $cityId
     *
     * @return $this|array|\yii\db\ActiveRecord[]
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionSpecialOffers($cityId = null)
    {
        $pages = Post::find()
            ->where(['postType' => Post::TYPE_STATIC_POST])
            ->andWhere(['status' => Post::STATUS_PUBLISH])
            ->andWhere(['important' => Post::POST_IS_IMPORTANT]);

        if ($cityId) {
            $pages->andWhere(['cityId' => $cityId]);
        }

        $pages = $pages->orderBy('createAt DESC')->limit(5)->all();

        return $pages;
    }

    /**
     * list information about app
     *
     * @param string|null $fields
     *
     * @author Mohammad Parvaneh <ak_1596@yahoo.com>
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionStatistics($fields = null)
    {
        $result = [];
        $city = $this->getCity();

        if ($fields) {
            $fields = explode(',', $fields);
        }

        if ($fields === null or array_search('installation', $fields) !== false) {
            $installationStatic = Device::find()->count();
            $result['installation'] = $installationStatic * 27 + 3;
        }

        if ($fields === null or array_search('activeUser', $fields) !== false) {
            $activeUserStatic = User::find()->where([
                'status' => User::STATUS_ACTIVE
            ])->count();
            $result['activeUser'] = $activeUserStatic;
        }

        if ($fields === null or array_search('businessRegistration', $fields) !== false) {
            $businessRegistrationStatic = Business::find()->leftJoin("{{%business_relation}}",
                "{{%businesses}}.id = {{%business_relation}}.businessId");

            if($city) {
                $businessRegistrationStatic->andWhere(["{{%business_relation}}.cityId" => $city->id]);
            }
            $businessRegistrationStatic = $businessRegistrationStatic->count();
            $result['businessRegistration'] = $businessRegistrationStatic;
        }
        if ($fields === null or array_search('activeBusiness', $fields) !== false) {
            $activeBusinessStatic = Business::find()->leftJoin("{{%business_relation}}",
                "{{%businesses}}.id = {{%business_relation}}.businessId");
            if ($city) {
                $activeBusinessStatic->andWhere(["{{%business_relation}}.cityId" => $city->id]);
            }
            $activeBusinessStatic = $activeBusinessStatic->andWhere([
                    'status' => Business::STATUS_CONFIRMED
                ])->count();
            $result['activeBusiness'] = $activeBusinessStatic;
        }

        return $result;
    }

}