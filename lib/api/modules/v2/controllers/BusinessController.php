<?php

namespace api\modules\v2\controllers;

use aminkt\uploadManager\components\Upload;
use backend\models\CategoryRelation;
use backend\models\Order;
use backend\models\Plan;
use common\models\Business;
use common\models\BusinessMeta;
use common\models\BusinessRelation;
use frontend\models\search\Search;
use frontend\models\User;
use saghar\address\models\Address;
use saghar\category\models\Category;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class BusinessController
 *
 * Handle business actions.
 *
 * @package api\modules\v2\controllers
 */
class BusinessController extends ActiveRestController
{
    public $modelClass = Business::class;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => 'yii\filters\HttpCache',
            'only' => ['view'],
            'etagSeed' => function ($action, $params) {
                $business = Business::findOne(\Yii::$app->getRequest()->get('id'));
                if ($business) {
                    return serialize([$business->updateAt, $business->createAt]);
                }
                return;
            },
            'lastModified' => function ($action, $params) {
                $business = Business::findOne(\Yii::$app->getRequest()->get('id'));
                if ($business) {
                    return \Yii::$app->formatter->asTimestamp($business->updateAt);
                }
                return;
            },
        ];
        return $behaviors;
    }

    public function verbs()
    {
        $verbs = parent::verbs();
        $verbs['update'] = ['POST', 'PUT', 'PATCH'];
        return $verbs;
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the default actions
        unset($actions['index'], $actions['view'], $actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * List of all filtered businesses
     *
     * @param integer $catId Filtered content by selected category id.
     *
     * @return \yii\data\ActiveDataProvider
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function actionIndex($catId = null)
    {
        $searchModel = new Search();
        $searchModel->catId = $catId;
        $dataProvider = $searchModel->searchBusiness(\Yii::$app->request->queryParams);
        return $dataProvider;
    }

    /**
     * Category traverse to find business
     *
     * @return \yii\data\ActiveDataProvider
     *
     * @deprecated Use v2/businesses/categories
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionCategories()
    {
        $searchModel = new Search();

        $depth = \Yii::$app->getRequest()->get('depth', 0);
        $searchModel->depth = $depth;

        $parentId = \Yii::$app->getRequest()->get('parentId', null);
        $searchModel->parentId = $parentId;

        $dataProvider = $searchModel->searchCategories(\Yii::$app->request->queryParams);

        return $dataProvider;
    }

    /**
     * View a single business.
     *
     * @param integer $id Business id.
     *
     * @return Business Business model.
     *
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function actionView($id)
    {
        $business = Business::findOne($id);

        if (!$business) {
            throw new NotFoundHttpException('Business does not exist');
        } else if ($business->ownerId != \Yii::$app->getUser()->getId() and
            $business->status != $business::STATUS_CONFIRMED) {
            throw new ForbiddenHttpException('User is not allowed to see this page');
        }

        $business->increaseSeenCount();

        return $business;
    }

    /**
     * List of plans
     *
     * @return string
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionPlans()
    {
        $plans = Plan::find()->where(['!=', 'status', Plan::STATUS_REMOVED])->all();
        if (!$plans) {
            throw new NotFoundHttpException('پلانی یافت نشد');
        }
        return $plans;
    }

    /**
     * Get nearby businesses
     *
     * @param integer $radius Radius of search in meter
     * @param float $longitude
     * @param float $latitude
     *
     * @return ActiveDataProvider
     *
     *
     * @throws \yii\db\Exception
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionNearby($radius, $longitude, $latitude)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Business::getNearbyBusinesses($radius, $longitude, $latitude)
        ]);

        return $dataProvider;
    }

    /**
     * Return user's businesses list.
     *
     * @return null|ActiveDataProvider
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionMyBusinesses()
    {
        $userId = \Yii::$app->getUser()->getId();
        $user = User::findOne($userId);
        if ($user) {
            $businesses = Business::find()->where(['ownerId' => $user->id])->andWhere(['!=', 'status', Business::STATUS_REMOVED])->orderBy(['createAt' => SORT_DESC]);
            $dataProvider = new ActiveDataProvider([
                'query' => $businesses
            ]);
            return $dataProvider;
        }
        return null;
    }


    /**
     * @inheritdoc
     *
     * @param string $action
     * @param Business $model
     * @param array $params
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params);

        if ($action == 'delete' and $model->ownerId != \Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Your access denied.");
        }

        if ($action == 'view' and $model->status != $model::STATUS_CONFIRMED and $model->ownerId != \Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Your access denied.");
        }

        if ($action == 'create' and !\Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Please login first.");
        }

        if ($action == 'update' and \Yii::$app->getUser()->getId() != $model->ownerId) {
            throw new ForbiddenHttpException("Please login first.");
        }
    }

    /**
     * Delete a model from database.
     *
     * @param integer $id Business Id.
     *
     * @return void
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $business = Business::findOne($id);
        $this->checkAccess('delete', $business);
        if (!$business) {
            throw new NotFoundHttpException("Business not found.");
        }
        $business->status = $business::STATUS_REMOVED;
        $business->save();
    }

    /**
     * Edit or add business information
     *
     * @param integer $id Business id.
     *
     * @return array|Business|null
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionUpdate($id)
    {
        $data = \Yii::$app->getRequest()->post();
        if (!$data)
            throw new BadRequestHttpException('information is not valid');

        $business = Business::findOne($id);
        $this->checkAccess('update', $business);

        if (!$business)
            throw new NotFoundHttpException('business is not valid');

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'name':
                    $res = $business->setName($value);
                    if (is_array($res)) {
                        \Yii::$app->getResponse()->setStatusCode(400);
                        return $res;
                    } else {
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    }
                    break;
                case 'description':
                    $res = $business->setDescription($value);
                    if (is_array($res)) {
                        \Yii::$app->getResponse()->setStatusCode(400);
                        return $res;
                    } else {
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    }
                    break;
                case 'ownerName':
                    $res = $business->setOwnerName($value);
                    if (is_array($res)) {
                        \Yii::$app->getResponse()->setStatusCode(400);
                        return $res;
                    } else {
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    }
                    break;
                case $business::BUSINESS_META_ADDRESS . '-city':
                    $meta = BusinessMeta::addAddress($business->id, $value, 'city');
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    break;
                case $business::BUSINESS_META_ADDRESS . '-address':
                    try {
                        $meta = BusinessMeta::addAddress($business->id, $value, 'address');
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    } catch (\InvalidArgumentException $exception) {
                        \Yii::$app->getResponse()->setStatusCode(400);
                        echo "'ابتدا شهر مورد نظر را انتخاب کنید'";
                    }
                    break;
                case 'MainImage':
                    try {
                        if (is_numeric($value)) {
                            BusinessMeta::add($business->id, $key, (string)$value);
                        } else {
                            $file = Upload::directUpload('value');
                            BusinessMeta::add($business->id, $key, (string)$file->id);
                        }
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    } catch (\InvalidArgumentException $exception) {
                        throw new BadRequestHttpException("Input is not valid");
                    } catch (\Exception $exception) {
                        throw new ServerErrorHttpException($exception->getMessage());
                    }
                    break;
                case 'Video':
                    try {
                        if (is_numeric($value)) {
                            BusinessMeta::add($business->id, $key, (string)$value);
                        } else {
                            $file = Upload::directUpload('value');
                            BusinessMeta::add($business->id, $key, (string)$file->id);
                        }
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    } catch (\InvalidArgumentException $exception) {
                        throw new BadRequestHttpException("Input is not valid");
                    } catch (\Exception $exception) {
                        throw new ServerErrorHttpException();
                    }
                    break;
                case 'Gallery':
                    try {
                        BusinessMeta::add($business->id, $key, (string)$value);
                        $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    } catch (\InvalidArgumentException $exception) {
                        throw new BadRequestHttpException("Input is not valid");
                    } catch (\Exception $exception) {
                        throw new ServerErrorHttpException($exception->getMessage());
                    }
                    break;
                case 'Map':
                    $address = Address::findOne($business->getMeta($business::BUSINESS_META_ADDRESS));
                    if (!$address) {
                        throw new NotFoundHttpException('آدرس مورد نظر قابل ویرایش نمی باشد');
                    }
                    $mapPoints = explode(',', $value);
                    $address->latitude = $mapPoints[0];
                    $address->longitude = $mapPoints[1];
                    if (!$address->save()) {
                        throw new BadRequestHttpException('مقدار وارد شده معتبر نیست');
                    }
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    break;
                case 'cities':
                    if (!is_array($value)) {
                        $value = explode(',', $value);
                    }
                    BusinessRelation::clean($business->id);
                    $order = Order::findOne(['businessId' => $business->id]);
                    if (!$order) {
                        $order = new Order();
                        $order->businessId = $business->id;
                        $order->planId = $business->planId;
                    }
                    $order->amount = count($value) * $business->plan->price;
                    if (!$order->save()) {
                        throw new ServerErrorHttpException('درخواست مورد نظر ثبت نشد');
                    }
                    foreach ($value as $city) {
                        BusinessRelation::add($business->id, $city);
                    }
                    $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                    break;
                case 'category':
                    $category = Category::findOne($value);
                    if (!$category) {
                        throw new NotFoundHttpException('دسته مورد نظر یافت نشد');
                    }
                    $categoryRelation = CategoryRelation::findOne(['businessId' => $business->id]);
                    if (!$categoryRelation) {
                        $categoryRelation = new CategoryRelation();
                        $categoryRelation->businessId = $business->id;
                    }

                    $categoryRelation->catId = $category->id;
                    if (!$categoryRelation->save()) {
                        throw new BadRequestHttpException('مقدار وارد شده معتبر نیست');
                    }
                    $business = $business->setEditedStatus(Business::BUSINESS_IS_EDITED);
                    break;
                case 'operatorNote':
                    break;
                default:
                    try {
                        if (BusinessMeta::validateMetaKey($key)) {
                            BusinessMeta::add($business->id, $key, $value);
                            $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                        } elseif (BusinessMeta::validateMetaKey(explode('-', $key)[0])) {
                            BusinessMeta::add($business->id, $key, $value);
                            $business = $business->setEditedStatus($business::BUSINESS_IS_EDITED);
                        } else {
                            throw new NotFoundHttpException("کلید یافت نشد.");
                        }
                    } catch (\InvalidArgumentException $exception) {
                        throw new BadRequestHttpException("Input is not valid");
                    } catch (NotFoundHttpException $exception) {
                        throw $exception;
                    } catch (\Exception $exception) {
                        throw new ServerErrorHttpException();
                    }
            }
        }

        return $business;
    }


    /**
     * Register business
     *
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     * @throws ServerErrorHttpException
     * @throws ForbiddenHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionCreate()
    {
        $this->checkAccess('create');
        $plan = Plan::findOne(\Yii::$app->getRequest()->post('planId'));
        if (!$plan) {
            throw new NotFoundHttpException('Plan does not exist');
        }

        $business = new Business();
        $categoryRelation = new CategoryRelation();

        $business->name = \Yii::$app->getRequest()->post('name');
        $business->description = \Yii::$app->getRequest()->post('description');
        $business->planId = \Yii::$app->getRequest()->post('planId');
        $business->ownerId = \Yii::$app->getUser()->getId();
        $user = User::findOne($business->ownerId);
        if ($user)
            $business->ownerName = $user->fullName;

        $catId = \Yii::$app->getRequest()->post('catId');
        if (!$catId) {
            throw new BadRequestHttpException('دسته انتخاب نشده');
        }
        if ($business->save()) {
            $categoryRelation->catId = $catId;
            $categoryRelation->businessId = $business->id;
            if ($categoryRelation->save()) {
                try {
                    if ($city = $this->getCity()) {
                        $meta = BusinessMeta::addAddress($business->id, $city->id, 'city');
                    }
                } catch (NotFoundHttpException $e) {
                    \Yii::warning("City not defined.");
                }

                return $business;
            }
            throw new ServerErrorHttpException('Could not save category');
        }

        throw new ServerErrorHttpException('Could not save business');
    }
}