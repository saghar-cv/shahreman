<?php

namespace api\modules\v2\controllers;


use aminkt\normalizer\Normalize;
use frontend\models\SignupForm;
use frontend\models\User;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class UserController
 * @package api\modules\v2\controllers
 */
class UserController extends ActiveRestController
{
    public $modelClass = User::class;
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'data',
    ];


    public function actions()
    {
        $actions = parent::actions();

        // disable the default actions
        unset($actions['index'], $actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * Request to send auth code to user mobile for login process.
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionLogin()
    {
        $mobile = Yii::$app->getRequest()->post('mobile');
        $user = User::findByUsername($mobile);
        if ($user) {
            $authCode = mt_rand(10000, 99999);
            $user->authCode = $authCode;

            /** @var \aminkt\sms\yii\SmsComponent $smsComponent */
            $smsComponent = Yii::$app->sms;
            $smsComponent->send([
                'message' => 'رمز عبور شما برای ورود به نرم افزار شهر من : ' . $authCode,
                'numbers' => [$user->mobile]
            ]);
            $user->save();

            return [
                'message' => 'Verification code send to user.'
            ];
        } else {
            throw new NotFoundHttpException("User not found");
        }
    }

    /**
     * Check user auth code and if correct return an auth key.
     *
     * @return array
     *
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\NotFoundHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionLoginAuthed(): array
    {
        $mobile = Yii::$app->getRequest()->post('mobile');
        $code = Yii::$app->getRequest()->post('code');

        $user = User::findByUsername($mobile);
        if ($user) {
            if ($user->authCode == $code) {
                $user->authCode = null;
                $user->generateAuthKey();
                $user->save();

                return [
                    'authKey' => $user->authKey
                ];
            } else {
                throw new BadRequestHttpException("Auth code is not valid.");
            }
        } else {
            throw new NotFoundHttpException("User not found.");
        }
    }

    /**
     * Revoke access token.
     *
     * @return array
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionRevokeToken(): array
    {
        $this->checkAccess('revoke-token');
        /** @var \frontend\models\User $user */
        $user = \Yii::$app->getUser()->getIdentity();
        if ($user) {
            $user->generateAuthKey();
            $user->save();
            return [
                'authKey' => $user->authKey
            ];
        } else {
            throw new NotFoundHttpException("User not found.");
        }
    }

    /**
     * Sign up function.
     *
     * @return array
     *
     * @throws \yii\web\BadRequestHttpException
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionSignUp(): array
    {
        $mobile = Yii::$app->getRequest()->post('mobile');
        $user = User::findByUsername($mobile);
        if ($user) {
            throw new BadRequestHttpException("User registered before.");
        }
        $model = new SignupForm();
        $model->mobile = $mobile;
        if ($model->loadByApi(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return [
                    'message' => "You are registered by {$user->mobile}"
                ];
            } else {
                Yii::$app->getResponse()->setStatusCode(400);
                return [
                    'message' => 'Validation false.',
                    'errors' => $model->getErrors()
                ];
            }
        } else {
            throw new BadRequestHttpException("Post data not defined in request");
        }
    }

    /**
     * Return user profile.
     *
     * @return null|User
     *
     * @throws \yii\web\ServerErrorHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws ForbiddenHttpException
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionProfile()
    {
        $this->checkAccess('profile');
        try {
            /** @var User $user */
            $user = \Yii::$app->getUser()->getIdentity();
        } catch (\Exception $e) {
            throw new ServerErrorHttpException("User not found.");
        } catch (\Throwable $e) {
            throw new ServerErrorHttpException("User not found.");
        }
        if (!$user) {
            throw new NotFoundHttpException("User not found.");
        }

        return $user;
    }

    /**
     * Edit user profile.
     *
     * @return array
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function actionEditProfile()
    {
        $this->checkAccess('edit-profile');
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        if (!$user) {
            throw new NotFoundHttpException("User not found.");
        }
        if ($user->load(Yii::$app->request->post())) {
            $user->mobile = Normalize::normalizeMobile($user->mobile);
            if (!$user->save()) {
                Yii::$app->getResponse()->setStatusCode(400);
                return [
                    'message' => 'Validation false.',
                    'errors' => $user->getErrors()
                ];
            }
            return [
                'message' => 'User data changed successfully.'
            ];
        }
        throw new BadRequestHttpException("Inputs are not valid.");
    }

    /**
     * @inheritdoc
     *
     * @param string $action
     * @param User $model
     * @param array $params
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        parent::checkAccess($action, $model, $params);

        if ($action == 'profile' and !\Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Your access denied.");
        }

        if ($action == 'edit-profile' and !\Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Your access denied.");
        }

        if ($action == 'revoke-token' and !\Yii::$app->getUser()->getId()) {
            throw new ForbiddenHttpException("Please login first.");
        }
    }

    /**
     * list of all models from users.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * View a single user.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Delete a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Update a model from database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }

    /**
     * Create a model in database.
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     *
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        throw new NotFoundHttpException("This action is not valid.");
    }
}