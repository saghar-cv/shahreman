<?php

namespace api\modules\v2;

/**
 * Class Module
 * API version 2
 *
 * @package api\modules\v2
 *
 * @author  Amin Keshavarz <amin@keshavarz.pro>
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v2\controllers';

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }
}