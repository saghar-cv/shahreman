<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \backend\models\NewsForm */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="post-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="container-fluid">
        <div class="col col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> محتوا</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'summary')->textarea(['rows' => 2, 'maxlength' => true]); ?>
                        <?=
                        $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                            'options' => ['rows' => 10],
//                                'preset' => 'custom',
                            'clientOptions' => [
                                'allowedContent' => true,
                                'extraPlugins' => 'glyphicons,widget,lineutils,widgetselection',
                                'contentsCss' => Url::base(true).'/ckeditor-plugins/glyphicons/css/css/bootstrap.css',
                                'toolbarGroups' => [
//                                        ['name' => 'undo'],
                                    ['name' => 'insert', 'groups' => [ 'glyphicons', 'Source' ]],
//                                        ['name' => 'paragraph', 'groups' => ['list']],
//                                        ['name' => 'basicstyles'],
//                                        ['name' => 'links', 'groups' => ['links', 'insert']],
                                ],
//                                    'removeButtons' => 'Anchor,Subscript,Superscript,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
//                                    'removePlugins' => 'elementspath',
                                'language' => 'fa'
                            ]
                        ]);
                        $this->registerJs("CKEDITOR.plugins.addExternal('glyphicons', '".Url::base(true)."/ckeditor-plugins/glyphicons/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('widget', '".Url::base(true)."/ckeditor-plugins/widget/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('lineutils', '".Url::base(true)."/ckeditor-plugins/lineutils/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('widgetselection', '".Url::base(true)."/ckeditor-plugins/widgetselection/plugin.js', '');");
                        $this->registerJs('CKEDITOR.dtd.$removeEmpty["span"] = false;');
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> انتشار</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <?= $form->field($model, 'status')->dropDownList([
                            \common\models\Post::STATUS_PUBLISH => 'انتشار',
                            \common\models\Post::STATUS_DRAFT => 'پیش نویس',
                        ])->label('وضعیت انتشار') ?>
                    </div>
                    <div class="form-group">
                        <?php
                        $cities = \saghar\address\models\City::find();
                        $accessCities = \Yii::$app->getUser()->getIdentity()->accessCities;
                        if ($accessCities) {
                            $cities = $cities->where(['id' => explode(',', $accessCities)]);
                        }
                        $cities = $cities->all();
                        $cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
                        if ($currentCity = \backend\components\GlobalComponent::getCurrentCity() and !$model->cityId) {
                            $model->cityId = $currentCity->id;
                        }
                        echo $form->field($model, 'cityId')->widget(\kartik\select2\Select2::className(), [
                            'data' => $cities,
                            'options' => ['placeholder' => 'شهر را انتخاب کنید ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'newsViewOption')->radioList([
                            \common\models\Post::SHOW_TITLE_AND_SUMMERY => 'نمایش',
                            \common\models\Post::DO_NOT_SHOW_TITLE_AND_SUMMERY => 'عدم نمایش'
                        ])->label('استایل عنوان و خلاصه خبر به روی عکس اسلایدشو') ?>
                    </div>
                    <div class="form-group ">
                        <?= Html::submitButton('ثبت پست', ['class' => 'btn btn-success save-post-btn', 'style' => 'float: left;']) ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> دسته و برچسب</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <?php
                        $categories = \saghar\category\models\Category::find()
                            ->where(['!=', 'status', \saghar\category\models\Category::STATUS_REMOVED])
                            ->andWhere(['=', 'section', \backend\models\CategoryRelation::SECTION_NEWS])
                            ->all();
                        $data = \yii\helpers\ArrayHelper::map($categories, 'id', 'name', 'parentName');
                        echo $form->field($model, 'categories')->widget(\kartik\select2\Select2::className(), [
                            'data' => $data,
                            'options' => ['placeholder' => 'دسته های پست را انتخاب کنید ...', 'multiple' => true],
                            'pluginOptions' => [
                                'tags' => true,
                                'tokenSeparators' => [',', ' '],
                                'maximumInputLength' => 10
                            ],
                        ]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'tags')->widget("aminkt\widgets\inputTag\InputTag", [
                            'options' => [
                                'maxlenth' => true,
                                'class' => 'form-control maxlength-handler'
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">تصویر شاخص</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group main-image-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-user-pic',
                            'model' => $model,
                            'attribute' => 'coverId',
                            'titleTxt' => 'تصویر شاخص را  کنید.',
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#coverId',
                            'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                            'btnTxt' => 'جایگذاری تصویر شاخص'
                        ]);
                        ?>
                        <div id="coverId"></div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">ویدئو</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group video-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-video',
                            'model' => $model,
                            'attribute' => 'video',
                            'titleTxt' => 'ویدئو را وارد کنید.',
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#video_container',
                            'showImagesTemplate' => "<video width='100%' height='300px' controls><source src='{url}' type='video/{file_extension}'>سیستم شما از ویدیو پشتیبانی نمیکند. </video>",
                            'btnTxt' => 'جایگذاری ویدئو'
                        ]);
                        ?>
                        <div id="video_container"></div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
