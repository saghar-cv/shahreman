<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\search\PostSearch */

$this->title = 'اخبار';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <p>
        <?= Html::a('ایجاد خبر', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'important',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->important == \common\models\Post::POST_IS_IMPORTANT) {
                        return Html::a('<i class="icon-check"></i>', ['set-important', 'id' => $model->id]);
                    } else {
                        return Html::a('<i class="icon-star"></i>', ['set-important', 'id' => $model->id]);
                    }
                },
            ],
            [
                'attribute'=>'title',
                'contentOptions'=>['style'=>'width: 25%;']
            ],
            'authorName',
            'cityName',
            'tags',
            [
                'label' => 'وضعیت',
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                },
                'filter' => [
                    \common\models\Post::STATUS_DRAFT => 'پیش نویس',
                    \common\models\Post::STATUS_PUBLISH => 'منتشر شده',
                ],
            ],
            [
                'attribute' => 'updateAt',
                'format' => 'dateTime',
                'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updateAt',
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy/mm/dd',
                        'placement' => 'left',
                        'todayBtn' => 'linked',
                        'class' => 'form-control',
                    ),
                ])
            ],
            [
                'attribute' => 'createAt',
                'format' => 'dateTime',
                'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createAt',
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy/mm/dd',
                        'placement' => 'left',
                        'todayBtn' => 'linked',
                        'class' => 'form-control',
                    ),
                ])
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<i class="icon-pencil"></i>', $url,[
                            'title' => 'ویرایش',
                            'class' => 'btn btn-primary'
                        ]);
                    },
                    'view' => function ($url, $model, $key) {
                        return Html::a('<i class="icon-size-fullscreen"></i>', $url);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<i class="icon-trash"></i>', $url, [
                            'data-confirm' => Yii::t('app', 'آیا مطمئن به حذف خبر هستید؟'),
                            'data-method' => 'post',
                            'title' => 'حذف',
                            'class' => 'btn btn-danger'
                        ]);
                    },
                ],
                'template' => '<div class="btn-group" role="group" aria-label="عملیات">{update}{delete}</div>',
                'contentOptions'=>['style'=>'width: 113px;']
            ],
        ],
    ]); ?>
</div>
