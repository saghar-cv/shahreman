<?php
/**
 * Created by PhpStorm.
 * User: Mohammad_Pvn
 * Date: 3/14/2018
 * Time: 4:01 PM
 */

use aminkt\ticket\models\Department;

/** @var $this \yii\web\View */
/** @var $model \backend\models\SettingForm */
$this->title = 'تنظیمات';
?>
<style>
    .code {
        height: 40%;
        width: 100%;
        border: #afafaf solid 1px;
        padding: 10px 2px;
        border-radius: 4px;
        text-align: left;
        direction: ltr;
    }


</style>

<div class="panel panel-default">

    <div class="panel-body">
        <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>

        <?php
        $departments = Department::find()->where(['status' => Department::STATUS_ACTIVE])->all();
        $departments = \yii\helpers\ArrayHelper::map($departments, 'id', 'name');
        echo $form->field($model, 'contactDefaultDepartment')->dropDownList($departments);
        ?>


        <?php
        $cities = \saghar\address\models\City::find()->all();
        $data = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
        echo $form->field($model, 'activeCities')->widget(\kartik\select2\Select2::className(), [
            'data' => $data,
            'options' => ['placeholder' => 'شهر های فعال را انتخاب کنید ...', 'multiple' => true],
            'pluginOptions' => [
                'tags' => true,
                'tokenSeparators' => [',', ' '],
                'maximumInputLength' => 10
            ],
        ]) ?>


        <?php
            $style = [
                \common\models\Post::STYLE_CLASSIC => 'معمولی (تصویر - عنوان - توضیحات)',
                \common\models\Post::STYLE_BIG => 'ویژه (تصویر بزرگ - عنوان - توضیحات)',
                \common\models\Post::STYLE_JUST_PHOTO => 'فقط تصویر',
                \common\models\Post::STYLE_JUST_TITLE => 'فقط عنوان',
                \common\models\Post::STYLE_TITLE_AND_DESCRIPTION => 'عنوان - توضیحات'
            ];

            echo $form->field($model, 'relatedPagesViewType')->widget(\kartik\select2\Select2::className(), [
                'data' => $style,
                'options' => ['placeholder' => 'استایل را انتخاب کنید ...'],
                'pluginOptions' => [
                'allowClear' => true
                ],
            ])->label('استایل پیش فرض نمایش مطالب مرتبط در اپ')
        ?>


        <?= $form->field($model, 'postStyleSheet')
            ->widget(bl\ace\AceWidget::class, [
                'language' => 'css',
                'theme' => 'xcode',
                'attributes' => ['class' => 'code']
            ]) ?>

        <?= \yii\helpers\Html::submitButton("ثبت", [
            'class' => 'btn btn-primary'
        ]) ?>
        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
</div>
