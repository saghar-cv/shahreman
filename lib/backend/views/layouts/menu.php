<?php

use yii\helpers\Url;

function setActive($pages, $arrow = false)
{
    $r = Yii::$app->requestedRoute;
    if (in_array($r, $pages) or in_array($r . '/index', $pages) or (empty($r) and in_array(Yii::$app->defaultRoute . '/index', $pages)))
        if ($arrow)
            return "open";
        else
            return "active open";
    return "";
}

$role = Yii::$app->authManager->getRolesByUser(Yii::$app->getUser()->id);

$editor = isset($role['editor']);
$author = isset($role['author']);
$contentManager = isset($role['contentManager']);
$finance = isset($role['finance']);
$businessManager = isset($role['businessManager']);
$admin = isset($role['admin']);
?>

<!-- BEGIN SIDEBAR -->
<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing  -->
<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start <?= setActive(['site/index']) ?>">
            <a href="<?= Url::to(['/site/index']) ?>" class="nav-link">
                <i class="icon-home"></i>
                <span class="title">میزکار</span>
            </a>
        </li>
        <!-- BLOG -->
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'page', 'page/index', 'page/create', 'page/update',
            'news', 'news/index', 'news/create', 'news/update',
            'post-categories/index', 'post-categories'
        ])) : ?>
            <li class="heading">
                <h3 class="uppercase">مدیریت محتوا</h3>
            </li>
        <?php endif; ?>
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'page', 'page/index', 'page/create', 'page/update',
        ])) : ?>
            <li class="nav-item <?= setActive(['page', 'page/index', 'page/create', 'page/update']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-doc"></i>
                    <span class="title">صفحات</span>
                    <span
                            class="arrow <?= setActive(['page', 'page/index', 'page/create', 'page/update'], true) ?>"></span>
                </a>
                <ul class="sub-menu">
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['page/create'])) : ?>
                        <li class="nav-item <?= setActive(['page/create']) ?> ">
                            <a href="<?= Url::to(['/page/create']) ?>" class="nav-link ">
                                <span class="title">صفحه جدید</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['page', 'page/index'])) : ?>
                        <li class="nav-item <?= setActive(['page', 'page/index']) ?> ">
                            <a href="<?= Url::to(['/page/index']) ?>" class="nav-link ">
                                <span class="title">لیست صفحات</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        <?php endif; ?>
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'news', 'news/index', 'news/create', 'news/update',
            'post-categories/index', 'post-categories'
        ])) : ?>
            <li class="nav-item <?= setActive(['news', 'news/index', 'news/create', 'news/update']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-globe"></i>
                    <span class="title">اخبار</span>
                    <span
                            class="arrow <?= setActive(['news', 'news/index', 'news/create', 'news/update'], true) ?>"></span>
                </a>
                <ul class="sub-menu">
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['news/create'])) : ?>
                        <li class="nav-item <?= setActive(['news/create']) ?> ">
                            <a href="<?= Url::to(['/news/create']) ?>" class="nav-link ">
                                <span class="title">خبر جدید</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['news', 'news/index'])) : ?>
                        <li class="nav-item <?= setActive(['news', 'news/index']) ?> ">
                            <a href="<?= Url::to(['/news/index']) ?>" class="nav-link ">
                                <span class="title">لیست اخبار</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['post-categories/index', 'post-categories'])) : ?>
                        <li class="nav-item <?= setActive(['post-categories/index', 'post-categories']) ?> ">
                            <a href="<?= Url::to(['/post-categories/index']) ?>" class="nav-link">
                                <span class="title">دسته بندی اخبار</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        <?php endif; ?>
        <!-- ./BLOG -->

        <!-- Business -->
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'business-categories', 'business-categories/index', 'business/update', 'business/index'
        ])) : ?>
            <li class="nav-item <?= setActive(['business-categories',
                'business-categories/index', 'business']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-basket"></i>
                    <span class="title">اصناف</span>
                    <span
                            class="arrow <?= setActive(['business-categories', 'business-categories/index'], true) ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= setActive(['business-categories', 'business-categories/index']) ?> ">
                        <a href="<?= Url::to(['/business-categories/index']) ?>" class="nav-link ">
                            <span class="title">مدیریت دسته ها</span>
                        </a>
                    </li>
                    <li class="nav-item <?= setActive(['business/index', 'business/update']) ?> ">
                        <a href="<?= Url::to(['/business/index']) ?>" class="nav-link ">
                            <span class="title">مدیریت اصناف</span>
                        </a>
                    </li>
                </ul>
            </li>
        <?php endif; ?>
        <!-- ./Business -->

        <!-- Application -->
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'site/mobile-menu-generator',
            'site/mobile-slider-generator',
        ])) : ?>
            <li class="nav-item <?= setActive(['site/mobile-menu-generator', 'site/mobile-slider-generator']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-screen-smartphone"></i>
                    <span class="title">اپ موبایل</span>
                    <span
                            class="arrow <?= setActive(['site/mobile-menu-generator', 'site/mobile-slider-generator'], true) ?>"></span>
                </a>
                <ul class="sub-menu">
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
                        'site/mobile-menu-generator',
                    ])) : ?>
                        <li class="nav-item <?= setActive(['site/mobile-menu-generator']) ?> ">
                            <a href="<?= Url::to(['/site/mobile-menu-generator']) ?>" class="nav-link ">
                                <span class="title">منو صفحات اصلی</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
                        'site/mobile-slider-generator',
                    ])) : ?>
                        <li class="nav-item <?= setActive(['site/mobile-slider-generator']) ?> ">
                            <a href="<?= Url::to(['/site/mobile-slider-generator']) ?>" class="nav-link ">
                                <span class="title">مدیریت اسلایدرها</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>

        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'message/sms-formats',
            'message/send-message-to'
        ])) : ?>
            <li class="nav-item <?= setActive(['message/sms-formats', 'message/send-message-to']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-volume-2"></i>
                    <span class="title">اطلاع رسانی</span>
                    <span
                            class="arrow <?= setActive(['message/sms-formats', 'message/send-message-to'], true) ?>"></span>
                </a>
                <ul class="sub-menu">

                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/send-message-to'])) : ?>
                        <li class="nav-item <?= setActive(['message/send-message-to']) ?> ">
                            <a href="<?= Url::to(['/message/send-message-to']) ?>" class="nav-link ">
                                <span class="title">ارسال گروهی یا تکی پیام</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/sms-formats'])) : ?>
                        <li class="nav-item <?= setActive(['message/sms-formats']) ?> ">
                            <a href="<?= Url::to(['/message/sms-formats']) ?>" class="nav-link">
                                <span class="title">فرمت های آماده</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/message-list'])) : ?>
                        <li class="nav-item <?= setActive(['message/message-list']) ?> ">
                            <a href="<?= Url::to(['/message/message-list']) ?>" class="nav-link">
                                <span class="title">لیست پیام ها</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>

        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'message/sms-formats',
            'message/send-message-to'
        ])) : ?>
            <li class="nav-item <?= setActive([
                'ticket/customer-care/index',
                'ticket/customer-care/department',
                'ticket/customer-care/user-department',
            ]) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-support"></i>
                    <span class="title">پشتیبانی</span>
                    <span
                            class="arrow <?= setActive([
                                'ticket/customer-care/index',
                                'ticket/customer-care/department',
                                'ticket/customer-care/user-department',
                            ], true) ?>"></span>
                </a>
                <ul class="sub-menu">

                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/send-message-to'])) : ?>
                        <li class="nav-item <?= setActive(['ticket/customer-care/index']) ?> ">
                            <a href="<?= Url::to(['/ticket/customer-care/index']) ?>" class="nav-link ">
                                <span class="title">لیست تیکت ها</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/sms-formats'])) : ?>
                        <li class="nav-item <?= setActive(['ticket/customer-care/department',]) ?> ">
                            <a href="<?= Url::to(['/ticket/customer-care/department',]) ?>" class="nav-link">
                                <span class="title">مدیریت دپارتمان ها</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess(['message/message-list'])) : ?>
                        <li class="nav-item <?= setActive(['ticket/customer-care/user-department']) ?> ">
                            <a href="<?= Url::to(['/ticket/customer-care/user-department']) ?>" class="nav-link">
                                <span class="title">انتساب کاربر به دپارتمان پشتیبانی</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>

        <?php endif; ?>
        <!-- ./Application -->

        <!-- More -->

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'admin',
            'permit/access/role',
            'permit/access/permission',
            'permit/user/view',
            'plan',
            'payment-data',
            'user',
            'setting'
        ])) : ?>

            <li class="heading">
                <h3 class="uppercase">ابزار های بیشتر</h3>
            </li>

        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'uploadManager/default/index'
        ])) : ?>

            <li class="nav-item " <?= setActive(['setting/uploadManager']) ?>>
                <a href="<?= Url::to(['/uploadManager/default/index']) ?>" class="nav-link">
                    <i class="icon-cloud-upload"></i>
                    <span class="title">مدیریت آپلودر</span>
                </a>
            </li>
        <?php endif; ?>


        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'plan/index', 'plan/create', 'plan/update', 'plan/view'
        ])) : ?>

            <li class="nav-item <?= setActive(['plan/index', 'plan/create', 'plan/update', 'plan/view']) ?> ">
                <a href="<?= Url::to(['/plan/index']) ?>" class="nav-link">
                    <i class="icon-basket"></i>
                    <span class="title">پلان های فروش</span>
                </a>
            </li>
        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'payment-data/index', 'payment-data', 'payment-data'
        ])) : ?>
            <li class="nav-item <?= setActive(['payment-data/index', 'payment-data', 'payment-data']) ?> ">
                <a href="<?= Url::to(['/payment-data']) ?>" class="nav-link">
                    <i class="icon-credit-card"></i>
                    <span class="title">گزارش های پرداخت</span>
                </a>
            </li>
        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'admin',
            'permit/access',
            'permit/user'
        ])) : ?>
            <li class="nav-item <?= setActive(['admin/index', 'permit/access/role', 'permit/access/permission', 'permit/user/view']) ?> ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">مدیریت همکاران</span>
                    <span
                            class="arrow <?= setActive(['admin/index', 'permit/access/role', 'permit/access/permission', 'permit/user/view'], true) ?>"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item <?= setActive(['admin/index', 'admin']) ?> ">
                        <a href="<?= Url::to(['/admin/index']) ?>" class="nav-link">
                            <span class="title">مدیریت همکاران</span>
                        </a>
                    </li>
                    <li class="nav-item <?= setActive(['permit/access/role']) ?> ">
                        <a href="<?= Url::to(['/permit/access/role']) ?>" class="nav-link ">
                            <span class="title">نقش ها</span>
                        </a>
                    </li>
                    <li class="nav-item <?= setActive(['permit/access/permission']) ?> ">
                        <a href="<?= Url::to(['/permit/access/permission']) ?>" class="nav-link ">
                            <span class="title">دسترسی ها</span>
                        </a>
                    </li>
                </ul>
            </li>

        <?php endif; ?>

        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'user/index', 'user'
        ])) : ?>

            <li class="nav-item <?= setActive(['user/index', 'user']) ?> ">
                <a href="<?= Url::to(['/user']) ?>" class="nav-link">
                    <i class="icon-user-following"></i>
                    <span class="title">مدیریت کاربران</span>
                </a>
            </li>

        <?php endif; ?>
        <?php if (\developeruz\db_rbac\Yii2DbRbac::checkRouteAccess([
            'setting/settings'
        ])) : ?>
            <li class="nav-item <?= setActive(['setting/settings']) ?> ">
                <a href="<?= Url::to(['/setting/settings']) ?>" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">تنظیمات</span>
                </a>
            </li>
            <li class="nav-item <?= setActive(['setting/settings']) ?> ">
                <a href="<?= Url::to('?r=uploadManager/default/index') ?>" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">مدیریت آپلودر</span>
                </a>
            </li>


        <?php endif; ?>


    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->