<?php

/* @var $this \yii\web\View */
/* @var $content string */

use aminkt\widgets\alert\Alert;
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!--[if IE 8]> <html lang="<?= Yii::$app->language ?>" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="<?= Yii::$app->language ?>" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="canonical" href="https://telbit.ir/admin"/>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link rel="apple-touch-icon" sizes="180x180" href="../logos/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../logos/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../logos/favicon/favicon-16x16.png">
    <link rel="shortcut icon" type="image/png" href="../logos/favicon/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
<?php $this->beginBody() ?>
    <?php $this->beginContent('@backend/views/layouts/header.php'); ?>
    Header become here
    <?php $this->endContent(); ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1><?= $this->title ?>
                        <?php if(isset($this->params['description'])): ?>
                            <small><?= $this->params['description'] ?></small>
                        <?php endif; ?>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
            <?= Breadcrumbs::widget([
                'options'=>['class'=>'page-breadcrumb breadcrumb'],
                'itemTemplate'=>"<li>{link}<i class=\"fa fa-circle\"></i></li>\n",
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <?php echo Alert::widget() ?>
            <?= $content ?>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <?php $this->beginContent('@backend/views/layouts/footer.php'); ?>
    Footer become here
    <?php $this->endContent(); ?>

    <!--[if lt IE 9]>
    <script src="template/global/plugins/respond.min.js"></script>
    <script src="template/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- END THEME LAYOUT SCRIPTS -->
<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>