<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="<?= Yii::$app->language ?>" dir="rtl">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="ورود به پنل کاربری" name="description" />
    <meta content="Telbit.ir" name="author" />
    <link rel="canonical" href="https://telbit.ir/admin"/>
    <?= Html::csrfMetaTags() ?>
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="../assets/template/pages/css/login-rtl.min.css" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <?php $this->head() ?>
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<body class=" login">
<?php $this->beginBody() ?>
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="<?= Yii::$app->getHomeUrl() ?>">
        <img src="../assets/pages/img/logo-big.png" alt=""/>
        <a href="http://telbit.ir" target="_blank" title="تیم تلکو">تلکو</a>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <?= $content ?>
</div>
<div class="copyright"> تمام حقوق برای تیم <a href="http://telbit.ir" target="_blank" title="تیم تلکو">تلکو</a> محفوض
    است.
</div>
<!--[if lt IE 9]>
<!--<script src="../assets/global/plugins/respond.min.js"></script>-->
<!--<script src="../assets/global/plugins/excanvas.min.js"></script>-->
<![endif]-->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

