<?php

use machour\yii2\notifications\widgets\NotificationsWidget;

Yii::warning(Yii::$app->getFormatter()->asDatetime('NOW', 'YYYY/MM/dd - hh:mm:ss'));
?>
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?= \yii\helpers\Url::to(['site/index'], true) ?>">
                <img src="<?= Yii::getAlias("@web") ?>/../logos/admin-logo.png"
                     alt="Telbit logo"
                     class="logo-default" style="width: 175px;height: 55px;margin-top: 10px;"/> </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE ACTIONS -->
        <div class="page-top" style="float: right;">
            <div class="top-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="date-container"> امروز <?= Yii::$app->getFormatter()->asDate('NOW'); ?> </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <?php $currentCity = \backend\components\GlobalComponent::getCurrentCity() ?>
                            <span class="username username-hide-on-mobile"><?= $currentCity ? $currentCity->name : 'انتخاب شهر' ?></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <?php if ($cityList = \backend\components\GlobalComponent::getCityList()): ?>
                                <li>
                                    <?= \yii\helpers\Html::a('همه شهرها', ['/site/remove-global-city']) ?>
                                </li>
                                <li class="divider"></li>
                                <?php foreach (\backend\components\GlobalComponent::getCityList() as $city): ?>
                                    <li <?= ($currentCity && $currentCity->id == $city['id']) ? 'class="active"' : '' ?>>
                                        <?= \yii\helpers\Html::a($city['name'], ['/site/set-global-city', 'cityId' => $city['id']]) ?>
                                    </li>
                                    <li class="divider"></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <li><a href="#">
                                        شهری برای نمایش وجود ندارد

                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>


                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <?php
                    NotificationsWidget::widget([
//                        'theme' => NotificationsWidget::THEME_TOASTR,
                        'clientOptions' => [
                            'location' => 'br',
                        ],
                        'counters' => [
                            '.dropdown-notification .badge.badge-success',
                            '.dropdown-notification .dropdown-menu .external .notification-count'
                        ],
                        'listSelector' => '.dropdown-notification .dropdown-menu-list',
                        'listItemTemplate' => '
                            <li class="notification-{type} {seenClass}" data-route="{url}" data-id="{id}">
                                <a href="{url}">
                                    <span class="time">{time}</span>
                                    <span class="details">
                                        <span class="label label-sm label-icon label-success">
                                            <i class="fa fa-{type}"></i>
                                        </span>
                                        {title}
                                     </span>
                                </a>
                            </li>
                        ',
                    ]);
                    ?>
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark"
                        id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-success" id="badge"> 0 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold notification-count">0 </span> اعلان جدید</h3>
                                <a href="<?= \yii\helpers\Url::to(['/site/all-notifications']) ?>">نمایش همه</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;"
                                    data-handle-color="#637283">
                                    <?php /* ?>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">just now</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">3 mins</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Server #12 overloaded. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">10 mins</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span> Server #2 not responding. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">14 hrs</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span> Application error. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">2 days</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Database overloaded 68%. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">3 days</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> A user IP blocked. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">4 days</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span> Storage Server #4 not responding dfdfdfd. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">5 days</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span> System Error. </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time">9 days</span>
                                            <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Storage server failed. </span>
                                        </a>
                                    </li>
 <?php /* */ ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <li class="separator "></li>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"> <?= Yii::$app->getUser()->getIdentity()->name ?> </span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <?= \yii\helpers\Html::a(" <i class=\"icon-user\"></i> پروفایل من ",
                                    ['/admin/index', 'id' => Yii::$app->getUser()->id]) ?>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <?= \yii\helpers\Html::a(" <i class=\"icon-key\"></i> خروج ", ['/site/logout']) ?>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">

        <?php $this->beginContent('@backend/views/layouts/menu.php'); ?>
        Menu become here
        <?php $this->endContent(); ?>
    </div>
    <!-- END SIDEBAR -->
    <script>
        window.addEventListener("load", function () {
            setTimeout(function () {
                var badge = document.getElementById("badge");
                console.log(badge.innerHTML);

                if (badge.innerHTML !== ' 0 ' || badge.innerHTML === '0') {
                    badge.classList.remove('badge-success');
                    badge.classList.add('badge-danger');
                }
            }, 500);
        }, false);
    </script>