
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner" style="direction: ltr; float: left">
        <?= date('Y') ?> &copy; <a href="http://telbit.ir" title="گروه نرم افزاری تلکو"
                                   target="_blank">Telco</a> softwate group.
    </div>
    <div class="page-footer-inner" style="direction: rtl; float: right">

        تمام بخش های این نرم افزار توسط گروه نرم افزاری <a href="http://telbit.ir" title="گروه نرم افزاری تلکو"
                                                           target="_blank">تلکو</a> طراحی٫ مستند و پیاده سازی شده است.
    </div>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->