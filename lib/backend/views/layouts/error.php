<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;

\backend\assets\ErrorAsset::register($this);
?>
<?php $this->beginPage() ?>

    <!--[if IE 8]>
    <html lang="<?= Yii::$app->language ?>" class="ie8 no-js"> <![endif]-->
    <!--[if IE 9]>
    <html lang="<?= Yii::$app->language ?>" class="ie9 no-js"> <![endif]-->
    <!--[if !IE]><!-->
    <html lang="<?= Yii::$app->language ?>" dir="rtl">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <title><?= Html::encode($this->title) ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
              type="text/css"/>
        <link rel="shortcut icon" href="favicon.ico"/>
        <?= Html::csrfMetaTags() ?>
        <?php $this->head() ?>
    </head>
    <!-- END HEAD -->

    <body class="page-500-full-page">
    <?php $this->beginBody() ?>
    <div class="row">
        <div class="col-md-12 page-500">
            <?= $content ?>
        </div>
    </div>
    <!--[if lt IE 9]>
    <script src="template/global/plugins/respond.min.js"></script>
    <script src="template/global/plugins/excanvas.min.js"></script>
    <![endif]-->
    <!-- END THEME LAYOUT SCRIPTS -->
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>