<?php
/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \backend\models\search\PaymentSearch $searchModel
 */

use kartik\select2\Select2;

$this->title = "گزارش های پرداخت";
?>
<?php

$cities = \saghar\address\models\City::find()->all();
$cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
echo \yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'amount'
        ],
        [
            'attribute' => 'businessName'
        ],
        [
            'label' => 'شهر انتشار',
            'attribute' => 'cities',
            'value' => function ($model) {
                /** @var \backend\models\Order $model */
                $cities = $model->business->cities;
                if ($cities) {
                    $return = '';
                    $i = 1;
                    $accessCities = \backend\components\GlobalComponent::getAccessCities();
                    foreach ($cities as $city) {
                        if (!$accessCities or in_array($city->cityId, $accessCities)) {
                            if ($i++ > 1) {
                                $return .= ', ';
                            }
                            $return .= $city->name;
                        }
                    }
                    return $return;
                }
                return null;
            },
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'cities',
                'data' => $cities,
                'options' => [
                    'placeholder' => 'شهر های فعال را انتخاب کنید',
                    'multiple' => true
                ]
            ]),
        ],
        [
            'attribute' => 'ownerName'
        ],
        [
            'attribute' => 'planId',
            'value' => function ($model) {
                return $model->planName;
            },
            'filter' => yii\helpers\ArrayHelper::map(\backend\models\Plan::find()->all(), 'id', 'name'),
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return $model::getLabel($model->status, 'status');
            },
            'filter' => [
                \aminkt\payment\models\TransactionSession::STATUS_NOT_PAID => 'پرداخت نشده',
                \aminkt\payment\models\TransactionSession::STATUS_PAID => 'پرداخت شده',
            ],
        ],
        [
            'attribute' => 'updateAt',
            'format' => 'dateTime',
            'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'updateAt',
                'options' => array(
                    'format' => 'yyyy-mm-dd',
                    'viewformat' => 'yyyy/mm/dd',
                    'placement' => 'left',
                    'todayBtn' => 'linked',
                    'class' => 'form-control',
                ),
            ])
        ],
        [
            'attribute' => 'createAt',
            'format' => 'dateTime',
            'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'createAt',
                'options' => array(
                    'format' => 'yyyy-mm-dd',
                    'viewformat' => 'yyyy/mm/dd',
                    'placement' => 'left',
                    'todayBtn' => 'linked',
                    'class' => 'form-control',
                ),
            ])
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="btn-group" role="group" aria-label="عملیات">{view}</div>',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return \yii\helpers\Html::a('<i class="icon-pencil"></i>', $url, [
                        'title' => 'ویرایش',
                        'class' => 'btn btn-primary'
                    ]);
                },
                'view' => function ($url, $model, $key) {
                    return \yii\helpers\Html::a('<i class="icon-size-fullscreen"></i>', $url, [
                        'title' => 'نمایش',
                        'class' => 'btn btn-success'
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return \yii\helpers\Html::a('<i class="icon-trash"></i>', $url, [
                        'title' => 'حذف',
                        'class' => 'btn btn-danger'
                    ]);
                },
            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                     if ($action == 'view') {
                         return ['/payment', 'TransactionSessionSearch[orderId]' => $model->id];
                     }
                 }
        ],
    ]
]);