<?php
/** @var \yii\web\View $this */
/** @var \backend\models\AccessForm $model */

$this->title = "پلن های اصناف";
?>

<?php $form = \yii\widgets\ActiveForm::begin([
    'method' => 'post'
]); ?>
<div class="row">
    <div class="col-md-6">
        <h1>مشخصات پلان</h1>


        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'description')->textarea() ?>
        <?= $form->field($model, 'price')->textInput() ?>
        <?= $form->field($model, 'duration')->textInput() ?>

        <?= \yii\helpers\Html::submitButton("ثبت ", [
            'class' => 'btn btn-primary'
        ]) ?>

    </div>
    <div class="col-md-6">
        <h1>دسترسی ها</h1>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'accessPhoneNumber')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'accessFaxNumber')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'accessMobileNumber')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'accessMainImage')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessVideo')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessMap')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessSocialTelegram')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessSocialInstagram')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessWebsite')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessEmail')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessAddress')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessGallery')->checkbox() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'accessIOS')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'accessAndroid')->checkbox() ?>
            </div>
        </div>
    </div>
</div>
<?php \yii\widgets\ActiveForm::end(); ?>
