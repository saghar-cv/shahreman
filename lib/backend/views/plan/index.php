<?php
/** @var $this yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \backend\models\Plan $model */
$this->title = "مدیریت پلان ها"

?>

<div class="plan-index">

    <h1><?= $this->title ?></h1>

    <p>
        <?= yii\helpers\Html::a('ایجاد پلان جدید', ['/plan/create'], ['class' => 'btn btn-primary']) ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => "",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'name',
                ],
                [
                    'attribute' => 'description'
                ],
                [
                    'attribute' => 'price'
                ],
                [
                    'attribute' => 'duration'
                ],
                [
                    'attribute' => 'createAt'
                ],
                [
                    'attribute' => 'updateAt'
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-pencil"></i>', $url, [
                                'title' => 'ویرایش',
                                'class' => 'btn btn-primary'
                            ]);
                        },
                        'view' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-size-fullscreen"></i>', $url, [
                                'title' => 'نمایش',
                                'class' => 'btn btn-success'
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-trash"></i>', $url, [
                                'title' => 'حذف',
                                'class' => 'btn btn-danger'
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'update') {
                            return \yii\helpers\Url::to(['/plan/update', 'id' => $model->id]);
                        }
                        if ($action === 'delete') {
                            return \yii\helpers\Url::to(['/plan/remove', 'id' => $model->id]);
                        }
                    },
                    'template' => '<div class="btn-group" role="group" aria-label="عملیات">{update}{delete}</div>',
                    'contentOptions'=>['style'=>'width: 113px;']
                ],
            ]
        ]); ?>

    </p>
</div>
