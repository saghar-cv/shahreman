<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\search\PostSearch */

$this->title = 'کاربران';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default">


    <div class="panel-heading">
        <h3 class="panel-title"> لیست کاربران</h3>
    </div>
    <div class="panel-body">


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
            'columns' => [

                'name',
                'family',
                'email',
                'mobile',
                'createAt:dateTime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-pencil"></i>', $url, [
                                'title' => 'ویرایش',
                                'class' => 'btn btn-primary'
                            ]);
                        },
                        'view' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-size-fullscreen"></i>', $url, [
                                'title' => 'نمایش'
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-trash"></i>', $url, [
                                'title' => 'حذف'
                            ]);
                        },
                        'sendMessage' => function ($url, $model, $key) {
                            return \yii\helpers\Html::a('<i class="icon-envelope"></i>', [
                                'message/send-message-to',
                                'id' => $model->id,
                                'receiverType' => 'user'
                            ],
                                [
                                    'title' => 'ارسال پیام',
                                    'class' => 'btn btn-success'
                                ]);
                        },
                    ],
                    'template' => '<div class="btn-group" role="group" aria-label="عملیات">{update}{sendMessage}</div>',
                    'contentOptions' => ['style' => 'width: 113px;']

                ],
            ],
        ]); ?>
    </div>
</div>
