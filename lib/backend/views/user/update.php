<?php
/**
 * Created by PhpStorm.
 * User: Mohammad_Pvn
 * Date: 3/14/2018
 * Time: 4:01 PM
 */
/** @var $this \yii\web\View */
/** @var $model \frontend\models\User */
$this->title = 'ویرایش کاربر - ' . $model->name;
?>
<div class="panel panel-default">

    <div class="panel-body">
        <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>

        <?= $form->field($model, 'name')->textInput() ?>

        <?= $form->field($model, 'family')->textInput() ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= \yii\helpers\Html::submitButton("ویرایش", [
            'class' => 'btn btn-primary'
        ]) ?>
        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
</div>
