<?php
/** @var $this yii\web\View */
/** @var $model \backend\models\Admin */

$this->title = 'ادمین های ثبت شده';
?>
<div class="admin-default-index">
    <h1><?= $this->title ?></h1>
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> لیست ادمین ها</h3>
                </div>
                <div class="panel-body">
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => "",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'name',
                            ],
                            [
                                'attribute' => 'family',
                            ],
                            [
                                'attribute' => 'email',
                            ],
                            [
                                'attribute' => 'mobile'
                            ],
                            [
                                'attribute' => 'accessCities',
                                'value' => function ($model) {
                                    $cities = explode(',', $model->accessCities);
                                    $allCities = \saghar\address\models\City::find()->where([
                                        'id' => $cities
                                    ])->all();
                                    $result = "";
                                    $i = 0;
                                    foreach ($allCities as $city) {
                                        if ($i++ > 0) {
                                            $result .= ', ';
                                        }
                                        $result .= $city->name;
                                    }
                                    return $result;
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '<div class="btn-group" role="group" aria-label="عملیات">{update} {delete} {permit}</div>',
                                'buttons' => [
                                    'update' => function ($url, $model, $key) {
                                        return \yii\helpers\Html::a('<i class="icon-pencil"></i>', $url, [
                                            'title' => 'ویرایش',
                                            'class' => 'btn btn-primary'
                                        ]);
                                    },
                                    'view' => function ($url, $model, $key) {
                                        return \yii\helpers\Html::a('<i class="icon-size-fullscreen"></i>', $url, [
                                            'title' => 'نمایش',
                                            'class' => 'btn btn-primary'
                                        ]);
                                    },
                                    'delete' => function ($url, $model, $key) {
                                        return \yii\helpers\Html::a('<i class="icon-trash"></i>', $url, [
                                            'title' => 'حذف',
                                            'class' => 'btn btn-danger'
                                        ]);
                                    },
                                    'permit' => function ($url, $model) {
                                        return \yii\helpers\Html::a('<i class="icon-lock-open"></i>', $url, [
                                            'title' => 'تغییر نقش کاربر',
                                            'class' => 'btn btn-warning'
                                        ]);
                                    },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    if ($action === 'update') {
                                        return \yii\helpers\Url::to(['/admin/index', 'id' => $model->id]);
                                    }
                                    if ($action === 'delete') {
                                        return \yii\helpers\Url::to(['/admin/remove', 'id' => $model->id]);
                                    }
                                    if ($action === 'permit') {
                                        return \yii\helpers\Url::to(['/permit/user/view', 'id' => $model->id]);
                                    }
                                },
                                'contentOptions'=>['style'=>'width: 200px;text-align:center']
                            ],
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> <?= $model->isNewRecord ? 'ایجاد ' : ' ویرایش '; ?> ادمین</h3>
                </div>
                <div class="panel-body">
                    <?= $this->render('_form', [
                        'model' => $model
                    ]) ?>

                </div>
            </div>
        </div>
    </div>