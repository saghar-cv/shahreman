<?php
/** @var $this \yii\web\View */
/** @var $model \backend\models\Admin */
$this->title = 'ثبت ادمین'
?>

<div class="admin-form">
    <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'family') ?>
    <?php
    $cities = \saghar\address\models\City::find()->all();
    $data = \yii\helpers\ArrayHelper::map($cities, 'id', 'name');
    echo $form->field($model, 'accessCities')->widget(\kartik\select2\Select2::class, [
        'data' => $data,
        'options' => ['placeholder' => 'شهر های فعال را انتخاب کنید ...', 'multiple' => true],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [',', ' '],
            'maximumInputLength' => 10
        ],
    ]) ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'mobile') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
    <?= \yii\helpers\Html::submitButton($model->isNewRecord ? "ذخیره" : "ویرایش", [
        'class' => 'btn btn-primary']) ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>

