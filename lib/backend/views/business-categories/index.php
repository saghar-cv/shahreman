<?php
/** @var \yii\web\View $this */
/** @var \saghar\category\models\Category[] $categories */
/** @var \saghar\category\models\Category $model */

$this->title = "مدیریت دسته ها"

?>

<div class="categoryManager-default-index">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> لیست دسته ها</h3>
                </div>
                <div class="panel-body">
                    <?= yii\helpers\Html::a('ایجاد دسته جدید',
                        ['/business-categories/index'],
                        ['class' => 'btn btn-primary'])
                    ?>

                    <?= \aminkt\widgets\tree\TreeView::widget([
                        'data' => \saghar\category\models\Category::getCategoriesBySectionAsArray(\backend\models\CategoryRelation::SECTION_BUSINESS),
                        'remove' => ['/business-categories/delete'],
                        'edit' => ['/business-categories/index'],
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> <?= $model->isNewRecord ? 'ایجاد ' : ' ویرایش '; ?> دسته</h3>
                </div>
                <div class="panel-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'post'
                    ]); ?>
                    <?= $form->field($model, 'name')->textInput() ?>
                    <?= $form->field($model, 'tags')->widget(\aminkt\widgets\inputTag\InputTag::className(), [
                        'options' => [
                            'maxlength' => true,
                            'class' => 'form-control maxlength-handler'
                        ]
                    ]) ?>
                    <?= $form->field($model, 'description')->textarea() ?>
                    <?php
                    if ($model->id) {
                        $categories = \saghar\category\models\Category::find()
                            ->where(['!=', 'status', \saghar\category\models\Category::STATUS_REMOVED])
                            ->andWhere(['!=', 'id', $model->id])
                            ->andWhere(['=', 'section', 'business'])
                            ->all();
                    } else {
                        $categories = \saghar\category\models\Category::find()
                            ->where(['!=', 'status', \saghar\category\models\Category::STATUS_REMOVED])
                            ->andWhere(['=', 'section', \backend\models\CategoryRelation::SECTION_BUSINESS])
                            ->all();
                    }
                    $categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'name', 'parentName');
                    echo \kartik\select2\Select2::widget([
                        'model' => $model,
                        'attribute' => 'parentId',
                        'data' => $categories,
                        'options' => ['placeholder' => 'دسته را انتخاب کنید ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                    <br>
                    <?= \yii\helpers\Html::submitButton("ثبت", [
                        'class' => 'btn btn-primary'
                    ]) ?>
                </div>
            </div>

            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>
</div>