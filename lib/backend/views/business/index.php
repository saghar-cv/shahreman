<?php
/** @var $this yii\web\View */

use kartik\select2\Select2;

/** @var $searchModel \common\models\search\BusinessSearch */
/** @var $dataProvider */

$this->title = "مدیریت اصناف"
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> لیست کسب و کار ها</h3>
    </div>
    <div class="panel-body">

        <?php try {
            $cities = \saghar\address\models\City::find()->all();
            $cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
            echo \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'label' => 'شهر انتشار',
                        'attribute' => 'cities',
                        'value' => function ($model) {
                            $cities = $model->cities;
                            if ($cities) {
                                $return = '';
                                $i = 1;
                                $accessCities = \backend\components\GlobalComponent::getAccessCities();
                                foreach ($cities as $city) {
                                    if (!$accessCities or in_array($city->cityId, $accessCities)) {
                                        if ($i++ > 1) {
                                            $return .= ', ';
                                        }
                                        $return .= $city->name;
                                    }
                                }
                                return $return;
                            }
                            return null;
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'cities',
                            'data' => $cities,
                            'options' => [
                                'placeholder' => 'شهر های فعال را انتخاب کنید',
                                'multiple' => true
                            ]
                        ]),
                    ],
                    [
                        'attribute' => 'ownerName',
                        'label' => 'نام صاحب',
                    ],
                    [
                        'label' => 'وضعیت',
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return $model->getStatusLabel();
                        },
                        'filter' => [
                            \common\models\Business::STATUS_NOT_PAYED => 'پرداخت نشده',
                            \common\models\Business::STATUS_PAYED => 'پرداخت شده',
                            \common\models\Business::STATUS_CONFIRMED => 'تایید شده',
                            \common\models\Business::STATUS_REJECTED => 'رد شده',
                            \common\models\Business::STATUS_EXPIRED => 'منقضی شده',
                            \common\models\Business::STATUS_REMOVED => 'حذف شده',
                        ],
                    ],
                    [
                        'label' => 'پلان',
                        'attribute' => 'planId',
                        'value' => function ($model) {
                            return $model->plan->name;
                        },
                        'filter' => yii\helpers\ArrayHelper::map(\backend\models\Plan::find()->all(), 'id', 'name'),
                    ],
                    [
                        'attribute' => 'expireDate',
                        'format' => 'dateTime',
                        'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'expireDate',
                            'options' => array(
                                'format' => 'yyyy-mm-dd',
                                'viewformat' => 'yyyy/mm/dd',
                                'placement' => 'left',
                                'todayBtn' => 'linked',
                                'class' => 'form-control',
                            ),
                        ])
                    ],
                    [
                        'attribute' => 'updateAt',
                        'format' => 'dateTime',
                        'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'updateAt',
                            'options' => array(
                                'format' => 'yyyy-mm-dd',
                                'viewformat' => 'yyyy/mm/dd',
                                'placement' => 'left',
                                'todayBtn' => 'linked',
                                'class' => 'form-control',
                            ),
                        ])
                    ],
                    [
                        'attribute' => 'createAt',
                        'format' => 'dateTime',
                        'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'createAt',
                            'options' => array(
                                'format' => 'yyyy-mm-dd',
                                'viewformat' => 'yyyy/mm/dd',
                                'placement' => 'left',
                                'todayBtn' => 'linked',
                                'class' => 'form-control',
                            ),
                        ])
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return \yii\helpers\Html::a('<i class="icon-pencil"></i>', $url, [
                                    'title' => 'ویرایش',
                                    'class' => 'btn btn-primary'
                                ]);
                            },
                            'view' => function ($url, $model, $key) {
                                return \yii\helpers\Html::a('<i class="icon-size-fullscreen"></i>', $url, ['title' => 'نمایش']);
                            },
                            'delete' => function ($url, $model, $key) {
                                return \yii\helpers\Html::a('<i class="icon-trash"></i>', $url, ['title' => 'حذف']);
                            },
                            'sendMessage' => function ($url, $model, $key) {
                                return \yii\helpers\Html::a('<i class="icon-envelope"></i>', [
                                    'message/send-message-to',
                                    'id' => $model->id,
                                    'receiverType' => 'business'
                                ],
                                    [
                                        'title' => 'ارسال پیام',
                                        'class' => 'btn btn-success'
                                    ]);
                            },
                        ],
                        'template' => '<div class="btn-group" role="group" aria-label="عملیات">{update}{sendMessage}</div>',
                        'contentOptions' => ['style' => 'width: 113px;']
                    ],
                ]
            ]);
        } catch (Exception $e) {
            throw $e;
        } ?>
    </div>
</div>