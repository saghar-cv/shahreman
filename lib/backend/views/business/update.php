<?php
/** @var $this \yii\web\View */

use aminkt\widgets\google\map\LocationInput;
use yii\httpclient\Response;

/** @var $model \common\models\Business */
/** @var $metaForm \backend\models\BusinessMetaForm */
/** @var $category \backend\models\CategoryRelation title */
$this->title = 'ویرایش صنف - ' . $model->name;
$plan = \backend\models\Plan::findOne($model->planId);
?>
<br>
<div class="business-form">
    <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>
    <div class="col col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> اطلاعات پایه کسب و کار</h3>
                <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات فرعی صنف", [
                    'class' => 'btn btn-default',
                    'style' => 'float: left;position: relative;top: -23px;padding: 5px;',
                ]) ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-6">

                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                        <div class="col-md-6">

                            <?= $form->field($model, 'ownerName')->textInput() ?>
                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'description')->textarea(['rows' => 8]) ?>
                        </div>
                        <div class="col-md-12">

                            <?= $form->field($model, 'operatorNote')->textarea(['rows' => 8]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">

                                <?php $status = [
                                    \common\models\Business::STATUS_NOT_PAYED => 'پرداخت نشده',
                                    \common\models\Business::STATUS_PAYED => 'پرداخت شده',
                                    \common\models\Business::STATUS_CONFIRMED => 'تایید شده',
                                    \common\models\Business::STATUS_REJECTED => 'رد شده',
                                    \common\models\Business::STATUS_EXPIRED => 'منقضی شده',
                                    \common\models\Business::STATUS_REMOVED => 'حذف شده',
                                ];
                                echo $form->field($model, 'status')->widget(\kartik\select2\Select2::class, [
                                    'data' => $status,
                                    'options' => ['placeholder' => 'وضعیت را انتخاب کنید ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ]
                                ]);
                                ?>

                                <?php $status = [
                                    \common\models\Business::BUSINESS_IS_EDITED => 'ویرایش شده و در انتظار تائید',
                                    \common\models\Business::BUSINESS_CHANGES_APPROVED => 'تائید و انتشار مجدد',
                                ];
                                echo $form->field($model, 'edited')->widget(\kartik\select2\Select2::class, [
                                    'data' => $status,
                                    'options' => ['placeholder' => 'وضعیت را انتخاب کنید ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ]
                                ]);
                                ?>

                                <?php
                                $categories = \saghar\category\models\Category::find()
                                    ->where(['status' => \saghar\category\models\Category::STATUS_ACTIVE])
                                    ->all();
                                $categories = \yii\helpers\ArrayHelper::map($categories, 'id', 'name');

                                echo $form->field($category, 'catId')->widget(\kartik\select2\Select2::class, [
                                    'data' => $categories,
                                    'options' => ['placeholder' => 'دسته را انتخاب کنید ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ]
                                ]);
                                ?>

                                <?php
                                $plans = \backend\models\Plan::find()->all();
                                $plans = \yii\helpers\ArrayHelper::map($plans, 'id', 'name');

                                echo $form->field($model, 'planId')->widget(\kartik\select2\Select2::class, [
                                    'data' => $plans,
                                    'options' => ['placeholder' => 'پلان را انتخاب کنید ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ]
                                ]);
                                ?>

                                <?= $form->field($model, 'expireDate')->widget(
                                    faravaghi\jalaliDatePicker\jalaliDatePicker::class, [
                                    'options' => array(
                                        'format' => 'yyyy-mm-dd',
                                        'viewformat' => 'yyyy/mm/dd',
                                        'placement' => 'right',
                                        'todayBtn' => 'linked',
                                        'class' => 'form-control',
                                    ),
                                ]);
                                ?>



                                <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات پایه صنف", [
                                    'class' => 'btn btn-primary',
                                    'style' => 'float:left',
                                ]) ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end() ?>

    <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>
    <div class="col col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">اطلاعات تماس کسب و کار</h3>
                <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات فرعی صنف", [
                    'class' => 'btn btn-default',
                    'style' => 'float: left;position: relative;top: -23px;padding: 5px;',
                ]) ?>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            آدرس محل استقرار صنف و شهر های محل انتشار
                        </h4>
                        <hr>
                    </div>
                    <div class="col-md-6">
                        <?php
                        $cityList = \saghar\address\models\City::find()->all();
                        $cityList = \yii\helpers\ArrayHelper::map($cityList, 'id', 'name', 'stateName');
                        echo $form->field($metaForm, 'city')->widget(\kartik\select2\Select2::class, [
                            'data' => $cityList,
                            'options' => ['placeholder' => 'شهر محل استقرار صنف را انتخاب کنید ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ]);

                        ?>
                    </div>

                    <div class="col-md-6">
                        <?php
                        /**
                         * Get available city lists.
                         */
                        $url = \backend\components\GlobalComponent::getApiUrl(['/v1/main/active-cities']);
                        $client = new \yii\httpclient\Client([
                            'baseUrl' => $url,
                            'transport' => 'yii\httpclient\CurlTransport'
                        ]);

                        /** @var Response $c ' */
                        $cities = $client->createRequest()
                            ->setFormat(\yii\httpclient\Client::FORMAT_JSON)
                            ->setMethod('GET')
                            ->send();
                        if ($cities->statusCode == 200) {
                            $cities = yii\helpers\Json::decode($cities->getContent());
                            $cityList = [];
                            foreach ($cities['data'] as $city) {
                                $cityList[$city['id']] = $city['name'];
                            }

                            echo $form->field($metaForm, 'publishCities')->widget(\kartik\select2\Select2::class, [
                                'data' => $cityList,
                                'options' => ['placeholder' => 'شهر محل انتشار صنف را انتخاب کنید...', 'multiple' => true],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'tag' => true,
                                ]
                            ]);
                        } else {
                            echo "خطای ارتباط با api";
                        }
                        ?>
                    </div>

                    <div class="col-md-12">

                        <?= $form->field($metaForm, 'address')->textarea(['rows' => 5]); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            شماره های ارتباطی صنف
                        </h4>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <?php $access = $plan->getAccessByName($model::BUSINESS_META_MOBILE);
                        for ($i = 1; $i <= $access->value; $i++): ?>
                            <?= $form->field($metaForm, 'mobiles[' . $i . ']')->textInput(); ?>
                        <?php endfor; ?>
                    </div>

                    <div class="col-md-4">
                        <?php $access = $plan->getAccessByName($model::BUSINESS_META_PHONE);
                        for ($i = 1; $i <= $access->value; $i++): ?>

                            <?= $form->field($metaForm, 'phones[' . $i . ']')->textInput(); ?>

                        <?php endfor; ?>
                    </div>

                    <div class="col-md-4">
                        <?php $access = $plan->getAccessByName($model::BUSINESS_META_FAX);
                        for ($i = 1; $i <= $access->value; $i++): ?>

                            <?= $form->field($metaForm, 'faxes[' . $i . ']')->textInput(); ?>

                        <?php endfor; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>
                            موارد دیگر
                        </h4>
                        <hr>
                    </div>
                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_WEBSITE);
                    if ($access and $access->value == 1): ?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'website')->textInput(); ?>
                        </div>
                    <?php endif; ?>

                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_EMAIL);
                    if ($access and $access->value == 1): ?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'email')->textInput(); ?>
                        </div>
                    <?php endif; ?>

                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_SOCIAL_INSTALGRAM);
                    if ($access and $access->value == 1): ?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'telegram')->textInput(); ?>
                        </div>
                    <?php endif; ?>

                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_SOCIAL_INSTALGRAM);
                    if ($access and $access->value == 1):?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'instagram')->textInput(); ?>
                        </div>
                    <?php endif; ?>

                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_ANDROID);
                    if ($access and $access->value == 1):?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'android')->textInput(); ?>
                        </div>
                    <?php endif; ?>

                    <?php $access = $plan->getAccessByName($model::BUSINESS_META_IOS);
                    if ($access and $access->value == 1):?>
                        <div class="col-md-4">
                            <?= $form->field($metaForm, 'ios')->textInput(); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات فرعی صنف", [
                            'class' => 'btn btn-primary',
                            'style' => 'float:left',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end() ?>

    <?php $form = \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>


    <div class="col col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> اطلاعات رسانه ای</h3>
                <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات رسانه ای", [
                    'class' => 'btn btn-default',
                    'style' => 'float: left;position: relative;top: -23px;padding: 5px;',
                ]) ?>
            </div>
            <div class="panel-body">
                <div class="row">

                    <div class="col-md-12">
                        <h4>
                            چند رسانه
                        </h4>
                        <hr>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group main-image-input">
                            <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                                'id' => 'upload-user-pic',
                                'model' => $metaForm,
                                'attribute' => 'mainImage',
                                'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_IMAGE,
                                'titleTxt' => 'تصویر شاخص را وارد کنید.',
                                'helpBlockEnable' => false,
                                'showImageContainer' => '#coverId',
                                'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                                'btnTxt' => 'جایگذاری تصویر شاخص'
                            ]);
                            ?>
                            <div id="coverId"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group video-input">
                            <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                                'id' => 'upload-video',
                                'model' => $metaForm,
                                'attribute' => 'video',
                                'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_VIDEO,
                                'titleTxt' => 'ویدئو را وارد کنید.',
                                'helpBlockEnable' => false,
                                'showImageContainer' => '#video_container',
                                'showImagesTemplate' => "<video width='100%' height='300px' controls><source src='{url}' type='video/{file_extension}'>سیستم شما از ویدیو پشتیبانی نمیکند. </video>",
                                'btnTxt' => 'جایگذاری ویدئو'
                            ]);
                            ?>
                            <div id="video_container"></div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group gallery-image-input">
                            <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                                'id' => 'upload-gallery-pic',
                                'model' => $metaForm,
                                'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_IMAGE,
                                'attribute' => 'gallery',
                                'multiple' => true,
                                'titleTxt' => 'تصویر گالری را وارد کنید.',
                                'helpBlockEnable' => false,
                                'showImageContainer' => '#gallery',
                                'showImagesTemplate' => "<img src='{url}' class='img-responsive' style='float:right; margin:10px; border-radius:5px; width:150px;'>",
                                'btnTxt' => 'جایگذاری گالری'
                            ]);
                            ?>
                            <div id="gallery"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h4>
                            نقشه
                        </h4>
                        <hr>
                    </div>

                    <div class="col-md-12">
                        <?php
                        $css = <<<CSS
.map-input-control{
width: 60%;
top: 7px !important;
}
CSS;
                        $this->registerCss($css);
                        echo $form->field($metaForm, 'map')->widget(LocationInput::class, [
                            'apiKey' => 'AIzaSyBjFztI9oEaunJToOiAQ9lSS8Bfbre3BNo',
                            'arrayMode' => false,
                            'options' => [
                                'class' => 'form-control inline-editable',
                            ],
                            'mapOptions' => [
                                'center' => [
                                    'lat' => 37.28083300,
                                    'lng' => 49.58305600,
                                ],
                                'zoom' => 10,
                            ],
                            'markerOptions' => [
                                'draggable' => true,
                            ],
                            'searchInputOptions' => [
                                'class' => 'map-input-control form-control',
                            ],
                            'disableLocationPicker' => 0, // Or 1 to define input become enabled or not to use just map view.
                            'width' => '100%', // Map container width
                            'height' => '400px', // Map container height
                        ])->label(false);
                        ?>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <?= \yii\helpers\Html::submitButton("ویرایش اطلاعات رسانه ای", [
                            'class' => 'btn btn-primary',
                            'style' => 'float:left',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>
