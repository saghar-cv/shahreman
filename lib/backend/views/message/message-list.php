<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \backend\models\search\MessageSearch */

$this->title = 'پیام ها';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'recipientName',
            'mobile',
            'email',
            'content',
            [
                'label' => 'نوع پیام',
                'attribute' => 'type',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                },
                'filter' => [
                    \common\models\Message::TYPE_SMS => 'پیامک',
                    \common\models\Message::TYPE_EMAIL => 'ایمیل',
                    \common\models\Message::TYPE_SMS_EMAIL => 'ایمیل و پیامک'
                ],
            ],
            [
                'label' => 'وضعیت',
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                },
                'filter' => [
                    \common\models\Message::STATUS_SEND => 'ارسال شده',
                    \common\models\Message::STATUS_NOT_SEND => 'ارسال نشده',
                ],
            ],

            [
                'attribute' => 'createAt',
                'format' => 'dateTime',
                'filter' => \faravaghi\jalaliDatePicker\jalaliDatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'createAt',
                    'options' => array(
                        'format' => 'yyyy-mm-dd',
                        'viewformat' => 'yyyy/mm/dd',
                        'placement' => 'left',
                        'todayBtn' => 'linked',
                        'class' => 'form-control',
                    ),
                ])
            ],

        ],
    ]); ?>
</div>
