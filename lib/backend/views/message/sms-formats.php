<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\SmsForm */

use yii\widgets\ActiveForm;

$this->title = 'پنل مدیریتی - فرمت های آماده پیام های سیستم';
?>
    <div class="post-form">
    <div class="container-fluid">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"> افزودن یک آیتم</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?php

                            echo $form->field($model, 'type')->widget(\kartik\select2\Select2::className(), [
                                'data' => $model::getTypeList(),
                                'options' => [
                                    'id' => 'message-type',
                                    'placeholder' => 'نوع آیتم را انتخاب کنید ...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);

                            $url = \yii\helpers\Url::to(['message/sms-formats']);

                            $js = <<<JS
$("#message-type").on('change', function() {
  window.location = "{$url}&type="+$(this).val();
})
JS;

                            $this->registerJs($js);
                            ?>




                            <?=
                            $form->field($model, 'content')->textarea(
                                ['rows' => '12']
                            );
                            ?>

                            <?= \yii\helpers\Html::submitButton('ثبت', ['class' => 'btn btn-success', 'style' => 'margin-top:15px; float:left']); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>
                            میتوانید برای ایجاد محتوایی پویا در پیامک ارسالی از short code های زیر استفاده کنید:
                        </h4>
                        <p>
                            {%businessName%} : نام صنف <br><br>
                            {%businessOwnerName%} : نام صاحبت صنف <br><br>
                            {%businessRegistrationAmount%} : هزینه ثبت نام صنف <br><br>
                            {%businessExpireDate%} : تاریخ انقضای صنف <br><br>
                            {%publishCitiesCount%} : تعداد شهر های منتشر شده صنف <br><br>
                            {%businessCityName%} : نام شهر های منتشر شده صنف <br><br>
                            {%businessPublishedCitiesNames%} : نام دسته صنف <br><br>
                            {%operatorNote%} : پیام اپراتور <br><br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>


