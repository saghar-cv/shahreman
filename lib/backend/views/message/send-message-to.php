<?php
/* @var $this yii\web\View */

/* @var $model \backend\models\MessageForm */

use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->title = 'پنل مدیریتی - اطلاع رسانی از طریق ایمیل یا پیامک';
?>
<div class="post-form">
    <div class="container-fluid">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="col col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> افزودن یک آیتم</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <?php
                                        if ($model->receiverType == '') {
                                            $model->receiverType = 'user';
                                        }

                                        echo $form->field($model, 'receiverType')->dropDownList($model::getReceiverTypeList(), [
                                            'id' => 'receiverType'
                                        ]);

                                        ?>
                                    </div>
                                    <div class="col-md-4">
                                        <?php
                                        $js = <<<JS
                        
let receiverType = $("#receiverType").val();

if(receiverType != 'business'){
  $("#business-selection").css('display', 'none');
}
if(receiverType != 'user'){
  $("#user-selection").css('display', 'none');
}
if(receiverType!='category'){
  $("#category-selection").css('display', 'none');
}


$("#receiverType").on('change', function() {
    let receiverType = $(this).val();
    if(receiverType == 'business'){
      $("#user-selection").css('display', 'none');
      $("#business-selection").css('display','block');
      $("#category-selection").css('display', 'none');
    } else if (receiverType == 'user'){
      $("#business-selection").css('display','none');
      $("#category-selection").css('display', 'none');
      $("#user-selection").css('display', 'block');
      }else if (receiverType=='category'){
          $("#category-selection").css('display', 'block');
          $("#business-selection").css('display','none');
          $("#user-selection").css('display', 'none');
      }
   
});
JS;
                                        $this->registerJs($js);
                                        ?>
                                        <div id="category-selection">
                                            <?php
                                            $category = \saghar\category\models\Category::find()->all();
                                            $category = ArrayHelper::map($category, 'id', 'name');
//                                            $category = ['0' => 'همه دسته ها'] + $category;
                                            echo $form->field($model, 'categoryIds')->widget(\kartik\select2\Select2::class, [
                                                'data' => $category,
                                                'options' => [
                                                    'multiple' => false,
                                                    'placeholder' => 'دسته را انتخاب کنید ...'
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                        <div id="business-selection">
                                            <?php
                                            $business = \common\models\Business::find()->all();
                                            $business = ArrayHelper::map($business, 'id', 'name');
                                            $business = ['0' => 'همه اصناف'] + $business;
                                            echo $form->field($model, 'businessIds')->widget(\kartik\select2\Select2::class, [
                                                'data' => $business,
                                                'options' => [
                                                    'multiple' => true,
                                                    'placeholder' => 'صنف را انتخاب کنید ...'
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>


                                        <div id="user-selection">
                                            <?php
                                            $users = \frontend\models\User::find()->all();
                                            $users = ArrayHelper::map($users, 'id', 'fullName');
                                            $users = ['0' => 'همه کاربران'] + $users;
                                            echo $form->field($model, 'userIds')->widget(\kartik\select2\Select2::class, [
                                                'data' => $users,
                                                'options' => [
                                                    'placeholder' => 'کاربر راانتخاب کنید ...',
                                                    'multiple' => true,

                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <?= $form->field($model, 'messageType')->dropDownList($model::getMessageTypeList()) ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <?= $form->field($model, 'body')->textarea(
                                            ['rows' => '12']
                                        ) ?>
                                    </div>
                                </div>

                                <?= \yii\helpers\Html::submitButton('ثبت', ['class' => 'btn btn-success', 'style' => 'margin-top:15px; float:left']); ?>
                            </div>
                        </div>
                        <?php /*
                        <div class="col-md-6">
                            <h4>
                                میتوانید برای ایجاد محتوایی پویا در پیام ارسالی از short code های زیر استفاده کنید:
                            </h4>
                            <p>
                                {%businessName%} : نام صنف <br><br>
                                {%businessOwnerName%} : نام صاحبت صنف <br><br>
                                {%businessRegistrationAmount%} : هزینه ثبت نام صنف <br><br>
                                {%businessExpireDate%} : تاریخ انقضای صنف <br><br>
                                {%publishCitiesCount%} : تعداد شهر های منتشر شده صنف <br><br>
                            </p>
                        </div>
                        */ ?>
                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>

