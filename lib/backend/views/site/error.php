<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception \yii\web\HttpException|\Exception */

$this->title = $name;

$code = isset($exception->statusCode) ? $exception->statusCode : $exception->getCode();
?>
<div class=" number font-red"> <?= $code ?> </div>
<div class=" details">
    <h3><?= $name ?></h3>
    <p> <?= $message ?>
        <br/></p>
    <p>
        <a href="<?= \yii\helpers\Url::to(['/site/index']) ?>" class="btn red btn-outline"> بازگشت به داشبورد </a>
        <br></p>
</div>
