<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        * {
            box-sizing: border-box;
        }

        /* Create two equal columns that floats next to each other */
        .column {
            float: right;
            width: 100%;
            height: 50px;
            padding: 10px;
        }

        .time {
            background: #90a1af;
            color: #f7f8f9;
            float: left;
            width: 100px;
            height: 25px;
            border-radius: 25px;
            text-align: center;
        }

        .title {
            font-size: 17px;
            padding-right: 7px;
            color: #0b0b0b;
        }

        .column:hover {
            background-color: #e7e7e7;
        }

        .column {
            background-color: white;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

    </style>
</head>
<body>

<h2>اعلان ها</h2>

<br>

<?php
foreach ($notifications as $notification) {
    if ($notification['seen'] == 1) {
        $labelColor = 'success';
        $faType = 'envelope-open-o';
    } else {
        $labelColor = 'danger';
        $faType = 'envelope-o';
    }
    ?>
    <a href="<?php echo $notification['url'] ?>">

        <div class="row">
            <div class="column">
                <div class="notification-<?php echo $notification['type'] ?>"
                     data-route="<?php echo $notification['url'] ?>"
                     data-id="<?php echo $notification['id'] ?>">
                        <span class="details">
                            <span class="label label-sm label-icon label-<?php echo $labelColor ?>">
                                <i class="fa fa-<?php echo $faType ?>">  </i>
                            </span>

                            <span class="title"><?php echo $notification['title'] ?> </span>

                        </span>
                    <span class="time"><?php echo $notification['date'] ?></span>
                </div>
            </div>
        </div>
    </a>
    <?php
}
echo \yii\widgets\LinkPager::widget([
    'pagination' => $pages,
]);
?>


</body>
</html>
