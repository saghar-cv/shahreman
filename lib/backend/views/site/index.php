<?php

/* @var $this yii\web\View */
/* @var $chartData array */

/* @var $deviceData array */

use developeruz\db_rbac\Yii2DbRbac;
use yii\helpers\Html;

$this->title = 'پنل مدیریتی - داشبورد';

$userCount = \frontend\models\User::find()->count();

$businessCount = \common\models\Business::find()->count();

$confirmedBusinessCount = \common\models\Business::find()
    ->where(['status' => \common\models\Business::STATUS_CONFIRMED])->count();

$expiredBusinessCount = \common\models\Business::find()
    ->where(['status' => \common\models\Business::STATUS_EXPIRED])->count();

//businesses which will expire in 30 days
$businessesWillExpireIn30Days = \common\models\Business::find()
    ->where(['between', 'expireDate', new \yii\db\Expression('NOW()'),
        new \yii\db\Expression('DATE_ADD(NOW(), INTERVAL 30 DAY)')]);
$in30daysDataProvider = new \yii\data\ActiveDataProvider([
    'query' => $businessesWillExpireIn30Days
]);

$unpaidBusinesses = \common\models\Business::find()
    ->where(['status' => \common\models\Business::STATUS_NOT_PAYED]);
$unpaidDataProvider = new \yii\data\ActiveDataProvider([
    'query' => $unpaidBusinesses
]);

//paid businesses which are not confirmed
$paidBusinesses = \common\models\Business::find()
    ->where(['status' => \common\models\Business::STATUS_PAYED]);
$paidDataProvider = new \yii\data\ActiveDataProvider([
    'query' => $paidBusinesses
]);

//transaction amount in past 30 days
$transactionsIn30Days = \aminkt\payment\models\TransactionSession::find()
    ->where(['status' => \aminkt\payment\models\TransactionSession::STATUS_PAID])
    ->andWhere(['between', 'createAt', new \yii\db\Expression('NOW()  - INTERVAL 30 DAY'),
        new \yii\db\Expression('NOW()')])->all();

//app installation in 10 days
$appInstallation = \common\models\Device::find()
    ->orderBy('createAt ASC')
    ->limit(10);
$appInstallationProvider = new \yii\data\ActiveDataProvider([
    'query' => $appInstallation,
    'pagination' => false
]);

$totalInstallationCount = \common\models\Device::find()->count();

?>
<?php if (Yii2DbRbac::checkInPageAccess(['dashboard-business'])): ?>
    <!--    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
    <!--        <a class="dashboard-stat dashboard-stat-v2 red" href="#">-->
    <!--            <div class="visual">-->
    <!--                <i class="fa fa-calendar-times-o"></i>-->
    <!--            </div>-->
    <!--            <div class="details">-->
    <!--                <div class="number">-->
    <!--                    <span data-counter="counterup">--><? //= $expiredBusinessCount ?><!--</span>-->
    <!--                </div>-->
    <!--                <div class="desc"> تعداد اصناف منقضی شده</div>-->
    <!--            </div>-->
    <!--        </a>-->
    <!--    </div>-->
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 green" href="#">
            <div class="visual">
                <i class="fa fa-check"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?= $confirmedBusinessCount ?></span>
                </div>
                <div class="desc"> تعداد اصناف فعال</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?= $businessCount ?></span>
                </div>
                <div class="desc"> تعداد کل اصناف</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-user"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?= $userCount ?></span>
                </div>
                <div class="desc"> کاربران ثبت شده</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
            <div class="visual">
                <i class="fa fa-mobile"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup"><?= $totalInstallationCount * 27 + 3 ?></span>
                </div>
                <div class="desc"> تعداد نصب اپلیکیشن</div>
            </div>
        </a>
    </div>
<?php endif; ?>
<?php if (Yii2DbRbac::checkInPageAccess(['dashboard-finance'])): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">آمار فروش سی روز گذشته</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $chartConfiguration = [
                        'type' => 'serial',
                        'theme' => 'light',
                        'dataProvider' => $chartData,
                        'categoryField' => 'day',
                        'startDuration' => 1,
                        'categoryAxis' => ['gridPosition' => 'start', 'axisColor' => 'orange'],
                        'valueAxes' => [['axisAlpha' => 0.2]],
                        'graphs' => [['type' => 'column',
                            'title' => 'فروش سی روز گدشته',
                            'valueField' => 'sale',
                            'lineAlpha' => 0,
                            'fillColors' => 'orange',
                            'fillAlphas' => 0.8,
                            'balloonText' => '[[title]] در  [[category]] : <b>[[value]]</b> تومان'
                        ]],
                    ];
                    echo speixoto\amcharts\Widget::widget([
                        'width' => '100%',
                        'chartConfiguration' => $chartConfiguration
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if (Yii2DbRbac::checkInPageAccess(['dashboard-device-data'])): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">آمار نصب اپلیکیشن</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $chartConfig = [
                        'type' => 'serial',
                        'theme' => 'light',
                        'dataProvider' => $deviceData,
                        'categoryField' => 'day',
                        'startDuration' => 1,
                        'categoryAxis' => ['gridPosition' => 'start', 'axisColor' => 'blue'],
                        'valueAxes' => [['axisAlpha' => 0.2]],
                        'graphs' => [['type' => 'column',
                            'title' => 'نصب اپلیکیشن در ۳۰ روز گذشته',
                            'valueField' => 'device',
                            'lineAlpha' => 0,
                            'fillColors' => 'blue',
                            'fillAlphas' => 0.8,
                            'balloonText' => '[[title]] در  [[category]] : <b>[[value]]</b> دفعه'
                        ]],
                    ];
                    echo speixoto\amcharts\Widget::widget([
                        'width' => '100%',
                        'chartConfiguration' => $chartConfig
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">جزییات ۱۰ نصب اخیر</h3>
                </div>
                <div class="panel-body">
                    <?php echo \yii\grid\GridView::widget([
                        'dataProvider' => $appInstallationProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'deviceId',
                                'label' => 'شناسه دستگاه'
                            ],
                            [
                                'attribute' => 'osType',
                                'label' => 'نوع سیستم عامل'
                            ],
                            [
                                'attribute' => 'osVersion',
                                'label' => 'نسخه سیستم عامل'
                            ],
                            [
                                'attribute' => 'deviceModel',
                                'label' => 'مدل دستگاه'
                            ],
                            [
                                'attribute' => 'appVersion',
                                'label' => 'نسخه اپلیکیشن'
                            ],
                            [
                                'attribute' => 'operator',
                                'label' => 'اپراتور'
                            ],
                            [
                                'attribute' => 'pusheId',
                                'label' => 'شناسه پوشه'
                            ],
                            [
                                'attribute' => 'createAt',
                                'value' => function ($model) {
                                    return \Yii::$app->getFormatter()->asDatetime($model->createAt, null);
                                }
                            ],
                            [
                                'attribute' => 'updateAt',
                                'value' => function ($model) {
                                    return \Yii::$app->getFormatter()->asDatetime($model->updateAt, null);
                                }
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (Yii2DbRbac::checkInPageAccess(['dashboard-business'])): ?>
    <div class="site-index">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">اصنافی که در ۳۰ روز آینده منقضی می شوند</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo \yii\grid\GridView::widget([
                            'dataProvider' => $in30daysDataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'cityName',
                                [
                                    'attribute' => 'ownerName',
                                    'label' => 'نام صاحب',
                                ],
                                [
                                    'label' => 'پلان',
                                    'attribute' => 'planId',
                                    'value' => function ($model) {
                                        return $model->plan->name;
                                    },
                                    'filter' => yii\helpers\ArrayHelper::map(\backend\models\Plan::find()->all(), 'id', 'name'),
                                ],
                                [
                                    'attribute' => 'expireDate',
                                    'value' => function ($model) {
                                        return \Yii::$app->getFormatter()->asDatetime($model->updateAt, null);
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update}',
                                    'buttons' => [
                                        'update' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-pencil"></i>', $url, [
                                                'title' => 'ویرایش',
                                                'class' => 'btn btn-primary'
                                            ]);
                                        },
                                        'view' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-size-fullscreen"></i>', $url);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-trash"></i>', $url);
                                        },
                                    ],
                                    'contentOptions'=>['style'=>'width: 30px;'],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        if ($action == 'update') {
                                            return ['/business/update', 'id' => $model->id];
                                        }
                                    }
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">اصناف پرداخت نشده</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo \yii\grid\GridView::widget([
                            'dataProvider' => $unpaidDataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'cityName',
                                [
                                    'attribute' => 'ownerName',
                                    'label' => 'نام صاحب',
                                ],
                                [
                                    'label' => 'پلان',
                                    'attribute' => 'planId',
                                    'value' => function ($model) {
                                        return $model->plan->name;
                                    },
                                    'filter' => yii\helpers\ArrayHelper::map(\backend\models\Plan::find()->all(), 'id', 'name'),
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update}',
                                    'buttons' => [
                                        'update' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-pencil"></i>', $url, [
                                                'title' => 'ویرایش',
                                                'class' => 'btn btn-primary'
                                            ]);
                                        },
                                        'view' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-size-fullscreen"></i>', $url);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-trash"></i>', $url);
                                        },
                                    ],
                                    'contentOptions'=>['style'=>'width: 30px;'],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        if ($action == 'update') {
                                            return ['/business/update', 'id' => $model->id];
                                        }
                                    }
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">اصناف تایید نشده</h3>
                    </div>
                    <div class="panel-body">
                        <?php echo \yii\grid\GridView::widget([
                            'dataProvider' => $paidDataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'name',
                                'cityName',
                                [
                                    'attribute' => 'ownerName',
                                    'label' => 'نام صاحب',
                                ],
                                [
                                    'label' => 'پلان',
                                    'attribute' => 'planId',
                                    'value' => function ($model) {
                                        return $model->plan->name;
                                    },
                                    'filter' => yii\helpers\ArrayHelper::map(\backend\models\Plan::find()->all(), 'id', 'name'),
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{update}',
                                    'buttons' => [
                                        'update' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-pencil"></i>', $url, [
                                                'title' => 'ویرایش',
                                                'class' => 'btn btn-primary'
                                            ]);
                                        },
                                        'view' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-size-fullscreen"></i>', $url);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return Html::a('<i class="icon-trash"></i>', $url);
                                        },
                                    ],
                                    'contentOptions'=>['style'=>'width: 30px;'],
                                    'urlCreator' => function ($action, $model, $key, $index) {
                                        if ($action == 'update') {
                                            return ['/business/update', 'id' => $model->id];
                                        }
                                    }
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php if (!(Yii2DbRbac::checkInPageAccess(['dashboard-business', 'dashboard-finance']))) : ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">معرفی نرم افزار شهر من</h3>
            </div>
            <div class="panel-body">

                <div class="jumbotron" style="padding: 15px;">
                    <h1>پرتال جامع شهری گیلان</h1>

                    <p class="lead">نسخه پیش روی شما نسخه اولیه میباشد. در صورت تمایل به تغییر داشبورد و یا منو و ظاهر
                        سایت با تیم
                        برنامه نویسی تماس بگیرید.</p>
                </div>

                <div class="body-content">

                    <div class="row">
                        <div class="col-lg-4">
                            <h2>صفحات ثابت و اخبار</h2>

                            <p>برای ایجاد صفحات ثابت از منوی سمت راست روی <strong>صفحات ثابت</strong> کلیک کنید. در زیر
                                منو شما
                                امکان نمایش تمام صفحات، ایجاد و یا ویرایش یک صفحه را خواهید داشت.<br>
                                اطلاعات مربوط به صفحه را وارد کنید و آن را در حالت انتشار قرار دهید تا در سایت قرار
                                گیرد.<br>
                            </p>

                            <p>
                                مطابق آنچه برای صفحات ثابت گفته شد میتوانید اقدام به ثبت خبر کنید. اخبار دارای دسته بندی
                                هستند و شما
                                میتوانید یک خبر را در چند دسته قرار دهید. اخبار در صفحه اصلی اپلیکیشن از طریق منو در
                                دسترس قرار
                                خواهند گرفت.
                            </p>

                            <p><a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/page/create']) ?>">ایجاد
                                    صفحه جدید
                                    &raquo;</a> <a class="btn btn-default"
                                                   href="<?= \yii\helpers\Url::to(['/news/create']) ?>">ایجاد
                                    خبر جدید &raquo;</a></p>
                        </div>
                        <div class="col-lg-4">
                            <h2>اصناف</h2>

                            <p>کار با اصناف در سیستم به سه بخش تقسیم میشود.</p>
                            <p> در ابتدا باید از طریق منوی راست روی <strong>پلن های فروش</strong> کلیک کنید و پلن های مد
                                نظر خود را
                                ایجاد کنید.</p>
                            <p>در مرحله بعد باید تعدادی دسته بندی برای اصناف ایجاد کنید.
                                <br> برای این کار از منو راست روی <strong>اصناف</strong> و سپس <strong>مدیریت دسته
                                    ها</strong> کلیک
                                کنید.
                                <br>توجه کنید که عمق پیمایش پیش فرض 3 سطح میباشد لذا شما حد اقل سه دسته باید در ابتدا
                                ایجاد کنید و
                                هر دسته باید دارای فرزند باشد. نهایی ترین دسته میتواند به عنوان دسته صنف انتخاب شود.</p>
                            <p>در مرحله بعد کاربر با ثبت نام در سایت درخواست ایجاد صنف میکند. سپس با پر کردن اطلاعات صنف
                                و پرداخت
                                هزینه پلن، صنف در انتظار تائید شما در می آید.</p>
                            <p>برای مشاهده و تائید اصناف در منوی راست روی <strong>اصناف</strong> و سپس روی <strong>مدیریت
                                    اصناف</strong> کلیک کنید.</p>
                            </p>

                            <p>
                                <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/plans/index']) ?>">1. ثبت
                                    پلن
                                    &raquo;</a>
                                <a class="btn btn-default"
                                   href="<?= \yii\helpers\Url::to(['/business-categories/index']) ?>">2. ثبت
                                    دسته &raquo;</a>
                                <a class="btn btn-default" href="<?= \yii\helpers\Url::to(['/business/index']) ?>">3.
                                    مدیریت اصناف
                                    &raquo;</a>
                            </p>
                        </div>
                        <div class="col-lg-4">
                            <h2>نحوه افزودن شهر های بیشتر</h2>

                            <p>سیستم به طور جامع و خودکار طراحی شده است تا با کمترین تنظیمات از تعداد نامحدودی شهر
                                پشتیبانی کند.
                                برای شروع تمام شهر های بزرگ استان گیلان در سیستم ثبت شده اند.</p>
                            <p> هنگام ثبت <strong>خبر و صفحات ثابت</strong> فیلد شهر را مطابق میل خود تنظیم کنید.</p>
                            <p> سپس با مراجعه به آدرس <code>http://shahreman.city/rasht</code> وارد صفحه رشت شوید. برای
                                شهر های دیگر
                                عبارت rasht را به عبارت لاتین شهر تغییر دهید. </p>
                            <p>اصناف نیز به انتخاب کاربران به شهر ها اضافه میشود. کاربران با مشخص کردن آدرس صنف خود، به
                                طور خودکار
                                به شهر موردنظر اضافه میشوند.</p>
                            <p><a class="btn btn-default" href="http://shahreman.city/rasht">سایت شهر رشت &raquo;</a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php endif; ?>
