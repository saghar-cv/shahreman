<?php

/* @var $this yii\web\View */

/* @var $model \backend\models\MobileSliderFrom */

use common\models\Post;
use kartik\sortable\Sortable;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->title = 'پنل مدیریتی - مدیریت اسلایدر های اپلیکیشن';
?>
    <div class="post-form">
        <div class="container-fluid">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class="col col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"> افزودن یک آیتم</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?php
                            $accessCities = \backend\components\GlobalComponent::getAccessCities();
                            $cities = \saghar\address\models\City::find();
                            if ($accessCities) {
                                $accessCities = $cities->where(['id' => $accessCities]);
                            }
                            $cities = $cities->all();
                            $cities = ArrayHelper::map($cities, 'id', 'name', 'stateName');

                            echo $form->field($model, 'cityId')->widget(\kartik\select2\Select2::class, [
                                'data' => $cities,
                                'options' => [
                                    'id' => 'slider-city',
                                    'placeholder' => 'شهر را انتخاب کنید ...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);

                            echo $form->field($model, 'place')->widget(\kartik\select2\Select2::class, [
                                'data' => $model::getPlaceList(),
                                'options' => [
                                    'id' => 'slider-place',
                                    'placeholder' => 'مکان قرار گیری را انتخاب کنید ...'
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);

                            $url = \yii\helpers\Url::to(['site/mobile-slider-generator']);

                            $js = <<<JS
$("#slider-place").on('change', function() {
  window.location = "{$url}&cityId="+$("#slider-city").val()+"&place="+$(this).val();
})
JS;

                            $this->registerJs($js);


                            ?>

                            <?= $form->field($model, 'title')->textInput() ?>

                            <?= $form->field($model, 'caption')->textInput() ?>

                            <?php

                            echo $form->field($model, 'type')->dropDownList($model::getTypeList(), [
                                'id' => 'postType'
                            ]);
                            ?>
                            <?php
                            $js = <<<JS
let postType = $("#postType").val();
if(postType != '/page/view'){
  $("#post-selection").css('display', 'none');
}
if(postType != '/business/list'){
  $("#business-category-selection").css('display', 'none');
}
if (postType!='/business/view'){
  $("#business-selection").css('display','none');
}

$("#postType").on('change', function() {
    let postType = $(this).val();
    if(postType == '/page/view'){
      $("#business-selection").css('display','none');
      $("#post-selection").css('display', 'block');
      $("#business-category-selection").css('display', 'none');
    } else if (postType == '/business/list'){
      $("#business-selection").css('display','none');  
      $("#post-selection").css('display', 'none');
      $("#business-category-selection").css('display', 'block');
    } else if(postType=='/business/view'){
         $("#business-selection").css('display','block');
         $("#post-selection").css('display', 'none');
         $("#business-category-selection").css('display', 'none');
    }
    else{
      $("#business-selection").css('display','none');
      $("#post-selection").css('display', 'none');
      $("#business-category-selection").css('display', 'none');
    }
});
JS;
                            $this->registerJs($js);
                            ?>

                            <div id="business-category-selection">
                                <?php
                                $categories = \saghar\category\models\Category::find()->where(['section' => 'business', 'depth' => 0])->asArray()->all();
                                $categories = ArrayHelper::map($categories, 'id', 'name');
                                echo $form->field($model, 'categoryId')->widget(\kartik\select2\Select2::class, [
                                    'data' => $categories
                                ]);
                                ?>
                            </div>

                            <div id="business-selection">
                                <?php
                                $business = \common\models\Business::find()
                                    ->where(['status' => \common\models\Business::STATUS_CONFIRMED])->all();
                                $business = ArrayHelper::map($business, 'id', 'name');
                                echo $form->field($model, 'businessId')->widget(\kartik\select2\Select2::class, [
                                    'data' => $business
                                ]);
                                ?>
                            </div>


                            <div id="post-selection">
                                <?php
                                $post = Post::find()->where(['postType' => Post::TYPE_STATIC_POST, 'cityId' => $model->cityId])->asArray()->all();
                                $post = ArrayHelper::map($post, 'id', 'title');
                                echo $form->field($model, 'postId')->widget(\kartik\select2\Select2::class, [
                                    'data' => $post
                                ]);
                                ?>
                            </div>


                            <?= $form->field($model, 'img')->widget(\aminkt\uploadManager\components\UploadManager::className(), [
                                'id' => 'upload-menu-pic',
                                'titleTxt' => 'تصویر اسلاید',
                                'helpBlockEnable' => false,
                                'showImageContainer' => '#itemId',
                                'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                                'btnTxt' => 'انتخاب تصویر'
                            ])->label(false) ?>
                            <div id="itemId"></div>


                            <?= \yii\helpers\Html::submitButton('ثبت', ['class' => 'btn btn-success', 'style' => 'margin-top:15px; float:left']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"> ساختار اسلایدر</h3>
                    </div>
                    <div class="panel-body">
                        <?php
                        $url = \yii\helpers\Url::to(['/site/mobile-slider-update-pos', 'city' => $model->cityId, 'place' => $model->place]);
                        $js = <<<JS
                        function (e, ui) {
                            let currentItems = e.target.children;
                            let currentIndexes = [];
                            for(let i=0; i<currentItems.length; i++){
                                currentIndexes[i] = currentItems[i].dataset.key;
                                console.log(currentIndexes[i]);
                                }

                                $.ajax({
                        url: "$url", 
                        method: "POST",
                        data: {
                            data: currentIndexes
                            },
                        dataType: "json",
                        success: function(result){
                                console.log(result);
                        for(let i=0; i<currentItems.length; i++){
                             currentItems[i].dataset.key = i;
                            }     
                        },
                        error: function(){
                        alert("خطا در هنگام ویرایش رخ داده است.")
                        }
                         });
                        }
JS;

                        echo Sortable::widget([
                            'id' => 'sortable-container',
                            'type' => Sortable::TYPE_LIST,
                            'pluginEvents' => [
                                'sortupdate' => $js,
                            ],
                            'items' => convertMenuFormToSortableItems($model->sliderArray, $model->cityId, $model->place)
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

<?php
function convertMenuFormToSortableItems($arr, $cityId, $place)
{

    $items = [];
    foreach ($arr as $key => $item) {
        try {
            $img = \aminkt\uploadManager\UploadManager::getInstance()->image($item['image']);
        } catch (\yii\web\NotFoundHttpException $e) {
            $img = \aminkt\uploadManager\UploadManager::getInstance()->noImage;
        }
        $deleteUrl = \yii\helpers\Url::to(['/site/mobile-slider-delete', 'pos' => $key, 'city' => $cityId, 'place' => $place]);
        $updateUrl = \yii\helpers\Url::to(['/site/mobile-slider-update', 'pos' => $key, 'city' => $cityId, 'place' => $place]);
        $html = <<<HTML
<div class="menu-item">
    <img src="{$img}">
    <div class="menu-content">
        <h4>{$item['title']}</h4>
        <p>{$item['caption']}</p>
    </div>
    <a class="text-danger delete" href="{$deleteUrl}"><i class="icon-close"></i></a>
    <a class="text-danger delete" href="{$updateUrl}"><i class="icon-pencil"></i></a>
</div>
HTML;

        $items[] = [
            'content' => $html,
            'options' => [
                'data' => [
                    'id' => $key,
                    'key' => $key
                ],
            ],

        ];
    }


    return $items;
}