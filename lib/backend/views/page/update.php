<?php

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = 'ویرایش صفحه';
$this->params['breadcrumbs'][] = ['label' => 'صفحات', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = ['label' => 'ویرایش'];
?>
<div class="post-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
