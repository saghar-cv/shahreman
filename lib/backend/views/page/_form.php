<?php

use aminkt\widgets\google\map\LocationInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model \backend\models\StaticPageForm */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="post-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
    $form->scrollToError = true;
    $form->scrollToErrorOffset = 130;
    ?>
    <div class="container-fluid">
        <div class="col col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> محتوا</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, 'summary')->textarea(['rows' => 2, 'maxlength' => true]); ?>
                        <?=
                        $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                            'options' => ['rows' => 10],
//                                'preset' => 'custom',
                            'clientOptions' => [
                                'allowedContent' => true,
                                'extraPlugins' => 'glyphicons,widget,lineutils,widgetselection',
                                'contentsCss' => Url::base(true) . '/ckeditor-plugins/glyphicons/css/css/bootstrap.css',
                                'toolbarGroups' => [
//                                        ['name' => 'undo'],
                                    ['name' => 'insert', 'groups' => ['glyphicons', 'Source']],
//                                        ['name' => 'paragraph', 'groups' => ['list']],
//                                        ['name' => 'basicstyles'],
//                                        ['name' => 'links', 'groups' => ['links', 'insert']],
                                ],
//                                    'removeButtons' => 'Anchor,Subscript,Superscript,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
//                                    'removePlugins' => 'elementspath',
                                'language' => 'fa'
                            ]
                        ]);
                        $this->registerJs("CKEDITOR.plugins.addExternal('glyphicons', '" . Url::base(true) . "/ckeditor-plugins/glyphicons/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('widget', '" . Url::base(true) . "/ckeditor-plugins/widget/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('lineutils', '" . Url::base(true) . "/ckeditor-plugins/lineutils/plugin.js', '');");
                        $this->registerJs("CKEDITOR.plugins.addExternal('widgetselection', '" . Url::base(true) . "/ckeditor-plugins/widgetselection/plugin.js', '');");
                        $this->registerJs('CKEDITOR.dtd.$removeEmpty["span"] = false;');
                        ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> نقشه</h3>
                </div>
                <div class="panel-body">
                    <div style="margin-bottom: 15px">
                        <?= $form->field($model, 'isMapEnabled')->radioList([
                            false => 'نقشه غیرفعال باشد',
                            true => 'نقشه فعال باشد'
                        ]) ?>
                    </div>
                    <br>
                    <?php

                    $css = <<<CSS
.map-input-control{
width: 60%;
top: 7px !important;
}
CSS;
                    $js = <<<JS
$(document).on('click', '.field-staticpageform-map', function(){
    $("#staticpageform-ismapenabled").find("input[value=1]").prop('checked', true);
}).change();
JS;
                    $this->registerJs($js);
                    $this->registerCss($css);
                    echo $form->field($model, 'map')->widget(LocationInput::class, [
                        'apiKey' => 'AIzaSyBjFztI9oEaunJToOiAQ9lSS8Bfbre3BNo',
                        'options' => [
                            'class' => 'form-control inline-editable',
                        ],
                        'mapOptions' => [
                            'center' => [
                                'lat' => 37.28083300,
                                'lng' => 49.58305600,
                            ],
                            'zoom' => 10,
                        ],
                        'markerOptions' => [
                            'draggable' => true,
                        ],
                        'searchInputOptions' => [
                            'class' => 'map-input-control form-control',
                        ],
                        'disableLocationPicker' => 0, // Or 1 to define input become enabled or not to use just map view.
                        'width' => '100%', // Map container width
                        'height' => '400px', // Map container height
                    ])->label(false);
                    ?>
                </div>
            </div>

        </div>
        <div class="col col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"> انتشار</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <?= $form->field($model, 'status')->dropDownList([
                            \common\models\Post::STATUS_PUBLISH => 'انتشار',
                            \common\models\Post::STATUS_DRAFT => 'پیش نویس',
                        ])->label('وضعیت انتشار') ?>
                    </div>
                    <div class="form-group">
                        <?php
                        $cities = \saghar\address\models\City::find();
                        $accessCities = \Yii::$app->getUser()->getIdentity()->accessCities;
                        if ($accessCities) {
                            $cities = $cities->where(['id' => explode(',', $accessCities)]);
                        }
                        $cities = $cities->all();
                        $cities = \yii\helpers\ArrayHelper::map($cities, 'id', 'name', 'stateName');
                        if ($currentCity = \backend\components\GlobalComponent::getCurrentCity() and !$model->cityId) {
                            $model->cityId = $currentCity->id;
                        }
                        echo $form->field($model, 'cityId')->widget(\kartik\select2\Select2::className(), [
                            'data' => $cities,
                            'options' => ['placeholder' => 'شهر را انتخاب کنید ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>


                    <div class="form-group">
                        <?= $form->field($model, 'tags')->widget("aminkt\widgets\inputTag\InputTag", [
                            'options' => [
                                'maxlenth' => true,
                                'class' => 'form-control maxlength-handler'
                            ]
                        ]) ?>
                    </div>

                    <div class="form-group">
                        <?php if ($postModel = $model->getPostModel()) : ?>
                            <?= Html::button('مدیریت زیر صفحه ها', ['id' => 'sub-page-modal-btn', 'value' => \yii\helpers\Url::to(['/page/ajax-add-sub-page', 'postId' => $postModel->id]), 'class' => 'btn btn-primary']) ?>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <?php

                        $style = [
                            \common\models\Post::STYLE_DEFAULT => 'پیشفرض تنظیمات',
                            \common\models\Post::STYLE_CLASSIC => 'معمولی (تصویر - عنوان - توضیحات)',
                            \common\models\Post::STYLE_BIG => 'ویژه (تصویر بزرگ - عنوان - توضیحات)',
                            \common\models\Post::STYLE_JUST_PHOTO => 'فقط تصویر',
                            \common\models\Post::STYLE_JUST_TITLE => 'فقط عنوان',
                            \common\models\Post::STYLE_TITLE_AND_DESCRIPTION => 'عنوان - توضیحات'
                        ];

                        echo $form->field($model, 'relatedPagesViewType')->widget(\kartik\select2\Select2::className(), [
                            'data' => $style,
                            'options' => ['placeholder' => 'استایل را انتخاب کنید ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('استایل نمایش مطالب مرتبط در اپ')
                        ?>
                    </div>
                    <div class="form-group">
                        <?php

                        $style = [
                            \common\models\Post::STYLE_DEFAULT => 'پیشفرض تنظیمات',
                            \common\models\Post::STYLE_CLASSIC => 'معمولی (تصویر - عنوان - توضیحات)',
                            \common\models\Post::STYLE_BIG => 'ویژه (تصویر بزرگ - عنوان - توضیحات)',
                            \common\models\Post::STYLE_JUST_PHOTO => 'فقط تصویر',
                            \common\models\Post::STYLE_JUST_TITLE => 'فقط عنوان',
                            \common\models\Post::STYLE_TITLE_AND_DESCRIPTION => 'عنوان - توضیحات'
                        ];

                        echo $form->field($model, 'childViewType')->widget(\kartik\select2\Select2::className(), [
                            'data' => $style,
                            'options' => ['placeholder' => 'استایل را انتخاب کنید ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('استایل نمایش مطالب صفحه فرزند مرتبط در اپ');

                        ?>
                    </div>
                    <div>
                        <?php
                        $parents = $model->parentPosts;
                        if (!empty($parents)) {
                            echo '<label>صفحات والد</label>';
                            echo "<ul>";
                            foreach ($parents as $parent) {
                                echo '<li>' . $parent->title . '</li>';
                            }
                            echo '</ul>';
                        }
                        ?>
                    </div>
                    <div class="form-group ">
                        <?= Html::submitButton('ثبت پست', ['class' => 'btn btn-success save-post-btn', 'style' => 'float: left;']) ?>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">تصویر شاخص</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group main-image-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-user-pic',
                            'model' => $model,
                            'attribute' => 'coverId',
                            'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_IMAGE,
                            'titleTxt' => 'تصویر شاخص را وارد کنید.',
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#coverId',
                            'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                            'btnTxt' => 'جایگذاری تصویر شاخص'
                        ]);
                        ?>
                        <div id="coverId"></div>
                    </div>
                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">اسلایدر</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group slider-image-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-slider-pic',
                            'model' => $model,
                            'attribute' => 'slider',
                            'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_IMAGE,
                            'multiple' => true,
                            'titleTxt' => 'تصویر اسلایدر را وارد کنید.',
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#slider',
                            'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                            'btnTxt' => 'جایگذاری اسلایدر'
                        ]);
                        ?>
                        <div id="slider"></div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">ویدئو</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group video-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-video',
                            'model' => $model,
                            'attribute' => 'video',
                            'titleTxt' => 'ویدئو را وارد کنید.',
                            'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_VIDEO,
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#video_container',
                            'showImagesTemplate' => "<video width='100%' height='300px' controls><source src='{url}' type='video/{file_extension}'>سیستم شما از ویدیو پشتیبانی نمیکند. </video>",
                            'btnTxt' => 'جایگذاری ویدئو'
                        ]);
                        ?>
                        <div id="video_container"></div>

                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">گالری</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group gallery-image-input">
                        <?php echo \aminkt\uploadManager\components\UploadManager::widget([
                            'id' => 'upload-gallery-pic',
                            'model' => $model,
                            'attribute' => 'gallery',
                            'mediaType' => \aminkt\uploadManager\models\UploadmanagerFiles::FILE_TYPE_IMAGE,
                            'multiple' => true,
                            'titleTxt' => 'تصویر گالری را وارد کنید.',
                            'helpBlockEnable' => false,
                            'showImageContainer' => '#gallery',
                            'showImagesTemplate' => "<img src='{url}' class='img-responsive'>",
                            'btnTxt' => 'جایگذاری گالری'
                        ]);
                        ?>
                        <div id="gallery"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<script>
    $('#w0').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
<?php if ($postModel = $model->getPostModel()) : ?>
    <style>
        .model-lg {
            width: 80%;
        }
    </style>
    <?php

    \yii\bootstrap\Modal::begin([
        'header' => '<h4>مدیریت زیر صفحه ها</h4>',
        'id' => 'sub-page-modal',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
        'size' => 'model-lg',
    ]);
    echo "<div id='modelContent'></div>";
    \yii\bootstrap\Modal::end();

    $js = <<<JS
$(function(){
    $('#sub-page-modal-btn').click(function(){
        $('#sub-page-modal').modal('show')
            .find('#modelContent')
            .load($(this).attr('value'));
    });
});
JS;
    $this->registerJs($js);
    ?>

<?php endif; ?>

