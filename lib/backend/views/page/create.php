<?php


/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = 'ایجاد صفحه';
$this->params['breadcrumbs'][] = ['label' => 'صفحات ثابت', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
