<?php
/** @var \yii\web\View $this */
/** @var Post $postModel */
/** @var PostSubPages $model */

/** @var PostSubPages[] $postSubPages */

use backend\models\PostSubPages;
use common\models\Post;
use kartik\sortable\Sortable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$pageState = $model::TYPE_PAGE;
$businessCategoryState = $model::TYPE_BUSINESS_CATEGORY;
$js = <<<JS
$(document).on('change', '#suggestion-type-selector', function(){
   let val = $(this).val();
   let pageList = $("#page-list");
   let businessCategoryList = $("#business-category-list");
   
   console.log("dssd");
   pageList.css('display', 'none');
   businessCategoryList.css('display', 'none');
   
   if(val === '$pageState'){
      pageList.css('display', 'block');
   }else if(val === '$businessCategoryState') {
      businessCategoryList.css('display', 'block');
   }
});
JS;
$this->registerJs($js);


\yii\widgets\Pjax::begin([
    'enablePushState' => false,
    'id' => 'sub_page_pjax_container',
]); ?>
    <div class="row">
        <div class="col-md-4">
            <h4>انتخاب زیر صفحه</h4>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'action' => ['/page/ajax-add-sub-page', 'postId' => $postModel->id],
                'enableAjaxValidation' => false,
                'options' => [
                    'data-pjax' => true,
                ]
            ]); ?>

            <?= $form->field($model, 'suggestionType')->dropDownList([
                null => 'یک مورد را انتخاب کنید',
                PostSubPages::TYPE_PAGE => 'صفحات ثابت',
                PostSubPages::TYPE_BUSINESS_CATEGORY => 'لیست کسب و کار های صنف',
            ], [
                'id' => 'suggestion-type-selector'
            ]); ?>


            <div id="page-list" style="display: none">
                <?php
                $post = Post::find()->where(['postType' => Post::TYPE_STATIC_POST]);
                if ($postModel->cityId) {
                    $post = $post->andWhere(['cityId' => $postModel->cityId])->orWhere(['cityId' => null]);
                }
                $post = $post->asArray()->all();
                $post = ArrayHelper::map($post, 'id', 'title');
                echo $form->field($model, 'pageId')->widget(\kartik\select2\Select2::class, [
                    'data' => $post,
                ]);
                ?>
            </div>

            <div id="business-category-list" style="display: none">
                <?php
                $categories = \saghar\category\models\Category::find()->where(['section' => 'business', 'depth' => 0])->asArray()->all();
                $categories = ArrayHelper::map($categories, 'id', 'name');
                echo $form->field($model, 'businessCategoryId')->widget(\kartik\select2\Select2::class, [
                    'data' => $categories
                ]);
                ?>
            </div>

            <?= Html::submitButton('افزودن', ['class' => 'btn btn-primary']) ?>

            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
        <div class="col-md-8">
            <?php
            if ($postSubPages) :
                $updateUrl = \yii\helpers\Url::to(['page/ajax-update-sub-page-order', 'postId' => $postModel->id]);
                $js = <<<JS
function (e, ui) {
    let currentItems = e.target.children;
    let currentIndexes = [];
    
    for(let i=0; i<currentItems.length; i++){
        currentIndexes[i] = {
            key : currentItems[i].dataset.index,
            sortNum : currentItems[i].dataset.sortnum,
            suggestType : currentItems[i].dataset.suggesttype,
            suggestId : currentItems[i].dataset.suggestid,
         };
    }
    
    console.log(currentIndexes);
    
    $.ajax({
        url: "$updateUrl", 
        method: "POST",
        data: {
            data: currentIndexes
        },
        dataType: "json",
        success: function(result){
            console.log(result);
        },
        error: function(){
            alert("خطا در هنگام ویرایش رخ داده است.")
        }
    });
}
JS;

                echo Sortable::widget([
                    'id' => 'sortable-container',
                    'type' => Sortable::TYPE_LIST,
                    'pluginEvents' => [
                        'sortupdate' => $js,
                    ],
                    'items' => convertMenuFormToSortableItems($postSubPages)
                ]);
            else : ?>
                فرزندی یافت نشد.
            <?php endif; ?>
        </div>
    </div>
<?php \yii\widgets\Pjax::end(); ?>

<?php
/**
 * @param PostSubPages[] $subpages
 * @return array
 */
function convertMenuFormToSortableItems($subpages)
{

    $items = [];
    foreach ($subpages as $key => $subpage) {
        $deleteUrl = \yii\helpers\Url::to([
            '/page/ajax-remove-sub-page',
            'postId' => $subpage->postId,
            'type' => $subpage->suggestionType,
            'sid' => $subpage->suggestionId
        ]);

        $subPageModel = $subpage->getSubPageModel();
        $img = null;
        $des = '';

        if ($subpage->isPage()) {
            $title = $subPageModel->title;

        } elseif ($subpage->isBusinessCategory()) {
            $title = $subPageModel->name;
        }

        $html = <<<HTML
<div class="menu-item">
    <!--<img src="{$img}"> -->
    <div class="menu-content">
        <h4>{$title}</h4>
        <p>{$des}</p>
    </div>
    <a class="text-danger delete" href="{$deleteUrl}" data-pjax><i class="icon-close"></i></a>
</div>
HTML;

        $items[] = [
            'content' => $html,
            'options' => [
                'data' => [
                    'index' => $key,
                    'suggestid' => $subpage->suggestionId,
                    'suggesttype' => $subpage->suggestionType,
                    'sortnum' => $subpage->sortNum
                ],
            ],

        ];
    }


    return $items;
}
