<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //BEGIN GLOBAL MANDATORY STYLES
        '../assets/template/global/plugins/font-awesome/css/font-awesome.min.css',
        '../assets/template/global/plugins/simple-line-icons/simple-line-icons.min.css',
        '../assets/template/global/plugins/bootstrap/css/bootstrap-rtl.min.css',
        '../assets/template/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css',


        //BEGIN THEME GLOBAL STYLES
        '../assets/template/global/css/components-md-rtl.min.css',
        '../assets/template/global/css/plugins-md-rtl.min.css',

        //BEGIN THEME LAYOUT STYLES
        '../assets/template/layouts/layout4/css/layout-rtl.min.css',
        '../assets/template/layouts/layout4/css/themes/light-rtl.min.css',
        '../assets/css/fontiran.css',
        '../assets/template/layouts/layout4/css/custom-rtl.min.css',

        //BEGIN CUSTOM CSS
        'assets/css/site.css',
    ];
    public $js = [
        //BEGIN CORE PLUGINS
        '../assets/template/global/plugins/js.cookie.min.js',
        '../assets/template/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
        '../assets/template/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        '../assets/template/global/plugins/jquery.blockui.min.js',
        '../assets/template/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',

        //BEGIN THEME GLOBAL SCRIPTS
        '../assets/template/global/scripts/app.min.js',

        //BEGIN THEME LAYOUT SCRIPTS
        '../assets/template/layouts/layout4/scripts/layout.min.js',
        '../assets/template/layouts/layout4/scripts/demo.min.js',
        '../assets/template/layouts/global/scripts/quick-sidebar.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
