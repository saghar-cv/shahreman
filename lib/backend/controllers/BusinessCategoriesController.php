<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\models\CategoryRelation;
use Imagine\Exception\InvalidArgumentException;
use saghar\category\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class CategoryController
 *
 * @package backend\controllers
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessCategoriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'businessManager'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the view for business category manager
     *
     * @param null $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id = null)
    {
        if ($id) {
            $model = Category::findOne($id);
            if (!$model) {
                throw new NotFoundHttpException("دسته مورر نظر یافت نشد");
            }
        } else {
            $model = new Category();
        }
        if (\Yii::$app->getRequest()->isPost) {
            try {
                $category = \Yii::$app->getRequest()->post('Category');
                $category['section'] = CategoryRelation::SECTION_BUSINESS;
                if (!$category['parentId']) {
                    $category['depth'] = 0;
                } else {
                    $parent = Category::findOne($category['parentId']);
                    if (!$parent) {
                        throw new InvalidArgumentException('والد مورد نظر مورد تایید نمی باشد');
                    }
                    $category['depth'] = $parent->depth + 1;
                }
                if ($model->isNewRecord) {
                    $model = Category::create($category);
                } else {
                    $model = Category::edit($id, $category);
                }
            } catch (NotFoundHttpException $e) {
                Alert::error('خطا در انجام عملیات', 'دسته مورد نظر ایجاد نشد');
            } catch (\RuntimeException $e) {
                Alert::error("خطا در ذخیره اطلاعات", "دسته مورد نظر ذخیره نشد");
            }
            return $this->redirect(['index']);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * Delete business category
     *
     * @param $id
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $category = Category::findOne($id);
        if ($category) {
            try {
                $category->setStatus(Category::STATUS_REMOVED);
                Alert::success('عملیات با موفقیت انجام شد', 'دسته مورد نظر حذف شد');
                $this->redirect(['index']);
            } catch (\Exception $e) {
                \Yii::error($e->getMessage());
                Alert::error('خطا در انجام عملیات', 'دسته مورد نظر حذف نشد');
            }
        } else {
            throw new NotFoundHttpException("دسته پیدا نشد");
        }
    }

}

