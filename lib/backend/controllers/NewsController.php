<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\components\GlobalComponent;
use backend\models\NewsForm;
use backend\models\search\PostSearch;
use common\models\Post;
use Yii;
use yii\base\InvalidValueException;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * NewsController implements the CRUD actions for Post model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $searchModel->postType = $searchModel::TYPE_NEWS_POST;
        if ($city = GlobalComponent::getCurrentCity()) {
            $searchModel->cityId = $city['id'];
        }
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Post model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NewsForm();
        if (Yii::$app->getRequest()->isPost) {
            $model->coverId = Yii::$app->getRequest()->post('coverId');
        }
        if ($model->load(Yii::$app->request->post())) {

            try {
                $post = $model->save();
                Alert::success("پست با موفقیت ذخیره شد.", "عنوان پست: {$model->title}");
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Alert::error('خطا', 'صفحه ی ساخته شده ذخیره نشد');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $model = NewsForm::find($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $model->update($id);
                Alert::success("خبر با موفقیت ویرایش شد.", '');
                return $this->redirect(['update', 'id' => $model->getPostModel()->id]);
            } catch (InvalidValueException $e) {
                Alert::error("خطا", "لطفا ورودی های خود را کنترل کنید.");
                Yii::warning($model->getErrors());
                Yii::error($e->getMessage());
            } catch (ForbiddenHttpException $e) {
                Alert::error("خطا", $e->getMessage());
                Yii::error($e->getMessage());
            } catch (\Exception $e) {
                Alert::error("خطا", "خبر ساخته شده بروزرسانی نشد");
                Yii::error($e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * 
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->delete()) {
                Alert::warning("خبر {$model->title} حذف شد.", "");
            } else {
                throw new \RuntimeException("Cant delete model");
            }
        } catch (StaleObjectException | \Exception | \Throwable $exception) {
            Alert::error("خطا در اجرای عملیات!", "خبر {$model->title} حذف نشد.");
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Set news important
     *
     * @param $id
     *
     * @return \yii\web\Response
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionSetImportant($id)
    {
        $news = Post::findOne($id);

        if (!$news) {
            Alert::error('خطا', 'خبر معتبر نیست');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $news->setImportant();
        return $this->redirect(Yii::$app->request->referrer);
    }
}
