<?php
/**
 * Created by PhpStorm.
 * User: Mohammad_Pvn
 * Date: 3/14/2018
 * Time: 9:44 AM
 */

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use frontend\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\rest\Controller;

class UserController extends Controller
{

    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * List of all users
     * @return mixed
     * @author  mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
            'sort' => [
                'defaultOrder' => [
                    'createAt' => SORT_DESC
                ]
            ]
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @author  mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public function actionUpdate($id)
    {
        $model = User::findOne($id);

        if (Yii::$app->request->isPost) {

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    Alert::success("کاربر با موفقیت ویرایش شد.", '');
                    return $this->redirect(['index', 'id' => $model->getId()]);
                } else {
                    Alert::error("خطا در ذخیره اطلاعات", "تغییرات در پایگاه داده ذخیره نشد.");
                    Yii::error($model->getErrors());
                }

            }

        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }
}