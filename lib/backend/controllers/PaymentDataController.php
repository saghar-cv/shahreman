<?php

namespace backend\controllers;
use backend\models\search\PaymentSearch;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class PaymentController
 *
 * @package backend\controllers
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class PaymentDataController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'finance'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Index action for payment data controller
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

}