<?php

namespace backend\controllers;

use backend\models\AccessForm;
use backend\models\Plan;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * Class PlanController
 *
 * @package backend\controllers
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class PlanController extends \yii\web\Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['remove', 'update', 'create', 'index'],
                        'allow' => true,
                        'roles' => ['admin', 'businessManager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                ],
            ],
        ];
    }

    /**
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $plans = Plan::find()->where(['!=', 'status', Plan::STATUS_REMOVED]);
        if (!$plans) {
            throw new NotFoundHttpException('پلانی یافت نشد');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $plans
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Create plan
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new AccessForm();

        if (\Yii::$app->getRequest()->isPost and $model->load(\Yii::$app->getRequest()->post())) {
            $plan = $model->save(\Yii::$app->getRequest()->post('AccessForm'));
            return $this->redirect(['index']);
        }
        return $this->render('update_plan', [
            'model' => $model
        ]);
    }

    /**
     * Update plan
     *
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $form = AccessForm::find($id);
        if ($form->load(\Yii::$app->getRequest()->post())) {
            $form->save(\Yii::$app->getRequest()->post('AccessForm'), $id);
            return $this->redirect(['index']);
        }
        return $this->render('update_plan', [
            'model' => $form
        ]);
    }

    /**
     * Delete plan by changing status
     *
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionRemove($id)
    {
        $plan = Plan::findOne($id);
        if ($plan) {
            $plan->setStatus(Plan::STATUS_REMOVED);
            return $this->redirect(['index']);
        } else {
            throw new NotFoundHttpException('پلان مورد نظر پیدا نشد');
        }
    }
}
