<?php


namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\models\SettingForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii2mod\settings\events\FormEvent;


/**
 * Class SettingController
 * Handle styles .
 *
 * @package backend\controllers
 *
 * @author mohammad parvaneh <mohammad.pvn1375@gmailcom>
 */
class SettingController extends Controller
{


    /**
     * @inheritdoc
     */
    public function behaviors()

    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [

                    [
                        'actions' => ['settings'],
                        'allow' => true,
                        'roles' => ['admin', 'contentManager']
                    ]

                ],

            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'settings' => [
                'class' => \yii2mod\settings\actions\SettingsAction::class,
                'sectionName' => SettingForm::SETTING_SECTION_CONTENT,
                'successMessage' => null,
                'prepareModel' => function ($model) {
                    return;
                },
                // also you can use events as follows:
                'on beforeSave' => function ($event) {
                    /** @var FormEvent $event */
                    /** @var SettingForm $form */
                    $form = $event->getForm();
                    $result = '';
                    $i = 0;
                    foreach ($form->activeCities as $city) {
                        if ($i++ > 0) {
                            $result .= ',';
                        }
                        $result .= $city;
                    }
                    $form->activeCities = $result;
                },
                'on afterSave' => function ($event) {
                    Alert::success('عملیات با موفقیت انجام شد.', 'تنظیمات در سیستم ذخیره شد.');
                },
                'modelClass' => SettingForm::class,
            ],
        ];
    }


}