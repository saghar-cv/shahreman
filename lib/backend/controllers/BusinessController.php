<?php

namespace backend\controllers;

use aminkt\normalizer\Normalize;
use aminkt\widgets\alert\Alert;
use backend\components\GlobalComponent;
use backend\models\BusinessMetaForm;
use backend\models\CategoryRelation;
use common\models\BaseActiveRecord;
use common\models\Business;
use common\models\search\BusinessSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class BusinessController
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessController extends Controller
{
    /**
     * Index page for business, shows all businesses
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BusinessSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Update business
     *
     * @param $id
     *
     * @return string
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionUpdate($id)
    {
        $accessCities = GlobalComponent::getAccessCities();
        $model = Business::find()->where(['id' => $id]);
        if ($accessCities) {
            $model = $model->leftJoin('{{%business_relation}}', 'id = businessId');
            if ($city = GlobalComponent::getCurrentCity()) {
                $model = $model->andWhere(['cityId' => $city->id]);
            } else {
                $model = $model->andWhere(['cityId' => $accessCities]);
            }
        }
        $model = $model->one();
        if (!$model) {
            throw new NotFoundHttpException('ًصنف مورد نظر یافت نشد');
        }
        if ($model->expireDate) {
            $model->expireDate = \Yii::$app->getFormatter()->asDatetime($model->expireDate, 'Y-M-d');
            $model->expireDate = Normalize::englishNumbers($model->expireDate);
        }
        $form = BusinessMetaForm::find($id);
        if (!$form) {
            throw new NotFoundHttpException('ًصنف مورد نظر یافت نشد');
        }

        $businessCategory = CategoryRelation::findOne(['businessId' => $id]);
        if (!$businessCategory) {
            throw new NotFoundHttpException('دسته صنف یافت نشد');
        }

        if ($model->load(\Yii::$app->getRequest()->post())) {
            $businessCategory->catId = \Yii::$app->getRequest()->post('CategoryRelation')['catId'];
            if ($model->expireDate) {
                $model->expireDate = BaseActiveRecord::convertJalaliDateToSqlDateTime($model->expireDate);
            }
            if ($model->save()) {
                if ($businessCategory->save()) {
                    Alert::success('عملیات با موفقیت انجام شد', 'صنف پایه اصناف ویرایش شد.');
                    return $this->redirect(['business/update', 'id' => $id]);
                } else {
                    Alert::warning('تغییری در اطلاعات پایه اصناف ایجاد نشد.', '');
                }
            } else {
                Alert::warning('تغییری در اطلاعات پایه اصناف ایجاد نشد.', '');
            }
        }


        if ($form->load(\Yii::$app->getRequest()->post())) {
            try {
                if ($form->save(\Yii::$app->getRequest()->post('BusinessMetaForm'), $id)) {
                    Alert::success('عملیات با موفقیت انجام شد', 'اطلاعات فرعی اصناف ویرایش شد.');
                    return $this->redirect(['business/update', 'id' => $id]);
                } else {
                    Alert::warning('تغییری در اطلاعات فرعی صنف ایجاد نشد.', '');
                }
            } catch (BadRequestHttpException $e) {
                Alert::error('عملیات مورد نظر انجام نشد', 'درخواست وارد شده صحیح نیست.<br>' . $e->getMessage());
            } catch (NotFoundHttpException $e) {
                Alert::error('عملیات مورد نظر انجام نشد', 'دسترسی مورد نظر یافت نشد. <br>' . $e->getMessage());
            } catch (ServerErrorHttpException $e) {
                Alert::error('عملیات مورد نظر انجام نشد', 'خطا در عملیات سرور.<br>' . $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
            'metaForm' => $form,
            'category' => $businessCategory
        ]);

    }

}
