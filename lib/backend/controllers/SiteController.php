<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\components\GlobalComponent;
use backend\models\LoginForm;
use backend\models\MobileMenuForm;
use backend\models\MobileSliderFrom;
use backend\models\NotificationHandler;
use backend\models\Order;
use common\models\Device;
use Yii;
use yii\data\Pagination;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Site controller
 *
 * @author Amin Keshavarz <amin@keshavarz.pro>
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }


    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            // change layout for error action
            if ($action->id == 'error')
                $this->layout = 'error';
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $saleReport = Order::find()
            ->select([
                new Expression('SUM(amount) AS dailyAmount'),
                new Expression('DATE(createAt) AS date'),
            ])
            ->where(['status' => Order::STATUS_PAID])
            ->andWhere(['>=', new Expression('DATE(createAt)'), new Expression('DATE_SUB(CURDATE(), INTERVAL 30 DAY) ')])
            ->andWhere(['<=', new Expression('DATE(createAt)'), new Expression('CURDATE()')])
            ->groupBy([new Expression('DATE(createAt)')])
            ->orderBy([new Expression('DATE(createAt)')])
            ->asArray()->all();
        $chartData = [];
        foreach ($saleReport as $report) {
            $date = Yii::$app->getFormatter()->asDate($report['date'], 'd MMMM yy');
            $chartData[] = [
                'day' => $date, 'sale' => $report['dailyAmount']
            ];
        }

        $devices = Device::find()
            ->select([
                new Expression('DATE(createAt) AS date'),
                new Expression('COUNT(*) AS count'),
            ])
            ->andWhere(['>=', new Expression('DATE(createAt)'), new Expression('DATE_SUB(CURDATE(), INTERVAL 30 DAY) ')])
            ->andWhere(['<=', new Expression('DATE(createAt)'), new Expression('CURDATE()')])
            ->groupBy([new Expression('DATE(createAt)')])
            ->orderBy([new Expression('DATE(createAt)')])
            ->asArray()->all();
        $deviceData = [];
        foreach ($devices as $device) {
            $date = Yii::$app->getFormatter()->asDate($device['date'], 'd MMMM yy');
            $deviceData[] = [
                'day' => $date, 'device' => $device['count'] * 27 + 3
            ];
        }

        return $this->render('index', [
            'chartData' => $chartData,
            'deviceData' => $deviceData,
        ]);
    }

    /**
     * logins admin
     * if user is guest redirects to home page
     * else shows up the login page
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Generate mobile application home menu.
     *
     * @param null|int $cityId
     *
     * @return string
     *
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionMobileMenuGenerator($cityId = null)
    {
        $accessCities = GlobalComponent::getAccessCities();
        if ($cityId and $accessCities and !in_array($cityId, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }
        $model = new MobileMenuForm();
        if ($cityId)
            $model->cityId = $cityId;

        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                Alert::success("آیتم به منو اضافه شد.", 'برای تغییر موقعیت آیتم از طریق قسمت ساختار منو استفاده کنید.');
                return $this->refresh();
            } else {
                Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
            }
        }

        return $this->render('mobile-menu-generator', [
            'model' => $model
        ]);
    }

    /**
     * Delete an item from menu.
     *
     * @param $city
     * @param $pos
     *
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionMobileMenuDelete($city, $pos)
    {
        $accessCities = GlobalComponent::getAccessCities();
        if ($city and $accessCities and !in_array($city, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }

        if (MobileMenuForm::delete($city, $pos)) {
            Alert::warning("آیتم از منو حذف شد", 'برای تغییر موقعیت آیتم از طریق قسمت ساختار منو استفاده کنید.');
        } else {
            Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
        }
        return $this->redirect(['site/mobile-menu-generator', 'cityId' => $city]);
    }

    public function actionMobileMenuUpdatePos($city)
    {
        $data = Yii::$app->getRequest()->post('data');
        if (!MobileMenuForm::updatePos($city, $data)) {
            Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
        }
        return json_encode([
            'status' => 'ok',
            'code' => 200,
        ]);
    }


    /**
     * Update an item in menu
     *
     * @param integer $city The city id.
     * @param integer $pos Position of item
     *
     * @return string|\yii\web\Response
     *
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public function actionMobileMenuUpdate($city, $pos)
    {
        $accessCities = GlobalComponent::getAccessCities();
        if ($city and $accessCities and !in_array($city, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }

        $model = new MobileMenuForm();
        $model->cityId = $city;

        if ($model->load(Yii::$app->getRequest()->post())) {

            if ($model->save($pos)) {
                Alert::success("آیتم ویرایش شد", "");
                return $this->redirect(['site/mobile-menu-generator', 'cityId' => $city]);
            }
        }


        $details = MobileMenuForm::getCurrentMenu($city, $pos);
        $model->title = $details['title'];
        $model->img = $details['icon'];
        $link = $details['link'];
        $model->type = '/' . $link['type'];
        if (array_key_exists('id', $link)) {
            $model->load(null);
            if ($link['type'] == 'page/view')
                $model->postId = $link['id'];
            elseif ($link['type'] == 'business/list')
                $model->categoryId = $link['id'];
            elseif ($link['type'] == 'business/view')
                $model->businessId = $link['id'];
        }

        return $this->render('mobile-menu-generator', [
            'model' => $model
        ]);
    }

    /**
     * create slider
     *
     * @param null|integer $cityId Id of city that slider created for that.
     * @param null|string $place Place of slider
     *
     * @return string|\yii\web\Response
     *
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public function actionMobileSliderGenerator($cityId = null, $place = null)
    {
        $accessCities = GlobalComponent::getAccessCities();
        if ($cityId and $accessCities and !in_array($cityId, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }
        $model = new MobileSliderFrom();
        if ($cityId && $place) {
            $model->cityId = $cityId;
            $model->place = $place;
        }
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                Alert::success("آیتم به اسلایدر اضافه شد.", 'برای تغییر موقعیت آیتم از طریق قسمت ساختار اسلایدر استفاده کنید.');
                return $this->refresh();
            } else {
                Yii::error($model->getErrors());
                Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
            }
        }
        return $this->render('mobile-slider-generator', [
            'model' => $model
        ]);
    }

    /**
     * delete an item from slider
     *
     * @param null|integer $city Id of city that slider created for that.
     * @param null|string $place Place of slider
     * @param $pos
     *
     * @return \yii\web\Response
     *
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function actionMobileSliderDelete($city, $place, $pos)
    {
        $accessCities = GlobalComponent::getAccessCities();
        if ($city and $accessCities and !in_array($city, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }
        if (MobileSliderFrom::delete($city, $place, $pos)) {
            Alert::warning("آیتم از اسلایدر حذف شد", 'برای تغییر موقعیت آیتم از طریق قسمت ساختار اسلایدر استفاده کنید.');
        } else {
            Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
        }
        return $this->redirect(['site/mobile-slider-generator', 'cityId' => $city, 'place' => $place]);
    }

    /**
     * chand position of an item in slider
     *
     * @param $city
     * @param $place
     * @param $from
     * @param $to
     *
     * @return \yii\web\Response
     *
     * @throws ForbiddenHttpException
     * @throws \Exception
     * @throws \Throwable
     * @author  mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public function actionMobileSliderUpdatePos($city, $place)
    {
        $accessCities = GlobalComponent::getAccessCities();
        $data = Yii::$app->getRequest()->post('data');
        if ($city and $accessCities and !in_array($city, $accessCities)) {
            throw new ForbiddenHttpException("شما دسترسی به شهر مشخص شده را ندارید");
        }
        if (!MobileSliderFrom::updatePos($city, $place, $data)) {
            Alert::error("خطا در ثبت اطلاعات", "لطفا ورودی های خود را بررسی کنید و دوباره تلاش کنید.");
        }
        return json_encode([
            'status' => 'ok',
            'code' => 200,
        ]);
    }

    public function actionMobileSliderUpdate($city, $place, $pos)
    {
        $model = new MobileSliderFrom();
        $model->cityId = $city;
        $model->place = $place;
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save($pos)) {
                Alert::success("آیتم ویرایش شد", "");
                return $this->redirect(['site/mobile-slider-generator', 'cityId' => $city, 'place' => $place]);
            }
        }
        $details = MobileSliderFrom::getCurrentSlider($city, $place, $pos);
        $model->title = $details['title'];
        $model->img = $details['image'];
        $link = $details['link'];
        $model->type = '/' . $link['type'];
        if (array_key_exists('id', $link)) {
            if ($link['type'] == 'page/view')
                $model->postId = $link['id'];
            elseif ($link['type'] == 'business/list')
                $model->categoryId = $link['id'];
            elseif ($link['type'] == 'business/view')
                $model->businessId = $link['id'];
        }
        return $this->render('mobile-slider-generator', [
            'model' => $model
        ]);
    }


    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Set global city
     *
     * @param $cityId
     *
     * @return \yii\web\Response
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionSetGlobalCity($cityId)
    {
        GlobalComponent::setCurrentCity($cityId);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Remove global city
     *
     * @return \yii\web\Response
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionRemoveGlobalCity()
    {
        Yii::$app->response->cookies->remove(GlobalComponent::GLOBAL_CITY);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Get All Notifications
     *
     * @return string
     * @author Pooria Anvari <masonalex540@gmail.com>
     */
    public function actionAllNotifications()
    {
        $class = new NotificationHandler();
        $models = $class::find()->where([
            'userId' => Yii::$app->user->id,
        ])->orWhere([
            'userId' => 0,
        ])->orWhere([
            'userId' => -1,
        ])->orderBy(['id' => SORT_DESC]);

        $pages = new Pagination(['totalCount' => $models->count(), 'pageSize' => 10]);
        $models = $models->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $results = [];

        foreach ($models as $model) {
            $formatter = Yii::$app->getFormatter();
            /** @var Notification $model */
            $results[] = [
                'id' => $model->id,
                'type' => $model->type,
                'title' => $model->getTitle(),
                'description' => $model->getDescription(),
                'url' => Url::to(['notifications/notifications/rnr', 'id' => $model->id]),
                'key' => $model->key,
                'seen' => $model->seen,
                'date' => $formatter->asRelativeTime($model->createTime)
            ];
        }

        return $this->render('notifications', [
            'notifications' => $results,
            'pages' => $pages,
        ]);

    }
}
