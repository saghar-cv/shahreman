<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\models\MessageForm;
use backend\models\search\MessageSearch;
use backend\models\SmsForm;
use common\models\Message;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Class MessageController
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class MessageController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * make message form for send sms or email
     *
     * @return string
     *
     * @author Mohammad Paravenh <mohammad.pvn1375@gmail.com>
     */
    public function actionSendMessageTo()
    {
        $model = new MessageForm();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save())
                Alert::success("موفقیت", 'پیام در لیست ارسالی ها قرار گرفت');

        }

        if (Yii::$app->request->isGet) {
            $model->receiverType = Yii::$app->getRequest()->get('receiverType');
            if (Yii::$app->getRequest()->get('receiverType') == 'business')
                $model->businessIds = Yii::$app->getRequest()->get('id');
            else if ((Yii::$app->getRequest()->get('receiverType') == 'user'))
                $model->userIds = Yii::$app->getRequest()->get('id');
            else
                $model->categoryIds = Yii::$app->getRequest()->get('id');

        }

        return $this->render('send-message-to', [
            'model' => $model,
        ]);

    }

    /**
     * Create sms format
     *
     * @param null $type
     *
     * @return string|\yii\web\Response
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function actionSmsFormats($type = null)
    {
        $model = new SmsForm();
        if ($type)
            $model->type = $type;

        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                Alert::success("آیتم  اضافه شد.", '');
                return $this->refresh();
            } else {
                Alert::error("خطا در ثبت اطلاعات", "");
            }
        }

        return $this->render('sms-formats', [
            'model' => $model
        ]);
    }

    /**
     * Return list of messages
     *
     * @return string|\yii\web\Response
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function actionMessageList()
    {

        $searchModel = new MessageSearch();

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('message-list', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }


}