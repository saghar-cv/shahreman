<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\components\GlobalComponent;
use backend\models\PostSubPages;
use backend\models\search\PostSearch;
use backend\models\StaticPageForm;
use common\models\Post;
use Yii;
use yii\caching\TagDependency;
use yii\db\StaleObjectException;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * PageController implements the CRUD actions for Post model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action == 'delete') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Post models.
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $searchModel->postType = $searchModel::TYPE_STATIC_POST;
        if ($city = GlobalComponent::getCurrentCity()) {
            $searchModel->cityId = $city['id'];
        }
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single Post model.
     *
     * @param integer $id
     *
     * @return string
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StaticPagePost model that extends Post active record.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * If not it will redirect to 'view' page with error message.
     *
     * @return string
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionCreate()
    {
        $model = new StaticPageForm();
        if (Yii::$app->getRequest()->isPost) {
            $model->coverId = Yii::$app->getRequest()->post('coverId');
            $slider = Yii::$app->getRequest()->post('slider');
            if (empty($slider))
                $model->slider = null;
            else
                $model->slider = $slider;
        }
        if ($model->load(Yii::$app->request->post())) {

            try {
                $post = $model->save();
                Alert::success("پست با موفقیت ذخیره شد.", "عنوان پست: {$model->title}");
                return $this->redirect(['index']);
            } catch (\Exception $e) {
                Alert::error('خطا', 'صفحه ی ساخته شده ذخیره نشد');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Post model.
     * Update Priority number of Post model's children
     * If update is successful, the browser will be redirected to the 'view' page.
     * If not it will redirect to 'view' page with error message.
     *
     * @param integer $id
     *
     * @return string
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     *
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isPost) {
            $name = Yii::$app->getRequest()->post('list');
            if ($name)
                foreach ($name as $child => $key) {
                    $childPost = Post::findOne($child);
                    $childPost->setSortNum($key);
                }
        }

        $model = StaticPageForm::find($id);
        if ($model->load(Yii::$app->request->post())) {
            if (!empty(Yii::$app->getRequest()->post('Post')['coverId']))
                $model->coverId = Yii::$app->getRequest()->post('Post')['coverId'];
            if (!empty(Yii::$app->getRequest()->post('Post')['slider']))
                $model->slider = Yii::$app->getRequest()->post('Post')['slider'];
            try {
                $post = $model->update($id);
                if (!$post) {
                    throw new \InvalidArgumentException("Post not saved.");
                }

                Alert::success("پست ویرایش شد", "عنوان پست ویرایش شده: {$post->title}");
            } catch (\Exception $e) {
                Alert::error("خطا", "صفحه ی ساخته شده بروزرسانی نشد");
            }
        }
        Yii::$app->apiCache->flush(); //api cache flush

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            if ($model->delete()) {
                Alert::warning("صفحه {$model->title} حذف شد.", "");
            } else {
                throw new \RuntimeException("Cant delete model");
            }
        } catch (StaleObjectException | \Exception | \Throwable $exception) {
            Alert::error("خطا در اجرای عملیات!", "صفحه {$model->title} حذف نشد.");
        }

        return $this->redirect(['index']);
    }

    /**
     * Create an view for adding sub pages using ajax requests.
     *
     * @param $postId
     *
     * @return string
     *
     * @throws NotFoundHttpException
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public function actionAjaxAddSubPage($postId)
    {
        $post = Post::findOne($postId);

        if (!$post) {
            throw new NotFoundHttpException("پست مورد نظر یافت نشد.");
        }

        $model = new PostSubPages();
        $model->scenario = $model::SCENARIO_ADD;
        $model->postId = $postId;

        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($model->save()) {
                $model = new PostSubPages();
                $model->scenario = $model::SCENARIO_ADD;
                $model->postId = $postId;
            } else {
                Yii::error($model->getErrors());
            }
        }

        $postSubPages = PostSubPages::find()->where(['postId' => $post->id])->orderBy(['sortNum' => SORT_ASC])->all();

        return $this->renderAjax('_subpage_form', [
            'postModel' => $post,
            'model' => $model,
            'postSubPages' => $postSubPages
        ]);
    }

    /**
     * Re order sub pages.
     *
     * @param integer $postId
     *
     * @return string
     *
     * @throws ServerErrorHttpException
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public function actionAjaxUpdateSubPageOrder($postId)
    {
        $data = Yii::$app->getRequest()->post('data');

        try {
            PostSubPages::updateOrder($postId, $data);
        } catch (\Exception $exception) {
            Yii::error($exception->getMessage());
            Yii::error($exception->getTrace());
            throw new ServerErrorHttpException("در عملیات مرتب سازی خطایی پیش آمده است.");
        }

        return json_encode([
            'status' => 'ok',
            'code' => 200,
        ]);
    }

    /**
     * Remove an subpage.
     *
     * @param $postId
     * @param $type
     * @param $sid
     *
     * @return string
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     *
     * @author Amin Keshavarz <Amin@keshavarz.pro>
     */
    public function actionAjaxRemoveSubPage($postId, $type, $sid)
    {
        $model = PostSubPages::findOne([
            'postId' => $postId,
            'suggestionType' => $type,
            'suggestionId' => $sid,
        ]);

        if (!$model) {
            throw new NotFoundHttpException("Sub page not found.");
        }

        $model->delete();

        return $this->redirect(['/page/update', 'id' => $postId]);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Post the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Set page important
     *
     * @param $id
     *
     * @return \yii\web\Response
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function actionSetImportant($id)
    {
        $post = Post::findOne($id);

        if (!$post) {
            Alert::error('خطا', 'صفحه معتبر نیست');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $post->setImportant();
        return $this->redirect(Yii::$app->request->referrer);
    }


}
