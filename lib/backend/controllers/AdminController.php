<?php

namespace backend\controllers;

use aminkt\widgets\alert\Alert;
use backend\components\GlobalComponent;
use backend\models\Admin;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class AdminController
 *
 * @package backend\controllers
 * @author Saghar Mojdhei <saghar.mojdehi@gmail.com>
 */
class AdminController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Index page for admin
     *
     * @param integer $id
     *
     * @return string
     */
    public function actionIndex($id = null)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Admin::find()
        ]);

        if ($id) {
            $model = Admin::findOne($id);
            if (!$model) {
                Alert::error('خطا در انجام عملیات', "ادمین مورد نظر یافت نشد");
                return $this->redirect(['index']);
            }
            $model->setScenario(Admin::SCENARIO_UPDATE);
        } else {
            $model = new Admin();
            $model->setScenario(Admin::SCENARIO_CREATE);
        }

        if (\Yii::$app->getRequest()->isPost and $model->load(\Yii::$app->request->post())) {
            if (empty($model->email))
                $model->email = null;
            if ($model->password)
                $model->setPassword($model->password);
            if ($model->isNewRecord)
                $model->generateAuthKey();
            if (is_array($model->accessCities)) {
                $model->accessCities = implode(',', $model->accessCities);
            } else {
                $model->accessCities = null;
            }
            GlobalComponent::invalidateCityListCookieCache();
            if (!$model->save()) {
                Alert::error('خطا در انجام عملیات', 'ادمین ذخیره نشد. دوباره تلاش کنید.');
                return $this->redirect(['index']);
            }
            Alert::success('عملیات با موفقیت انجام شد', '');
            return $this->redirect(['index']);
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }


}
