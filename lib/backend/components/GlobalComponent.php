<?php

namespace backend\components;

use aminkt\ticket\Ticket;
use common\components\SmsJob;
use saghar\address\models\City;
use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Class GlobalComponent
 *
 * @package backend\components
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class GlobalComponent extends Component
{
    const PARAM_CITY_LIST = 'active_city_lists_for_';
    const GLOBAL_CITY = 'current_city_selected';

    /** @var null|array $accessCities */
    private static $accessCities = null;

    /**
     * Set city in cookie
     *
     * @param $cityId
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function setCurrentCity($cityId): void
    {
        if (!self::getAccessCities() or in_array($cityId, self::getAccessCities())) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => self::GLOBAL_CITY,
                'value' => $cityId
            ]));
        }
    }

    /**
     * Return current access City ids in array or null.
     *
     * @return array|null
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public static function getAccessCities()
    {
        if (self::$accessCities) {
            return self::$accessCities;
        }
        try {
            $cities = \Yii::$app->getUser()->getIdentity()->accessCities;
        } catch (\Exception | \Throwable $e) {
            return null;
        }

        if ($cities) {
            self::$accessCities = explode(',', $cities);
        }
        return self::$accessCities;
    }

    /**
     * Get current city
     *
     * @return null|City
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function getCurrentCity()
    {
        $cityId = Yii::$app->request->cookies->get(self::GLOBAL_CITY);
        if (!self::getAccessCities() or in_array($cityId, self::getAccessCities())) {
            return City::findOne($cityId);
        }
        return null;
    }

    /**
     * Invalidate list of cities cookie based cache.
     *
     * @throws ForbiddenHttpException   When user not logged in.
     *
     * @return void
     */
    public static function invalidateCityListCookieCache(): void
    {
        $user = Yii::$app->getUser()->getId();
        if (!$user) {
            return;
        }
        Yii::$app->getCache()->delete(self::PARAM_CITY_LIST . $user);
    }

    /**
     * Init city list and checks if admin has access to payment module
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function init()
    {
        self::getCityList();

        Yii::$app->on(Ticket::EVENT_ON_REPLY, [$this, 'onTicketReply']);
    }

    /**
     * Get City List
     *
     * @return mixed|null
     *
     * @throws ForbiddenHttpException When user not logged in.
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public static function getCityList()
    {
        $user = Yii::$app->getUser()->getId();
        if (!$user) {
            return null;
        }

        if ($cache = Yii::$app->cache->get(self::PARAM_CITY_LIST . $user)) {
            return $cache;
        }

        $url = self::getApiUrl(['/v1/main/active-cities']);
        $client = new Client([
            'baseUrl' => $url,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);

        /** @var Response $response */
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('GET')
            ->send();

        if ($response->getStatusCode() != 200) {
            return null;
        } else {
            $response = Json::decode($response->getContent());
            $cities = self::getAccessCities();
            if ($cities) {
                $i = 0;
                foreach ($response['data'] as $city) {
                    if (!in_array($city['id'], $cities)) {
                        unset($response['data'][$i]);
                    }
                    $i++;
                }
            }
            Yii::$app->cache->set(self::PARAM_CITY_LIST . $user, $response['data'], 1 * 60 * 60);
            return $response['data'];
        }
    }


    /**
     * Return api address to defined route.
     *
     * @param $route
     *
     * @return string
     */
    public static function getApiUrl($route)
    {
        $url = Yii::$app->apiUrlManager->createAbsoluteUrl($route);
        $url = FileHelper::normalizePath($url, '/');
        $url = str_replace(['admin', 'http:/', 'https:/'], ['api', 'http://', 'https://'], $url);
        return $url;
    }

    /**
     * event for send sms when ticket reply
     *
     * @param $event
     *
     * @author Mohammad Parvane <mohammad.pvn1375@gmail.com>
     */
    public function onTicketReply($event)
    {
        $messageContent = $event->sender->userName . ' گرامی به تیکت شما پاسخ داده شد ';
        Yii::$app->queue->push(new SmsJob([
            'mobile' => $event->sender->userMobile,
            'message' => $messageContent,
        ]));
    }

}