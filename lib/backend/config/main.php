<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'as AccessBehavior' => [
        'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
        'login_url' => ['site/login'],
        'rules' => [
            'site' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [
                    'actions' => ['logout', 'index', 'set-global-city', 'remove-global-city'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
            'permit/access' => [
                [
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
            'permit/user' => [
                [
                    'allow' => true,
                    'roles' => ['admin'],
                ],
            ],
            'uploadManager/default' => [
                [
                    'allow' => true,
                ],
            ],
            'notifications/notifications' => [
                [
                    'allow' => true,
                ],
            ],
            'ticket/admin/customer-care' => [
                [
                    'allow' => true,
                    'roles' => ['admin']
                ]
            ],
            'debug/default' => [
                [
                    'allow' => true,
                ],
            ],
            'gii/default' => [
                [
                    'allow' => (!YII_ENV_TEST and YII_DEBUG) ? true : false,
                ],
            ]
        ]
    ],
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'globalComponent'],
    'modules' => [
        'uploadManager' => [
            'class' => aminkt\uploadManager\UploadManager::className(),
            'uploadPath'=>Yii::getAlias("@frontendWeb")."/upload",
            'uploadUrl' => "../upload",
            'acceptedFiles'=>"*",
            'fileIcon' => "../upload/image_not_found.jpg",
            'noImage' => "../upload/image_not_found.jpg",
            'adminId' => [1, 2, 6]
        ],
        'address' => [
            'class' => \saghar\address\Address::className(),
        ],
        'category' => [
            'class' => \saghar\category\Category::className(),
        ],
        'payment' => [
            'class' => 'aminkt\payment\Payment',
            'controllerNamespace' => 'aminkt\payment\controllers\backend',
        ],
        'permit' => [
            'class' => 'developeruz\db_rbac\Yii2DbRbac',
            'params' => [
                'userClass' => \backend\models\Admin::class
            ]
        ],
        'notifications' => [
            'class' => 'machour\yii2\notifications\NotificationsModule',
            // Point this to your own Notification class
            // See the "Declaring your notifications" section below
            'notificationClass' => 'backend\models\NotificationHandler',
            // Allow to have notification with same (user_id, key, key_id)
            // Default to FALSE
            'allowDuplicate' => true,
            'userType' => \machour\yii2\notifications\models\Notification::USER_TYPE_ADMIN,
            // This callable should return your logged in user Id
            'userId' => function () {
                return \Yii::$app->user->id;
            }
        ],
        'ticket' => [
            'class' => \aminkt\ticket\Ticket::class,
            'controllerNamespace' => 'aminkt\ticket\controllers\admin',
            'adminModel' => \backend\models\Admin::class,
            'userModel' => \frontend\models\User::class,
        ]

    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'apiCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@api') . '/runtime/cache'
        ],
        'user' => [
            'identityClass' => 'backend\models\Admin',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'mycity-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'globalComponent' => [
            'class' => backend\components\GlobalComponent::className(),
        ],
        'apiUrlManager' => [
            'class' => '\yii\web\UrlManager',
//            'baseUrl' => '@web/../api',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '.json',
            'rules' => require(dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'url-rules.php')
        ],
    ],
    'params' => $params,
];
