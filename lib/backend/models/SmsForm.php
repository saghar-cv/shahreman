<?php
namespace backend\models;

use yii\base\Model;
use yii2mod\settings\components\Settings;
use yii2mod\settings\models\enumerables\SettingType;

/**
 * sms form
 *
 * @property mixed $smsArray
 * @property string body
 *
 * @author mohammad parvaneh <mohammmad.pvn1375@gmail.com>
 */
class SmsForm extends Model
{
    const SETTING_SECTION = 'messages';

    const TYPE_BUSINESS_REGISTERED = 'businessRegistered';
    const TYPE_BUSINESS_EDITED = 'businessEdited';
    const TYPE_BUSINESS_PUBLISHED = 'businessPublished';
    const TYPE_BUSINESS_CONFIRMED = 'businessConfirmed';
    const TYPE_BUSINESS_PAYMENT_CONFIRMED = 'businessPaymentConfirmed';
    const TYPE_BUSINESS_EXPIRED = 'businessExpired';
    const TYPE_BUSINESS_REJECTED = 'businessRejected';

    public $content;
    public $type;

    public function init()
    {
        parent::init();

    }

    public static function getTypeList()
    {
        return [
            self::TYPE_BUSINESS_REGISTERED => 'ثبت صنف',
            self::TYPE_BUSINESS_EDITED => 'ویرایش صنف',
            self::TYPE_BUSINESS_PUBLISHED => 'تایید ویرایش صنف',
            self::TYPE_BUSINESS_CONFIRMED => 'تایید صنف',
            self::TYPE_BUSINESS_PAYMENT_CONFIRMED => 'تایید پرداخت صنف',
            self::TYPE_BUSINESS_EXPIRED => 'منقضی شدن صنف',
            self::TYPE_BUSINESS_REJECTED => 'رد شدن صنف',
        ];
    }

    public function rules()
    {
        return [
            [['content', 'type'], 'required'],
            [['type', 'content'], 'string'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'type' => 'عنوان',
            'content' => 'محتوا',

        ];
    }

    /**
     * load smsForm model
     *
     * @param array $data
     * @param null $formName
     * @return bool
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function load($data, $formName = null)
    {
        $this->content = $this->loadSms($this->type);
        return parent::load($data, $formName);
    }

    /**
     * get smsFormats by type
     *
     * @param $type
     * @return mixed
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    private static function loadSms($type)
    {
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        return $settings->get(self::SETTING_SECTION, $type, "");

    }


    /**
     * save a smsForm
     *
     * @return bool
     */
    public function save()
    {
        if ($this->type and $this->content) {
            /** @var Settings $settings */
            $settings = \Yii::$app->settings;
            if ($settings->set(self::SETTING_SECTION, $this->type, $this->content, SettingType::STRING_TYPE)) {
                return true;
            }
        }
        return false;
    }

    /**
     * set content of smsForm
     *
     * @param mixed $content
     *
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * set type of smsForm
     *
     * @param mixed $type
     *
     * @return void
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function setType($type)
    {
        $this->type=$type;
    }

    /**
     * for get the smsFormat by kind of type! this method is using in handler
     *
     * @param $type
     * @return mixed
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public static function getSmsContent($type)
    {
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        return $settings->get(self::SETTING_SECTION, $type, "");
    }

}