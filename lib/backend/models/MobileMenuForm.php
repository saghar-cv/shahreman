<?php

namespace backend\models;

use aminkt\uploadManager\UploadManager;
use api\components\UrlMaker;
use backend\components\GlobalComponent;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii2mod\settings\components\Settings;
use yii2mod\settings\models\enumerables\SettingType;

/**
 * Mobile menu form
 *
 * @property mixed $menuArray
 *
 * @author Amin Keshavarz <amin@keshavarz.pro>
 */
class MobileMenuForm extends Model
{
    const SETTING_SECTION = 'mobileMenu';

    public $cityId;
    public $title;
    public $des;
    public $img;
    public $type;
    public $postId;
    public $categoryId;
    public $businessId;

    private $menu = [];

    public static function getCurrentMenu($cityId, $pos)
    {
        $menu = self::loadMenu($cityId);
        $item = $menu[$pos];
        return $item;
    }


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $currentCity = GlobalComponent::getCurrentCity();
        if ($currentCity) {
            $this->cityId = $currentCity->id;
        }
    }


    /**
     * Return available type list.
     *
     * @return array
     */
    public static function getTypeList()
    {
        return [
            '/business/list' => 'لیست کسب و کارهای یک صنف',
            '/page/view' => 'انتخاب یک پست',
            '/page/index' => 'لیست پست ها',
            '/news/index' => 'لیست اخبار',
            '/business/index' => 'لیست اصناف',
            '/business/view' => 'انتخاب یک صنف',
            '/offer/list '=>'لیست پیشنهادات ویژه',
            '/search/index' => 'جستجو',
        ];
    }

    /**
     * Delete an item from list.
     *
     * @param $cityId
     * @param $position
     *
     * @return bool
     */
    public static function delete($cityId, $position)
    {
        $menu = self::loadMenu($cityId);
        unset($menu[$position]);
        $menu = array_values($menu);
        $menu = Json::encode($menu);


        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        if ($settings->set(self::SETTING_SECTION, $cityId, $menu, SettingType::STRING_TYPE)) {
            return true;
        }

        return false;
    }

    /**
     * Load menu data.
     *
     * @param $cityId
     *
     * @return mixed
     */
    private static function loadMenu($cityId)
    {
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        return Json::decode($settings->get(self::SETTING_SECTION, $cityId, "{}"));
    }

    /**
     * Update position of element
     *
     * @param int $cityId
     * @param $data
     * @return bool
     *
     */
    public static function updatePos($cityId, $data)
    {
        $menu = self::loadMenu($cityId);

        $newMenu = array();

        foreach ($data as $key => $val) {
            $newMenu[$key] = $menu[$val];
        }

        $menu = array_values($newMenu);
        $menu = Json::encode($menu);


        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        if ($settings->set(self::SETTING_SECTION, $cityId, $menu, SettingType::STRING_TYPE)) {
            return true;
        }

        return false;
    }

    public function load($data, $formName = null)
    {
        $this->menu = $this->loadMenu($this->cityId);
        return parent::load($data, $formName);
    }

    /**
     * validation rules for creating or updating post and pages
     *
     * @return array
     *
     */
    public function rules()
    {
        return [
            [['cityId', 'title', 'type'], 'required'],
            [['postId', 'categoryId', 'businessId', 'img', 'cityId'], 'integer'],
            [['title', 'des'], 'string'],
            [['type'], 'in', 'range' => ['/page/view', '/page/index', '/news/index',
                '/business/index', '/search/index', '/business/list', '/business/view','/offer/list ']],
            [['menu'], 'safe'],
        ];
    }

    /**
     * Set labels for creating and updating post or pages
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cityId' => 'محل منو',
            'title' => 'عنوان',
            'type' => 'نوع آیتم',
            'des' => 'توضیحات',
            'img' => 'تصویر',
            'postId' => 'شناسه پست',
            'businessId' => 'شناسه صنف',
            'categoryId' => 'صنف',
        ];
    }

    /**
     * Return menu structure as array.
     *
     * @return mixed
     */
    public function getMenuArray()
    {
        return $this->menu;
    }

    /**
     * Save or update menu in settings.
     *
     * @param null $pos
     *
     * @return bool
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function save($pos = null)
    {
        if ($this->cityId and $this->title and $this->type) {
            if ($pos != null) {
                $menu = self::loadMenu($this->cityId);
                $menu[$pos] = $this->exportItemToArray();
                $menu = array_values($menu);
                $menu = Json::encode($menu);
            } else {
                $this->menu[] = $this->exportItemToArray();
                $this->menu = array_values($this->menu);
                $menu = Json::encode($this->menu);
            }


            /** @var Settings $settings */
            $settings = \Yii::$app->settings;
            if ($settings->set(self::SETTING_SECTION, $this->cityId, $menu, SettingType::STRING_TYPE)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Export current form to array.
     *
     * @param bool $useImgUrl
     *
     * @return array
     */
    private function exportItemToArray($useImgUrl = false)
    {
        $img = $this->img;
        if ($useImgUrl) {
            try {
                $img = UploadManager::getInstance()->image($this->img);
            } catch (NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->getNoImage();
            }
        }

        $link[] = $this->type;
        if ($this->postId and $this->type == '/page/view') {
            $link['id'] = $this->postId;
        } elseif ($this->categoryId and $this->type == '/business/list') {
            $link['id'] = $this->categoryId;
        } elseif ($this->businessId and $this->type == '/business/view') {
            $link['id'] = $this->businessId;
        }

        $item = [
            'icon' => $img,
            'title' => $this->title,
            'description' => $this->des,
            'link' => UrlMaker::to($link)
        ];

        return $item;
    }
}