<?php

namespace backend\models;

use common\models\BaseActiveRecord;
use common\models\Business;
use common\models\Post;
use saghar\category\models\Category;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%category_relation}}".
 *
 * @property int      $id
 * @property int      $catId
 * @property int      $postId
 * @property int      $businessId
 * @property string   $updateAt
 * @property string   $createAt
 *
 * @property Business $business
 * @property Category $cat
 * @property Post     $post
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class CategoryRelation extends BaseActiveRecord
{
    const SECTION_BUSINESS = 'business';
    const SECTION_NEWS = 'news';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['catId'], 'required'],
            [['businessId'], 'exist', 'skipOnError' => true, 'targetClass' => Business::className(), 'targetAttribute' => ['businessId' => 'id']],
            [['catId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['catId' => 'id']],
            [['postId'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['postId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'catId' => 'دسته',
            'postId' => 'پست',
            'businessId' => 'صنف',
            'updateAt' => 'Update At',
            'createAt' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusiness()
    {
        return $this->hasOne(Business::className(), ['id' => 'businessId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Category::className(), ['id' => 'catId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'postId']);
    }

    /**
     * Get businesses
     *
     * @param $businessId
     *
     * @return array
     *
     * @throws NotFoundHttpException
     */
    public static function getBusinesses($businessId)
    {
        $relations = self::find()->where(['businessId' => $businessId]);
        if ($relations) {
            $businesses = [];
            foreach ($relations as $relation) {
                $businesses[] = Business::find()->where(['id' => $relation->businessId]);
            }
            if (!empty($businesses)) {
                return $businesses;
            }
        }
        throw new NotFoundHttpException('اصناف یافت نشد');
    }

    /**
     * Get posts
     *
     * @param $postId
     *
     * @return array
     *
     * @throws NotFoundHttpException
     */
    public static function getPosts($postId)
    {
        $relations = self::find()->where(['postId' => $postId]);
        if ($relations) {
            $posts = [];
            foreach ($relations as $relation) {
                $posts[] = Business::find()->where(['id' => $relation->postId]);
            }
            if (!empty($posts)) {
                return $posts;
            }
        }
        throw new NotFoundHttpException('پست یافت نشد');
    }

    /**
     * Add a post to one category
     *
     * @param integer $catId
     * @param integer $itemId
     *
     * @return \backend\models\CategoryRelation
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function addPost($catId, $itemId)
    {
        $relation = self::findOne([
            'catId' => $catId,
            'postId' => $itemId,
        ]);

        if ($relation) {
            return $relation;
        }

        $relation = new self();
        $relation->catId = $catId;
        $relation->postId = $itemId;
        if ($relation->save()) {
            return $relation;
        }

        \Yii::error($relation->getErrors(), self::className());
        throw new \InvalidArgumentException("Can not add post by id={$itemId} to category by catId={$catId}");
    }

    /**
     * Add a business to one category
     *
     * @param integer $catId
     * @param integer $itemId
     *
     * @return \backend\models\CategoryRelation
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public static function addBusiness($catId, $itemId)
    {
        $relation = self::findOne([
            'catId' => $catId,
            'businessId' => $itemId,
        ]);

        if ($relation) {
            return $relation;
        }

        $relation = new self();
        $relation->catId = $catId;
        $relation->businessId = $itemId;
        if ($relation->save()) {
            return $relation;
        }

        \Yii::error($relation->getErrors(), self::className());
        throw new \InvalidArgumentException("Can not add business by id={$itemId} to category by catId={$catId}");
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        // Delete post sub pages relation when category go to delete.
        if ($this->businessId) {
            PostSubPages::deleteAll([
                'suggestionType' => PostSubPages::TYPE_BUSINESS_CATEGORY,
                'suggestionId' => $this->catId
            ]);
        }
        return parent::beforeDelete();
    }
}
