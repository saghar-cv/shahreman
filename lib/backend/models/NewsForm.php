<?php

namespace backend\models;

use backend\components\GlobalComponent;
use common\models\Post;
use common\models\PostMeta;
use Yii;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * News form
 */
class NewsForm extends Model
{
    /*fields to save into Post_table*/
    public $title;
    public $coverId;
    public $video;
    public $content;
    public $tags;
    public $authorId;
    public $cityId;
    public $categories;
    public $postType;
    public $status;
    public $newsViewOption;
    public $summary;

    /* @var Post $postModel this is used for view form to show current model */
    private $postModel = null;


    /**
     * validation rules for creating or updating post and pages
     *
     * @return array
     *
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['coverId', 'authorId', 'cityId', 'status', 'newsViewOption', 'video'], 'integer'],
            ['categories', 'each', 'rule' => ['integer']],
            [['content', 'summary'], 'string'],
            [['status'], 'validatePublishAccess'],
            [['cityId'], 'validateCityAccess'],
            [['updateAt', 'createAt'], 'safe'],
            [['title', 'content', 'summary', 'tags'], 'arabicToPersian'],
            [['title', 'tags', 'postType', 'summary'], 'string', 'max' => 191],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::class, 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validateCityAccess($attribute, $params, $validator)
    {
        if ($user = Yii::$app->getUser()->getIdentity() and $user->accessCities) {
            if (!$this->$attribute or empty($this->$attribute)) {
                $this->addError($attribute, 'شما دسترسی ثبت پست برای همه شهر ها را ندارید.');
            } else {
                $cities = explode(',', $user->accessCities);
                if (!in_array($this->$attribute, $cities)) {
                    $this->addError($attribute, 'شهر وارد شده مجاز نیست.');
                }
            }
        }
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validatePublishAccess($attribute, $params, $validator)
    {
        if ($this->$attribute == Post::STATUS_PUBLISH and !Yii::$app->getUser()->can("#post-write-edit-publish")) {
            $this->addError($attribute, "شما اجازه انتشار پست را ندارید.");
        }
    }


    /**
     * Set labels for creating and updating post or pages
     *
     * @return array
     *
     */
    public function attributeLabels()
    {
        return [
            'title' => 'عنوان',
            'coverId' => 'عکس',
            'video' => 'ویدئو',
            'content' => 'متن',
            'tags' => 'برچسب ها',
            'authorId' => 'نویسنده',
            'categories' => 'دسته ها',
            'cityId' => 'شهر',
            'postType' => 'نوع پست',
            'status' => 'وضعیت',
            'summary' => 'خلاصه متن'
        ];
    }


    /**
     * Saves a new Static page by using Post active record with type "static page"
     * If the model is not saved, a InvalidParamsException will be thrown.
     *
     * @return Post|boolean the saved model or false if validation become field.
     *
     * @throws InvalidValueException If the model can not be saved
     *
     * @throws \yii\web\ForbiddenHttpException
     */
    public function save()
    {
        if (!$this->validate() or !$this->beforeSave(true))
            throw new InvalidValueException("Validation become failed.");

        $post = new Post();
        $post->title = $this->title;
        $post->coverId = $this->coverId;
        $post->content = $this->content;
        $post->tags = $this->tags;
        $post->authorId = Yii::$app->getUser()->getId();
        $post->cityId = $this->cityId;
        $post->postType = $post::TYPE_NEWS_POST;
        $post->status = $this->status;
        $post->summary = $this->summary;
        if ($post->save()) {
            if ($this->categories) {
                foreach ($this->categories as $category) {
                    CategoryRelation::addPost($category, $post->id);
                }
            }

            PostMeta::add($post->id, Post::META_NEWS_VIEW_OPTION, $this->newsViewOption);
            PostMeta::add($post->id, Post::META_VIDEO, $this->video);

            return $post;
        } else {
            Yii::error($post->getErrors(), self::class);
            throw new InvalidValueException("Post not saved into database");
        }
    }

    /**
     * Update Post Model
     *
     * @param $id
     *
     * @return Post|boolean the saved model or false if validation become field.
     *
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function update($id)
    {
        if (!$this->validate() or !$this->beforeSave(false))
            throw new InvalidValueException("Validation become failed.");

        $postModel = $this->getPostModel();

        foreach ($this->getAttributes() as $key => $value) {
            if ($postModel->hasAttribute($key)) {
                $postModel->$key = $value;
                Yii::debug("$key = $value");
            }
        }
        if ($postModel->save()) {

            if ($this->categories) {
                /* Add all new categories to category relation table. */
                foreach ($this->categories as $category) {
                    CategoryRelation::addPost($category, $postModel->id);
                }
            }

            /* Remove all removed categories from category relation table. */
            $postCategories = CategoryRelation::find()->where([
                'postId' => $postModel->id
            ])->all();
            foreach ($postCategories as $category) {
                if (!in_array($category->catId, $this->categories)) {
                    $category->delete();
                }
            }
            PostMeta::add($postModel->id, Post::META_NEWS_VIEW_OPTION, $this->newsViewOption);
            PostMeta::add($postModel->id, Post::META_VIDEO, $this->video);
            return $postModel;
        } else {
            Yii::error($postModel->getErrors(), self::class);
            throw new InvalidValueException("Post not saved into database");
        }


    }

    /**
     * Before save event.
     * @param $insert bool
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     */
    private function beforeSave($insert)
    {
        if ($insert) {
            if (!Yii::$app->getUser()->can("#post-write-edit-publish")) {
                $this->status = Post::STATUS_DRAFT;
            }
            GlobalComponent::invalidateCityListCookieCache();
            return true;
        } else {
            if (Yii::$app->getUser()->can("#post-write-edit-publish")) {
                return true;
            } elseif (Yii::$app->getUser()->can("#post-write-edit-draft")) {
                $this->status = Post::STATUS_DRAFT;
                return true;
            } elseif (Yii::$app->getUser()->can("#post-write-own-edit-draft")) {
                if (Yii::$app->getUser()->getId() == $this->authorId) {
                    $this->status = Post::STATUS_DRAFT;
                    return true;
                }
            }
            throw new \yii\web\ForbiddenHttpException("شما دسترسی ویرایش پست را ندارید.");
        }
    }

    /**
     * Finds model.if it is not null it maps the loaded model to key-value model.
     *
     * @param $id
     *
     * @return \backend\models\NewsForm mapped model name 'form'
     *
     * @throws \yii\web\NotFoundHttpException
     *
     */
    public static function find($id)
    {
        $post = Post::findOne($id);
        if (!$post) {
            throw new NotFoundHttpException("Model not found by id=$id");
        }
        $form = new self();
        $form->setPostModel($post);
        foreach ($post->getAttributes() as $key => $value) {
            if (property_exists($form, $key)) {
                $form->$key = $post->$key;
            }

            $form->newsViewOption = $post->getNewsViewOption();
        }

        /** @var \backend\models\CategoryRelation[] $categories */
        $categories = CategoryRelation::find()->where([
            'postId' => $post->id,
        ])->all();

        foreach ($categories as $category) {
            $form->categories[] = $category->cat->id;
        }

        if ($video = $post->getVideo()) {
            $form->video = $video['attachmentId'];
        }

        return $form;

    }


    /**
     * It is used for view form to show the current model.
     *
     * @return Post|null
     */
    public function getPostModel()
    {
        return $this->postModel;
    }

    /**
     * Sets loaded model to use it in view form by getPostModel method
     *
     *
     * @param Post $post
     */
    public function setPostModel($post)
    {
        $this->postModel = $post;
    }


    /**
     * Changes arabic letters with persian letters
     *
     * @param $attribute
     *
     * @return mixed
     *
     * @author Pooria Anvari <masonalex540@gmail.com>
     */

    public function arabicToPersian($attribute)
    {
        $characters = [
            'ك' => 'ک',
            'دِ' => 'د',
            'بِ' => 'ب',
            'زِ' => 'ز',
            'ذِ' => 'ذ',
            'شِ' => 'ش',
            'سِ' => 'س',
            'ى' => 'ی',
            'ي' => 'ی',
            '١' => '۱',
            '٢' => '۲',
            '٣' => '۳',
            '٤' => '۴',
            '٥' => '۵',
            '٦' => '۶',
            '٧' => '۷',
            '٨' => '۸',
            '٩' => '۹',
            '٠' => '۰',
        ];

        return $this->$attribute = str_replace(array_keys($characters), array_values($characters),$this->$attribute);
    }

}
