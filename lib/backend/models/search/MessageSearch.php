<?php

namespace backend\models\search;


use common\models\BaseActiveRecord;
use common\models\Message;
use yii\data\ActiveDataProvider;

/**
 * Class MessageSearch search model for Message
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class MessageSearch extends Message
{
    public $filterCreateAt;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status'], 'integer'],
            [['recipientName', 'mobile', 'email', 'content'], 'string'],
            [['updateAt', 'createAt'], 'safe']
        ];
    }

    public function afterValidate()
    {
        if ($this->createAt) {
            $this->filterCreateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt . ' 23:59:59'),
            ];
        }


        parent::beforeValidate();
    }

    /**
     * Search messages
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ]
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'createAt' => SORT_DESC
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%messages}}.id' => $this->id,
            '{{%messages}}.type' => $this->type,
            '{{%messages}}.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'recipientName', $this->recipientName]);


        if ($this->createAt) {
            $query->andFilterWhere(['between', '{{%messages}}.createAt', $this->filterCreateAt[0], $this->filterCreateAt[1]]);
        }
        return $dataProvider;
    }

}