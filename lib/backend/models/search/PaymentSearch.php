<?php

namespace backend\models\search;


use backend\components\GlobalComponent;
use backend\models\Order;
use common\models\BaseActiveRecord;
use yii\data\ActiveDataProvider;

/**
 * Class PaymentSearch
 *
 * @package backend\models\search
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class PaymentSearch extends Order
{

    public $businessName;
    public $cities;
    public $businessId;
    public $ownerName;
    public $planName;

    public $filterCreateAt;
    public $filterUpdateAt;

    public function rules()
    {
        $rules[] = [
            ['ownerName', 'businessName', 'planName', 'planId', 'status', 'updateAt', 'createAt', 'cities'], 'safe'
        ];

        return $rules;
    }

    public function afterValidate()
    {
        if ($this->createAt) {
            $this->filterCreateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt . ' 23:59:59'),
            ];
        }
        if ($this->updateAt) {
            $this->filterUpdateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt . ' 23:59:59'),
            ];
        }

        //todo check this (calendar search)
        parent::beforeValidate();
    }


    public $param;

    /**
     * @param $params
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->leftJoin('{{%plans}}', '{{%orders}}.planId = {{%plans}}.id')
            ->andFilterWhere(['like', '{{%plans}}.id', $this->planId]);
        $query->leftJoin('{{%businesses}}', '{{%orders}}.businessId = {{%businesses}}.id')
            ->andFilterWhere(['like', '{{%businesses}}.name', $this->businessName]);
        $query->leftJoin("{{%business_relation}}", '{{%orders}}.businessId = {{%business_relation}}.businessId');


        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $dataProvider->setSort( [
            'defaultOrder' => [
                'createAt' => SORT_DESC,
                'updateAt' => SORT_DESC,
            ]
        ]);

        $accessCities = GlobalComponent::getAccessCities();

        if ($this->cities and (!$accessCities or in_array($this->cities, $accessCities))) {
            $query->andWhere(['{{%business_relation}}.cityId' => $this->cities]);
        } elseif ($accessCities) {
            $query->andWhere(['{{%business_relation}}.cityId' => $accessCities]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->cities) {
            $query->andFilterWhere(['{{%business_relation}}.cityId' => $this->cities]);
        }

        $query->andFilterWhere(['like', '{{%businesses}}.ownerName', $this->ownerName]);

        $query->andFilterWhere(['like', '{{%orders}}.status', $this->status]);

        if ($this->updateAt) {
            $query->andFilterWhere(['between', '{{%orders}}.updateAt', $this->filterUpdateAt[0], $this->filterUpdateAt[1]]);
        }

        if ($this->createAt) {
            $query->andFilterWhere(['between', '{{%orders}}.createAt', $this->filterCreateAt[0], $this->filterCreateAt[1]]);
        }

        $dataProvider->query = $query;
        return $dataProvider;
    }

}