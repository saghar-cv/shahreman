<?php

namespace backend\models\search;

use backend\components\GlobalComponent;
use common\models\BaseActiveRecord;
use common\models\Post;
use yii\data\ActiveDataProvider;

/**
 * Class PostSearch search model for Business
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class PostSearch extends Post
{
    public $filterCreateAt;
    public $filterUpdateAt;

    public $cityName;
    public $authorName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cityId', 'status', 'authorId'], 'integer'],
            [['title', 'postType', 'cityName', 'tags', 'authorName'], 'string'],
            [['updateAt', 'createAt'], 'safe']
        ];
    }

    public function afterValidate()
    {
        if ($this->createAt) {
            $this->filterCreateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->createAt . ' 23:59:59'),
            ];
        }
        if ($this->updateAt) {
            $this->filterUpdateAt = [
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt),
                BaseActiveRecord::convertJalaliDateToSqlDateTime($this->updateAt . ' 23:59:59'),
            ];
        }

        parent::beforeValidate();
    }

    /**
     * Search businesses
     *
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();
        $query->leftJoin('{{%users}}', '{{%posts}}.authorId = {{%users}}.id');
        $query->leftJoin('{{%city}}', '{{%posts}}.cityId = {{%city}}.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ]
        ]);

        $dataProvider->setSort([
            'defaultOrder' => [
                'createAt' => SORT_DESC,
                'updateAt' => SORT_DESC,
            ]
        ]);

        $query->where(['{{%posts}}.postType' => $this->postType]);

        $accessCities = GlobalComponent::getAccessCities();

        if ($this->cityId and (!$accessCities or in_array($this->cityId, $accessCities))) {
            $query->andWhere(['{{%posts}}.cityId' => $this->cityId]);
        } elseif ($accessCities) {
            $query->andWhere(['{{%posts}}.cityId' => $accessCities]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            '{{%posts}}.id' => $this->id,
            '{{%posts}}.cityId' => $this->cityId,
            '{{%posts}}.authorId' => $this->authorId,
            '{{%posts}}.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', '{{%posts}}.title', $this->title])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['like', 'concat({{%users}}.name, " " , {{%users}}.family) ', $this->authorName])
            ->andFilterWhere(['like', '{{%city}}.name', $this->cityName]);


        if ($this->updateAt) {
            $query->andFilterWhere(['between', '{{%posts}}.updateAt', $this->filterUpdateAt[0], $this->filterUpdateAt[1]]);
        }

        if ($this->createAt) {
            $query->andFilterWhere(['between', '{{%posts}}.createAt', $this->filterCreateAt[0], $this->filterCreateAt[1]]);
        }
        return $dataProvider;
    }

}