<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\NotFoundHttpException;

/**
 * Access form
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class AccessForm extends Model
{
    public $name;
    public $description;
    public $price;
    public $duration;
    public $accessMainImage;
    public $accessVideo;
    public $accessMap;
    public $accessSocialTelegram;
    public $accessSocialInstagram;
    public $accessWebsite;
    public $accessEmail;
    public $accessPhoneNumber;
    public $accessFaxNumber;
    public $accessMobileNumber;
    public $accessAddress;
    public $accessIOS;
    public $accessAndroid;
    public $accessGallery;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['duration'], 'integer'],
            [['name'], 'string', 'max' => 191],
            [['accessPhoneNumber', 'accessFaxNumber', 'accessMobileNumber'],
                'integer', 'min' => 0, 'max' => 10],
            [['accessPhoneNumber', 'accessFaxNumber', 'accessMobileNumber'],
                'default', 'value' => 1],
            [['accessMainImage', 'accessVideo', 'accessSocialTelegram',
                'accessSocialInstagram', 'accessWebsite',
                'accessEmail', 'accessAddress', 'accessIOS', 'accessAndroid', 'accessGallery'],
                'boolean'],
            [['accessMainImage', 'accessMap', 'accessVideo', 'accessSocialTelegram',
                'accessSocialInstagram', 'accessWebsite',
                'accessEmail', 'accessAddress', 'accessIOS', 'accessAndroid', 'accessGallery'],
                'default', 'value' => false]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'نام',
            'description' => 'توضیحات',
            'price' => 'قیمت (تومان)',
            'duration' => 'مدت اعتبار (ماه)',
            'updateAt' => 'تاریج ویرایش',
            'createAt' => 'تاریخ ایجاد',
            'accessPhoneNumber' => 'تعداد شماره تلفن',
            'accessFaxNumber' => 'تعداد شماره فکس',
            'accessMobileNumber' => 'تعداد شماره موبایل',
            'accessMainImage' => 'عکس شاخص',
            'accessVideo' => 'ویدئو',
            'accessMap' => 'نقشه',
            'accessSocialTelegram' => 'لینک تلگرام',
            'accessSocialInstagram' => 'لینک اینستاگرام',
            'accessWebsite' => 'وبسایت',
            'accessEmail' => 'ایمیل',
            'accessAddress' => 'آدرس',
            'accessIOS' => 'لینک دانلود اپلیکیشن iOS',
            'accessAndroid' => 'لینک دانلود اپلیکیشن اندروید',
            'accessGallery' => 'گالری تصاویر'
        ];
    }

    /**
     * Save plan and plan access
     *
     * @param $data
     * @param null $id
     *
     * @throws NotFoundHttpException
     */
    public function save($data, $id = null)
    {
        if ($id) {
            $plan = Plan::edit($id, $data);
        } else {
            $plan = Plan::create($data);
        }

        Yii::error($this->getAttributes());
        foreach ($this->getAttributes() as $key => $val) {
            if (preg_match("/access\w+/si", $key, $match)) {
                PlanAccess::addAccess($plan->id, $key, $val);
            }
        }
    }

    /**
     * Find plan and its accesses
     *
     * @param $planId
     *
     * @return AccessForm
     * @throws NotFoundHttpException
     */
    public static function find($planId)
    {
        $plan = Plan::findOne($planId);
        if ($plan) {
            $form = new self();
            $form->name = $plan->name;
            $form->description = $plan->description;
            /** @var PlanAccess[] $accesses */
            $accesses = PlanAccess::find()->where(['planId' => $planId])->all();
            if ($accesses) {
                foreach ($plan->getAttributes() as $key => $value) {
                    if (property_exists($form, $key)) {
                        $form->$key = $plan->$key;
                    }
                }

                foreach ($accesses as $access) {
                    $name = $access->name;
                    if (property_exists($form, $name)) {
                        $form->$name = $access->value;
                    }
                }

                return $form;
            }
            throw new NotFoundHttpException('دسترسی های پلان مورد نظر یافت نشد');
        } else {
            throw new NotFoundHttpException('پلان مورد نظر یافت نشد');
        }
    }

}
