<?php
/**
 * Created by PhpStorm.
 * User: Mohammad_Pvn
 * Date: 4/15/2018
 * Time: 5:36 PM
 */

namespace backend\models;


use aminkt\uploadManager\UploadManager;
use api\components\UrlMaker;
use backend\components\GlobalComponent;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii2mod\settings\components\Settings;
use yii2mod\settings\models\enumerables\SettingType;

/**
 * mobile slider form
 *
 * @property mixed sliderArray
 *
 * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
 */
class MobileSliderFrom extends Model
{

    const SETTING_SECTION = 'mobileSlider';

    public $title;
    public $caption;
    public $type;
    public $place;
    public $url;
    public $cityId;
    public $postId;
    public $categoryId;
    public $businessId;
    public $img;

    private $slider = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $currentCity = GlobalComponent::getCurrentCity();
        if ($currentCity) {
            $this->cityId = $currentCity->id;
        }
    }

    public function rules()
    {
        return [
            [['cityId'], 'required'],
            [['postId', 'categoryId','businessId','img', 'cityId'], 'integer'],
            [['title', 'caption', 'type'], 'string'],
            [['type'], 'in', 'range' => ['/page/view', '/page/index', '/news/index',
                '/business/index', '/search/index', '/business/list', '/business/view']],
            [['place'], 'in', 'range' => ['mainPage', 'businessCategory', 'news', 'specialOffers']],
            [['slider'], 'safe'],
        ];
    }

    public static function getTypeList()
    {
        return [
            '' => 'بدون لینک',
            '/business/list' => 'لیست کسب و کارهای یک صنف',
            '/page/view' => 'انتخاب یک پست',
            '/page/index' => 'لیست پست ها',
            '/news/index' => 'لیست اخبار',
            '/business/index' => 'لیست اصناف',
            '/business/view' => 'انتخاب یک صنف',
            '/search/index' => 'جستجو',
        ];
    }

    /**
     * Return available place list as array.
     *
     * @return array
     */
    public static function getPlaceList()
    {
        return [
            'mainPage' => 'صفحه اصلی',
            'businessCategory' => 'دسته بندی اصناف',
            'news' => 'اخبار',
            'specialOffers' => 'پیشنهاد های ویژه'
        ];
    }


    /**
     * Set labels for creating and updating post or pages
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'cityId' => 'محل اسلایدر',
            'title' => 'عنوان',
            'caption' => 'توضیحات',
            'url' => 'لینک',
            'img' => 'تصاویر',
            'type' => 'نوع آیتم',
            'postId' => 'شناسه پست',
            'businessId' => 'شناسه صنف',
            'categoryId' => 'صنف',
            'place' => 'محل قرار گیری'

        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $this->slider = $this->loadSlider($this->cityId, $this->place);
        return parent::load($data, $formName);
    }

    /**
     * Return slider data as array.
     *
     * @return mixed
     */
    public function getSliderArray()
    {
        return $this->slider;
    }

    /**
     * Save slider item
     *
     * @return bool
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */
    public function save($pos = null)
    {
        if ($this->cityId and $this->img) {
            if ($pos != null) {
                $slider = self::loadSlider($this->cityId, $this->place);
                $slider[$pos] = $this->exportItemToArray();
                $slider = array_values($slider);
                $slider = Json::encode($slider);
            } else {
                $this->slider[] = $this->exportItemToArray();
                $this->slider = array_values($this->slider);
                $slider = Json::encode($this->slider);
            }
            $key = $this->place . '-' . $this->cityId;
            /** @var Settings $settings */
            $settings = \Yii::$app->settings;
            if ($settings->set(self::SETTING_SECTION, $key, $slider, SettingType::STRING_TYPE)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Delete item of slider
     *
     * @param integer $cityId
     * @param string $place
     * @param integer $position
     * @return bool
     *
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public static function delete($cityId, $place, $position)
    {
        $slider = self::loadSlider($cityId, $place);
        unset($slider[$position]);
        $slider = array_values($slider);
        $slider = Json::encode($slider);
        $key = $place . '-' . $cityId;
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        if ($settings->set(self::SETTING_SECTION, $key, $slider, SettingType::STRING_TYPE)) {
            return true;
        }
        return false;
    }

    /**
     * update postion of item in slider
     * @param $cityId
     * @param $place
     * @param $data
     * @return bool
     * @internal param $from
     * @internal param $to
     * @author mohammad parvaneh <mohammad.pvn1375@gmail.com>
     */

    public static function updatePos($cityId, $place, $data)
    {
        $slider = self::loadSlider($cityId, $place);

        $newSlider = array();

        foreach($data as $key => $val) {
            $newSlider[$key] = $slider[$val];
        }

        $slider = array_values($newSlider);
        $slider = Json::encode($slider);
        $key = $place . '-' . $cityId;
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        if ($settings->set(self::SETTING_SECTION, $key, $slider, SettingType::STRING_TYPE)) {
            return true;
        }

        return false;
    }


    /**
     * Export slider data to an array format.
     *
     * @param bool $useImgUrl
     *
     * @return array
     */
    private function exportItemToArray($useImgUrl = false)
    {
        $img = $this->img;
        if ($useImgUrl) {
            try {
                $img = UploadManager::getInstance()->image($this->img);
            } catch (NotFoundHttpException $e) {
                $img = UploadManager::getInstance()->getNoImage();
            }
        }
        $link[] = $this->type;
        if ($this->postId and $this->type == '/page/view') {
            $link['id'] = $this->postId;
        } elseif ($this->categoryId and $this->type == '/business/list') {
            $link['id'] = $this->categoryId;
        } elseif ($this->businessId and $this->type == '/business/view') {
            $link['id'] = $this->businessId;
        }else{
            $link['id']=null;
        }
        $item = [
            'image' => $img,
            'title' => $this->title,
            'caption' => $this->caption,
            'link' => UrlMaker::to($link)
        ];
        return $item;
    }

    /**
     * Load defined city slider.
     *
     * @param $cityId
     * @param $place
     *
     * @return mixed
     */
    public static function loadSlider($cityId, $place)
    {
        $key = $place . '-' . $cityId;
        /** @var Settings $settings */
        $settings = \Yii::$app->settings;
        $settings->invalidateCache();
        return Json::decode($settings->get(self::SETTING_SECTION, $key, "{}"));
    }

    public static function getCurrentSlider($cityId, $place, $pos)
    {
        $slider = self::loadSlider($cityId, $place);
        $item = $slider[$pos];
        return $item;
    }


}