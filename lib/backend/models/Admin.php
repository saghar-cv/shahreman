<?php

namespace backend\models;

use aminkt\ticket\interfaces\CustomerCareInterface;
use common\models\AbstractUser;
use developeruz\db_rbac\interfaces\UserRbacInterface;

/**
 * This is the model class for table "{{%admins}}".
 *
 * @property string $accessCities
 */
class Admin extends AbstractUser implements UserRbacInterface, CustomerCareInterface
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $password;
    public $confirmPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admins}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'family', 'mobile', 'passwordHash', 'ip', 'confirmPassword', 'password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'family', 'mobile', 'ip'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['status'], 'integer'],
            [['name', 'family'], 'string', 'max' => 70],
            [['mobile', 'ip', 'lastIp'], 'string', 'max' => 15],
            [['mobile', 'email'], 'unique'],
            [['passwordHash', 'confirmPassword', 'authKey'], 'string', 'max' => 255],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_CREATE => ['name', 'family', 'mobile', 'email', 'passwordHash', 'confirmPassword', 'password', 'accessCities'],
            self::SCENARIO_UPDATE => ['name', 'family', 'mobile', 'email', 'accessCities'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'نام',
            'family' => 'نام خانوادگی',
            'email' => 'ایمیل',
            'mobile' => 'موبایل',
            'passwordHash' => 'گذرواژه',
            'passwordResetCode' => 'Password Reset Code',
            'authKey' => 'Auth Key',
            'status' => 'وضعیت',
            'ip' => 'IP',
            'lastIp' => 'آخرین IP',
            'lastSeen' => 'آخرین بازدید',
            'updateAt' => 'تاریخ ویرایش',
            'createAt' => 'تاریخ ایجاد',
            'confirmPassword' => 'تکرار گذرواژه',
            'password' => 'گذرواژه',
            'accessCities' => 'شهر های دارای دسترسی',
        ];
    }

    /**
     * finds user by it's username(phone number)
     * @inheritdoc
     * @author mr-exception
     */
    public static function findByUsername($mobile)
    {
        return static::findOne(['mobile' => $mobile, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Return user ids that have selected accesses.
     *
     * @param string|array $access Access to search user ids.
     *
     * @return array
     */
    public function getUserIdsCan($access)
    {
        $rows = (new \yii\db\Query())
            ->select(['user_id'])
            ->from('{{%auth_assignment}}')
            ->leftJoin('{{%auth_item_child}}', 'parent = item_name')
            ->where(['child' => $access])
            ->all();

        $ids = [];
        foreach ($rows as $row) {
            $ids[] = $row['user_id'];
        }

        return $ids;
    }

    public static function getAdminIdsCan($access)
    {
        $admin = new Admin();
        return $admin->getUserIdsCan($access);
    }

    /**
     * Get User name.
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->getFullName();
    }
}
