<?php

namespace backend\models;

use saghar\address\models\City;
use yii\base\Model;

/**
 * Class SettingForm
 * @package backend\models
 */
class SettingForm extends Model
{
    const SETTING_SECTION_CONTENT = 'contentManagement';

    /** @var string $postStyleSheet Default style sheets. */
    public $postStyleSheet;

    /** @var array|string $activeCities System active cities. */
    public $activeCities;

    /** @var integer $relatedPagesViewType Default sub pages style.  */
    public $relatedPagesViewType;

    /** @var integer $contactDefaultDepartment Default ticket department to send contact us message to that. */
    public $contactDefaultDepartment;

    public function init()
    {
        parent::init();
        foreach ($this->attributes() as $attribute) {
            $value = \Yii::$app->settings->get(self::SETTING_SECTION_CONTENT, $attribute);

            if (!is_null($value)) {
                $this->{$attribute} = $value;
            }
        }

        if ($this->activeCities) {
            $this->activeCities = explode(',', $this->activeCities);
        }
        \Yii::warning($this->activeCities);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['postStyleSheet', 'activeCities', 'relatedPagesViewType', 'contactDefaultDepartment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postStyleSheet' => 'شیوه نامه پست ها',
            'activeCities' => 'شهر های فعال در سیستم',
            'relatedPagesViewType' => 'استایل پیش فرض زیر صفحه ها',
            'contactDefaultDepartment' => 'دپارتمان تیکت دریافت کننده پیام های ارتباط با ما'
        ];
    }

    /**
     * Return all city models.
     *
     * @return \yii\db\ActiveQuery|null|City[]
     */
    public function getCities()
    {
        $cities = City::find();
        foreach ($this->activeCities as $city) {
            $cities->orWhere(['id' => $city]);
        }
        $cities->all();

        return $cities;
    }

}