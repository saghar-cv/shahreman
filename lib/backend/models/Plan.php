<?php

namespace backend\models;

use aminkt\widgets\alert\Alert;
use common\models\Business;
use frontend\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%plans}}".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property int $duration
 * @property string $updateAt
 * @property string $createAt
 * @property int $status
 *
 * @property Business[] $businesses
 * @property PlanAccess[] $planAccesses
 * @property User[] $users
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class Plan extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_REMOVED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%plans}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['duration', 'status'], 'integer'],
            [['name'], 'string', 'max' => 191],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'نام',
            'description' => 'توضیحات',
            'price' => 'قیمت (تومان)',
            'duration' => 'مدت اعتبار (ماه)',
            'status' => 'وضعیت',
            'updateAt' => 'تاریخ ویرایش',
            'createAt' => 'تاریخ ایجاد',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusinesses()
    {
        return $this->hasMany(Business::class, ['planId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanAccesses()
    {
        return $this->hasMany(PlanAccess::class, ['planId' => 'id']);
    }

    /**
     * Get access by name
     *
     * @param string $key
     *
     * @return PlanAccess|null
     */
    public function getAccessByName($key) {
        foreach ($this->planAccesses as $access) {
            if ($access->name == 'access' . $key) {
                return $access;
            }
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['planId' => 'id']);
    }

    /**
     * Create new plan
     *
     * @param array $data <code>
     *      [
     *          'name' => $name,
     *          'description' => $description,
     *          'price' => $price,
     *          'duration' => $duration,
     *      ]
     * </code>
     *
     * @internal string $name name
     *
     * @internal string $description description
     *
     * @internal double $price price
     *
     * @internal integer $duration duration
     *
     * @return Plan
     * @throws \RuntimeException
     */
    public static function create($data)
    {
        $plan = new Plan();
        $plan->name = isset($data['name']) ? $data['name'] : null;
        $plan->description = isset($data['description']) ? $data['description'] : null;
        $plan->price = isset($data['price']) ? $data['price'] : null;
        $plan->duration = isset($data['duration']) ? $data['duration'] : null;

        if ($plan->save()) {
            Alert::success('پلان با موفقیت ایجاد شد', 'اسم پلان جدید : ' . $plan->name);
            return $plan;
        }
        \Yii::error($plan->getErrors());
        throw new \RuntimeException('پلان ذخیره نشد.');
    }

    /**
     * Create new plan
     *
     * @param $id
     * @param array $data <code>
     *      [
     *          'name' => $name,
     *          'description' => $description,
     *          'price' => $price,
     *          'duration' => $duration,
     *      ]
     * </code>
     *
     * @return Plan
     * @throws NotFoundHttpException
     * @internal string $name name
     *
     * @internal string $description description
     *
     * @internal double $price price
     *
     * @internal integer $duration duration
     *
     */
    public static function edit($id, $data)
    {
        $plan = self::findOne($id);
        if ($plan) {
            $plan->name = isset($data['name']) ? $data['name'] : null;
            $plan->description = isset($data['description']) ? $data['description'] : null;
            $plan->price = isset($data['price']) ? $data['price'] : null;
            $plan->duration = isset($data['duration']) ? $data['duration'] : null;

            if ($plan->save()) {
                Alert::success('پلان با موفقیت ویرایش شد', 'اسم پلان : ' . $plan->name);
                return $plan;
            } else {
                throw new \RuntimeException('تغییرات ذخیره نشد');
            }
        } else {
            throw new NotFoundHttpException('پلان یافت نشد');
        }
    }

    /**
     * Change plan status
     *
     * @param $status
     *
     * @throws \RuntimeException
     */
    public function setStatus($status)
    {
        $this->status = $status;
        if (!$this->save())
            throw new \RuntimeException('plan status did not change');
    }

    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'price',
            'duration',
            'planAccesses'
        ];
    }

}
