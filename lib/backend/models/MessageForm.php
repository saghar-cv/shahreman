<?php

namespace backend\models;

use common\components\EmailJob;
use common\components\SmsJob;
use common\models\Business;
use common\models\Message;
use frontend\models\User;
use Yii;
use yii\base\Model;

/**
 * Class MessageForm
 *
 * @package backend\models
 *
 * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
 */
class MessageForm extends Model
{
    public $receiverType;
    public $messageType;
    public $businessIds = [];
    public $categoryIds = [];
    public $userIds = [];
    public $body;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['body', 'receiverType', 'messageType'], 'required'],
            [['body', 'receiverType', 'messageType'], 'string'],
            [['businessIds', 'categoryIds', 'userIds'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * get kind of user that we want to send message : user or owner of a business
     *
     * @return array
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public static function getReceiverTypeList()
    {
        return [
            'user' => 'کاربر',
            'business' => 'صنف',
            'category' => 'اصناف یک دسته'
        ];
    }

    /**
     * get kind of massage that we want to send : sms or email
     *
     * @return array
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    public static function getMessageTypeList()
    {
        return [
            'email&sms' => 'ایمیل و پیامک',
            'email' => 'ایمیل',
            'sms' => 'پیامک',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'receiverType' => 'نوع گیرنده',
            'messageType' => 'نوع ارسال',
            'body' => 'پیام',
            'businessIds' => 'صاحبان اصناف انتخاب شده',
            'categoryIds' => ' دسته ی انتخاب شده',
            'userIds' => 'کاربران انتخاب شده'
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        $users = [];
        if ($this->receiverType == 'user') {
            if (isset($this->userIds[0]) and $this->userIds[0] == 0) {
                $users = User::find()->all();
            } else {
                $users = User::find()->where(['id' => $this->userIds])->all();
            }
        } else if ($this->receiverType == 'business') {
            if (isset($this->businessIds[0]) and $this->businessIds[0] == 0) {
                $businesses = Business::find()->all();

            } else {
                $businesses = Business::find()->where(['id' => $this->businessIds])->all();
            }
            foreach ($businesses as $business) {
                $users[] = $business->owner;
            }
        } else if ($this->receiverType == 'category') {
            $categoryRelations = CategoryRelation::find()->where(['catId' => $this->categoryIds])->all();
            foreach ($categoryRelations as $category) {
                $business = Business::findOne($category->businessId);
                $users[] = $business->owner;
            }
        }
        $this->sendMessage($users);
        return true;
    }

    /**
     * push email or sms to queue
     *
     * @param   User[] $users
     *
     * @return  void
     *
     * @author Mohammad Parvaneh <mohammad.pvn1375@gmail.com>
     */
    private function sendMessage($users)
    {
        if (count($users) > 0) {
            foreach ($users as $user) {

                if ($user) {
                    $message = new Message();
                    $message->recipientName = $user->getFullName();
                    $message->mobile = $user->mobile;
                    $message->email = $user->email;
                    $message->content = $this->body;

                    if ($this->messageType == "sms" or $this->messageType == "email&sms" and $user->mobile) {
                        Yii::$app->queue->push(new SmsJob([
                            'mobile' => $user->mobile,
                            'message' => $this->body,
                        ]));

                        if ($this->messageType == "email&sms")
                            $message->type = 3;
                    }
                    if ($this->messageType == "email" or $this->messageType == "email&sms" and $user->email) {
                        Yii::$app->queue->push(new EmailJob([
                            'email' => $user->email,
                            'content' => $this->body,
                        ]));
                        if ($this->messageType == "email&sms")
                            $message->type = 3;
                        else
                            $message->type = 2;
                    }
                    $message->save();
                }
            }
        }
    }


}
