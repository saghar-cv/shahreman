<?php

namespace backend\models;

use backend\components\GlobalComponent;
use common\models\Post;
use common\models\PostMeta;
use Yii;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Login form
 */
class StaticPageForm extends Model
{
    /*fields to save into Post_table*/
    public $title;
    public $coverId;
    public $video;
    public $slider;
    public $gallery;
    public $content;
    public $tags;
    public $authorId;
    public $cityId;
    public $postId;
    public $postType;
    public $status;
    public $createAt;
    public $updateAt;
    public $relatedPagesViewType;
    public $parentPosts;
    public $childViewType;
    public $summary;

    public $map;
    public $isMapEnabled = 0;

    /* this is used for view form to show current model*/
    private $postModel = null;

    /**
     * Finds model.if it is not null it maps the loaded model to key-value model.
     *
     * @param $id
     *
     * @return \backend\models\StaticPageForm mapped model name 'form'
     *
     * @throws \yii\web\NotFoundHttpException
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     */
    public static function find($id)
    {
        $post = Post::findOne($id);
        if (!$post) {
            throw new NotFoundHttpException("Model not found by id=$id");
        }
        $form = new self();
        $form->setPostModel($post);
        foreach ($post->getAttributes() as $key => $value) {
            if (property_exists($form, $key)) {
                $form->$key = $post->$key;
            }

            $map = $post->getMap();
            $form->isMapEnabled = $map['status'] ? 1 : 0;
            if ($map['longitude']) {
                $form->map['longitude'] = $map['longitude'];
            } else {
                $form->map['longitude'] = null;
            }

            if ($map['latitude']) {
                $form->map['latitude'] = $map['latitude'];
            } else {
                $form->map['latitude'] = null;
            }

            if ($map['zoom']) {
                $form->map['zoom'] = $map['zoom'];
            } else {
                $form->map['zoom'] = null;
            }

            $form->gallery = $post->getGallery();
            if ($video = $post->getVideo()) {
                $form->video = $video['attachmentId'];
            }

            $form->relatedPagesViewType = $post->getRelatedPagesViewType();
            $form->childViewType = $post->getChildViewType();
            $form->parentPosts = $post->parentPosts;
        }
        return $form;

    }

    /**
     * validation rules for creating or updating post and pages
     *
     * @return array
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['coverId', 'authorId', 'cityId', 'status', 'relatedPagesViewType', 'video', 'childViewType'], 'integer'],
            [['slider', 'gallery'], 'string'],
            [['status'], 'validatePublishAccess'],
            [['map'], 'validateMap'],
            [['content', 'summary'], 'string'],
            ['isMapEnabled', 'boolean'],
            [['cityId'], 'validateCityAccess'],
            [['updateAt', 'createAt'], 'safe'],
            [['title', 'tags', 'postType', 'summary'], 'string', 'max' => 191],
            [['title', 'tags', 'postType', 'summary', 'content'], 'arabicToPersian'],
            [['authorId'], 'exist', 'skipOnError' => true, 'targetClass' => Admin::class, 'targetAttribute' => ['authorId' => 'id']],
        ];
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validateCityAccess($attribute, $params, $validator)
    {
        if ($user = Yii::$app->getUser()->getIdentity() and $user->accessCities) {
            if (!$this->$attribute or empty($this->$attribute)) {
                $this->addError($attribute, 'شما دسترسی ثبت پست برای همه شهر ها را ندارید.');
            } else {
                $cities = explode(',', $user->accessCities);
                if (!in_array($this->$attribute, $cities)) {
                    $this->addError($attribute, 'شهر وارد شده مجاز نیست.');
                }
            }
        }
    }

    /**
     * Validate access to add cities.
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validatePublishAccess($attribute, $params, $validator)
    {
        if ($this->$attribute == Post::STATUS_PUBLISH and !Yii::$app->getUser()->can("#post-write-edit-publish")) {
            $this->addError($attribute, "شما اجازه انتشار پست را ندارید.");
        }

        if (!Yii::$app->getUser()->can("#post-write-edit-publish")) {
            return;
        } elseif (!Yii::$app->getUser()->can("#post-write-edit-draft")) {
            return;
        } elseif (!Yii::$app->getUser()->can("#post-write-own-edit-draft")) {
            if ($this->postModel and $this->authorId != Yii::$app->getUser()->getId()) {
                $this->addError($attribute, "شما تنها قادر به ویرایش مطالب خود هستید.");
            } else {
                return;
            }
        } elseif (!Yii::$app->getUser()->can("#post-just-write-draft")) {
            if ($this->postModel) {
                $this->addError($attribute, "شما قادر به ویرایش پست ها نیستید.");
            }
        }
    }

    /**
     * Validate map input
     *
     * @param string $attribute Attribute name.
     * @param array $params Key value of params.
     * @param $validator
     *
     * @throws \Exception
     * @throws \Throwable
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    public function validateMap($attribute, $params, $validator)
    {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, "ساختار نقشه به صورت آرایه ایجاد میشود.");

        }

        if (!isset($this->$attribute['longitude']) or !is_float((float)$this->$attribute['longitude'])) {
            $this->addError($attribute, "مقدار طول جغرافیایی صحیح نیست.");

        }

        if (!isset($this->$attribute['latitude']) or !is_float((float)$this->$attribute['latitude'])) {
            $this->addError($attribute, "مقدار عرض جغرافیایی صحیح نیست.");

        }

        if (!isset($this->$attribute['zoom'])) {
            $this->$attribute['zoom'] = 10;
        }

        if (!is_integer((integer)$this->$attribute['zoom'])) {
            $this->addError($attribute, "مقدار عرض جغرافیایی صحیح نیست.");

        }
    }

    /**
     * Set labels for creating and updating post or pages
     *
     * @return array
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     */
    public function attributeLabels()
    {
        return [
            'title' => 'عنوان',
            'coverId' => 'عکس',
            'video' => 'ویدئو',
            'slider' => 'اسلایدر',
            'gallery' => 'گالری',
            'content' => 'متن',
            'tags' => 'برچسب ها',
            'authorId' => 'نویسنده',
            'cityId' => 'شهر',
            'postType' => 'نوع پست',
            'status' => 'وضعیت',
            'isMapEnabled' => 'وضعیت نقشه در اپلیکیشن',
            'relatedPagesViewType' => 'استایل',
            'childViewType' => 'استایل فرزند',
            'map' => 'نقشه',
            'summary' => 'خلاصه متن',
        ];
    }

    /**
     * Saves a new Static page by using Post active record with type "static page"
     * If the model is not saved, a InvalidParamsException will be thrown.
     *
     * @return Post|boolean the saved model or false if validation become field.
     *
     * @throws InvalidValueException If the model can not be saved
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     * @throws ForbiddenHttpException
     */
    public function save()
    {
        if (!$this->validate() or !$this->beforeSave(true))
            throw new InvalidValueException("Validation become failed.");

        $post = new Post();
        $post->title = $this->title;
        $post->coverId = $this->coverId;
        $post->slider = $this->slider;
        $post->content = $this->content;
        $post->tags = $this->tags;
        $post->authorId = Yii::$app->getUser()->getId();
        $post->cityId = $this->cityId;
        $post->postType = $post::TYPE_STATIC_POST;
        $post->status = $this->status;
        $post->summary = $this->summary;
        if ($post->save()) {
            if ($this->isMapEnabled) {
                PostMeta::add($post->id, Post::META_MAP_LONGITUDE, $this->map['longitude']);
                PostMeta::add($post->id, Post::META_MAP_LATITUDE, $this->map['latitude']);
                PostMeta::add($post->id, Post::META_MAP_ZOOM, $this->map['zoom']);
                PostMeta::add($post->id, Post::META_MAP_STATUS, 1);
            } else {
                PostMeta::add($post->id, Post::META_MAP_STATUS, 0);
                PostMeta::add($post->id, Post::META_MAP_LONGITUDE, $this->map['longitude']);
                PostMeta::add($post->id, Post::META_MAP_LATITUDE, $this->map['latitude']);
                PostMeta::add($post->id, Post::META_MAP_ZOOM, $this->map['zoom']);
            }

            if (empty($this->relatedPagesViewType))
                $this->relatedPagesViewType = 0;

            if (empty($this->childViewType))
                $this->childViewType = 0;

            PostMeta::add($post->id, Post::META_RELATED_PAGES_VIEW_TYPE, $this->relatedPagesViewType);
            PostMeta::add($post->id, Post::META_CHILD_PAGES_VIEW_TYPE, $this->childViewType);
            PostMeta::add($post->id, Post::META_GALLERY, $this->gallery);
            PostMeta::add($post->id, Post::META_VIDEO, $this->video);

            return $post;
        } else {
            Yii::error($post->getErrors(), self::class);
            throw new InvalidValueException("Post not saved into database");
        }
    }

    /**
     * Before save event.
     * @param $insert bool
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     */
    private function beforeSave($insert)
    {
        if ($insert) {
            if (!Yii::$app->getUser()->can("#post-write-edit-publish")) {
                $this->status = Post::STATUS_DRAFT;
            }
            GlobalComponent::invalidateCityListCookieCache();
            return true;
        } else {
            if (Yii::$app->getUser()->can("#post-write-edit-publish")) {
                return true;
            } elseif (Yii::$app->getUser()->can("#post-write-edit-draft")) {
                $this->status = Post::STATUS_DRAFT;
                return true;
            } elseif (Yii::$app->getUser()->can("#post-write-own-edit-draft")) {
                if (Yii::$app->getUser()->getId() == $this->authorId) {
                    $this->status = Post::STATUS_DRAFT;
                    return true;
                }
            }
            throw new \yii\web\ForbiddenHttpException("شما دسترسی ویرایش پست را ندارید.");
        }
    }

    /**
     * Update Post Model
     *
     * @param $id
     *
     * @return Post|boolean the saved model or false if validation become field.
     *
     * @throws \InvalidArgumentException   When post not saved into database this exption will throw.
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     * @throws ForbiddenHttpException
     */
    public function update($id)
    {
        if (!$this->validate() or !$this->beforeSave(false))
            throw new InvalidValueException("Validation become failed.");

        $this->postModel = Post::findOne($id);

        foreach ($this->getAttributes() as $key => $value) {
            if ($this->postModel->hasAttribute($key)) {
                $this->postModel->$key = $value;
            }
        }

        if ($this->postModel->save()) {
            if ($this->isMapEnabled) {
                PostMeta::add($this->postModel->id, Post::META_MAP_LONGITUDE, $this->map['longitude']);
                PostMeta::add($this->postModel->id, Post::META_MAP_LATITUDE, $this->map['latitude']);
                PostMeta::add($this->postModel->id, Post::META_MAP_ZOOM, $this->map['zoom']);
                PostMeta::add($this->postModel->id, Post::META_MAP_STATUS, 1);
            } else {
                PostMeta::add($this->postModel->id, Post::META_MAP_STATUS, 0);
                PostMeta::add($this->postModel->id, Post::META_MAP_LONGITUDE, $this->map['longitude']);
                PostMeta::add($this->postModel->id, Post::META_MAP_LATITUDE, $this->map['latitude']);
                PostMeta::add($this->postModel->id, Post::META_MAP_ZOOM, $this->map['zoom']);
            }

            PostMeta::add($this->postModel->id, Post::META_GALLERY, $this->gallery);
            PostMeta::add($this->postModel->id, Post::META_VIDEO, $this->video);

            if (empty($this->childViewType))
                $this->childViewType = 0;

            if (empty($this->relatedPagesViewType))
                $this->relatedPagesViewType = 0;

            PostMeta::add($this->postModel->id, Post::META_RELATED_PAGES_VIEW_TYPE, $this->relatedPagesViewType);
            PostMeta::add($this->postModel->id, Post::META_CHILD_PAGES_VIEW_TYPE, $this->childViewType);

            return $this->postModel;
        } else {
            Yii::error($this->postModel->getErrors(), self::class);
            throw new InvalidValueException("Post not saved into database");
        }
    }

    /**
     * It is used for view form to show the current model.
     *
     * @return Post|null
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     */
    public function getPostModel()
    {
        return $this->postModel;
    }

    /**
     * Sets loaded model to use it in view form by getPostModel method
     *
     * @author Shayan KhaleghParast <shayankhaleghparast@gmail.com>
     *
     * @param Post $post
     */
    public function setPostModel($post)
    {
        $this->postModel = $post;
    }

    /**
     * Changes arabic letters with persian letters
     *
     * @param $attribute
     *
     * @return mixed
     *
     * @author Pooria Anvari <masonalex540@gmail.com>
     */

    public function arabicToPersian($attribute)
    {
        $characters = [
            'ك' => 'ک',
            'دِ' => 'د',
            'بِ' => 'ب',
            'زِ' => 'ز',
            'ذِ' => 'ذ',
            'شِ' => 'ش',
            'سِ' => 'س',
            'ى' => 'ی',
            'ي' => 'ی',
            '١' => '۱',
            '٢' => '۲',
            '٣' => '۳',
            '٤' => '۴',
            '٥' => '۵',
            '٦' => '۶',
            '٧' => '۷',
            '٨' => '۸',
            '٩' => '۹',
            '٠' => '۰',
        ];

        return $this->$attribute = str_replace(array_keys($characters), array_values($characters), $this->$attribute);
    }
}
