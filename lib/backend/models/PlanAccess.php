<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%plan_accesses}}".
 *
 * @property int $planId
 * @property string $name
 * @property string $value
 * @property string $updateAt
 * @property string $createAt
 * @property string $type
 *
 * @property Plan $plan
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class PlanAccess extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%plan_accesses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['planId', 'name'], 'required'],
            [['planId'], 'integer'],
            [['name', 'value'], 'string', 'max' => 191],
            [['type'], 'string', 'max' => 255],
            [['planId', 'name'], 'unique', 'targetAttribute' => ['planId', 'name']],
            [['planId'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['planId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'planId' => 'Plan ID',
            'name' => 'Name',
            'value' => 'Value',
            'updateAt' => 'Update At',
            'createAt' => 'Create At',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'planId']);
    }

    /**
     * Returns default accesses
     *
     * @return array
     */
    public static function getDefaultAccesses()
    {
        return [
            'accessMainImage' => false,
            'accessVideo' => false,
            'accessMap' => false,
            'accessSocialTelegram' => false,
            'accessSocialInstagram' => false,
            'accessWebsite' => false,
            'accessEmail' => false,
            'accessPhoneNumber' => 1,
            'accessFaxNumber' => 1,
            'accessMobileNumber' => 1,
            'accessAddress' => true,
            'accessGallery' => true,
            'accessIOS' => true,
            'accessAndroid' => true,
        ];
    }

    /**
     * Add plan access
     *
     * @param $planId
     * @param $access
     * @param $val
     *
     * @return PlanAccess|null|static
     */
    public static function addAccess($planId, $access, $val)
    {
        $plan = PlanAccess::findOne([
            'planId' => $planId,
            'name' => $access
        ]);

        if ($plan) {
            $plan->value = $val;
        } else {
            $plan = new PlanAccess();
            $plan->planId = $planId;
            $plan->name = $access;
            $plan->value = $val;
            $name = explode('access', $plan->name);
            if ($name[1] == 'PhoneNumber' or $name[1] == 'MobileNumber' or $name[1] == 'FaxNumber') {
                $plan->type = 'integer';
            } else {
                $plan->type = 'boolean';
            }
        }

        if ($plan->save())
            return $plan;

        Yii::error($plan->getErrors(), self::className());
        throw new \InvalidArgumentException("Inputs are not valid");
    }

    /**
     * Get Accesses names in array
     *
     * @return array
     */
    public static function getAccessesNames()
    {
        return [
            'accessMainImage' => 'تصویر شاخص',
            'accessVideo' => 'ویدئو',
            'accessMap' => 'نقشه',
            'accessSocialTelegram' => 'لینک تلگرام',
            'accessSocialInstagram' => 'لینک اینستاگرام',
            'accessWebsite' => 'آدرس وبسایت',
            'accessEmail' => 'آدرس ایمیل',
            'accessPhoneNumber' => 'تعداد شماره تلفن',
            'accessFaxNumber' => 'تعداد شماره فکس',
            'accessMobileNumber' => 'تعداد شماره موبایل',
            'accessAddress' => 'آدرس',
            'accessIOS' => 'لینک دانلود اپلیکیشن iOS',
            'accessAndroid' => 'لینک دانلود اپلیکیشن اندروید',
            'accessGallery' => 'گالری تصاویر',
        ];
    }

    /**
     * Get labels by name
     *
     * @return mixed|string
     */
    public function getLabel()
    {
        if (Yii::$app->getCache()->exists('plan_access_' . $this->name)) {
            return Yii::$app->getCache()->get('plan_access_' . $this->name);
        }
        $labels = self::getAccessesNames();
        if (key_exists($this->name, $labels)) {
            $label = $labels[$this->name];
        } else if (!key_exists($this->name, self::getDefaultAccesses())) {
            throw new \InvalidArgumentException('Access not defined');
        } else {
            $label = $this->name;
        }
        Yii::$app->getCache()->set('plan_access_' . $this->name, $label);
        return $label;
    }

    /**
     * @inheritdoc
     *
     * @author Amin Keshavarz <amin@keshavarz.pro>
     */
    public function fields()
    {
        return [
            'name',
            'label',
            'value',
            'type'
        ];
    }
}
