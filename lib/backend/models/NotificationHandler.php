<?php

namespace backend\models;

use common\models\Business;
use machour\yii2\notifications\models\Notification;
use yii\web\NotFoundHttpException;

/**
 * Class NotificationHandler
 * Handle notifications.
 *
 * @property string $route
 * @property string $description
 * @property string $title
 *
 * @package backend\models
 *
 * @author Amin Keshavarz <ak_1596@yahoo.com>
 */
class NotificationHandler extends Notification
{
    /**
     * @var array Holds all usable notifications
     */
    public static $keys = [
        Business::EVENT_BUSINESS_REGISTERED,
        Business::EVENT_BUSINESS_PAYMENT_CONFIRMED,
        Business::EVENT_BUSINESS_CONFIRMED,
        Business::EVENT_BUSINESS_REJECTED,
        Business::EVENT_BUSINESS_EDITED,
        Business::EVENT_BUSINESS_PUBLISHED,
        Business::EVENT_BUSINESS_EXPIRED
    ];
    /** @var Business|null $businessModel */
    private $businessModel;

    /**
     * Gets the notification title
     *
     * @return string
     */
    public function getTitle()
    {
        switch ($this->key) {
            case Business::EVENT_BUSINESS_REGISTERED:
                return 'یک صنف جدید ثبت شده است';
            case Business::EVENT_BUSINESS_PAYMENT_CONFIRMED:
                return 'پرداخت موفق';
            case Business::EVENT_BUSINESS_CONFIRMED:
                return 'صنف تائید شد';
            case Business::EVENT_BUSINESS_REJECTED:
                return 'صنف رد شد';
            case Business::EVENT_BUSINESS_EDITED:
                return 'صنف ویرایش شد';
            case Business::EVENT_BUSINESS_PUBLISHED:
                return 'صنف منتشر شد';
            case Business::EVENT_BUSINESS_EXPIRED:
                return 'صنف منقضی شد';
        }

        return 'نا مشخص';
    }

    /**
     * Gets the notification description
     *
     * @return string
     *
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function getDescription()
    {
        try {
            $businessName = $this->getBusinessModel()->name;
        } catch (NotFoundHttpException $exception) {
            \Yii::error("Notification {$this->id} has invalid keyId.");
            $this->delete();
        }

        switch ($this->key) {
            case Business::EVENT_BUSINESS_REGISTERED:
                $ownerName = $this->getBusinessModel()->ownerName;
                $createTime = \Yii::$app->formatter->asDate($this->getBusinessModel()->createAt);
                return "صنف {$businessName}  توسط  {$ownerName}  در تاریخ {$createTime} ثبت شد.";
            case Business::EVENT_BUSINESS_PAYMENT_CONFIRMED:
                return "صنف {$businessName} با موفقیت پرداخت شد.";
            case Business::EVENT_BUSINESS_CONFIRMED:
                return "صنف {$businessName} تائید شد و در لیست انصاف منتشر شد. ";
            case Business::EVENT_BUSINESS_REJECTED:
                return "صنف {$businessName} توسط اپراتور رد شد. ";
            case Business::EVENT_BUSINESS_EDITED:
                return "صنف {$businessName} ویرایش شد و در لیست بررسی مجدد قرار گرفت. ";
            case Business::EVENT_BUSINESS_PUBLISHED:
                return "صنف {$businessName} منتشر شد. ";
            case Business::EVENT_BUSINESS_EXPIRED:
                return "صنف {$businessName} منقضی شده است. ";
        }

        return 'نا مشخص';
    }

    /**
     * Return an business by id.
     *
     * @return Business
     *
     * @throws NotFoundHttpException
     *
     * @author Amin Keshavarz <ak_1596@yahoo.com>
     */
    private function getBusinessModel()
    {
        if ($this->businessModel) {
            return $this->businessModel;
        }
        $business = Business::findOne($this->keyId);
        if ($business) {
            $this->businessModel = $business;
            return $this->businessModel;
        }
        throw new NotFoundHttpException("Business not found.");
    }

    /**
     * Gets the notification route
     *
     * @return string|array
     */
    public function getRoute()
    {
        switch ($this->key) {
            case Business::EVENT_BUSINESS_REGISTERED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_PAYMENT_CONFIRMED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_CONFIRMED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_REJECTED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_EDITED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_PUBLISHED:
                return ['/business/update', 'id' => $this->keyId];
            case Business::EVENT_BUSINESS_EXPIRED:
                return ['/business/update', 'id' => $this->keyId];
        }

        return null;
    }
}