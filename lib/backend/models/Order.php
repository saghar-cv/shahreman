<?php

namespace backend\models;

use aminkt\payment\models\TransactionSession;
use common\models\Business;
use Imagine\Exception\RuntimeException;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%orders}}".
 *
 * @property int $id
 * @property int $businessId
 * @property int $planId
 * @property double $amount
 * @property int $status
 * @property string $createAt
 * @property string $updateAt
 *
 * @property Business $business
 * @property Plan $plan
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class Order extends ActiveRecord
{
    const STATUS_NOT_PAID = 1;
    const STATUS_PAID = 2;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';

    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createAt', 'updateAt'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updateAt'],
                ],
                // if you're using datetime instead of UNIX timestamp:
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['businessId', 'planId'], 'required'],
            [['businessId', 'planId', 'status'], 'integer'],
            [['amount'], 'number'],
            [['businessId'], 'exist', 'skipOnError' => true, 'targetClass' => Business::className(), 'targetAttribute' => ['businessId' => 'id']],
            [['planId'], 'exist', 'skipOnError' => true, 'targetClass' => Plan::className(), 'targetAttribute' => ['planId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'businessId' => 'Business ID',
            'businessName' => 'نام صنف',
            'planName' => 'پلان',
            'ownerName' => 'صاحب صنف',
            'planId' => 'پلان',
            'amount' => 'مبلغ',
            'status' => 'وضعیت',
            'createAt' => 'تاریخ ایجاد',
            'updateAt' => 'تاریخ ویرایش',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBusiness()
    {
        return $this->hasOne(Business::className(), ['id' => 'businessId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plan::className(), ['id' => 'planId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessions()
    {
        return $this->hasMany(TransactionSession::className(), ['orderId' => 'id']);
    }

    /**
     * Create new order
     *
     * @param $businessId
     * @param $planId
     * @param $amount
     * @param int $status
     *
     * @return Order
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function add($businessId, $planId, $amount, $status = self::STATUS_NOT_PAID)
    {
        $order = new self();
        $order->businessId = $businessId;
        $order->planId = $planId;
        $order->amount = $amount;
        $order->status = $status;
        if ($order->save()) {
            return $order;
        }
        return null;
    }

    /**
     * Get const label
     *
     * @param $label
     *
     * @return string
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public static function getLabel($label)
    {
        switch ($label) {
            case self::STATUS_NOT_PAID:
                return 'پرداخت نشده';
                break;
            case self::STATUS_PAID:
                return 'پرداخت شده';
                break;
            default:
                return 'نامشخص';

        }
    }

    /**
     * Set order status
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function setStatus()
    {
        $this->status;
        if (!$this->save()) {
            throw new RuntimeException('وضعیت تغییر نیافت');
        }
    }

    /**
     * Get business name
     *
     * @return string
     */
    public function getBusinessName()
    {
        return $this->business->name;
    }

    /**
     * Get owner name by business
     *
     * @return string
     */
    public function getOwnerName()
    {
        return $this->business->ownerName;
    }

    /**
     * Get plan name
     *
     * @return string
     */
    public function getPlanName()
    {
        return $this->plan->name;
    }
}
