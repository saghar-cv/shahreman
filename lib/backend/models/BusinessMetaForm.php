<?php

namespace backend\models;

use common\models\Business;
use common\models\BusinessMeta;
use common\models\BusinessRelation;
use Imagine\Exception\RuntimeException;
use saghar\address\models\Address;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Class BusinessForm
 *
 * @package backend\models
 *
 * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
 */
class BusinessMetaForm extends Model
{
    public $phones;
    public $mobiles;
    public $faxes;
    public $address;
    public $email;
    public $website;
    public $instagram;
    public $ios;
    public $android;
    public $telegram;
    public $video;
    public $mainImage;
    public $city;
    public $publishCities;
    public $gallery;
    public $map;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'email', 'website', 'instagram', 'telegram', 'video', 'mainImage', 'map', 'city', 'ios',
                'android', 'gallery'], 'string'],
            [['name'], 'string', 'max' => 191],
            [['phone', 'mobiles', 'faxes', 'publishCities'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mainImage' => 'تصویر شاخص',
            'video' => 'ویدئو',
            'telegram' => 'لینک تلگرام',
            'publishCities' => 'شهر های محل انتشار صنف',
            'instagram' => 'لینک اینستاگرام',
            'website' => 'آدرس وبسایت',
            'email' => 'آدرس ایمیل',
            'address' => 'آدرس',
            'mobiles' => 'موبایل',
            'faxes' => 'فکس',
            'phones' => 'تلفن',
            'city' => 'شهر',
            'ios' => 'لینک دانلود اپ ios',
            'android' => 'لینک دانلود اپ اندروید',
            'map' => 'نقشه',
            'gallery' => 'گالری',
        ];
    }

    /**
     * Save metas
     *
     * @param $data
     * @param $id
     *
     * @return bool
     *
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     */
    public function save($data, $id)
    {
        $business = Business::findOne($id);
        if ($business) {
            foreach ($data as $key => $value) {
                switch ($key) {
                    case 'mainImage':
                        if (isset($value)) {
                            try {
                                BusinessMeta::add($business->id, Business::BUSINESS_META_MAIN_IMAGE, (string)$value);
                            } catch (\InvalidArgumentException $exception) {
                                throw new BadRequestHttpException("Input is not valid");
                            } catch (\Exception $exception) {
                                throw new ServerErrorHttpException();
                            }
                        }
                        break;
                    case 'video':
                        if ($value != '') {
                            try {
                                BusinessMeta::add($business->id, Business::BUSINESS_META_VIDEO, (string)$value);
                            } catch (\InvalidArgumentException $exception) {
                                throw new BadRequestHttpException("Input is not valid");
                            } catch (\Exception $exception) {
                                Yii::warning($exception);
                                throw new ServerErrorHttpException();
                            }
                        }
                        break;
                    case 'gallery':
                        if ($value != '') {
                            try {
                                BusinessMeta::add($business->id, Business::BUSINESS_META_GALLERY, (string)$value);
                            } catch (\InvalidArgumentException $exception) {
                                throw new BadRequestHttpException("Input is not valid");
                            } catch (\Exception $exception) {
                                throw new ServerErrorHttpException($exception->getMessage());
                            }
                        }
                        break;
                    case 'map':
                        $address = Address::findOne($business->getMeta($business::BUSINESS_META_ADDRESS));
                        if (!$address) {
                            throw new NotFoundHttpException('آدرس مورد نظر قابل ویرایش نمی باشد');
                        }

                        if ($value != '') {
                            $mapPoints = explode(',', $value);
                            $address->latitude = $mapPoints[0];
                            $address->longitude = $mapPoints[1];
                            if (!$address->save()) {
                                throw new RuntimeException('مقدار وارد شده معتبر نیست');
                            }
                        }
                        break;
                    case 'publishCities':
                        BusinessRelation::clean($business->id);
                        if ($value) {
                            foreach ($value as $city) {
                                BusinessRelation::add($business->id, $city);
                            }
                        }
                        break;
                    case 'city':
                        $meta = BusinessMeta::addAddress($business->id, $value, 'city');
                        break;
                    case 'address':
                        try {
                            $meta = BusinessMeta::addAddress($business->id, $value, 'address');
                        } catch (\InvalidArgumentException $exception) {
                            \Yii::$app->getResponse()->setStatusCode(400);
                            echo "'ابتدا شهر مورد نظر را انتخاب کنید'";
                        }
                        break;
                    case 'email':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_EMAIL, $value);
                        break;
                    case 'website':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_WEBSITE, $value);
                        break;
                    case 'telegram':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_SOCIAL_TELEGRAM, $value);
                        break;
                    case 'instagram':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_SOCIAL_INSTALGRAM, $value);
                        break;
                    case 'android':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_ANDROID, $value);
                        break;
                    case 'ios':
                        BusinessMeta::add($business->id, $business::BUSINESS_META_IOS, $value);
                        break;
                    case 'phones':
                        foreach ($value as $itemKey => $item) {
                            BusinessMeta::add($business->id, $business::BUSINESS_META_PHONE . '-' . $itemKey, $item);
                        }
                        break;
                    case 'mobiles':
                        foreach ($value as $itemKey => $item) {
                            BusinessMeta::add($business->id, $business::BUSINESS_META_MOBILE . '-' . $itemKey, $item);
                        }
                        break;
                    case 'faxes':
                        foreach ($value as $itemKey => $item) {
                            BusinessMeta::add($business->id, $business::BUSINESS_META_FAX . '-' . $itemKey, $item);
                        }
                        break;
                    default:
                        throw new NotFoundHttpException("کلید یافت نشد.");
                }

            }

            if (!$business->save()) {
                throw new \RuntimeException('تغییرات ذخیره نشد');
            } else {
                return true;
            }
        } else {
            throw new NotFoundHttpException('صنف یافت نشد');
        }
    }

    /**
     * Find metas by business id
     *
     * @param $id
     *
     * @return BusinessMetaForm
     * @throws NotFoundHttpException
     *
     * @author Saghar Mojdehi <saghar.mojdehi@gmail.com>
     * @throws InvalidConfigException
     */
    public static function find($id)
    {
        $business = Business::findOne($id);
        if ($business) {
            $form = new self();

            $form->website = $business->getMeta($business::BUSINESS_META_WEBSITE);
            $form->email = $business->getMeta($business::BUSINESS_META_EMAIL);
            $form->mobiles = $business->getMeta($business::BUSINESS_META_MOBILE);
            $form->faxes = $business->getMeta($business::BUSINESS_META_FAX);
            $form->telegram = $business->getMeta($business::BUSINESS_META_SOCIAL_TELEGRAM);
            $form->instagram = $business->getMeta($business::BUSINESS_META_SOCIAL_INSTALGRAM);
            $form->android = $business->getMeta($business::BUSINESS_META_ANDROID);
            $form->ios = $business->getMeta($business::BUSINESS_META_IOS);
            $form->address = $business->getAddress()->address;
            $form->city = $business->getAddress()->cityId;
            $form->mainImage = $business->getImage(true, false);
            if ($form->mainImage and isset($form->mainImage['attachmentId'])) {
                $form->mainImage = $form->mainImage['attachmentId'];
            }
            if ($video = $business->getVideo()) {
                $form->video = $video['attachmentId'];
            }
            if ($gallery = $business->getGallery(false)) {
                foreach ($gallery as $item) {
                    $form->gallery = $form->gallery . $item['attachmentId'] . ',';
                }
                $form->gallery = rtrim($form->gallery, ',');
            }

            if ($latitude = $business->getAddress()->latitude and $longitude = $business->getAddress()->longitude) {
                $form->map = $latitude . ',' . $longitude;
            }

            $cities = $business->cities;
            foreach ($cities as $city) {
                $form->publishCities[] = $city->id;
            }

            $phoneCount = $business->plan->getAccessByName($business::BUSINESS_META_PHONE);
            if ($phoneCount) {
                for ($i = 1; $i <= $phoneCount->value; $i++) {
                    $form->phones[$i] = $business->getMeta($business::BUSINESS_META_PHONE . '-' . $i);
                }
            }

            $mobileCount = $business->plan->getAccessByName($business::BUSINESS_META_MOBILE);
            if ($mobileCount) {
                for ($i = 1; $i <= $mobileCount->value; $i++) {
                    $form->mobiles[$i] = $business->getMeta($business::BUSINESS_META_MOBILE . '-' . $i);
                }
            }

            $faxesCount = $business->plan->getAccessByName($business::BUSINESS_META_FAX);
            if ($faxesCount) {
                for ($i = 1; $i <= $faxesCount->value; $i++) {
                    $form->faxes[$i] = $business->getMeta($business::BUSINESS_META_FAX . '-' . $i);
                }
            }

            return $form;
        } else {
            throw new NotFoundHttpException('صنف مورد نظر یافت نشد');
        }
    }


}