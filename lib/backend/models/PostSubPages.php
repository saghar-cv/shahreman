<?php

namespace backend\models;

use common\models\Business;
use common\models\Post;
use saghar\category\models\Category;
use yii\base\InvalidArgumentException;

/**
 * This is the model class for table "{{%post_sub_pages}}".
 *
 * @property int $postId
 * @property int $suggestionId
 * @property string $suggestionType
 * @property int $sortNum
 *
 * @property Post $post
 *
 * @author Amin Keshavarz <amin@keshavarz.pro>
 */
class PostSubPages extends \yii\db\ActiveRecord
{
    const TYPE_PAGE = 'page';
    const TYPE_NEWS = 'news';
    const TYPE_BUSINESS = 'business';
    const TYPE_BUSINESS_CATEGORY = 'business_category';

    const SCENARIO_ADD = 'scenario_add';


    //---------------------------------------  Form Vars  ---------------------------------------------

    /**
     * Used only to get form data from front and convert it to correct format to save into database.
     *
     * @var integer $pageId
     */
    public $pageId;

    /**
     * Used only to get form data from front and convert it to correct format to save into database.
     *
     * @var integer $businessCategoryId
     */
    public $businessCategoryId;

    //------------------------------------------------------------------------------------------------

    /** @var null|Post $postModel */
    protected $postModel = null;
    /** @var null|Business $businessModel */
    protected $businessModel = null;
    /** @var null|Category $businessCategoryModel */
    protected $businessCategoryModel = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post_sub_pages}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD] = array_merge($scenarios[self::SCENARIO_DEFAULT], ['pageId', 'businessCategoryId']);
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['postId', 'suggestionId', 'suggestionType'], 'required'],
            [['postId', 'suggestionId', 'pageId', 'businessCategoryId'], 'integer'],
            [['suggestionType'], 'string', 'max' => 64],
            [['postId', 'suggestionId', 'suggestionType'], 'unique', 'targetAttribute' => ['postId', 'suggestionId', 'suggestionType']],
            [['postId'], 'exist', 'skipOnError' => true, 'targetClass' => Post::class, 'targetAttribute' => ['postId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pageId' => 'یک پست را انتخاب کنید',
            'businessCategoryId' => 'یک صنف را مشخص کنید',
            'postId' => 'والد اصلی',
            'suggestionId' => 'شناسه زیر صفحه',
            'suggestionType' => 'نوع زیر صفحه',
            'sortNum' => 'موقعیت در لیست زیر صفحات',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        // Check and set suggestionId by suggestion Type.
        if ($this->suggestionType == self::TYPE_PAGE and $this->pageId) {
            $this->suggestionId = $this->pageId;
        } elseif ($this->suggestionType == self::TYPE_BUSINESS_CATEGORY and $this->businessCategoryId) {
            $this->suggestionId = $this->businessCategoryId;
        }
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $lastItem = self::find()->where([
                'postId' => $this->postId
            ])->orderBy(['sortNum' => SORT_DESC])->one();
            $this->sortNum = ($lastItem ? $lastItem->sortNum : 0) + 1;
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::class, ['id' => 'postId']);
    }

    /**
     * Return sub page item.
     * Returned object may be post or business model so you should use isSome() methods to define what kind of models
     * are returned.
     *
     * @return Business|Post|null|Category
     */
    public function getSubPageModel()
    {
        if ($this->isPage() or $this->isNews()) {
            return $this->getPostModel();
        }

        if ($this->isBusiness()) {
            return $this->getBusinessModel();
        }

        if ($this->isBusinessCategory()) {
            return $this->getBusinessCategoryModel();
        }

        return null;
    }

    /**
     * Return true if sub page is a page.
     *
     * @return bool
     */
    public function isPage()
    {
        return $this->suggestionType == self::TYPE_PAGE;
    }

    /**
     * Return true if sub page is a news.
     *
     * @return bool
     */
    public function isNews()
    {
        return $this->suggestionType == self::TYPE_NEWS;
    }

    /**
     * Return post model.
     *
     * @return Post|null
     */
    protected function getPostModel()
    {
        if ($this->postModel)
            return $this->postModel;

        $this->postModel = Post::findOne($this->suggestionId);
        return $this->postModel;
    }

    /**
     * Return true if sub page is a business.
     *
     * @return bool
     */
    public function isBusiness()
    {
        return $this->suggestionType == self::TYPE_BUSINESS;
    }

    /**
     * Return business model.
     *
     * @return Business|null
     */
    protected function getBusinessModel()
    {
        if ($this->postModel)
            return $this->businessModel;

        $this->businessModel = Business::findOne($this->suggestionId);
        return $this->businessModel;
    }

    /**
     * Return true if sub page is a business category.
     *
     * @return bool
     */
    public function isBusinessCategory()
    {
        return $this->suggestionType == self::TYPE_BUSINESS_CATEGORY;
    }

    /**
     * Return business category model.
     *
     * @return Category|null
     */
    protected function getBusinessCategoryModel()
    {
        if ($this->businessCategoryModel)
            return $this->businessCategoryModel;

        $this->businessCategoryModel = Category::findOne($this->suggestionId);
        return $this->businessCategoryModel;
    }

    /**
     * Update order of sub pages by array.
     *
     * @param integer $postId Post id.
     * @param array $data Data that should re order in array like below :
     * <code>
     * [
     *      0 => [
     *          'key' => '1'
     *          'sortNum' => '2'
     *          'suggestType' => 'business_category'
     *          'suggestId' => '1'
     *      ],
     *      1 => [
     *          'key' => '2'
     *          'sortNum' => '3'
     *          'suggestType' => 'business_category'
     *          'suggestId' => '8'
     *      ],
     *      2 => [
     *          'key' => '0'
     *          'sortNum' => '1'
     *          'suggestType' => 'page'
     *          'suggestId' => '1'
     *      ],
     *      3 => [
     *          'key' => '3'
     *          'sortNum' => '4'
     *          'suggestType' => 'page'
     *          'suggestId' => '8'
     *      ]
     * ]
     * </code>
     *
     * Be aware that sub page will re sort by order of data in input array. for example in this sample data will re order
     * like below:
     *
     * <code>
     * [
     *      0 => [
     *          'key' => '0'
     *          'sortNum' => '1'
     *          'suggestType' => 'business_category'
     *          'suggestId' => '1'
     *      ],
     *      1 => [
     *          'key' => '1'
     *          'sortNum' => '2'
     *          'suggestType' => 'business_category'
     *          'suggestId' => '8'
     *      ],
     *      2 => [
     *          'key' => '2'
     *          'sortNum' => '3'
     *          'suggestType' => 'page'
     *          'suggestId' => '1'
     *      ],
     *      3 => [
     *          'key' => '3'
     *          'sortNum' => '4'
     *          'suggestType' => 'page'
     *          'suggestId' => '8'
     *      ]
     * ]
     * </code>
     *
     * @return bool
     */
    public static function updateOrder($postId, $data)
    {
        \Yii::warning($data);
        $count = 0;
        foreach ($data as $key => $datum) {
            $model = self::findOne([
                'postId' => $postId,
                'suggestionType' => $datum['suggestType'],
                'suggestionId' => $datum['suggestId'],
            ]);

            $model->sortNum = $key + 1;

            \Yii::error($datum);
            \Yii::error("Key: $key - sortNum:" . $model->sortNum);

            if (!$model->save(false)) {
                \Yii::error($model->getErrors());
                throw new \RuntimeException("Cant save data into database.");
            }
            $count++;
        }
        if ($count == count($data)) {
            return true;
        }

        throw new InvalidArgumentException("Invalid input.");
    }
}
