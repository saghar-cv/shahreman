How install
---

1. Clone project into your system.

2. Run below commands in `lib` folder.

```shell
php composer update

php init

php yii init --dbname="mycity" --dbuser="root" --dbpass="1234" --dbhost="localhost" --tblprefix="tbl_"
```

Project installed successfully in your system.


To shave your project and start from first
------

No need to clone again. just run below code to shave and update your codes from master:

```shell
php yii init/update --composer=install
```